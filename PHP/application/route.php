<?php

// 设限制URL兼容模式
\think\Url::root('index.php?s=');
return [
    '__pattern__' => [
        'name' => '\w+',
    ],
    '[hello]'  => [
        ':id'   => ['index/hello', ['method' => 'get'], ['id' => '\d+']],
        ':name' => ['index/hello', ['method' => 'post']],
    ],
	'[index]' => [
		':key' => ['index/index',['method' => 'get','ext' => 'html'],['key' => '\w{1,100}']],
	],

];

