<link rel="stylesheet" href="/assets/plugins/swiper/swiper.min.css">
<div class="section layui-carousel banner" id="banner">
  <div carousel-item>
    <div style="background: url('/assets/index/img/index_banner1.png') center center no-repeat; width:100%;">
        <div style="height:150px;"></div>
        <h1 class="banner-title">专业微信小程序解决方案</h1>
        <p class="banner-info">
            无需开发 &nbsp;&nbsp; 一键生成 &nbsp;&nbsp; 操作简单
        </p>
        <p class="banner-info">
            <a href="/regist.php" class="layui-btn layui-btn-normal layui-btn-lg">立即注册</a>
        </p>
    </div>
    <div style="background: url('/assets/index/img/index_banner2.png') center center no-repeat; width:100%;">
        <div style="height:150px;"></div>
        <h1 class="banner-title">点餐，外卖，配送，会员，营销，连锁经营一体化解决方案</h1>
        <p class="banner-info">快速搭建专属小程序 助力餐厅拥抱移动社交流量红利</p>
        <p class="banner-info">
            <a href="/regist.php" class="layui-btn layui-btn-normal layui-btn-lg">立即注册</a>
        </p>
    </div>
    <div style="background: url('/assets/index/img/index_banner3.png') center center no-repeat; width:100%;">
        <div style="height:150px;"></div>
        <h1 class="banner-title">拼团，秒杀，砍价，优惠券，智慧零售商城解决方案</h1>
        <p class="banner-info">赋能线下门店，重构消费连接</p>
        <p class="banner-info">
            <a href="/regist.php" class="layui-btn layui-btn-normal layui-btn-lg">立即注册</a>
        </p>
    </div>
  </div>
</div>
<div class="section">
    <div class="main">
        <div class="bind">
            <img src="/assets/index/img/saas.png" class="wow rollIn bing-back clearfix">
            <div class="info">
                <h1>河马微信SaaS，能做什么？</h1>
                <p>• 代商户注册小程序，免300元审核费</p>
                <p>• 统一发布小程序模板500+套</p>
                <p>• 商户在线DIY小程序，一键生成，一键发布</p>
                <p>• 集中管理N+商户，一键升级小程序</p>
                <p>• 全面托管小程序、公众号、微信支付等所有功能</p>
                <p>• 批量管理商户，等级，续费等权限管理，助力快速盈利</p>
                <p>• 大数据分析，引入开放平台，实现各平台数据打通</p>
                <p>• 代码结构简单，大量注释说明，便于二次开发</p>
            </div>
			<div class="demo">
                <h1><br></h1>
                <table>
                    <tr class="title">
                        <td colspan="2">河马云店演示</td>
                    </tr>
                    <tr class="title">
                        <td colspan="2"><p> </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr class="demo_tr font">
                                    <td class="demo_td">名称</td>
                                    <td class="demo_td">PC端演示网址</td>
                                </tr>
                                <tr class="demo_tr">
                                    <td class="demo_td">超管入口</td>
                                    <td class="demo_td"><a href="https://www.hemaphp.com/admin.php" target="_blank">https://www.hemaphp.com/admin.php</a></td>
                                </tr>
                                <tr class="demo_tr">
                                    <td class="demo_td">代理入口</td>
                                    <td class="demo_td"><a href="https://www.hemaphp.com/agent.php" target="_blank">https://www.hemaphp.com/agent.php</a></td>
                                </tr>
                                <tr class="demo_tr">
                                    <td class="demo_td">商家入口</td>
                                    <td class="demo_td"><a href="https://www.hemaphp.com/login.php" target="_blank">https://www.hemaphp.com/login.php</a></td>
                                </tr>
                                <tr class="demo_tr">
                                    <td class="demo_td">店长入口</td>
                                    <td class="demo_td"><a href="https://www.hemaphp.com/shop.php" target="_blank">https://www.hemaphp.com/shop.php</a></td>
                                </tr>
                            </table>
                        </td>
                        <td  class="demo_tr">
                            <img src="<?= $web['qrcode']?>">
                            <br>手机端演示
                        </td>
                    </tr>
                </table>

                <div>
                </div>
				
			</div>
            <div class="clearfix "></div>
        </div>
    </div>
</div>
<div class="section back-img" style="background-image: url('/assets/index/img/banner-6.jpg'); z-index: 10; height: 770px;">
    <div class="main" style="position: relative; padding-bottom: 10px; ">
        <div class="title white">
            <p class="h1">一套模板同步十大平台</p>
            <p>提供uni-app版本模板源代码，实现多平台发布</p>
        </div>
        <div class="item-code-desc clearfix">
            <div class="left fl  wow slideInLeft">
                <img src="/assets/index/img/banner-7.png">
            </div>
            <div class="right fr wow bounceInRight">
                <ul>
                    <li>
                        <img src="/assets/index/img/code-icon1.png">
                        <h1>Android</h1>
                    </li>
                    <li>
                        <img src="/assets/index/img/code-icon2.png">
                        <h1>IOS</h1>
                    </li>
                    <li>
                        <img src="/assets/index/img/code-icon3.png">
                        <h1>H5</h1>
                    </li>
                    <li>
                        <img src="/assets/index/img/code-icon4.png">
                        <h1>微信小程序</h1>
                    </li>
                    <li>
                        <img src="/assets/index/img/code-icon5.png">
                        <h1>支付宝小程序</h1>
                    </li>
                    <li>
                        <img src="/assets/index/img/code-icon6.png">
                        <h1>百度小程序</h1>
                    </li>
                    <li>
                        <img src="/assets/index/img/code-icon7.png">
                        <h1>字节跳动小程序</h1>
                    </li>
                    <li>
                        <img src="/assets/index/img/code-icon8.png">
                        <h1>QQ小程序</h1>
                    </li>
                    <li>
                        <img src="/assets/index/img/code-icon9.png">
                        <h1>快应用</h1>
                    </li>
                    <li>
                        <img src="/assets/index/img/code-icon10.png">
                        <h1>360小程序</h1>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--
<div class="section back-img" style="background-image: url('/assets/index/img/banner-6.jpg'); z-index: 10; height: 770px;">
    <div class="main" style="position: relative; padding-bottom: 10px; ">
        <div class="title white">
            <p class="h1">开启在线点餐外卖新时代</p>
            <p>全方位解决传统餐饮业成本高，营销扩客难等问题</p>
        </div>
        <div class="item-order-desc clearfix">
            <div class="left fl">
                <img src="/assets/index/img/banner-7.png">
            </div>
            <div class="right fr">
                <ul>
                    <li>
                        <img src="/assets/index/img/icon-1.png">
                        <h1>降低成本</h1>
                        <p>自助点餐、买单、加菜等便捷服务可帮助商家降低人工成本</p>
                    </li>
                    <li>
                        <img src="/assets/index/img/icon-2.png">
                        <h1>提高效率</h1>
                        <p>在线点餐无需排队，扫码加单方便快捷，提高经营效率</p>
                    </li>
                    <li>
                        <img src="/assets/index/img/icon-3.png">
                        <h1>粉丝经营</h1>
                        <p>客户点餐后，存储客户信息，可对其进行二次营销，提升二次消费</p>
                    </li>
                    <li>
                        <img src="/assets/index/img/icon-4.png">
                        <h1>提高翻台率</h1>
                        <p>用户通过点餐小程序在微信自助点餐、支付，不需要排队等待，节省时间</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
-->
<div class="section gray">
    <div class="main">
        <div class="title">
            <p class="h1">功能模块</p>
            <p>实时监控订单情况，灵活手动调度订单，高效完成配送</p>
        </div>
        <div class="function-list wow bounceInLeft">
            <ul>
                <li>
                    <img src="/assets/index/img/wake-icon4.png">
                    <h1>实时接单</h1>
                    <p>商家实时接单，通过订单调度中心，实时监控配送情况，灵活手动调度订单，高效完成配送</p>
                </li>
                <li>
                    <img src="/assets/index/img/wake-icon5.png">
                    <h1>支付方式</h1>
                    <p>用户可通过线上微信、余额支付，线下货到付款的方式，轻松完成小程序外卖下单，不一样的下单体验</p>
                </li>
                <li>
                    <img src="/assets/index/img/wake-icon6.png">
                    <h1>会员管理</h1>
                    <p>会员信息触手可得，方便查找，高效会员营销推广，各种营销方案尽在手中</p>
                </li>
                <li>
                    <img src="/assets/index/img/wake-icon7.png">
                    <h1>数据统计</h1>
                    <p>会员统计、订单统计、销售统计、详细的订单数据统计，一目了然</p>
                </li>
                <li>
                    <img src="/assets/index/img/wake-icon8.png">
                    <h1>订单管理</h1>
                    <p>后台轻松管理订单，方便查看订单号、总额、支付状态等信息</p>
                </li>
                <li>
                    <img src="/assets/index/img/wake-icon9.png">
                    <h1>客户维系</h1>
                    <p>轻松管理公众号粉丝，可积累的客户数据，客户通过关注注册，后台即可生成用户管理数据</p>
                </li>
                <li>
                    <img src="/assets/index/img/wake-icon10.png">
                    <h1>营销工具</h1>
                    <p>优惠券、满减活动、限时秒杀、拼图、砍价、充值送优惠等可随意组合，在吸粉引流的同时刺激客户直接消费</p>
                </li>
                <li>
                    <img src="/assets/index/img/wake-icon11.png">
                    <h1>多样配送</h1>
                    <p>可选择商家自主配送，上门自提，第三方配送，提高配送效率，降低配送成本</p>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="section" style=" height:860px; background:url('/assets/index/img/down-img-3.jpg')no-repeat center center;background-size: cover;background-attachment: fixed;">
    <div class="main" style="padding-top:50px;">
        <div class="title white">
            <p class="h1">丰富营销模块 增强用户黏合性</p>
            <p>丰富新颖的营销手法，让您的商城促销玩出新花样。拉新、转化、促活、复购、留存、推广，店铺经营面面俱到。</p>
        </div>
        <div class="trait-list">
            <ul>
                <li>拼团</li>
                <li>秒杀</li>
                <li>砍价</li>
                <li>优惠券</li>
            </ul>
        </div>
        <div class="img-list wow bounceInRight">
            <img src="/assets/index/img/down-img-4.png">
            <img src="/assets/index/img/down-img-5.png" class="img-2">
            <img src="/assets/index/img/down-img-6.png" class="img-3">
            <img src="/assets/index/img/down-img-7.png" class="img-4">
            <img src="/assets/index/img/down-img-8.png" class="img-5">
        </div>
    </div>
</div>
<div class="section gray" style="height:544px;">
    <div class="main">
        <div class="title" style="color:#fff;">
            <p class="h1">五大管理模块</p>
            <p>打造全方位运营模式让运维简单，高效，智能，助力快速开展小程序业务。</p>
        </div>
        <div class="management-list">
            <ul>
                <li>
                    <img src="/assets/index/img/mode-icon1.png" alt="">
                    <p class="h1">超管端</p>
                    <p>对商户集中管理，统一发布小程序模板，等级收费，打印机授权，消息推送等</p>
                </li>
                <li>
                    <img src="/assets/index/img/mode-icon2.png" alt="">
                    <p class="h1">代理端</p>
                    <p>多级分销，区域保护，代理自由定价，对自己的客户全方位服务管理</p>
                </li>
                <li>
                    <img src="/assets/index/img/mode-icon3.png" alt="">
                    <p class="h1">商户端</p>
                    <p>扫码点餐端和商城端，上传商品，营销活动，订单管理，会员管理,DIY生成小程序，多门店等</p>
                </li>
                <li>
                    <img src="/assets/index/img/mode-icon4.png" alt="">
                    <p class="h1">助手端</p>
                    <p>手机移动管理，让商家随时随地关注门店经营状态，帮助店长店员更方便的维护订单状态和服务顾客。</p>
                </li>
                <li>
                    <img src="/assets/index/img/mode-icon5.png" alt="">
                    <p class="h1">店长端</p>
                    <p>门店管理一步到位，解锁店长管理权限</p>
                </li>
            </ul>
        </div>
    </div>
</div>



<div class="section back-img" style="background-image: url('/assets/index/img/banner-item-back.jpg'); z-index: 10;">
    <div class="main" style="position: relative; padding-bottom: 10px; padding-top: 50px;">
        <div class="title white">
            <p class="h1">商业用户案例</p>
            <p>无论项目大小，我们都会不遗余力的为您提供最优质的产品和高质量的服务</p>
        </div>
    </div>
    <div class="swiper-box">
        <div class="swiper-container-item">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="img-box">
                        <img src="/assets/index/img/banner-item1.png">
                    </div>
                    <a href="javascript:;">火锅料理</a>
                </div>
                <div class="swiper-slide">
                    <div class="img-box">
                        <img src="/assets/index/img/banner-item1.png">
                    </div>
                    <a href="javascript:;">特色中餐</a>
                </div>
                <div class="swiper-slide">
                    <div class="img-box">
                        <img src="/assets/index/img/banner-item1.png">
                    </div>
                    <a href="javascript:;">高档西餐</a>
                </div>
                <div class="swiper-slide">
                    <div class="img-box">
                        <img src="/assets/index/img/banner-item1.png">
                    </div>
                    <a href="javascript:;">咖啡简餐</a>
                </div>
                <div class="swiper-slide">
                    <div class="img-box">
                        <img src="/assets/index/img/banner-item1.png">
                    </div>
                    <a href="javascript:;">火锅料理</a>
                </div>
            </div>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</div>

<div class="section gray">
    <div class="main">
        <div class="title">
            <p class="h1">六步开店 一站完成</p>
            <p>特有接口，一路无阻，快速上线</p>
        </div>
        <div class="item-line">
            <ul class="warp">
                <li>
                    <div class="left">
                        <span><img src="/assets/index/img/mode-icon5.png"></span>
                        <p class="h1">商家注册</p>
                        <p>微信扫码注册登录一步完成</p>
                    </div>
                    <div class="right">
                        <img src="/assets/index/img/gengduo.png">
                    </div>
                </li>
                <li>
                    <div class="left">
                        <span><img src="/assets/index/img/mode-icon4.png"></span>
                        <p class="h1">授权小程序</p>
                        <p>扫码授权或免费快速注册</p>
                    </div>
                    <div class="right">
                        <img src="/assets/index/img/gengduo.png">
                    </div>
                </li>
                <li>
                    <div class="left">
                        <span><img src="/assets/index/img/mode-icon6.png"></span>
                        <p class="h1">对接支付</p>
                        <p>在线开通收款账户或对接已有账户</p>
                    </div>
                    <div class="right">
                        <img src="/assets/index/img/gengduo.png">
                    </div>
                </li>
                <li>
                    <div class="left">
                        <span><img src="/assets/index/img/mode-icon7.png"></span>
                        <p class="h1">发布商品</p>
                        <p>商品分类设置，上传单双规格商品</p>
                    </div>
                    <div class="right">
                        <img src="/assets/index/img/gengduo.png">
                    </div>
                </li>
                <li>
                    <div class="left">
                        <span><img src="/assets/index/img/mode-icon8.png"></span>
                        <p class="h1">装修店铺</p>
                        <p>DIY小程序店铺页面，配置店铺</p>
                    </div>
                    <div class="right">
                        <img src="/assets/index/img/gengduo.png">
                    </div>
                </li>
                <li>
                    <div class="left">
                        <span><img src="/assets/index/img/mode-icon9.png"></span>
                        <p class="h1">发布上线</p>
                        <p>一键发布与升级小程序</p>
                    </div>
                </li>
            </ul>
        </div>
        <div class="title">
            <a href="/regist.php" class="layui-btn layui-btn-normal layui-btn-lg">立即免费开店</a>
        </div>
    </div>
</div>
<script src="/assets/plugins/swiper/swiper.min.js"></script>
<script>
    var swipers = new Swiper('.swiper-container-item', {
        slidesPerView: 4,
        spaceBetween: 30,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    layui.use('carousel', function(){
      var carousel = layui.carousel;
      //建造实例
      carousel.render({
        elem: '#banner'
        ,width: '100%' //设置容器宽度
        ,height: '500px'//设置高度
        ,anim: 'default' //轮播切换方式，default（左右切换）updown（上下切换）fade（渐隐渐显切换）
        ,interval: 5000 //自动切换的时间间隔，单位：ms（毫秒），不能低于800
        ,arrow: 'always' //始终显示箭头
      });
    });
</script>
