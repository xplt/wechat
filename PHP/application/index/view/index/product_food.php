<div class="section banner" style="background: url('/assets/index/img/product_food.png') no-repeat center center">
	<div style="height:150px;"></div>
    <h1 class="banner-title">外卖堂食一网打尽</h1>
    <p class="banner-info">
        中餐厅、西餐厅、日韩料理、酒吧、咖啡简餐、火锅店、快餐店、小吃店、甜品店、美食广场等
    </p>
    <p class="banner-info">支持外卖、零佣金</p>
</div>
<div class="section back-img" style="height:750px;">
    <div class="main">
        <div class="title">
            <p class="h1">河马点餐VS传统点餐</p>
            <p>河马点餐直接扫码点餐，无需要服务员，可以省时又省力</p>
        </div>
        <div class="compared-box">
            <div class="odds wow slideInLeft">
                <div class="odds-name">河马点餐</div>
                <ul>
                    <li>
                        <img src="/assets/index/img/true.png">
                        客户到店直接扫码点餐，自动识别台号
                    </li>
                    <li>
                        <img src="/assets/index/img/true.png">
                        无需服务人员，点餐自动到打印
                    </li>
                    <li>
                        <img src="/assets/index/img/true.png">
                        客户到店，无需等待服务员
                    </li>
                    <li>
                        <img src="/assets/index/img/true.png">
                        客户还没到店已经点好了，到店直接食用即可
                    </li>
                    <li>
                        <img src="/assets/index/img/true.png">
                        储存用户数据，实现会员经营
                    </li>
                    <li>
                        <img src="/assets/index/img/true.png">
                        提升50%
                    </li>
                    <li>
                        <img src="/assets/index/img/true.png">
                        发送优惠券，提升客户二次消费
                    </li>
                </ul>
            </div>
            <div class="pieces">
                <img src="/assets/index/img/sanjiao.png">
                <ul>
                    <li class="back-color-1">顾客体验</li>
                    <li class="back-color-2">服务人员</li>
                    <li class="back-color-3">时间效率</li>
                    <li class="back-color-4">预约点餐</li>
                    <li class="back-color-5">用户数据</li>
                    <li class="back-color-6">经营效率</li>
                    <li class="back-color-7">二次营销</li>
                </ul>
            </div>
            <div class="odds warp wow slideInRight">
                <div class="odds-name">传统点餐</div>
                <ul>
                    <li>
                        <img src="/assets/index/img/false.png">
                        需要呼叫服务员，体验糟糕
                    </li>
                    <li>
                        <img src="/assets/index/img/false.png">
                        需服务人员手写订单
                    </li>
                    <li>
                        <img src="/assets/index/img/false.png">
                        需要等待服务员，高峰时间超过半个小时
                    </li>
                    <li>
                        <img src="/assets/index/img/false.png">
                        必须到店才能点餐
                    </li>
                    <li>
                        <img src="/assets/index/img/false.png">
                        没有数据
                    </li>
                    <li>
                        <img src="/assets/index/img/false.png">
                        保存原状
                    </li>
                    <li>
                        <img src="/assets/index/img/false.png">
                        不能二次营销
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="section back-img" style="background-image: url('/assets/index/img/banner-8.jpg'); z-index: 10; height: 954px;">
    <div class="main" style="position: relative; padding-bottom: 10px; ">
        <div class="title white">
            <p class="h1">河马点餐的功能</p>
            <p>用户可以通过微信支付，也可以直接到收银台付款的方式，轻松买单</p>
        </div>
        <div class="features-list">
            <ul>
                <li>
                    <div class="left fl">
                        <img src="/assets/index/img/icon-5.png">
                        <p>在线点餐</p>
                    </div>
                    <div class="right fr">
                        微信在线选餐，一键提交，厨房实时接收订单，到店即可享用，无需排队等待
                    </div>
                </li>
                <li>
                    <div class="left fl">
                        <img src="/assets/index/img/icon-6.png">
                        <p>桌面二维码</p>
                    </div>
                    <div class="right fr">
                        通过小程序后台创建桌号，生成二维码，商户可将二维码贴在对应的桌子，用户到店后微信扫码便可进入店铺下单
                    </div>
                </li>
                <li>
                    <div class="left fl">
                        <img src="/assets/index/img/icon-7.png">
                        <p>微信加单</p>
                    </div>
                    <div class="right fr">
                        顾客如需继续加单，可再次扫码进入加单的时候只需要选购商品即可，无需再重新填写信息
                    </div>
                </li>
                <li>
                    <div class="left fl">
                        <img src="/assets/index/img/icon-8.png">
                        <p>方式支付</p>
                    </div>
                    <div class="right fr">
                        用户可以通过微信支付，也可以直接到收银台付款的方式，轻松买单
                    </div>
                </li>
                <li>
                    <div class="left fl">
                        <img src="/assets/index/img/icon-9.png">
                        <p>会员管理</p>
                    </div>
                    <div class="right fr">
                        会员信息触手可得，方便查找，高效会员营销推广，各种营销方案尽在手中
                    </div>
                </li>
                <li>
                    <div class="left fl">
                        <img src="/assets/index/img/icon-10.png">
                        <p>数据统计</p>
                    </div>
                    <div class="right fr">
                        会员统计、订单统计、销售统计，详细的订单数据统计，一目了然
                    </div>
                </li>
                <li>
                    <div class="left fl">
                        <img src="/assets/index/img/icon-11.png">
                        <p>订单管理</p>
                    </div>
                    <div class="right fr">
                        后台轻松管理订单，方便查看订单号、总额、支付状态等信息
                    </div>
                </li>
                <li>
                    <div class="left fl">
                        <img src="/assets/index/img/icon-12.png">
                        <p>客户维系</p>
                    </div>
                    <div class="right fr">
                        轻松管理公众号粉丝，可积累客户数据，客户通过扫码关注公众号后台即可生成用户管理数据
                    </div>
                </li>
                <li>
                    <div class="left fl">
                        <img src="/assets/index/img/icon-13.png">
                        <p>多人扫码</p>
                    </div>
                    <div class="right fr">
                        支持多人同时扫码点餐，不用浪费时间在讨论点餐上，省时又省力。
                    </div>
                </li>
                <li>
                    <div class="left fl">
                        <img src="/assets/index/img/icon-14.png">
                        <p>预付费充值卡</p>
                    </div>
                    <div class="right fr">
                        充值送营销解决方案除快速帮助商家回笼资金外，还能精准锁定消费者，受到消费者的热捧.
                    </div>
                </li>
                <li>
                    <div class="left fl">
                        <img src="/assets/index/img/icon-15.png">
                        <p>优惠券营销</p>
                    </div>
                    <div class="right fr">
                        商家可发行满减券.红包等方式获客，从而促进二次消费，红包券可在点餐前后，或支付前后，线上渠道等地领取
                    </div>
                </li>
                <li>
                    <div class="left fl">
                        <img src="/assets/index/img/icon-16.png">
                        <p>厨房云打印</p>
                    </div>
                    <div class="right fr">
                        消费者点单，收银台确认，多档口管理，依据配置打印不同菜品信息
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="section">
    <div class="main" style="width:1000px;">
        <div class="title">
            <p class="h1">外卖接单神器，快捷高效</p>
            <p class="small">自己的外卖平台，再不用多个平台跳转，统计分析更准确</p>
        </div>
        <div class="device-box mg-top clearfix">
            <div class="left fl wow flipInY">
                <img src="/assets/index/img/banner-11.png">
            </div>
            <div class="right fr pt-20 tal">
                <h1>拥有自己的外卖平台</h1>
                <p>顾客点单，商家接单支持在线支付，货到付款商家后台自主管理店铺信息，专门负责餐厅所有外卖订单含第三方订单接收、管理，再有不用多个平台跳转，统计分析更准确！</p>
                <ul class="tac big">
                    <li>
                        <img src="/assets/index/img/wm-icon1.png">
                        <p>自定义配送区域</p>
                    </li>
                    <li>
                        <img src="/assets/index/img/wm-icon2.png">
                        <p>支持第三方配送</p>
                    </li>
                    <li>
                        <img src="/assets/index/img/wm-icon3.png">
                        <p>支持活动促销</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="section back-img" style="background-image: url('/assets/index/img/banner-12.png'); z-index: 10; height: 743px;">
    <div class="main" style="position: relative; padding-bottom: 10px; ">
        <div class="title white">
            <p class="h1">为什么要做外卖小程序</p>
            <p>客户是自己的，流量是自己的，小程序也是自己的，实现回头客经营</p>
        </div>
        <div class="takeaway-table">
            <table>
                <tr>
                    <th class="small"> </th>
                    <th class="fwb"><img src="/assets/index/img/true2.png"> 外卖小程序</th>
                    <th class="fwb"><img src="/assets/index/img/false2.png"> 第三方平台</th>
                </tr>
                <tr>
                    <td class="fwb">竞争情况</td>
                    <td>客户直接进入自己的小程序，不会跳到其他竞争对手页面</td>
                    <td>通过平台检索功能即可对拼，竞争惨烈</td>
                </tr>
                <tr>
                    <td class="fwb">客户私有化</td>
                    <td>客户是自己的，流量是自己的、小程序也是自己的，实现回头客经营</td>
                    <td>客户、流量都是平台的，客户随时跑到其他店铺消费</td>
                </tr>
                <tr>
                    <td class="fwb">入口方面</td>
                    <td>外卖小程序拥有独立店铺二维码，小程序码、太阳码，附近的小程序、公众号关联等，多端入口，全网营销</td>
                    <td>平台入口，没有其他入口，你自己的老客户也成了平台的客户</td>
                </tr>
                <tr>
                    <td class="fwb">营销方式</td>
                    <td>外卖的特色化营销，让菜品保准化，让营销丰富化，让买单便捷化，如：满减活动、优惠券、充值送优惠</td>
                    <td>平台营销，如果还做优惠活动基本要亏本做</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="section gray">
    <div class="main">
        <div class="title">
            <p class="h1">六步开店 一站完成</p>
            <p>特有接口，一路无阻，快速上线</p>
        </div>
        <div class="item-line">
            <ul class="warp">
                <li>
                    <div class="left">
                        <span><img src="/assets/index/img/mode-icon5.png"></span>
                        <p class="h1">商家注册</p>
                        <p>微信扫码注册登录一步完成</p>
                    </div>
                    <div class="right">
                        <img src="/assets/index/img/gengduo.png">
                    </div>
                </li>
                <li>
                    <div class="left">
                        <span><img src="/assets/index/img/mode-icon4.png"></span>
                        <p class="h1">授权小程序</p>
                        <p>扫码授权或免费快速注册</p>
                    </div>
                    <div class="right">
                        <img src="/assets/index/img/gengduo.png">
                    </div>
                </li>
                <li>
                    <div class="left">
                        <span><img src="/assets/index/img/mode-icon6.png"></span>
                        <p class="h1">对接支付</p>
                        <p>在线开通收款账户或对接已有账户</p>
                    </div>
                    <div class="right">
                        <img src="/assets/index/img/gengduo.png">
                    </div>
                </li>
                <li>
                    <div class="left">
                        <span><img src="/assets/index/img/mode-icon7.png"></span>
                        <p class="h1">发布商品</p>
                        <p>商品分类设置，上传单双规格商品</p>
                    </div>
                    <div class="right">
                        <img src="/assets/index/img/gengduo.png">
                    </div>
                </li>
                <li>
                    <div class="left">
                        <span><img src="/assets/index/img/mode-icon8.png"></span>
                        <p class="h1">装修店铺</p>
                        <p>DIY小程序店铺页面，配置店铺</p>
                    </div>
                    <div class="right">
                        <img src="/assets/index/img/gengduo.png">
                    </div>
                </li>
                <li>
                    <div class="left">
                        <span><img src="/assets/index/img/mode-icon9.png"></span>
                        <p class="h1">发布上线</p>
                        <p>一键发布与升级小程序</p>
                    </div>
                </li>
            </ul>
        </div>
        <div class="title">
            <a href="/regist.php" class="layui-btn layui-btn-normal layui-btn-lg">立即免费开店</a>
        </div>
    </div>
</div>
