<div class="section banner" style="background: url('/assets/index/img/edition_banner.png') no-repeat center center">
    <div style="height:150px;"></div>
    <div class="banner-title">专业定制开发</div>
    <p class="banner-info">打造您专属的互联网产品</p>
</div>
<link rel="stylesheet" href="/assets/plugins/amazeui/amazeui.min.css"/>
<link rel="stylesheet" href="/assets/store/css/hema.app.css"/>
<style>
    .icon__success {
        color: #5eb95e;
    }

    .icon__danger {
        color: #F37B1D;
    }
</style>
<div class="section row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
					<div class="widget-title am-fl">版本功能区别</div>
				</div>
				<div class="help-item diff-table">
					<table class="am-table am-table-bordered am-table-centered">
						<tr>
							<th colspan="2">服务内容</th>
							<th>开源版</th>
							<th>商业版</th>
						</tr>
						<tr>
							<td rowspan="3" class="am-text-middle" width="15%">售后服务</td>
							<td>商业授权</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>技术支持</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>更新指导</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td rowspan="5" class="am-text-middle" width="15%">微信小程序</td>
							<td>外卖平台（仿美团）含H5端</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>单门店餐饮版</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
						</tr>
						<tr>
							<td>餐饮版(单/多门店+直播)</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>商城版(单/多门店+直播)</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>商家助手小程序</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td rowspan="5" class="am-text-middle" width="15%">代理中心</td>
							<td>小程序管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>数据统计</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>用户管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>财务管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>打印机管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td rowspan="22" class="am-text-middle" width="15%">超级管理端</td>
							<td>小程序申请工单管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>微信支付申请进件管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>小程序管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>小程序模板管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>小程序版本管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>小程序插件管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>公众号菜单设置</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>公众号智能回复管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>公众号模板消息配置</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>公众号群发消息功能</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>公众号素材管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>微信支付服务商配置</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>数据统计</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>代理管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>用户管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>财务管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>打印机管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>站点基本设置</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>微信开放平台 - 第三方平台对接</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>微信开放平台 - 网站应用对接</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>第三方配送平台对接（顺丰同城、达达配送、UU跑腿）</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>商家助手小程序对接</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td rowspan="28" class="am-text-middle" width="15%">商家管理端</td>
							<td>快速免审核费注册小程序</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>一键发布、升级小程序</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>小程序类目管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>小程序体验用户管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>附近小程序设置</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>小程序页面DIY</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>订阅模板消息配置</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>小程序插件安装</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>直播管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>商家多门店管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>店员管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>餐桌管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>用户口味选项设置</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>门店打印机配置</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>资讯文章管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>单、多规格商品管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>一键复制商品到其它门店</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>订单管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>订单评价</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>订单退款</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>用户管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>数据统计</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>优惠活动管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>会员等级管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>用户充值套餐管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>用户充值记录管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>分销管理，分销海报生成</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>商家基本设置</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td rowspan="12" class="am-text-middle" width="15%">店长管理端</td>
							<td>门店信息功能配置</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>店员管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>餐桌管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>门店打印机配置</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>资讯文章管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>单、多规格商品管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>订单管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>订单评价</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>订单退款</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>会员管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>门店数据统计</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>优惠活动管理</td>
							<td><a href="javascript:void(0);" class="am-icon-close icon__danger"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						
						<tr>
							<td rowspan="5" class="am-text-middle" width="15%">站点前端</td>
							<td>自定义导航菜单</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>友情链接管理</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>用户创建微信小程序</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>用户在线续费</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
						<tr>
							<td>用户钱包充值</td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
							<td><a href="javascript:void(0);" class="am-icon-check icon__success"></a></td>
						</tr>
					</table>
				</div>
            </div>
        </div>
    </div>
</div>

