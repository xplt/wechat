<div class="section banner" style="background: url('/assets/index/img/empower_banner.png') no-repeat center center">
    <div style="height:150px;"></div>
    <h1 class="banner-title">小投入 赢得大机遇</h1>
    <p class="banner-info">快人一步抢占风口，抢占微信千亿红利市场</p>
</div>
<div class="section">
    <div class="main">
        <div class="title">
            <p class="h1">为什么需要授权？</p>
            <p>对于商业用户,我们建议您购买商业授权以保护您的合法权益，通过授权会解除所有限制，获得更多的增值服务</p>
			<p>授权联系QQ：19966591</p>
        </div>
        <div class="item-list">
            <ul>
                <li>
                    <span><img src="/assets/index/img/select-red1.png"></span>
                    <p class="h1">合法经营</p>
                    <p>系统商业运行合法性避免法律纠纷，尊重我们的知识产权</p>
                </li>
                <li>
                    <span><img src="/assets/index/img/select-red4.png" class="mr1"></span>
                    <p class="h1">解除限制</p>
                    <p>解除所有限制，解封专属功能，赠送商家助手小程序</p>
                </li>
                <li>
                    <span><img src="/assets/index/img/shengji.png" class="mr2"></span>
                    <p class="h1">免费升级</p>
                    <p>升级、功能调整时，优先考虑授权用户需求，可通过升级包一键升级</p>
                </li>
                <li>
                    <span><img src="/assets/index/img/function-icon12.png"></span>
                    <p class="h1">技术支持</p>
                    <p>商业授权用户，无需担心技术问题，我们是您最坚强的技术后盾</p>
                </li>
            </ul>
        </div>
    </div>
</div>