<script src="https://res.wx.qq.com/connect/zh_CN/htmledition/js/wxLogin.js"></script>
<div class="section">
	<div class="login-in">
		<div class="login-info">
			<div class="form">
				<div class="login-title">
					<i class="layui-icon layui-icon-login-wechat"></i>
					<span>微信扫码注册/登录</span>
				</div>
				<div id="login_container" class="login_container"></div>
				<div class="login-nav">
					<span class="login">已有账号？<a href="/login.php">登录</a></span>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var obj = new WxLogin({
		id: "login_container",
		appid: "<?= $app_id?>",
		scope: "snsapi_login",
		redirect_uri: encodeURIComponent('<?= $base_url?>index.php?s=/task/wechat/login'), 
		state: Math.ceil(Math.random()*1000),
		style: "black",
		href: "data:text/css;base64,QGNoYXJzZXQgIlVURi04IjsKLmltcG93ZXJCb3ggLnFyY29kZSB7d2lkdGg6IDI4MHB4O30KLmltcG93ZXJCb3ggLnRpdGxlIHtkaXNwbGF5OiBub25lO30KLmltcG93ZXJCb3ggLmluZm8ge3dpZHRoOiAyODBweDt9Ci5zdGF0dXNfaWNvbiB7ZGlzcGxheTogbm9uZX0KLmltcG93ZXJCb3ggLnN0YXR1cyB7dGV4dC1hbGlnbjogY2VudGVyO30g"
	});
</script>