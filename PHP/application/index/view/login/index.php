<script src="assets/plugins/layer/layer.js"></script>
<script src="assets/plugins/jquery/jquery.form.min.js"></script>
<script src="assets/plugins/amazeui/amazeui.min.js"></script>
<script src="https://res.wx.qq.com/connect/zh_CN/htmledition/js/wxLogin.js"></script>
<div class="section">
	<div class="login-in">
		<div class="login-info">
			<div class="form">
				<div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
				  <ul class="layui-tab-title">
				    <li class="layui-this">微信登录</li>
				    <li>密码登录</li>
				  </ul>
				  <div class="layui-tab-content">
				    <div class="layui-tab-item layui-show">
						<div id="login_container" class="login_container"></div>
				    </div>
				    <div class="layui-tab-item">
						<form id="my-form">
							<span class="input">
								<img src="/assets/index/img/people.png">
								<input type="text" name="User[user_name]" value="test" placeholder="请输入账号/手机号" required>
							</span>
							<span class="input">
								<img src="/assets/index/img/lock.png">
								<input type="password" name="User[password]" value="test" placeholder="请输入密码" required>
							</span>
							<button id="btn-submit" class="layui-btn sub-btn" type="submit">登录</button>
						</form>
						<div class="login-nav">
							<span class="login">找回账号密码：官方公众号回复 “找回账号”</span>
							<span class="login">我还没有账号？<a href="regist.php">去注册</a></span>
						</div>
				    </div>
				  </div>
				</div> 
			</div>
		</div>
	</div>
</div>
<script>
	var obj = new WxLogin({
		id: "login_container",
		appid: "<?= $app_id?>",
		scope: "snsapi_login",
		redirect_uri: encodeURIComponent('<?= $base_url?>index.php?s=/task/wechat/login'), 
		state: Math.ceil(Math.random()*1000),
		style: "black",
		href: "data:text/css;base64,QGNoYXJzZXQgIlVURi04IjsKLmltcG93ZXJCb3ggLnFyY29kZSB7d2lkdGg6IDI4MHB4O30KLmltcG93ZXJCb3ggLnRpdGxlIHtkaXNwbGF5OiBub25lO30KLmltcG93ZXJCb3ggLmluZm8ge3dpZHRoOiAyODBweDt9Ci5zdGF0dXNfaWNvbiB7ZGlzcGxheTogbm9uZX0KLmltcG93ZXJCb3ggLnN0YXR1cyB7dGV4dC1hbGlnbjogY2VudGVyO30g"
	});

	$(function () {

        $('#my-form').formPost();

    });
</script>
