<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?= $web['name'] . ' - ' . $title?></title>
	<meta name="author" content="<?= $web['name']?>">
	<meta name="keywords" content="<?= empty($keywords)?$web['keywords']:$keywords?>">
	<meta name="description" content="<?= empty($description)?$web['description']:$description?>">
	<meta name="referrer" content="never">
	<link rel="icon" href="/assets/icon.png" type="image/x-icon">
	<link rel="stylesheet" href="/assets/index/css/main.css">
	<link rel="stylesheet" href="/assets/plugins/layui/css/layui.css">
	<link rel="stylesheet" href="/assets/plugins/animate/animate.min.css">
	<script src="assets/store/js/lang.js"></script>
	<script type="text/javascript" src="/assets/plugins/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/layui/layui.js"></script>
	<script type="text/javascript" src="/assets/plugins/vue/vue.min.js"></script>
</head>
<body>
<script>
layui.use('element', function(){
  var element = layui.element;
});
</script>
<div class="section scroll">
	<ul class="layui-nav" lay-filter="">
		<li class="nav-logo"><img src="/assets/index/index-logo.png"></li>
		<?php if (!empty($menu)): foreach ($menu as $first): ?>
		<?php if (isset($first['child']) AND $first['status']['value']==1):?>
		<li class="layui-nav-item <?= $key === $first['key']?'layui-this':''?>">
			<a href="javascript:;"><?=$first['name']?></a>
			<dl class="layui-nav-child"> <!-- 二级菜单 -->
				<?php foreach ($first['child'] as $two): if($two['status']['value']==1): ?>
				<dd>
					<?php if(empty($two['url'])):?>
						<a href="/index/<?= $two['key']?>.html"><?=$two['name']?></a>
					<?php else:?>
						<a href="<?= $two['url']?>" target="_blank"><?=$two['name']?></a>
					<?php endif;?>
				</dd>
				<?php endif; endforeach;?>
			</dl>
		</li>
		<?php elseif($first['status']['value']==1):?>
		<li class="layui-nav-item <?= $key===$first['key']?'layui-this':''?>">
			<?php if(empty($first['url'])):?>
				<a href="/index/<?= $first['key']?>.html"><?=$first['name']?></a>
			<?php else:?>
				<a href="<?= $first['url']?>" target="_blank"><?=$first['name']?></a>
			<?php endif;?>
		</li>
		<?php endif;endforeach;endif;?>
	</ul>
		<?php if(isset($store['is_login']) AND isset($store['is_admin']) AND $store['is_login'] AND $store['is_admin']):?>
		<ul class="layui-nav layui-layout-right">
		  <li class="layui-nav-item <?= $key === 'user'?'layui-this':''?>">
			<a href="javascript:;">
			  <img src="<?= $store['user']['avatarUrl']?>" class="layui-nav-img">
			  <?= $store['user']['user_name']?>
			</a>
			<dl class="layui-nav-child">
				<dd><a href="/user.php">用户中心</a></dd>
				<dd><a href="/index.php?s=/user/wxapp/index">应用管理</a></dd>
				<dd><a href="/index.php?s=/user/wallet/index">账户钱包</a></dd>
				<dd><a href="/index.php?s=/user/setting/renew">修改密码</a></dd>
			</dl>
		  </li>
		  <li class="layui-nav-item"><a href="<?= url('login/logout')?>">退出</a></li>
		</ul>
	<?php else:?>
		<div class="nav-login layui-btn-container layui-layout-right">
		<a href="/login.php" class="layui-btn layui-btn-normal layui-btn-sm">商家登录</a>
		</div>
	<?php endif;?>
</div>
<!-- 内容区域 start -->
<div>
        {__CONTENT__}
</div>
<!-- 内容区域 end -->
<!--
<div class="section foot">
    <div class="bottom-info">
        <h1>定制小程序、公众号，抢占微信海量用户红利！</h1>
        <a href="/login.php">立即定制</a>
    </div>
</div>
-->
<div class="section">
    <div class="foot-nav-box">
    <!--
    	<div style="width:100%; height:100%; background:url('/assets/index/img/footer-bg.jpg') top center no-repeat; position:absolute; top:0; left:0;"></div>
		<div class="cloud_effect">
			<div class="clouds_one"></div>
			<div class="clouds_two"></div>
			<div class="clouds_three"></div>
		</div>
	-->
        <div class="foot-nav">
            <div class="nav-list">
				<span class="title">联系我们：</span>&nbsp;&nbsp;&nbsp;
				QQ：<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=19966591&site=qq&menu=yes">19966591</a> &nbsp;&nbsp;
				QQ群：<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=3d8a3f28e60006b3868b12a4ec750ca5606a67c0c1b61081833dbbdeeb5ca698">916181349</a> &nbsp;&nbsp;
				热线：<?= $web['phone']?> &nbsp;&nbsp;
				<p>
				<span class="title">上班时间：</span>&nbsp;&nbsp;&nbsp;
				周一到周日（9:00-17:30）
				</p>
				
				<span class="title">友情链接：</span>&nbsp;&nbsp;&nbsp;
				<?php foreach ($link as $idx => $item): if($idx==0):?>
				<a href="<?= $item['url']?>" target="_blank"><?= $item['name']?></a>&nbsp;&nbsp;&nbsp;
				<?php else:?>
				丨&nbsp;&nbsp;&nbsp;<a href="<?= $item['url']?>" target="_blank"><?= $item['name']?></a>&nbsp;&nbsp;&nbsp;
				<?php endif;endforeach;?>
            </div>
            <div class="downApp">
                <img src="<?= $web['qrcode']?>">
                <p>关注我们</p>
            </div>
            <div class="copy">
				Copyright© <a href="<?= $web['domain']?>" target="_blank"> <?= $web['company']?></a> All rights reserved.
				<br>
				<a href="http://beian.miit.gov.cn/" target="_blank"><?= $web['icp']?></a>
	        </div>
        </div>
    </div>
</div>
<div class="RightFrame">
    <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=19966591&site=qq&menu=yes">
        <div class="SmallBox">
            <div class="Pic">
                <img src="/assets/index/img/Right-pic-TX.png"/>
                <span></span>
            </div>
            <div class="Botton">
                <em></em>联系客服
            </div>
        </div>
    </a>
</div>
<script src="/assets/plugins/wow/wow.min.js"></script>
<script>
    new WOW().init();
</script>
</body>
</html>