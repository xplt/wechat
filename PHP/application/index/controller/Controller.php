<?php
namespace app\index\controller;
use think\Config;
use think\Session;
use think\Cookie;
use app\index\model\WebSet;
use app\index\model\WebMenu as WebMenuModel;
use app\index\model\WebLink as WebLinkModel;
use app\index\model\Wechat as WechatModel;

/**
 * 用户控制器基类
 */
class Controller extends \think\Controller
{
    protected $store;			//用户登录信息
    protected $store_user_id;	//用户ID
    protected $controller = '';	//当前控制器名称
    protected $action = '';		//当前方法名称
    protected $routeUri = '';	//当前路由uri
    protected $group = '';		//当前路由：分组名称
	protected $agent_id = '';//代理id
	
	/* 登录验证白名单 */
    protected $allowAllAction = [
        'index/index',	// 首页
		'about/index',	// 关于我们
		'cases/index',	// 案例展示
		'device/index',	// 智能设备
		'joins/index',	// 招商加盟
		'login/index',	// 用户登录
        'login/logout', //用户退出
		'down/index',	// 下载
		'product/food',	// 点餐
		'product/take',	// 外卖
		'regist/index',	// 用户注册
    ];

    /**
     * 后台初始化
     */
    public function _initialize()
    {
        $this->getWxapp();//初始化数据
        $this->getRouteinfo();	// 当前路由信息
        $this->checkLogin();	// 验证登录
        $this->layout();		// 全局layout
    }

    /**
     * 全局layout模板输出
     */
    private function layout()
    {
		$web = WebSet::getItem('web');
		$wechat = WechatModel::getWechat(['wxapp_id' => 0]);
		$web['qrcode'] = isset($wechat['qrcode_url'])?$wechat['qrcode_url']:'';
        // 输出到view
        $this->assign([
            'base_url' => web_url(), 	// 当前域名
            'web' => $web,//站点设置
            'group' => $this->group,
            'store' => $this->store, // 商家登录信息
			'menu' => WebMenuModel::getCacheTree(),
			'link' => WebLinkModel::getAll(),
        ]);
    }

    /**
     * 解析当前路由参数 （分组名称、控制器名称、方法名）
     */
    protected function getRouteinfo()
    {
        // 控制器名称
        $this->controller = toUnderScore($this->request->controller());
        // 方法名称
        $this->action = $this->request->action();
        // 控制器分组 (用于定义所属模块)
        $groupstr = strstr($this->controller, '.', true);
        $this->group = $groupstr !== false ? $groupstr : $this->controller;
        // 当前uri
        $this->routeUri = $this->controller . '/' . $this->action;
    }
	
	/**
     * 验证登录状态
     */
    private function checkLogin()
    {
        // 验证当前请求是否在白名单
        if (in_array($this->routeUri, $this->allowAllAction)) {
            return true;
        }
		// 验证登录状态
        if (empty($this->store) 
            || (int)$this->store['is_login'] !== 1 
            || (int)$this->store['is_admin'] !== 1
        ){
			$this->error('您还没有登录', '/login.php');
            return false;
        }
        return true;
    }
	
	/**
     * 初始化数据
     */
    protected function getWxapp()
    {
        $this->store = Session::get('hema_store');  // 用户登录信息
        $this->store_user_id = $this->store['user']['store_user_id']; // 用户ID
        $this->agent_id = $this->store['user']['agent_id'];//代理id
    }

    /**
     * 返回封装后的 API 数据到客户端
     */
    protected function renderJson($code = 1, $msg = '', $url = '', $data = [])
    {
        return compact('code', 'msg', 'url', 'data');
    }

    /**
     * 返回操作成功json
     */
    protected function renderSuccess($msg = 'success', $url = '', $data = [])
    {
        return $this->renderJson(1, $msg, $url, $data);
    }

    /**
     * 返回操作失败json
     */
    protected function renderError($msg = 'error', $url = '', $data = [])
    {
        return $this->renderJson(0, $msg, $url, $data);
    }

    /**
     * 获取post数据 (数组)
     */
    protected function postData($key)
    {
        return $this->request->post($key . '/a');
    }

}
