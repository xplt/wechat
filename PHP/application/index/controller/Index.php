<?php
namespace app\index\controller;
use app\index\model\WebMenu as WebMenuModel;

/**
 * 页面
 */
class Index extends Controller
{
    public function index($key='index')
    {
		$menu = WebMenuModel::detail(['key' =>$key]);
		$set = explode('_',$menu['key']);
		$key = $set[0];
		$title = $menu['title'];
		$this->assign('key', $key);
		$this->assign('title', $title);
		$this->assign('description', $menu['description']);
		$this->assign('keywords', $menu['keywords']);
		return $this->fetch($menu['key']);
    }
}
