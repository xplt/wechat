<?php
namespace app\index\controller;
use app\index\model\StoreUser;
use app\index\model\WebSet;
use think\Session;

/**
 * 用户登录
 */
class Login extends Controller
{
    /**
     * 用户登录 - 首页
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $model = new StoreUser;
            if ($model->login($this->postData('User'))) {
                return $this->renderSuccess('登录成功', 'user.php');
            }
            return $this->renderError($model->getError() ?: '登录失败');
        }
		// 验证登录状态
        if (isset($this->store) AND (int)$this->store['is_login'] === 1 AND (int)$this->store['is_admin'] === 1) {
			$this->redirect('/user.php');
        }
        $values = WebSet::getItem('wxweb');
		$this->assign('title','商户登录');
		$this->assign('key', 'login');
        $this->assign('app_id', $values['app_id']);
        return $this->fetch('index');
    }

    /**
     * 退出登录
     */
    public function logout()
    {
        Session::clear('hema_store');
        $this->redirect('/index.php');
    }

}
