<?php
namespace app\index\controller;
use app\index\model\WebSet;

/**
 * 用户注册
 */
class Regist extends Controller
{
    public function index()
    {
		$values = WebSet::getItem('wxweb');
		$this->assign('title','商户注册');
		$this->assign('key', 'regist');
        $this->assign('app_id', $values['app_id']);
        return $this->fetch('index');
    }
}
