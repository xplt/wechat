<?php
namespace app\index\model;
use app\common\model\StoreUser as StoreUserModel;
use think\Session;

/**
 * 商家用户模型
 */
class StoreUser extends StoreUserModel
{
    /**
     * 商家用户登录
     */
    public function login($data)
    {
		$filter = [
            'user_name' => $data['user_name'],
            'password' => hema_hash($data['password']),
			'is_agent' => ['<',2]
        ];
        // 验证用户名密码是否正确
		if($user = $this->useGlobalScope(false)->where($filter)->find()){
			// 保存登录状态
			Session::set('hema_store', [
				'user' => [
					'store_user_id' => $user['store_user_id'],
					'user_name' => $user['user_name'],
					'agent_id' => $user['agent_id'],
                    'avatarUrl' => $user['avatarUrl']
				],
				'is_login' => true,
				'is_admin' => true,
			]);
			return true;
		}else{
			$this->error = '登录失败, 用户名或密码错误';
            return false;
		}
    }  

}
