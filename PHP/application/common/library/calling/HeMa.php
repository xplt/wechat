<?php
namespace app\common\library\calling;
use app\common\model\WebSet;

/*
 * 河马云叫号SKD V1.0.0
*/
class HeMa
{
	private $api_url = '';//接口路径
	private $secret_id = '';		
	private $secret_key = '';
	
	/**
	 * 构造函数
	 */
	public function __construct()
	{
		$values = WebSet::getItem('calling');
		$this->api_url = $values['api_url'];
		$this->secret_id = $values['secret_id'];
		$this->secret_key = $values['secret_key'];
		
	}
	
	/**
	* 推送消息
	*/
	public function push($mode,$device_name,$row_no='')
	{
		$params = $this->params();
		$params['device_name'] = $device_name;//设备ID
		$params['mode'] = $mode;//播报模式
		$params['row_no'] = $row_no;
        $res = json_decode($this->http_post_json('device/push',$params),true);
		if($res['code']!=0){
			return $res['msg'];
		}
		return false;
	}
	
	/**
	* 授权绑定设备
	*/
	public function add($data)
	{
		$params = $this->params();
		$params['calling_name'] = $data['calling_name'];//自定义名称(可填)
		$params['device_name'] = $data['device_name'];//设备ID
		$params['device_key'] = $data['device_key'];//设备密钥
        $res = json_decode($this->http_post_json('device/add',$params),true);
		if($res['code']!=0){
			return $res['msg'];
		}
		return false;
	}

	/**
	* 修改绑定设备
	*/
	public function edit($data)
	{
		$params = $this->params();
		$params['calling_name'] = $data['calling_name'];//自定义名称(可填)
		$params['device_name'] = $data['device_name'];//设备ID
		$params['device_key'] = $data['device_key'];//设备密钥
        $res = json_decode($this->http_post_json('device/edit',$params),true);
		if($res['code']!=0){
			return $res['msg'];
		}
		return false;
	}
	
	/**
	* 删除授权绑定的设备
	*/
	public function delete($device_name){
		$params = $this->params();
		$params['device_name'] = $device_name;//设备ID
        $res = json_decode($this->http_post_json('device/delete',$params),true);
		if($res['code']!=0){
			return $res['msg'];
		}
		return false;
	}
	
	/**
	* 获取设备状态接口 
	*/
	public function getStatus($device_name){
		$params = $this->params();
		$params['device_name'] = $device_name;//编号
		return json_decode($this->http_post_json('device/getStatus',$params),true);
	}

	/**
	* 获取Token
	*/
	public function getToken($appid,$appsecret)
    {
		$this->appid = $appid;
		$this->appsecret = $appsecret;
        $time = time();
        $params = [
            'client_id' => $this->appid,
            'timestamp' => $time,
            'sign' => $this->getSign($time),
            'id' => $this->uuid4(),
            'scope' => 'all'
        ];
        $params['grant_type'] = 'client_credentials';
        $res = json_decode($this->http_post_json($params,'oauth/oauth'),true);
		if($res['error']==0){
			return $res['body']['access_token'];
		}
		return '';
    }
	
	/**
	* 公共参数
	*/
	private function params(){
		$time = time();         //请求时间
		return $params = [
			'secret_id' => $this->secret_id,
			'secret_key' => $this->secret_key,
            'timestamp' => $time,
            'sign' => $this->getSign($time)
		];
	}
	
	/**
	* 生成签名
	*/
	private function getSign($timestamp)
    {
        return md5($this->secret_id . $timestamp . $this->secret_key);
    }
	
	
	//发送post请求
	/**
	 * PHP发送Json对象数据
	 * @return string
	 */
	private function http_post_json($url,$data=array(),$headers=array())
	{
		$url = $this->api_url . $url;
		$curl = curl_init();
		if( count($headers) >= 1 ){
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		}
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($curl);
		$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
		return $output;
	}
}