<?php
namespace app\common\library\mstching;
use app\common\model\WebSet;
use app\common\model\PrinterLog;

/*
 * 飞鹅打印机驱动模块
 * 测试机型号：FP-V58W
*/
class Feie
{
	private $baseurl = '';//接口路径
	private $appid = '';		
	private $appsecret = '';
	
	/**
	 * 构造函数
	 */
	public function __construct()
	{
		$values = WebSet::getItem('printer');
		$this->baseurl = $values['feie']['api_url'];
		$this->appid = $values['feie']['app_key'];
		$this->appsecret = $values['feie']['app_secret'];
	}
	
	/**
	* [打印订单接口 Open_printMsg]
	* @param  [string] $sn      [打印机编号sn]
	* @param  [string] $data 	[打印数据]
	* @param  [string] $times   [打印联数]
	* @param  [string] $tpl     [模板类型 0=订单 1=退单]
	* @return [string]          [接口返回值]
	*/
	public function printMsg($data ,$sn ,$times='1',$tpl)
	{
		if($tpl==0){
			$content = $this->make_order_templet($data);//制作订单模板
		}else{
			$content = $this->make_refund_templet($data);//制作退单模板
		}
		$msgInfo = $this->msgInfo();
		$msgInfo['apiname'] = 'Open_printMsg';
		$msgInfo['sn'] = $sn;
		$msgInfo['content'] = $content;
		$msgInfo['times'] = $times;
		$res = json_decode($this->http_post_json($msgInfo),true);
		if($res['ret']!=0){
			//记录打印失败日志
			$model = new PrinterLog;
			$model->save([
				'brand' => 20,
				'open_user_id' => $sn,
				'uuid' => '',
				'tpl' => $tpl,
				'content' => $content,
				'order_no' => $data['order_no'],
				'shop_id' => $data['shop_id'],
				'wxapp_id' => $data['wxapp_id']
			]);
		}
		return true;
	}
	
	/**
	* [查询订单是否打印成功接口 Open_queryOrderState]
	* @param  [string] $orderid [调用打印机接口成功后,服务器返回的JSON中的编号 例如：123456789_20190919163739_95385649]
	* @return [string]          [接口返回值]
	*/
	public function queryOrderState($orderid){
		$msgInfo = $this->msgInfo();
		$msgInfo['apiname'] = 'Open_queryOrderState';
		$msgInfo['orderid'] = $orderid;
		$jsonStr = json_encode($msgInfo);
		$res = json_decode($this->http_post_json($jsonStr),true);
		if($res['ret']==0){
			return true;
		}
		return false;
	}
	
	/**
	* [批量添加打印机接口 Open_printerAddlist]
	* @param  [string] $printerContent [打印机的sn#key]
	* @return [string]                 [接口返回值]
	*/
	public function printerAddlist($printerContent){
		//$printerContent,编号(必填) # 打印机识别码(必填) # 备注名称(选填) # 流量卡号码(选填)
		$msgInfo = $this->msgInfo();
		$msgInfo['apiname'] = 'Open_printerAddlist';
		$msgInfo['printerContent'] = $printerContent;
		$res = json_decode($this->http_post_json($msgInfo),true);
		if($res['ret']==0){
			return true;
		}
		return false;
	}
	
	/**
	* [批量删除打印机 Open_printerDelList]
	* @param  [string] $snlist [打印机编号，多台打印机请用减号“-”连接起来]
	* @return [string]         [接口返回值]
	*/
	public function printerDelList($snlist){
		$msgInfo = $this->msgInfo();
		$msgInfo['apiname'] = 'Open_printerDelList';
		$msgInfo['snlist'] = $snlist;
		$res = json_decode($this->http_post_json($msgInfo),true);
		if($res['ret']==0){
			return true;
		}
		return false;
	}
	
	/**
	* [修改打印机信息接口 Open_printerEdit]
	* @param  [string] $sn       [打印机编号]
	* @param  [string] $name     [打印机备注名称]
	* @param  [string] $phonenum [打印机流量卡号码,可以不传参,但是不能为空字符串]
	* @return [string]           [接口返回值]
	*/
	public function printerEdit($data){
		$msgInfo = $this->msgInfo();
		$msgInfo['apiname'] = 'Open_printerEdit';
		$msgInfo['sn'] = $data['uuid'];
		$msgInfo['name'] = $data['name'];
		$msgInfo['phone'] = $data['phone'];
		$jsonStr = json_encode($msgInfo);
		$res = json_decode($this->http_post_json($jsonStr),true);
		if($res['ret']==0){
			return true;
		}
		return false;
	}
	
	/**
	* [获取某台打印机状态接口 Open_queryPrinterStatus]
	* @param  [string] $sn [打印机编号]
	* @return [string]     [接口返回值]
	*/
	public function queryPrinterStatus($sn){
		$msgInfo = $this->msgInfo();
		$msgInfo['apiname'] = 'Open_queryPrinterStatus';
		$msgInfo['sn'] = $sn;
		$res = json_decode($this->http_post_json($msgInfo),true);
		if($res['ret']==0){
			$msg = '离线';
			if($res['data']=='在线，工作状态正常。'){
				$msg = '正常';
			}
			if($res['data']=='在线，工作状态不正常。'){
				$msg = '异常';
			}
			return $msg;
		}
		return false;
	}
	
	/**
	* 公共参数
	*/
	private function msgInfo(){
		$time = time();         //请求时间
		return $msgInfo = [
			'user'=>$this->appid,
			'stime'=>$time,
			'sig'=>$this->signature($time)
		];
	}
	
	/**
	* [signature 生成签名]
	* @param  [string] $time [当前UNIX时间戳，10位，精确到秒]
	* @return [string]       [接口返回值]
	*/
	private function signature($time){
		return sha1($this->appid . $this->appsecret . $time);//公共参数，请求公钥
	}
	
	
	//发送post请求
	/**
	 * PHP发送Json对象数据
	 * @return string
	 */
	private function http_post_json($data)
	{
		$data = http_build_query($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_URL, $this->baseurl);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/x-www-form-urlencoded',
				'Content-Length: ' . strlen($data)
			)
		);
		$response = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		return $response;
	}
	
	/**
	 * 制作订单模板
	 * $data 订单数据
	 * 58mm的机器,一行打印16个汉字,32个字母;
	 */
	private function make_order_templet($data)
	{
		/*
		 <BR> ：换行符
		 <CUT> ：切刀指令(主动切纸,仅限切刀打印机使用才有效果) 
		 <LOGO> ：打印LOGO指令(前提是预先在机器内置LOGO图片)
		 <PLUGIN> ：钱箱或者外置音响指令
		 <CB></CB>：居中放大
		 <B></B>：放大一倍
		 <C></C>：居中
		 <L></L>：字体变高一倍
		 <W></W>：字体变宽一倍
		 <QR></QR>：二维码（单个订单，最多只能打印一个二维码）
		 <RIGHT></RIGHT>：右对齐
		 <BOLD></BOLD>：字体加粗
		*/
		if($data['order_mode']['value']==10){
			if(isset($data['table']['table_name'])){
				$title = $data['table']['table_name'];
			}else{
				$title = '堂食'.$data['row_no'].'号';
			}
		}
		if($data['order_mode']['value']==20){
			$title = '外卖'.$data['row_no'].'号';
		}
		if($data['order_mode']['value']==30){
			$title = '自取'.$data['row_no'].'号';
		}
		$content = '<CB># '.$title.' #</CB><BR>';
		if($data['arrive_time']['value']>0){
			$content .= '到店时间:'.$data['arrive_time']['text'].'<BR>';
		}
		$content .= '--------------------------------<BR>';
		$content .= '<C>'.$data['shop']['shop_name'].'</C><BR>';
		if($data['transaction_id']){
			$content .= '<C>-- 微信支付 --</C><BR>';
		}else{
			$content .= '<C>-- 余额支付 --</C><BR>';
		}
		
		$content .= '支付时间:'.date("Y-m-d H:i:s",$data['pay_time']).'<BR>';
		$content .= '<B>名称        数量</B><BR>';//字符占位数量4+8+4
		$content .= '--------------------------------<BR>';
		//循环拼接打印模板
		for ($i = 0; $i < sizeof($data['goods']); $i++) {
				$goods_name = $data['goods'][$i]['goods_name'].$data['goods'][$i]['goods_attr'];//产品名字
				$total_num = $data['goods'][$i]['total_num'];	//产品数量
				//设置名称字符长度
				$lan = mb_strlen($goods_name,'utf-8')*2;//一个汉字2个字符长度
				for($n=$lan;$n<13;$n++){
					$goods_name .= ' ';
				}
				$content .= '<B>' . $goods_name . '×' .$total_num . '</B><BR>';
		}
		$content .= '--------------------------------<BR>';
		$content .= '商品金额：+￥'.$data['total_price'].'<BR>';
		if($data['express_price']>0){
			$content .= '配送费用：+￥'.$data['express_price'].'<BR>';
		}
		if($data['pack_price']>0){
			$content .= '餐盒费用：+￥'.$data['pack_price'].'<BR>';
		}
		if($data['ware_price']>0){
			$content .= '餐具调料：+￥'.$data['ware_price'].'<BR>';
		}
		if($data['activity_price']>0){
			$content .= '优惠金额：-￥'.$data['activity_price'].'<BR>';
		}
		$content .= '实付金额：￥'.$data['pay_price'].'<BR>';
		if($data['flavor']){
			$content .= '口味要求：'.$data['flavor'].'<BR>';
		}
		if($data['message']){
			$content .= '顾客留言：'.$data['message'].'<BR>';
		}
		if($data['order_mode']['value']==20){
			$content .= '配送地址：'.$data['address']['detail'].'<BR>';
			$content .= '接 收 人：'.$data['address']['name'].$data['address']['phone'].'<BR>';
		}
		$content .= '<CB># '.$title.' # 完</CB><BR>';
		$content .= '<BR><BR><CUT>';
		return $content;
	}
	
	/**
	 * 制作退单模板
	 * $data 退单数据
	 * 58mm的机器,一行打印16个汉字,32个字母;
	 */
	private function make_refund_templet($data)
	{
		if($data['order_mode']['value']==10){
			if(isset($data['table']['table_name'])){
				$title = $data['table']['table_name'];
			}else{
				$title = '堂食'.$data['row_no'].'号';
			}
		}
		if($data['order_mode']['value']==20){
			$title = '外卖'.$data['row_no'].'号';
		}
		if($data['order_mode']['value']==30){
			$title = '自取'.$data['row_no'].'号';
		}
		$content = '<CB># 退单 #</CB><BR>';
		$content .= '<CB>- '.$title.' -</CB><BR>';
		$content .= '--------------------------------<BR>';
		$content .= '<C>'.$data['shop']['shop_name'].'</C><BR>';
		$content .= '<B>名称        数量</B><BR>';//字符占位数量4+8+4
		$content .= '--------------------------------<BR>';
		//循环拼接打印模板
		for ($i = 0; $i < sizeof($data['goods']); $i++) {
			if($data['goods'][$i]['refund_num']>0){
				$goods_name = $data['goods'][$i]['goods_name'].$data['goods'][$i]['goods_attr'];//产品名字
				$refund_num = $data['goods'][$i]['refund_num'];	//产品数量
				//设置名称字符长度
				$lan = mb_strlen($goods_name,'utf-8')*2;//一个汉字2个字符长度
				for($n=$lan;$n<13;$n++){
					$goods_name .= ' ';
				}
				$content .= '<B>' . $goods_name . '×' . $refund_num . '</B><BR>';
			}
		}
		$content .= '--------------------------------<BR>';		
		$content .= '付款金额：￥'.$data['pay_price'].'<BR>';
		$content .= '退款金额：￥'.$data['refund_price'].'<BR>';
		$content .= '退款理由：'.$data['refund_desc'].'<BR>';
		$content .= '<CB># 退单 # 完</CB><BR>';
		$content .= '<BR><BR><CUT>';
		return $content;
	}
}