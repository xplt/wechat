<?php
namespace app\common\library\wechat;
use app\common\model\WebSet as WebSetModel;
use app\common\model\Setting as SettingModel;
use app\common\library\sms\Driver as SmsDriver;
use app\common\model\Wxapp as WxappModel;
use app\common\exception\BaseException;

/**
 * 微信支付
 */
class WxPay
{
    private $config; // 微信支付配置

    /**
     * 构造方法
     */
    public function __construct($config=[])
    {
        /*
        $config = [
            'is_sub' => '是否为特约商户，值=0/1',
            'app_id' => '调起支付的应用',
            'mchid' => '商户号',
            'apikey' => '商户密钥',
            'cert_pem' => '(退款)证书',
            'key_pem' => '(退款)证书密钥',
        ];
        */
        $this->config = $config;
    }

    /**
     * 统一下单API
     * 参数 $order_no=订单号 $openid=微信用户ID, $total_fee=支付金额, $type=支付类型,$body=商品描述
     */
    public function unifiedorder($order_no, $openid='', $total_fee, $type='',$body='')
    {
		$notify_url = 'notice.php';
		$trade_type = 'JSAPI';
		$attach = '订单支付';
		//判断是否为扫码支付
		if(empty($openid) AND $type=='native'){
			$trade_type = 'NATIVE';
			$notify_url = 'native.php';
			$attach = '商户充值';
			$this->config['is_sub'] = 0;
		}
		//判断是否为助手小程序充值
		if($type=='store'){
			$notify_url = 'recharges.php';
			$attach = '商户充值';
			$this->config['is_sub'] = 0;
		}
		//如果是会员充值
		if($type=='recharge'){
			$notify_url = 'recharge.php';
			$attach = '会员充值';
		}
		if(empty($body)){
			$body = $attach;
		}
        // 当前时间
        $time = time();
        // 生成随机字符串
        $nonceStr = md5($time . $openid);
		// API参数
		$params = [
			'attach' => $attach,
			'nonce_str' => $nonceStr,//随机字符串
			'body' => $body,//商品描述
			'out_trade_no' => $order_no,//商户订单号
			'total_fee' => $total_fee * 100, // 价格:单位分
			'spbill_create_ip' => \request()->ip(),//服务终端IP
			'notify_url' => web_url() .$notify_url,  // 异步通知地址
			'trade_type' => $trade_type,//交易类型	
		];
		
		if($this->config['is_sub'] == 1){
			//服务商统一下单
			$values = WebSetModel::getItem('wxpay');//获取支付服务商配置
			$this->config['apikey'] = $values['apikey'];//服务商商户的密钥
			$params['appid'] = $values['app_id'];//服务商商户的APPID
			$params['mch_id'] = $values['mchid'];//服务商商户号
			$params['sub_appid'] = $this->config['app_id'];//当前调起支付的小程序APPID
			$params['sub_mch_id'] = $this->config['mchid'];//服务商分配的子商户号
			$params['sub_openid'] = $openid;//下单用户标识
			
		}else{
			$params['appid'] = $this->config['app_id'];//小程序ID
			$params['mch_id'] = $this->config['mchid'];//商户号
			$params['openid'] = $openid;//下单用户标识
		}
		
        // 生成签名
        $params['sign'] = $this->makeSign($params);
        // 请求API
        $url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
        $result = $this->postXmlCurl($this->toXml($params), $url);
        $prepay = $this->fromXml($result);
        // 请求失败
        if ($prepay['return_code'] === 'FAIL') {
            throw new BaseException(['msg' => $prepay['return_msg'], 'code' => -10]);
        }
        if ($prepay['result_code'] === 'FAIL') {
            throw new BaseException(['msg' => $prepay['err_code_des'], 'code' => -10]);
        }
		//WEB扫码支付
		if(empty($openid) AND $type=='native'){
			return $prepay['code_url'];//返回收款二维码 
		}
        // 生成 nonce_str 供前端使用
        $paySign = $this->makePaySign($params['nonce_str'], $prepay['prepay_id'], $time);
		//小程序支付
		return [
            'prepay_id' => $prepay['prepay_id'],
            'nonceStr' => $nonceStr,
            'timeStamp' => (string)$time,
            'paySign' => $paySign
        ];
    }

    /**
     * 退款申请API
     */
    public function refund($refund_no,$transaction_id,$total_fee,$refund_fee,$refund_desc)
    {
        // 当前时间
        $time = time();
        // 生成随机字符串
        $nonceStr = md5($time);
        // API参数
        $params = [
            'nonce_str' => $nonceStr,//随机字符串
            'transaction_id' => $transaction_id,//微信支付订单号
            'out_refund_no' => $refund_no,//退款订单号
            'total_fee' => $total_fee * 100, // 订单总金额，价格:单位分
            'refund_fee' => $refund_fee * 100, // 退款总金额，价格:单位分
            'refund_desc' => $refund_desc,//退款原因
            'notify_url' => web_url() . 'refund.php'  // 异步通知地址
        ];
        if($this->config['is_sub'] == 1){
            //服务商统一下单
            $values = WebSetModel::getItem('wxpay');//获取支付服务商配置
            $this->config['apikey'] = $values['apikey'];//服务商商户的密钥
            $this->config['cert_pem'] = $values['cert_pem'];//证书
            $this->config['key_pem'] = $values['key_pem'];//证书密钥
            $params['appid'] = $values['app_id'];//服务商商户的APPID
            $params['mch_id'] = $values['mchid'];//服务商商户号
            $params['sub_appid'] = $this->config['app_id'];//当前调起支付的小程序APPID
            $params['sub_mch_id'] = $this->config['mchid'];//服务商分配的子商户号
            
        }else{
            $params['appid'] = $this->config['app_id'];//小程序ID
            $params['mch_id'] = $this->config['mchid'];//商户号
        }
        //判断证书是否存在
        if(empty($this->config['cert_pem']) OR empty($this->config['key_pem'])){
            return '未配置证书';
        }
        // 生成签名
        $params['sign'] = $this->makeSign($params);
        // 请求API
        $url = 'https://api.mch.weixin.qq.com/secapi/pay/refund';
        $result = $this->postXmlCurl($this->toXml($params), $url,true);
        $prepay = $this->fromXml($result);
        // 请求失败
        if ($prepay['return_code'] === 'FAIL') {
            return $prepay['return_msg'];
        }
        if ($prepay['result_code'] === 'FAIL') {
            return $prepay['err_code_des'];
        }
        return false;
    }

    /**
     * 企业转账到零钱API
     */
    public function transfers($open_id,$order_no,$price)
    {
        // 生成随机字符串
        $nonceStr = md5(time());
        // API参数
        $params = [
            'openid' => $open_id,//用户openid
            'partner_trade_no' => $order_no, // 商户订单号
            'amount' => $price * 100, // 转账金额
            'mch_appid' => $this->config['app_id'],//商户账号appid    
            'mchid' => $this->config['mchid'],//商户号
            'check_name' => 'NO_CHECK',  // 校验用户姓名 NO_CHECK：不校验真实姓名 FORCE_CHECK：强校验真实姓名
            'nonce_str' => $nonceStr,//随机字符串
            'desc' => '用户提现' // 商户订单号
        ];
        // 生成签名
        $params['sign'] = $this->makeSign($params);
        // 请求API
        $url = 'https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers';
        $result = $this->postXmlCurl($this->toXml($params), $url,true);
        $prepay = $this->fromXml($result);
        // 请求失败
        if ($prepay['return_code'] === 'FAIL') {
            return [
                'code' => 1,
                'msg' => $prepay['return_msg']
            ];
        }
        if ($prepay['result_code'] === 'FAIL') {
            return [
                'code' => 1,
                'msg' => $prepay['err_code_des']
            ];
        }
        return [
            'code' => 0,
            'msg' => $prepay
        ];
    }

    /**
     * 支付成功异步通知
     */
    public function notify($Model,$apikey='')
    {
		//接收微信服务器回调的数据流
        if (!$xml = file_get_contents('php://input')) {
            $this->returnCode(false, 'Not found DATA');
        }
        // 将服务器返回的XML数据转化为数组
        $data = $this->fromXml($xml);
        // 记录日志
        //$this->doLogs($xml);
       $this->doLogs($data);
        // 订单信息
        $order = $Model->payDetail($data['out_trade_no']);
        empty($order) && $this->returnCode(true, '订单不存在');
        // 小程序配置信息
		if(empty($apikey)){
			$setting = SettingModel::getItem('payment',$order['wxapp_id']);
			if($setting['wx']['is_sub'] == 1){
				$values = WebSetModel::getItem('wxpay');//获取支付服务商配置
				$apikey = $values['apikey'];//服务商商户的密钥
			}else{
				$apikey = $setting['wx']['apikey'];
			}
		}
		$this->config['apikey'] = $apikey;
        // 保存微信服务器返回的签名sign
        $dataSign = $data['sign'];
        // sign不参与签名算法
        unset($data['sign']);
        // 生成签名
        $sign = $this->makeSign($data);
        // 判断签名是否正确  判断支付状态
        if (($sign === $dataSign)
            && ($data['return_code'] == 'SUCCESS')
            && ($data['result_code'] == 'SUCCESS')) {
            // 更新订单状态
            $order->updatePayStatus($data['transaction_id']);
            // 发送短信通知
            //$this->sendSms($order['wxapp_id'], $order['order_no']);
            // 返回状态
            $this->returnCode(true, 'OK');
        }
        // 返回状态
        $this->returnCode(false, '签名失败');
    }

    /**
    * 退款成功异步通知
    */
    public function notifyRefund($Model)
    {
        //接收微信服务器回调的数据流
        if (!$xml = file_get_contents('php://input')) {
            $this->returnCode(false, 'Not found DATA');
        }
        // 将服务器返回的XML数据转化为数组
        $data = $this->fromXml($xml);
        if($data['return_code'] != 'SUCCESS'){
            return false;
        }
        if(isset($data['sub_appid'])){
            $values = WebSetModel::getItem('wxpay');//获取支付服务商配置
            $this->config['apikey'] = $values['apikey'];//服务商商户的密钥
        }else{
            $wxapp = WxappModel::getWxapp(['app_id' => $data['appid']]);// 小程序配置信息
            $value = SettingModel::getItem('payment',$wxapp['wxapp_id']);
            $this->config['apikey'] = $value['wx']['apikey'];// 设置支付秘钥
        }
        //解密数据
        $data = $this->refund_decrypt($data['req_info']);
        $data = $this->fromXml($data);
        //$this->doLogs($data);
        // 订单信息
        $order = $Model->refundDetail($data['out_refund_no']);
        empty($order) && $this->returnCode(false, '订单不存在');
        if ($data['refund_status'] == 'SUCCESS') {
            // 更新订单状态
            $order->updateRefundStatus($data['refund_id']);
            // 返回状态
            $this->returnCode(true, '退款成功');
        }
        // 返回状态
        $this->returnCode(false, '退款失败');
    }

    /**
     * 发送短信通知
     */
    private function sendSms($wxapp_id, $order_no)
    {
        // 短信配置信息
        $config = SettingModel::getItem('sms', $wxapp_id);
        $SmsDriver = new SmsDriver($config);
        return $SmsDriver->sendSms('order_pay', compact('order_no'));
    }

    /*
     * 退款通知解密
     */
    private function refund_decrypt($req_info) 
    {
        $key = strtolower(md5($this->config['apikey']));
        return openssl_decrypt($req_info, "AES-256-ECB", $key);
    }

    /**
     * 返回状态给微信服务器
     */
    private function returnCode($is_success = true, $msg = null)
    {
        $xml_post = $this->toXml([
            'return_code' => $is_success ? 'SUCCESS' : 'FAIL',
            'return_msg' => $is_success ? 'OK' : $msg,
        ]);
        die($xml_post);
    }

    /**
     * 写入日志记录
     */
    private function doLogs($values)
    {
        return write_log($values, __DIR__);
    }

    /**
     * 生成paySign
     */
    private function makePaySign($nonceStr, $prepay_id, $timeStamp)
    {
        $data = [
            'appId' => $this->config['app_id'],
            'nonceStr' => $nonceStr,
            'package' => 'prepay_id=' . $prepay_id,
            'signType' => 'MD5',
            'timeStamp' => $timeStamp,
        ];
        //签名步骤一：按字典序排序参数
        ksort($data);
        $string = $this->toUrlParams($data);
        //签名步骤二：在string后加入KEY
        $string = $string . '&key=' . $this->config['apikey'];
        //签名步骤三：MD5加密
        $string = md5($string);
        //签名步骤四：所有字符转为大写
        $result = strtoupper($string);
        return $result;
    }

    /**
     * 将xml转为array
     */
    private function fromXml($xml)
    {
        // 禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        return json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
    }

    /**
     * 以post方式提交xml到对应的接口url
     */
    private function postXmlCurl($xml, $url, $cert = false, $second = 30)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);// 设置超时时间
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);//https请求 不验证证书和host
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);//严格校验
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);// 要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, TRUE);// post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);// 是否返回请求头
        //判断是否使用证书
        if($cert){
            $path = './application/common/library/wechat/cert/';
            file_put_contents($path . 'apiclient_cert.pem',$this->config['cert_pem']);
            file_put_contents($path . 'apiclient_key.pem',$this->config['key_pem']);
            curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
            curl_setopt($ch,CURLOPT_SSLCERT,$path . 'apiclient_cert.pem');
            curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
            curl_setopt($ch,CURLOPT_SSLKEY,$path . 'apiclient_key.pem');
        }
        $data = curl_exec($ch);// 运行curl
        curl_close($ch);
        return $data;
    }

    /**
     * 生成签名
     */
    private function makeSign($values)
    {
        //签名步骤一：按字典序排序参数
        ksort($values);
        $string = $this->toUrlParams($values);
        //签名步骤二：在string后加入KEY
        $string = $string . '&key=' . $this->config['apikey'];
        //签名步骤三：MD5加密
        $string = md5($string);
        //签名步骤四：所有字符转为大写
        $result = strtoupper($string);
        return $result;
    }

    /**
     * 格式化参数格式化成url参数
     */
    private function toUrlParams($values)
    {
        $buff = '';
        foreach ($values as $k => $v) {
            if ($k != 'sign' && $v != '' && !is_array($v)) {
                $buff .= $k . '=' . $v . '&';
            }
        }
        return trim($buff, '&');
    }

    /**
     * 输出xml字符
     */
    private function toXml($values)
    {
        if (!is_array($values)
            || count($values) <= 0
        ) {
            return false;
        }

        $xml = "<xml>";
        foreach ($values as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";
        return $xml;
    }

}
