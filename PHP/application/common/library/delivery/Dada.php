<?php
namespace app\common\library\delivery;
use app\common\model\WebSet as WebSetModel;
use app\common\model\Order as OrderModel;

/**
 * 达达配送
*/
class Dada
{
	private $callback_url = 'index.php?s=/task/delivery/dada';	//回调URL（查看回调说明）
    private $dev_id = '';	//开发者app_key
    private $dev_key = '';	//发者app_secret
	private $api_url = '';	//api接口地址
	private $source_id = '';	//商户ID(测试号：73753)
	private $format = "json";	//数据格式
    private $v = "1.0";	//api版本
	private $order;	//订单详情
	private $businessParams = '';//请求数据（body内容）

    /**
     * 构造函数
    */
    public function __construct($order=[]){
		$values = WebSetModel::getItem('delivery');
		$this->dev_id = $values['dd']['app_key'];
        $this->dev_key = $values['dd']['app_secret'];
		$this->api_url = $values['dd']['api_url'];
		$this->order = $order;
    }
	
	/**
     * 预发布订单创建
	*/
	public function preOrder()
	{
		$order = $this->order;
		$user = $order['address'];//收货人信息
		$user_location = explode(',',$user['location']);//拆分收货人定位经纬度
		$shop = $order['shop'];//发货门店
		$shop_location = explode(',',$shop['coordinate']);//拆分门店定位经纬度
		$post_data = [
			'shop_no' => $shop['dada_shop_id'],//是 门店编号
			'origin_id' => $order['order_id'],//是 第三方订单ID
			'city_code' => $this->getCiytCode($shop['city']),//是 订单所在城市的code
			'cargo_price' => $order['pay_price'],//是 订单金额
			'is_prepay' => 0,//是否需要垫付 1:是 0:否 (垫付订单金额，非运费)
			'receiver_name' => $user['name'],//是 收货人姓名
			'receiver_address' => $user['detail'],//是 收货人地址
			'callback' => api_url() . $this->callback_url,//是 回调URL
			'receiver_lat' => $user_location[0], //否 收货人地址纬度
			'receiver_lng' => $user_location[1],//否 收货人地址经度
			'receiver_phone' => $user['phone'],//否 收货人手机号
			//'tips' => 0,//否 小费（单位：元，精确小数点后一位）
			'info' => $order['message'],//否 订单备注
			//'cargo_type' => 1,//否 订单商品类型：食品小吃-1,饮料-2,鲜花-3,文印票务-8,便利店-9,水果生鲜-13,同城电商-19, 医药-20,蛋糕-21,酒品-24,小商品市场-25,服装-26,汽修零配-27,数码-28,小龙虾-29,火锅-51,其他-5
			'cargo_weight' => 0.1,//是	订单重量（单位：Kg）
			//'origin_mark_no' => '河马云店',//否	订单来源编号
			//'is_use_insurance' => 0,//是否使用保价费（0：不使用保价，1：使用保价； 同时，请确保填写了订单金额（cargo_price））
		];
		$this->$businessParams = json_encode($post_data);
		$post_data = $this->bulidRequestParams();
		return $this->getHttpRequestWithPost($post_data,'/api/order/queryDeliverFee');	
	}

	/**
     * 查询运费后发单接口
	*/
	public function queryAddOrder($delivery_id='')
	{
		$post_data = [
			'deliveryNo' => $delivery_id//是 平台订单编号
		];
		$this->$businessParams = json_encode($post_data);
		$post_data = $this->bulidRequestParams();
		return $this->getHttpRequestWithPost($post_data,'/api/order/addAfterQuery');	
	}
	
	/**
     * 获取城市编码
     */
	private function getCiytCode($city)
	{
		$this->$businessParams = '';
		$post_data = $this->bulidRequestParams();
		$post_data['app_secret'] = $this->dev_key;
		$result = $this->getHttpRequestWithPost($post_data,'/api/cityCode/list');
		$code = '';
		for($n=0;$n<sizeof($result['result']);$n++){
			if($result['result'][$n]['cityName'] == $city){
				$code = $result['result'][$n]['cityCode'];
				break;
			}
		}
		return $code;
	}

    /**
     * 发送请求,POST
     * @param $url 指定URL完整路径地址
     * @param $data 请求的数据
     */
    private function getHttpRequestWithPost($data,$url){
		$data = json_encode($data,JSON_UNESCAPED_UNICODE);
        $url = $this->api_url . $url;
        // json
        $headers = array(
            'Content-Type: application/json',
        );
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_TIMEOUT, 3);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $resp = curl_exec($curl);
        var_dump(curl_error($curl));//如果在执行curl的过程中出现异常，可以打开此开关查看异常内容。
        $info = curl_getinfo($curl);
        curl_close($curl);
        return json_decode($resp, true);
        /*
        if (isset($info['http_code']) && $info['http_code'] == 200) {
            return json_decode($resp, true);
        }
        return '';
        */
    }
	
	/**
     * 构造请求数据
     * data:业务参数，json字符串
     */
    private function bulidRequestParams()
    {
        $requestParams['app_key'] = $this->dev_id;
        $requestParams['body'] = $this->businessParams;
        $requestParams['format'] = $this->format;
        $requestParams['source_id'] = $this->source_id;
        $requestParams['timestamp'] = time();
        $requestParams['v'] = $this->v;
        $requestParams['signature'] = $this->_sign($requestParams);
        return $requestParams;
    }

    /**
     * 签名生成signature
     */
    private function _sign($data){
		
        //1.升序排序
        ksort($data);

        //2.字符串拼接
        $args = "";
        foreach ($data as $key => $value) {
            $args.=$key.$value;
        }
        $args = $this->dev_key . $args . $this->dev_key;
        //3.MD5签名,转为大写
        $sign = strtoupper(md5($args));
        return $sign;
    }
	
	
}
