<?php
namespace app\common\library\delivery;
use app\common\model\WebSet as WebSetModel;
use app\common\model\Order as OrderModel;

/**
 * 顺丰配送
*/
class ShunFeng
{
	private $dev_id;	//开发者ID
    private $dev_key;	//发者app_secret
	private $api_url;	//api接口地址
	private $order;//订单数据private $order;//订单数据
	private $callback_url = 'index.php?s=/task/delivery/shunfeng';	//订单状态回调
	
    /**
     * 构造函数
    */
    public function __construct($order_id=''){
		$values = WebSetModel::getItem('delivery');
		$this->dev_id = (int)$values['sf']['app_key'];
        $this->dev_key = $values['sf']['app_secret'];
		$this->api_url = $values['sf']['api_url'];
		if(!empty($order_id)){
			$this->order = OrderModel::detail($order_id);
		}
    }
	
	/**
	 * 自动配送
	 */
	public function delivery()
	{
		$result = $this->addOrder();
		if($result['error_code']==0){
			//更新配送数据
			$delivery_price = $result['result']['total_price']/100;//实际需要支付金额
			$delivery_distance = $result['result']['delivery_distance_meter'];//配送距离（单位：米）
			$delivery_id = $result['result']['sf_order_id']; //配送单号
			$order = OrderModel::detail($this->order['order_id']);
			$order->save([
				'delivery_mode' => 1,
				'delivery_price' => $delivery_price,
				'delivery_distance' => $delivery_distance,
				'delivery_id' => $delivery_id,
				'delivery_status' => 20,
				'delivery_time' => time(),
				'transport_status' => '下单成功'
			]);
			return false;
		}
		return $result['error_msg'];
	}
	
	/*
	物品类型product_type枚举值：
	1:快餐，2:送药，3:百货，4:脏衣服收，5:干净衣服派，6:生鲜，7:保单，8:高端饮品，9:现场勘验，10:快递
	12:文件，13:蛋糕，14:鲜花，15:电子数码，16:服装鞋帽，17:汽车配件，18:珠宝，20:披萨，21:中餐，22:水产
	27:专人直送，32:中端饮品，32:便利店，32:面包糕点，32:火锅，32:证照，32:其他
	*/
	
	/**
     * 预发布订单创建
	*/
	public function preOrder($order=[])
	{
		if(!$order){
			$order = $this->order;
		}
		$user = $order['address'];//收货人信息
		$user_location = explode(',',$user['location']);//拆分收货人定位经纬度
		$shop = $order['shop'];//发货门店
		$shop_location = explode(',',$shop['coordinate']);//拆分门店定位经纬度
		$post_data = [
			'dev_id' => $this->dev_id,//开发者ID
			'shop_id' => $shop['sf_shop_id'],//$this->shop_id,//店铺ID
			'shop_type' => 1,//店铺ID类型	1:顺丰店铺ID 2:接入方店铺ID
			'user_lng' => $user_location[1],//用户地址经度
			'user_lat' => $user_location[0],//用户地址纬度
			'user_address' => $user['district'].','.$user['detail'],//用户详细地址
			'city_name' => $user['city'],//发单城市
			'weight' => 0,//物品重量（单位：克）
			'product_type' => 1, //物品类型，测试店铺请填写1，否则会造成【没有匹配的计价规则或计价规则已失效】
			'is_appoint' => 0,//是否是预约单0：非预约单；1：预约单
			'expect_time' => time()+1800,//期望送达时间,预约单需必传	秒级时间戳
			//'expect_pickup_time' => time()+600,//期望取货时间
			'lbs_type' => 2,//坐标类型，1：百度坐标，2：高德坐标
			'pay_type' => 1,//用户支付方式：1、已支付 0、货到付款
			'receive_user_money' => 0,//代收金额，单位：分
			'is_insured' => 0,//是否保价，0：非保价；1：保价
			'is_person_direct' => 0,//是否是专人直送订单，0：否；1：是
			'declared_value' => 0,//保价金额，单位：分
			'gratuity_fee' => 0,//订单小费，不传或者传0为不加小费	单位分，加小费最低不能少于100分
			'rider_pick_method' => 1,//物流流向	1：从门店取件送至用户 2：从用户取件送至门店
			'return_flag' => 511,//1:商品总价格，2:配送距离，4:物品重量，8:起送时间，16:期望送达时间，32:支付费用，64:实际支持金额，128:优惠卷总金额，256:结算方式,例如全部返回为填入511
			'push_time' => time(),
			//发货店铺信息；Obj,平台级开发者需要传入
			'shop' => [
				'shop_name' => $shop['shop_name'],//店铺名称
				'shop_phone' => $shop['phone'],//店铺电话
				'shop_address' => $shop['address'],//店铺地址
				'shop_lng' => $shop_location[1],//店铺经度
				'shop_lat' => $shop_location[0]//店铺纬度		
			]
		];
		return $this->post($post_data, "/open/api/external/precreateorder?sign=");
	}
	
	/**
     * 创建订单
	*/
	public function addOrder($order=[])
	{
		if(!$order){
			$order = $this->order;
		}
		$user = $order['address'];//收货人信息
		$user_location = explode(',',$user['location']);//拆分收货人定位经纬度
		$shop = $order['shop'];//发货门店
		$shop_location = explode(',',$shop['coordinate']);//拆分门店定位经纬度
		$time = time();
		$post_data = [
			'dev_id' => $this->dev_id,//开发者ID
			'shop_id' => $shop['sf_shop_id'],//$this->shop_id,//店铺ID
			'shop_type' => 1,//店铺ID类型 1：顺丰店铺ID ；2：接入方店铺ID
			'shop_order_id' => $order['order_no'],//商家订单号	不允许重复
			'order_source' => '河马云店',//订单接入来源
			'order_sequence' => '外卖'.$order['row_no'].'号',//取货序号
			'lbs_type' => 2,//坐标类型	1：百度坐标，2：高德坐标
			'pay_type' => 1,//用户支付方式	1：已付款 0：货到付款
			'receive_user_money' => 0,//代收金额,单位：分
			'order_time' => $order['pay_time'],//用户下单时间	秒级时间戳
			'is_appoint' => 0,//是否是预约单,0：非预约单；1：预约单
			'expect_time' => $time + 1800,//用户期望送达时间	预约单需必传,秒级时间戳
			'is_insured' => 0,//是否保价，0：非保价；1：保价
			'declared_value' => 0,//保价金额,单位：分
			'is_person_direct' => 0,//是否是专人直送订单，0：否；1：是
			'declared_value' => 0,//保价金额	单位：分
			'gratuity_fee' => 0,//订单小费，不传或者传0为不加小费	单位分，加小费最低不能少于100分
			'remark' => $order['message'],//订单备注
			'rider_pick_method' => 1,//物流流向	1：从门店取件送至用户,2：从用户取件送至门店
			'return_flag' => 511,	//返回字段控制标志位（二进制）	1:商品总价格，2:配送距离，4:物品重量，8:起送时间，16:期望送达时间，32:支付费用，64:实际支持金额，128:优惠卷总金额，256:结算方式,例如全部返回为填入511
			'push_time' => $time,
			'version' => 17,
			//发货店铺信息；Obj,平台级开发者需要传入
			'shop' => [
				'shop_name' => $shop['shop_name'],//店铺名称
				'shop_phone' => $shop['phone'],//店铺电话
				'shop_address' => $shop['address'],//店铺地址
				'shop_lng' => $shop_location[1],//店铺经度
				'shop_lat' => $shop_location[0]//店铺纬度		
			],
			'receive' => [
				'user_name' => $user['name'],//用户姓名
				'user_phone' => $user['phone'],//用户电话
				'user_lng' => $user_location[1],//用户地址经度
				'user_lat' => $user_location[0],//用户地址纬度
				'user_address' => $user['detail'], //用户地址
				'city_name' => $user['city'] //发单城市,用来校验是否跨城；请填写城市的中文名称，如北京市
			],
			'order_detail' => [
				'total_price' => $order['pay_price']*100,//用户订单总金额（单位：分）
				'product_type' => 1, //物品类型,测试店铺请填写1，否则会造成【没有匹配的计价规则或计价规则已失效】
				'user_money' => $order['pay_price']*100, //实收用户金额（单位：分）
				'shop_money' => $order['pay_price']*100,//实付商户金额（单位：分）
				'weight_gram' => 1,//物品重量（单位：克）
				'volume_litre' => 5,//物品体积（单位：升）
				'delivery_money' => 1,//商户收取的配送费（单位：分）
				'product_num' => sizeof($order['goods']),//物品个数
				'product_type_num' => sizeof($order['goods'])//物品种类个数
			]
		];
		$product_detail = [];//商品列表详情
		for($n=0;$n<sizeof($order['goods']);$n++){
			$product_detail[$n] = [
				'product_name' => $order['goods'][$n]['goods_name'],//物品名称
				'product_id' => $order['goods'][$n]['goods_id'],//物品ID
				'product_num' => $order['goods'][$n]['total_num'],//物品数量
				'product_price' => $order['goods'][$n]['goods_price'],//物品价格
				'product_unit' => '个',//物品单位
				'product_remark' => '',//备注
				'item_detail'=> ''//详情
			];
		}
		$post_data['order_detail']['post_data'] = $product_detail;//商品详情
		return $this->post($post_data, "/open/api/external/createorder?sign=");
	}

    /**
     * 发送请求,POST
     * @param $url 指定URL完整路径地址
     * @param $data 请求的数据
     */
    private function post($data,$url)
	{
		//$data = json_encode($data,JSON_UNESCAPED_UNICODE);
		$data = json_encode($data,JSON_UNESCAPED_UNICODE);
		$sign = $this->_sign($data);
        $url = $this->api_url . $url . $sign;
		// json
        $headers = array(
            'Content-Type: application/json',
        );
        $curl = curl_init($url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_TIMEOUT, 3);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $resp = curl_exec($curl);
        //var_dump( curl_error($curl) );//如果在执行curl的过程中出现异常，可以打开此开关查看异常内容。
        $info = curl_getinfo($curl);
        curl_close($curl);
        if (isset($info['http_code']) && $info['http_code'] == 200) {
            return json_decode($resp, true);
        }
        return false;
    }
    /**
     * 签名生成signature
     */
    private function _sign($data)
	{
		//$data = json_encode($data);
		$signChar = $data . '&'. $this->dev_id . '&' . $this->dev_key;
		$sign = base64_encode(MD5($signChar)); // 注：md5出来的结果是32位小写16进制字符串，$sign 的最终结果末尾包含等号=
		return $sign;
    }
	
}
