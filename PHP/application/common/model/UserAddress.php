<?php
namespace app\common\model;
use think\Request;

/**
 * 用户收货地址模型
 */
class UserAddress extends BaseModel
{
    protected $name = 'user_address';

    /**
     * 关联用户表
     */
    public function user()
    {
        return $this->hasOne('User','user_id','user_id');
    }

    /**
     * 关联平台用户表
     */
    public function store()
    {
        return $this->hasOne('StoreUser','store_user_id','store_user_id');
    }
    
    /**
     * 获取列表
     */
    public function getList($search)
    {
        // 筛选条件
        $filter = [];
        if(!empty($search)){  
            //是否是手机号
            if(isMobile($search)){
                $filter['phone'] = $search;
            }else{
                $filter['name'] = ['like', '%' . trim($search) . '%'];
            }
        }
        // 执行查询
        $list = $this->with(['user'])
            ->order('address_id','desc')
            ->where($filter)
            ->paginate(15, false, ['query' => Request::instance()->request()]);
        return $list;
    }

    /**
     * 平台获取列表
     */
    public function getOutList($search)
    {
        // 筛选条件
        $filter = [];
        $filter['store_user_id'] = ['>',0];
        if(!empty($search)){  
            //是否是手机号
            if(isMobile($search)){
                $filter['phone'] = $search;
            }else{
                $filter['name'] = ['like', '%' . trim($search) . '%'];
            }
        }
        // 执行查询
        $list = $this->useGlobalScope(false)
            ->with(['store'])
            ->order('address_id','desc')
            ->where($filter)
            ->paginate(15, false, ['query' => Request::instance()->request()]);
        return $list;
    }
}
