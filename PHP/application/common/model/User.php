<?php
namespace app\common\model;
use think\Request;

/**
 * 用户模型类
 */
class User extends BaseModel
{
    protected $name = 'user';
		
	/**
     * 显示性别
     */
    public function getGenderAttr($value)
    {
        $status = [0 => '未知', 1 => '男', 2 => '女'];
        return $status[$value];
    }
	
	/**
     * 推荐人
     */
    public function getRecommenderAttr($value)
    {
        if($value==0){
            return ['text' => '平台推荐', 'value' => $value];
        }else{
            return ['text' => 'ID:'.$value, 'value' => $value];
        }
    }
	
	/**
     * 关联会员等级
     */
    public function grade()
    {
        return $this->belongsTo('UserGrade');
    }

    /**
     * 关联收货地址表
     */
    public function address()
    {
        return $this->hasMany('UserAddress');
    }

    /**
     * 关联收货地址表 (默认地址)
     */
    public function addressDefault()
    {
        return $this->belongsTo('UserAddress', 'address_id');
    }

    /**
     * 获取用户列表
     */
    public function getList($user_grade_id = '', $gender = '', $search='')
    {
        // 筛选条件
        $filter = [];
		if(!empty($user_grade_id) AND $user_grade_id > -1){
			$filter['user_grade_id'] = $user_grade_id;
		}
		if(!empty($gender) AND $gender > -1){
			$filter['gender'] = $gender;
		}
		if(!empty($search)){
			//是否是数字
			if(is_numeric($search)){   
				//是否是手机号
				if(isMobile($search)){
					$filter['mobile'] = $search;
				}else{
					$filter['user_id'] = $search;
				}
			}else{
			   //不是数字   
				$filter['nickName'] = ['like', '%' . trim($search) . '%'];
			}
		}
        return $this->with(['grade','address', 'addressDefault'])
			->order(['user_id' => 'desc'])
			->where($filter)
			->paginate(15, false, ['query' => Request::instance()->request()]);
    }
	
	/**
     * API获取团队成员
     */
    public function getApiList($user_id)
    {
        $list = $this->order(['user_id' => 'desc'])
			->where(['recommender' => $user_id])
			->paginate(15, false, ['query' => Request::instance()->request()]);
		for($n=0;$n<sizeof($list);$n++){
			$list[$n]['all'] = $this->where(['recommender' => $list[$n]['user_id']])->count();
		}
		return $list;
    }
	
	/**
     * API统计二级团队成员人数
     */
    public function getApiAll($user_id)
    {
		$all = 0;
        $list = $this->where(['recommender' => $user_id])->select();
		for($n=0;$n<sizeof($list);$n++){
			$all = $all + $this->where(['recommender' => $list[$n]['user_id']])->count();
		}
		return $all;
    }

    /**
     * 获取用户信息
     */
    public static function detail($where)
    {
        return self::get($where, ['address', 'addressDefault','grade']);
    }

    /**
     * 获取用户信息
     */
    public static function getPhone($mobile)
    {
        return self::where(['mobile' => $mobile])->find();
    }
	
	/**
     * 获取用户信息
     */
    public static function getUser($where)
    {
        return self::useGlobalScope(false)->where($where)->find();
    }

    /**
     * 删除所有记录
     */
    public static function delALL($wxapp_id = null)
    {
        $self = new static();
        $where = [];
        is_null($wxapp_id) && $wxapp_id = $self::$wxapp_id;
        $wxapp_id > 0 && $where['wxapp_id'] = $wxapp_id;
        return $self->useGlobalScope(false)->where($where)->delete();
    }
	
	/**
     * 根据条件统计数量
     */
    public static function getCount()
    {
		$self = new static;
		 // 筛选条件
        $filter = [];
		$wxapp_id = $self::$wxapp_id;
		$wxapp_id > 0 && $filter['wxapp_id'] = $wxapp_id;
        $count = array();
		//全部
		$count[0] = self::useGlobalScope(false)->where($filter)->count();
		//今天
		$star = strtotime(date("Y-m-d"),time());
		$count[1] = self::useGlobalScope(false)->where($filter)->where('create_time','>',$star)->count();
		//昨天
		$star = strtotime("-1 day");
		$end = strtotime(date("Y-m-d"),time());
		$count[2] = self::useGlobalScope(false)->where($filter)->where('create_time','>',$star)->where('create_time','<',$end)->count();
		//前天
		$star = strtotime("-2 day");
		$end = strtotime("-1 day");
		$count[3] = self::useGlobalScope(false)->where($filter)->where('create_time','>',$star)->where('create_time','<',$end)->count();
		//-4天
		$star = strtotime("-3 day");
		$end = strtotime("-2 day");
		$count[4] = self::useGlobalScope(false)->where($filter)->where('create_time','>',$star)->where('create_time','<',$end)->count();
		//-5天
		$star = strtotime("-4 day");
		$end = strtotime("-3 day");
		$count[5] = self::useGlobalScope(false)->where($filter)->where('create_time','>',$star)->where('create_time','<',$end)->count();
		//-6天
		$star = strtotime("-5 day");
		$end = strtotime("-4 day");
		$count[6] = self::useGlobalScope(false)->where($filter)->where('create_time','>',$star)->where('create_time','<',$end)->count();
		//-7天
		$star = strtotime("-6 day");
		$end = strtotime("-5 day");
		$count[7] = self::useGlobalScope(false)->where($filter)->where('create_time','>',$star)->where('create_time','<',$end)->count();
		//本月起至时间 - 月度统计 
		$end = mktime(0,0,0,date('m'),1,date('y')); 
		$count[8] = self::useGlobalScope(false)->where($filter)->where('create_time','>',$end)->count();
		
		//上月开始  
		$star = mktime(0,0,0,date('m')-1,1,date('y')); 
		$count[9] = self::useGlobalScope(false)->where($filter)->where('create_time','>',$star)->where('create_time','<',$end)->count();
		
        return $count;
    }

}
