<?php
namespace app\common\model;
use think\Request;

/**
 * 预约模型
 */
class Pact extends BaseModel
{
    protected $name = 'pact';

	/**
     * 关联门店表
     */
    public function shop()
    {
        return $this->belongsTo('Shop');
    }
	
	/**
     * 关联桌位表
     */
    public function table()
    {
        return $this->belongsTo('Table');
    }
	
	/**
     * 状态
     */
    public function getStatusAttr($value)
    {
        $status = [10 => '预约中', 20 => '已过期'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取列表
     */
    public function getList($mode=10,$shop_id=0)
    {
		//筛选
		$where = [];
		$where['mode'] = $mode;
		$shop_id > 0 && $where['shop_id'] = $shop_id;
        // 排序规则
        $sort = [];
        $sort = ['pact_time' => 'asc'];//按照约定时间排序
        // 执行查询
        $list = $this->with(['shop','table'])
            ->where($where)
			->order($sort)
            ->paginate(15, false, ['query' => Request::instance()->request()]);
        return $list;
    }

    /**
     * 获取详情
     */
    public static function detail($where)
    {
        return self::get($where, ['shop','table']);
    }
	
	/**
     * 根据时间段统计数量
     */
    public static function getDateCount($shop)
    {
		$self = new static;
		 // 筛选条件
        $filter = [];
        $shop['shop_id'] > 0 && $filter['shop_id'] = $shop['shop_id'];
		$wxapp_id = $self::$wxapp_id;
		$wxapp_id > 0 && $filter['wxapp_id'] = $wxapp_id;
        $count = array();
		$count['sort'] = self::useGlobalScope(false)->where('mode',10)->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->count();
		$count['table'] = self::useGlobalScope(false)->where('mode',20)->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->count();
		return $count;
    }
	
	/**
     * 根据条件统计数量
     */
    public static function getCount($shop_id=0)
    {
		$self = new static;
		 // 筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
		$wxapp_id = $self::$wxapp_id;
		$wxapp_id > 0 && $filter['wxapp_id'] = $wxapp_id;
        $count = array();
		//全部
		$count[0]['sort'] = self::useGlobalScope(false)->where('mode',10)->where($filter)->count();
		$count[0]['table'] = self::useGlobalScope(false)->where('mode',20)->where($filter)->count();
		//今天
		$star = strtotime(date("Y-m-d"),time());
		$count[1]['sort'] = self::useGlobalScope(false)->where('mode',10)->where('create_time','>',$star)->where($filter)->count();
		$count[1]['table'] = self::useGlobalScope(false)->where('mode',20)->where('create_time','>',$star)->where($filter)->count();
		//昨天
		$star = strtotime("-1 day");
		$end = strtotime(date("Y-m-d"),time());
		$count[2]['sort'] = self::useGlobalScope(false)->where('mode',10)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[2]['table'] = self::useGlobalScope(false)->where('mode',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		//本月起至时间 - 月度统计 
		$end = mktime(0,0,0,date('m'),1,date('y')); 
		$count[8]['sort'] = self::useGlobalScope(false)->where('mode',10)->where('create_time','>',$end)->where($filter)->count();
		$count[8]['table'] = self::useGlobalScope(false)->where('mode',20)->where('create_time','>',$end)->where($filter)->count();
		//上月开始  
		$star = mktime(0,0,0,date('m')-1,1,date('y'));
		$count[9]['sort'] = self::useGlobalScope(false)->where('mode',10)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[9]['table'] = self::useGlobalScope(false)->where('mode',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();		
        return $count;
    }
}
