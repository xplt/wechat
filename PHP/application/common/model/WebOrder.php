<?php
namespace app\common\model;
use app\common\library\wechat\WxPay;
use think\Request;
use think\Db;

/**
 * 站点订单模型
 */
class WebOrder extends BaseModel
{
    protected $name = 'web_order';

    /**
     * 关联用户信息
     */
    public function store()
    {
        return $this->hasOne('StoreUser','store_user_id','store_user_id');
    }
	
	/**
     * 支付状态
     */
    public function getPayStatusAttr($value)
    {
        $status = [10 => '失败', 20 => '成功', 30 => '待处理'];
        return ['text' => $status[$value], 'value' => $value];
    }
	
	/**
     * 订单类型
     */
    public function getOrderTypeAttr($value)
    {
        $status = [10 => '充值', 20 => '扣费', 30 => '分红', 40 => '提现'];
        return ['text' => $status[$value], 'value' => $value];
    }
	
	/**
     * 获取列表
     */
    public function getList($agent_id='',$store_user_id='',$order_type=0)
    {
		$where = [];
		!empty($agent_id) && $where['agent_id'] = $agent_id;
		!empty($store_user_id) && $where['store_user_id'] = $store_user_id;
		$order_type > 0 && $where['order_type'] = $order_type;
		//删除超过两个小时未支付的订单
		$this->useGlobalScope(false)
			->where('pay_status' ,'=',10)
			->where('create_time' ,'<=',time()-7200)
			->delete();
         // 执行查询
        $list = $this->useGlobalScope(false)
			->where($where)
			->order('web_order_id','desc')
            ->paginate(15, false, ['query' => Request::instance()->request()]);
        return $list;
    }

	/**
     * 详情
     */
    public static function detail($web_order_id)
    {
        return self::useGlobalScope(false)->where('web_order_id',$web_order_id)->find();
    }

    /**
     * 新增订单
     */
    public function add($data)
    {
    	$data['order_no'] = orderNo();
    	$data['pay_time'] = time();
    	 // 开启事务
        Db::startTrans();
        try {
	    	//提现操作
	    	if(isset($data['order_type']) AND $data['order_type'] == 40){
	    		$data['pay_status'] = 30;
	    		StoreUser::where('store_user_id', $data['store_user_id'])->setDec('wallet', $data['pay_price']);
	    		//发送余额状态模板消息
	    	}
	        // 记录订单信息
	        $this->allowField(true)->save($data);
	     	Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }

    /**
     * 确认用户提现
     */
    public function cash()
    {
    	//微信转账处理
        $values = WebSet::getItem('payment');
        $config = $values['wx'];
        $cash_fee = 0; //手续费
        //验证是否收取手续费
        if($config['cash_fee'] > 0){
        	if($config['cash_mode'] == 'fixed'){
        		$cash_fee = round($config['cash_fee'],2); //固定数值收取 四舍五入保留两位小数
        	}else{
        		$cash_fee = $this->pay_price * $config['cash_fee'] / 1000; //比例提取数值
        		$cash_fee = round($cash_fee,2); //四舍五入保留两位小数
        	}
        }
        $wxapp = WebSet::getItem('wxapp');
        $config['app_id'] = $wxapp['app_id'];
		$WxPay = new WxPay($config);
		$result = $WxPay->transfers($this->store['wxapp_open_id'],$this->order_no,$this->pay_price - $cash_fee);
		if($result['code'] == 1){
			$this->error = $result['msg'];
			return false;
		}
		return $this->save([
			'transaction_id' => $result['payment_no'],
			'pay_status' => 20,
			'pay_time' => time()
		]) !== false;
    }
	
	/**
     * 根据条件统计数量
     */
    public static function getCount($agent_id=0)
    {
		$self = new static;
        $count = array();
		//全部统计
		$count['all']['all'] = self::useGlobalScope(false)->where(['pay_status' => 20])->count();//全部订单数量
		$count['all']['recharge'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 10])->count();//全部充值数量
		$count['all']['deduct'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 20])->count();//全部扣费数量
		$count['all']['bonus'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 30])->count();//全部分红数量
		$count['all']['take'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 40])->count();//全部提现数量
		$count['agent']['all'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20])->count();//全部订单数量
		$count['agent']['recharge'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 10])->count();//全部充值数量
		$count['agent']['deduct'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 20])->count();//全部扣费数量
		$count['agent']['bonus'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 30])->count();//全部分红数量
		$count['agent']['take'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 40])->count();//全部提现数量
		$count['money']['agent_take'] = self::useGlobalScope(false)->where(['affair_id' => $agent_id,'agent_id' => $agent_id,'pay_status' => 20,'order_type' => 40])->sum('pay_price');//全部提现数量
		$count['money']['store_take'] = self::useGlobalScope(false)->where(['affair_id' => ['<>',$agent_id],'agent_id' => $agent_id,'pay_status' => 20,'order_type' => 40])->sum('pay_price');//全部提现数量
		
		//今天统计
		$star = strtotime(date('Y-m-d 00:00:00',time()));
		$count['today']['all'] = self::useGlobalScope(false)->where(['pay_status' => 20,'create_time' => ['>',$star]])->count();//全部订单数量
		$count['today']['recharge'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 10,'create_time' => ['>',$star]])->count();//全部充值数量
		$count['today']['deduct'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 20,'create_time' => ['>',$star]])->count();//全部扣费数量
		$count['today']['bonus'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 30,'create_time' => ['>',$star]])->count();//全部分红数量
		$count['today']['take'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 40,'create_time' => ['>',$star]])->count();//全部提现数量
		$count['today']['agent_all'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'create_time' => ['>',$star]])->count();//全部订单数量
		$count['today']['agent_recharge'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 10,'create_time' => ['>',$star]])->count();//全部充值数量
		$count['today']['agent_deduct'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 20,'create_time' => ['>',$star]])->count();//全部扣费数量
		$count['today']['agent_bonus'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 30,'create_time' => ['>',$star]])->count();//全部分红数量
		$count['today']['agent_take'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 40,'create_time' => ['>',$star]])->count();//全部提现数量
		$count['today']['money']['all'] = self::useGlobalScope(false)->where(['pay_status' => 20,'create_time' => ['>',$star]])->sum('pay_price');//全部订单数量
		$count['today']['money']['recharge'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 10,'create_time' => ['>',$star]])->sum('pay_price');//全部充值数量
		$count['today']['money']['deduct'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 20,'create_time' => ['>',$star]])->sum('pay_price');//全部扣费数量
		$count['today']['money']['bonus'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 30,'create_time' => ['>',$star]])->sum('pay_price');//全部分红数量
		$count['today']['money']['take'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 40,'create_time' => ['>',$star]])->sum('pay_price');//全部提现数量
		$count['today']['money']['agent_all'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'create_time' => ['>',$star]])->sum('pay_price');//全部订单数量
		$count['today']['money']['agent_recharge'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 10,'create_time' => ['>',$star]])->sum('pay_price');//全部充值数量
		$count['today']['money']['agent_deduct'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 20,'create_time' => ['>',$star]])->sum('pay_price');//全部扣费数量
		$count['today']['money']['agent_bonus'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 30,'create_time' => ['>',$star]])->sum('pay_price');//全部分红数量
		$count['today']['money']['agent_take'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 40,'create_time' => ['>',$star]])->sum('pay_price');//全部提现数量
		//昨天统计
		$star = strtotime("-1 day");
		$end = strtotime(date('Y-m-d 00:00:00',time()));
		$count['today2']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20])->count();//全部订单数量
		$count['today2']['recharge'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 10])->count();//全部充值数量
		$count['today2']['deduct'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 20])->count();//全部扣费数量
		$count['today2']['bonus'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 30])->count();//全部分红数量
		$count['today2']['take'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 40])->count();//全部提现数量
		$count['today2']['agent_all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20])->count();//全部订单数量
		$count['today2']['agent_recharge'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 10])->count();//全部充值数量
		$count['today2']['agent_deduct'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 20])->count();//全部扣费数量
		$count['today2']['agent_bonus'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 30])->count();//全部分红数量
		$count['today2']['agent_take'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 40])->count();//全部提现数量
		$count['today2']['money']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20])->sum('pay_price');//全部订单数量
		$count['today2']['money']['recharge'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 10])->sum('pay_price');//全部充值数量
		$count['today2']['money']['deduct'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 20])->sum('pay_price');//全部扣费数量
		$count['today2']['money']['bonus'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 30])->sum('pay_price');//全部分红数量
		$count['today2']['money']['take'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 40])->sum('pay_price');//全部提现数量
		$count['today2']['money']['agent_all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20])->sum('pay_price');//全部订单数量
		$count['today2']['money']['agent_recharge'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 10])->sum('pay_price');//全部充值数量
		$count['today2']['money']['agent_deduct'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 20])->sum('pay_price');//全部扣费数量
		$count['today2']['money']['agent_bonus'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 30])->sum('pay_price');//全部分红数量
		$count['today2']['money']['agent_take'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 40])->sum('pay_price');//全部提现数量
		//前天统计
		$star = strtotime("-2 day");
		$end = strtotime("-1 day");
		$count['today3']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20])->count();//全部数量
		$count['today3']['agent_all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'agent_id' => $agent_id])->count();//所属代理数量
		//-4天统计
		$star = strtotime("-3 day");
		$end = strtotime("-2 day");
		$count['today4']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20])->count();//全部数量
		$count['today4']['agent_all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'agent_id' => $agent_id])->count();//所属代理数量
		//-5天统计
		$star = strtotime("-4 day");
		$end = strtotime("-3 day");
		$count['today5']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20])->count();//全部数量
		$count['today5']['agent_all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'agent_id' => $agent_id])->count();//所属代理数量
		//-6天统计
		$star = strtotime("-5 day");
		$end = strtotime("-4 day");
		$count['today6']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20])->count();//全部数量
		$count['today6']['agent_all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'agent_id' => $agent_id])->count();//所属代理数量
		//-7天统计
		$star = strtotime("-6 day");
		$end = strtotime("-5 day");
		$count['today7']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20])->count();//全部数量
		$count['today7']['agent_all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'agent_id' => $agent_id])->count();//所属代理数量
		//本月统计 
		$end = mktime(0,0,0,date('m'),1,date('y')); 
		$count['month']['all'] = self::useGlobalScope(false)->where(['pay_status' => 20,'create_time' => ['>',$end]])->count();//全部订单数量
		$count['month']['recharge'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 10,'create_time' => ['>',$end]])->count();//全部充值数量
		$count['month']['deduct'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 20,'create_time' => ['>',$end]])->count();//全部扣费数量
		$count['month']['bonus'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 30,'create_time' => ['>',$end]])->count();//全部分红数量
		$count['month']['take'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 40,'create_time' => ['>',$end]])->count();//全部提现数量
		$count['month']['agent_all'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'create_time' => ['>',$end]])->count();//全部订单数量
		$count['month']['agent_recharge'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 10,'create_time' => ['>',$end]])->count();//全部充值数量
		$count['month']['agent_deduct'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 20,'create_time' => ['>',$end]])->count();//全部扣费数量
		$count['month']['agent_bonus'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 30,'create_time' => ['>',$end]])->count();//全部分红数量
		$count['month']['agent_take'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 40,'create_time' => ['>',$end]])->count();//全部提现数量
		$count['month']['money']['all'] = self::useGlobalScope(false)->where(['pay_status' => 20,'create_time' => ['>',$end]])->sum('pay_price');//全部订单数量
		$count['month']['money']['recharge'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 10,'create_time' => ['>',$end]])->sum('pay_price');//全部充值数量
		$count['month']['money']['deduct'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 20,'create_time' => ['>',$end]])->sum('pay_price');//全部扣费数量
		$count['month']['money']['bonus'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 30,'create_time' => ['>',$end]])->sum('pay_price');//全部分红数量
		$count['month']['money']['take'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_type' => 40,'create_time' => ['>',$end]])->sum('pay_price');//全部提现数量
		$count['month']['money']['agent_all'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'create_time' => ['>',$end]])->sum('pay_price');//全部订单数量
		$count['month']['money']['agent_recharge'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 10,'create_time' => ['>',$end]])->sum('pay_price');//全部充值数量
		$count['month']['money']['agent_deduct'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 20,'create_time' => ['>',$end]])->sum('pay_price');//全部扣费数量
		$count['month']['money']['agent_bonus'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 30,'create_time' => ['>',$end]])->sum('pay_price');//全部分红数量
		$count['month']['money']['agent_take'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 40,'create_time' => ['>',$end]])->sum('pay_price');//全部提现数量
		//上月统计  
		$star = mktime(0,0,0,date('m')-1,1,date('y'));
		$count['month2']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20])->count();//全部订单数量
		$count['month2']['recharge'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 10])->count();//全部充值数量
		$count['month2']['deduct'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 20])->count();//全部扣费数量
		$count['month2']['bonus'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 30])->count();//全部分红数量
		$count['month2']['take'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 40])->count();//全部提现数量
		$count['month2']['agent_all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20])->count();//全部订单数量
		$count['month2']['agent_recharge'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 10])->count();//全部充值数量
		$count['month2']['agent_deduct'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 20])->count();//全部扣费数量
		$count['month2']['agent_bonus'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 30])->count();//全部分红数量
		$count['month2']['agent_take'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 40])->count();//全部提现数量
		$count['month2']['money']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20])->sum('pay_price');//全部订单数量
		$count['month2']['money']['recharge'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 10])->sum('pay_price');//全部充值数量
		$count['month2']['money']['deduct'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 20])->sum('pay_price');//全部扣费数量
		$count['month2']['money']['bonus'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 30])->sum('pay_price');//全部分红数量
		$count['month2']['money']['take'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['pay_status' => 20,'order_type' => 40])->sum('pay_price');//全部提现数量
		$count['month2']['money']['agent_all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20])->sum('pay_price');//全部订单数量
		$count['month2']['money']['agent_recharge'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 10])->sum('pay_price');//全部充值数量
		$count['month2']['money']['agent_deduct'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 20])->sum('pay_price');//全部扣费数量
		$count['month2']['money']['agent_bonus'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 30])->sum('pay_price');//全部分红数量
		$count['month2']['money']['agent_take'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id,'pay_status' => 20,'order_type' => 40])->sum('pay_price');//全部提现数量
		return $count;
    }
}
