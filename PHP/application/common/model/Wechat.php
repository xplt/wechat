<?php
namespace app\common\model;
use think\Db;

/**
 * 公众号模型
 */
class Wechat extends BaseModel
{
    protected $name = 'wechat';
	
    /**
     * 获取详情
     */
    public static function detail()
    {
        $wxapp_id = self::$wxapp_id;
        // 筛选条件
        $filter = [];
        $wxapp_id<1 && $filter['wxapp_id'] = 0;
        return self::where($filter)->find();
    }
    /**
     * 获取详情
     */
    public static function getWechat($where=[])
    {
        return self::useGlobalScope(false)->where($where)->find();
    }

    /**
     * 批量删除小程序数据
     */
    public function delAll($wxapp_id=0)
    {
        $filter['wxapp_id'] = $wxapp_id;
        // 开启事务
        Db::startTrans();
        try {
            //拼团商品
            //GoodsGroup::useGlobalScope(false)->where($filter)->delete();
            $this->where($filter)->delete();
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }
}
