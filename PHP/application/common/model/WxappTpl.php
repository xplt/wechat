<?php
namespace app\common\model;
use app\common\model\Template;
use think\Request;
/**
 * 商户发布的模板
 */
class WxappTpl extends BaseModel
{
    protected $name = 'wxapp_tpl';
	
	/**
     * 设置小程序模板信息
     */
    public function getTemplateAttr($value)
    {
		if($value==0){
			return ['template_id' => $value];
		}
		return Template::detail($value);
         
    }
	
	/**
     * 获取列表
     */
    public function getList()
    {
        //删除超过一个月的审核记录
        $this->where(['create_time' => ['<',time()-86400*30]])->delete();
        // 执行查询
        $list = $this->order('wxapp_tpl_id','desc')
            ->paginate(15, false, [
                'query' => Request::instance()->request()
            ]);
        return $list;
    }
	
    /**
     * 获取详情
     */
    public static function detail($wxapp_tpl_id)
    {
        return self::get($wxapp_tpl_id);
    }
	
	/**
     * 获取最新
     */
    public static function getNew($wxapp_id = null)
    {
        $self = new static();
		$where = [];
		is_null($wxapp_id) && $wxapp_id = $self::$wxapp_id;
        $wxapp_id > 0 && $where['wxapp_id'] = $wxapp_id;
        return $self->useGlobalScope(false)->where($where)->order('wxapp_tpl_id','desc')->find();
    }

    /**
     * 删除所有记录
     */
    public static function delALL($wxapp_id = null)
    {
        $self = new static();
        $where = [];
        is_null($wxapp_id) && $wxapp_id = $self::$wxapp_id;
        $wxapp_id > 0 && $where['wxapp_id'] = $wxapp_id;
        return $self->useGlobalScope(false)->where($where)->delete();
    }

}
