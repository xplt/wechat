<?php
namespace app\common\model;
use app\common\library\calling\HeMa;

/**
 * 云叫号模型
 */
class Calling extends BaseModel
{
    protected $name = 'calling';

    /**
     * 关联门店
     */
    public function shop()
    {
        return $this->belongsTo('Shop');
    }

    /**
     * 开关
     */
    public function getIsOpenAttr($value,$data)
    {
        $call = new HeMa();
        $device = $call->getStatus($data['device_name']);
        $status = [0 => '关闭', 1 => '开启'];
        return ['text' => $status[$value], 'value' => $value, 'status' => $device['text']];
    }

    /**
     * 音量
     */
    public function getVolumeAttr($value)
    {
        $status = [0 => '静', 5 => '低', 10 => '中', 15 => '高'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 新订单
     */
    public function getIsNewAttr($value)
    {
        $status = [0 => '关闭', 1 => '开启'];
        return ['text' => $status[$value], 'value' => $value];
    }
    /**
     * 收款
     */
    public function getIsPayAttr($value)
    {
        $status = [0 => '关闭', 1 => '开启'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取列表
     */
    public function getList($shop_id='',$is_open=-1)
    {
		// 筛选条件
        $filter = [];
        !empty($shop_id) && $filter['shop_id'] = $shop_id;
        $is_open >= 0 && $filter['is_open'] = $is_open;
        return $this->with(['shop'])->where($filter)->order('calling_id','desc')->select();
    }
	
    /**
     * 获取详情
     */
    public static function detail($calling_id)
    {
        return self::get($calling_id,['shop']);
    }

    /**
     * 获取详情
     */
    public static function getDevice($shop_id)
    {
        return self::where('shop_id',$shop_id)->find();
    }

	/**
	 * 播报语音
	 */
	public function push($mode,$device_name,$row_no='')
	{
		$call = new HeMa();
        if($msg = $call->push($mode,$device_name,$row_no)){
            $this->error = $msg;
            return false;
        }
        return true;
	}
}
