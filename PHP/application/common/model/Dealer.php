<?php
namespace app\common\model;
use think\Request;

/**
 * 分销商进出帐模型
 */
class Dealer extends BaseModel
{
    protected $name = 'dealer';
	
	/**
     * 关联用户表
     */
    public function user()
    {
        return $this->belongsTo('User');
    }
	
	/**
     * 关联订单表
     */
    public function order()
    {
        return $this->belongsTo('Order');
    }
	
	/**
     * 记录类型
     */
    public function getModeAttr($value)
    {
        $status = [10 => '进账', 20 => '提现'];
        return ['text' => $status[$value], 'value' => $value];
    }
	
    /**
     * 提现状态
     */
    public function getTakeStatusAttr($value)
    {
        $status = [10 => '审核中', 20 => '已打款'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取列表
     */
    public function getList($mode=0)
    {
		// 筛选条件
        $filter = [];
        $mode > 0 && $filter['mode'] = $mode;
        // 排序规则
        $sort = [];
        $sort = ['dealer_id' => 'desc'];
        // 执行查询
        $list = $this->with(['user','order'])
            ->where($filter)
			->order($sort)
            ->paginate(15, false, ['query' => Request::instance()->request()]);
        return $list;
    }

    /**
     * 获取详情
     */
    public static function detail($where)
    {
        return self::get($where, ['user','order']);
    }
}
