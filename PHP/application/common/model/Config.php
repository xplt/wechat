<?php
namespace app\common\model;

/**
 * 站点配置模型
 */
class Config extends BaseModel
{
    protected $name = 'config';
	protected $append = ['status'];
	
	/**
     * 通讯状态
     */
    public function getStatusAttr($value, $data)
    {
        $status = ['失败','正常'];
		if(!empty($data['component_verify_ticket']) AND !empty($data['component_access_token']) AND $data['expires_in']>time()){
			$value = 1;
		}else{
			$value = 0;
		}
        return ['text' => $status[$value], 'value' => $value];
    }
	
	/**
     * 初始化二维码
     */
    public function getWechatQrcodeAttr($value)
    {
        if(!empty($value)){
			return $value;
		}
        return '/assets/images/no_pic.jpg';
    }

    /**
     * 获取配置信息
     */
    public static function detail()
    {
        return self::useGlobalScope(false)->find();
    }
}
