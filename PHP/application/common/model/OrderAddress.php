<?php
namespace app\common\model;

/**
 * 订单收货地址模型
 */
class OrderAddress extends BaseModel
{
    protected $name = 'order_address';
    protected $updateTime = false;
}
