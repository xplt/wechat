<?php
namespace app\common\model;
use think\Request;

/**
 * 平台用户模型
 */
class StoreUser extends BaseModel
{
    protected $name = 'store_user';
	protected $append = ['wxapp','details'];

	/**
     * 关联收货地址表
     */
    public function address()
    {
        return $this->hasMany('UserAddress');
    }

    /**
     * 关联收货地址表 (默认地址)
     */
    public function addressDefault()
    {
        return $this->belongsTo('UserAddress', 'address_id');
    }
	
	/**
     * 小程序数量
     */
    public function getWxappAttr($value, $data)
    {
        return Wxapp::useGlobalScope(false)->where([
			'store_user_id' => $data['store_user_id']
		])->count();
    }
    /**
     * 用户详情
     */
    public function getDetailsAttr($value, $data)
    {
        return StoreDetail::detail($data['store_user_id']);
    }
	/**
     * 显示性别
     */
    public function getGenderAttr($value)
    {
        $status = [0 => '未知', 1 => '男', 2 => '女'];
        return $status[$value];
    }
	
	/**
     * 推荐人
     */
    public function getRecommenderAttr($value)
    {
        if($value==0){
            return ['text' => '平台推荐', 'value' => $value];
        }else{
            return ['text' => 'ID:'.$value, 'value' => $value];
        }
    }
	
	/**
     * 用户身份
     */
    public function getIsAgentAttr($value)
    {
        $status = ['用户','代理','管理'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 用户头像
     */
    public function getAvatarUrlAttr($value)
    {
        empty($value) && $value = web_url() . 'assets/images/head.jpg';
        return $value;
    }
	
	/**
     * 获取列表
	 */
    public function getList($dataType='all',$agent_id=0)
    {
		$where = [];
		$dataType == 'agent' && $where['is_agent'] = 1;
        $dataType == 'store' && $where['user_name'] = ['<>',''];
		$agent_id > 0 && $where['agent_id'] = $agent_id;
        // 执行查询
        return $this->useGlobalScope(false)
        	->with(['address', 'addressDefault'])
			->where($where)
            ->order('store_user_id','desc')
            ->paginate(15, false, ['query' => Request::instance()->request()]);
    }

    /**
     * 平台获取列表
	 */
    public function getOutList()
    {
		$where = [];
        // 执行查询
        return $this->useGlobalScope(false)
        	->with(['address', 'addressDefault'])
			->where($where)
			->where('out_open_id','<>', '')
            ->order('store_user_id','desc')
            ->paginate(15, false, ['query' => Request::instance()->request()]);
    }

	/**
     * 获取详情信息
     */
    public static function detail($store_user_id)
    {
        return self::useGlobalScope(false)->with(['address', 'addressDefault'])->where(['store_user_id' => $store_user_id])->find();
    }
	
	/**
     * 获取详情信息 - 跟据条件
    */
    public static function getStore($where)
    {
        return self::useGlobalScope(false)->where($where)->find();
    }
	
	/**
     * 根据条件统计数量
     */
    public static function getCount($agent_id=0)
    {
		$self = new static;
        $count = array();
		//全部统计
		$count['all']['all'] = self::useGlobalScope(false)->where(['is_agent' => ['<',2]])->count();//全部数量
		$count['all']['agent'] = self::useGlobalScope(false)->where(['is_agent' => 1])->count();//全部代理数量
		$count['all']['store'] = self::useGlobalScope(false)->where(['is_agent' => 0])->count();//全部商户
		//余额统计
		$count['money']['all'] = self::useGlobalScope(false)->where(['is_agent' => ['<',2]])->sum('wallet');//全部用户余额
		$count['money']['web'] = self::useGlobalScope(false)->where(['is_agent' => 2])->sum('wallet');//站点余额
		$count['money']['store'] = self::useGlobalScope(false)->where(['is_agent' => 0])->sum('wallet');//商家余额
		$count['money']['agent'] = self::useGlobalScope(false)->where(['is_agent' => 1])->sum('wallet');//代理余额
		//代理数据
		$count['agent']['all'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id])->count();//代理用户数量
		$count['agent']['money'] = self::useGlobalScope(false)->where(['store_user_id' => $agent_id])->sum('wallet');//代理余额
		$count['agent']['money_store'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id])->sum('wallet');//代理商户余额
		//今天统计
		$star = strtotime(date('Y-m-d 00:00:00',time()));
		$count['today']['all'] = self::useGlobalScope(false)->where(['create_time' => ['>',$star]])->count();//全部数量
		$count['today']['agent'] = self::useGlobalScope(false)->where(['agent_id' => $agent_id,'create_time' => ['>',$star]])->count();//所属代理数量
		//昨天统计
		$star = strtotime("-1 day");
		$end = strtotime(date('Y-m-d 00:00:00',time()));
		$count['today2']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->count();//全部数量
		$count['today2']['agent'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
		//前天统计
		$star = strtotime("-2 day");
		$end = strtotime("-1 day");
		$count['today3']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->count();//全部数量
		$count['today3']['agent'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
		//-4天统计
		$star = strtotime("-3 day");
		$end = strtotime("-2 day");
		$count['today4']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->count();//全部数量
		$count['today4']['agent'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
		//-5天统计
		$star = strtotime("-4 day");
		$end = strtotime("-3 day");
		$count['today5']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->count();//全部数量
		$count['today5']['agent'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
		//-6天统计
		$star = strtotime("-5 day");
		$end = strtotime("-4 day");
		$count['today6']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->count();//全部数量
		$count['today6']['agent'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
		//-7天统计
		$star = strtotime("-6 day");
		$end = strtotime("-5 day");
		$count['today7']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->count();//全部数量
		$count['today7']['agent'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
		//本月统计 
		$end = mktime(0,0,0,date('m'),1,date('y')); 
		$count['month']['all'] = self::useGlobalScope(false)->where('create_time','>',$end)->count();//全部数量
		$count['month']['agent'] = self::useGlobalScope(false)->where('create_time','>',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
		//上月统计  
		$star = mktime(0,0,0,date('m')-1,1,date('y'));
		$count['month2']['all'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->count();//全部数量
		$count['month2']['agent'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
		return $count;
    }
}
