<?php
namespace app\common\model;
use app\common\library\wechat\WxPay;
use app\common\library\delivery\ShunFeng;
use app\common\library\delivery\Dada;
use app\common\library\delivery\Uu;
use think\Request;
use think\Hook;
use think\Cache;
use think\Db;

/**
 * 订单模型
 */
class Order extends BaseModel
{
    protected $name = 'order';
    protected $append = ['user','source'];

    /**
     * 用户资料
     */
    public function getUserAttr($value, $data)
    {
        if($data['user_id']>0){
            $user = User::detail($data['user_id']);
        }else{
             $user = StoreUser::detail($data['store_user_id']);
             if($data['order_mode'] == 20){
                $user['open_id'] = $user['out_open_id'];
             }
             if($data['order_mode'] == 10){
                $user['open_id'] = $user['wxapp_open_id'];
             }
        }
        return $user;
    }

    /**
     * 订单来源
     */
    public function getSourceAttr($value, $data)
    {
        $status = [10 => '自助点单', 20 => '平台点单', 30 => '代客点单'];
        if($data['store_user_id']>0 AND $data['order_mode']==10){
            $value = 30;
        }elseif($data['store_user_id']>0 AND $data['order_mode'] > 10){
            $value = 20;
        }else{
            $value = 10;
        }
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 订单模型初始化
     */
    public static function init()
    {
        parent::init();
        // 监听订单处理事件
        $static = new static;
        Hook::listen('order', $static);
    }

    /**
     * 订单商品列表
     */
    public function goods()
    {
        return $this->hasMany('OrderGoods');
    }

    /**
     * 关联订单收货地址表
     */
    public function address()
    {
        return $this->hasOne('OrderAddress');
    }

    /**
     * 关联订单配送表
     */
    public function delivery()
    {
        return $this->hasOne('OrderDelivery');
    }

    /**
     * 关联评论表
     */
    public function comment()
    {
        return $this->hasOne('Comment');
    }
	
	/**
     * 关联餐桌/包间表
     */
    public function table()
    {
        return $this->belongsTo('Table');
    }
	
	/**
     * 关联门店表
     */
    public function shop()
    {
        return $this->belongsTo('Shop');
    }

	/**
     * 格式化到店时间
     */
    public function getArriveTimeAttr($value)
    {
        return ['text' => date("Y-m-d H:i:s",$value), 'value' => $value];
    }
	/**
     * 订单类型
     */
    public function getOrderModeAttr($value)
    {
        $status = [10 => '堂食', 20 => '外卖', 30 => '自取'];
        return ['text' => $status[$value], 'value' => $value];
    }
    /**
     * 付款状态
     */
    public function getPayStatusAttr($value)
    {
        $status = [10 => '待付款', 20 => '已付款', 30 => '后付款'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 商家状态
     */
    public function getShopStatusAttr($value)
    {
        $status = [10 => '待接单', 20 => '已接单', 30 => '已接单'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 配送状态
     */
	public function getDeliveryStatusAttr($value)
    {
        $status = [10 => '待配送', 20 => '配送中', 30 => '已配送'];
        return ['text' => $status[$value], 'value' => $value];
    }
	

    /**
     * 收货状态
     */	
	public function getReceiptStatusAttr($value)
    {
        $status = [10 => '待收货', 20 => '已收货'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 退款状态
     */ 
    public function getRefundStatusAttr($value)
    {
        $status = [10 => '待退款', 20 => '已退款'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 订单状态
     */
    public function getOrderStatusAttr($value)
    {
        $status = [10 => '进行中', 20 => '被取消', 30 => '已完成', 40 => '退款'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 是否评价
     */
    public function getIsCmtAttr($value)
    {
        $status = ['待评价', '已评价'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 按照分类获取单列表
     * $type 类别 all=所有 payment=待付款 comment=待评价 refund=退款/售后
     */
	public function getList($dataType='all',$shop_id='',$user_id='',$search='')
    {
        // 筛选条件
        $filter = [];
		!empty($shop_id) && $filter['shop_id'] = $shop_id; 
        !empty($user_id) && $filter['user_id'] = $user_id; 
        switch ((string)$dataType) {
            case 'all'; //全部
                break;
            case 'payment'; //待付款
                $filter['pay_status'] = ['<>',20];
                $filter['order_status'] = 10;
                break;
            case 'collection'; //待收款
                $filter['pay_status'] = 30;
                $filter['delivery_status'] = ['>',10];
                $filter['receipt_status'] = 10;
                $filter['order_status'] = 10;
                break;
            case 'shop';//待接单
                $filter['order_status'] = 10;
                $filter['pay_status'] = ['>',10];
                $filter['shop_status'] = 10;
                break;
            case 'delivery';//待配送
                $filter['order_status'] = 10;
                $filter['shop_status'] = ['>',10];
                $filter['delivery_status'] = ['<',30];
                break;
            case 'receipt';//待收货
                $filter['order_status'] = 10;
                $filter['delivery_status'] = 30;
                $filter['receipt_status'] = 10;
                break;
            case 'cancel';//被取消
                $filter['order_status'] = 20;
                break;
            case 'complete';//已完成
                $filter['order_status'] = 30;
                break;
            case 'comment';//待评价
                $filter['is_cmt'] = 0;
                $filter['order_status'] = 30;
                break;
            case 'refund';//全部退款
                $filter['order_status'] = 40;
                break;
            case 'refund10';//待退款
                $filter['refund_status'] = 10;
                $filter['order_status'] = 40;
                break;
            case 'refund20';//已退款
                $filter['refund_status'] = 20;
                $filter['order_status'] = 40;
                break;
        }
        return $this->with(['goods.image','shop','table','comment','delivery'])
            ->where($filter)
            ->order(['create_time' => 'desc'])
            ->paginate(15, false, [
                'query' => Request::instance()->request()
            ]); 
    }

    /**
     * 平台获取订单列表
     * @param  [type] $dataType 订单类型 all=所有 payment=待付款 comment=待评价 refund=退款/售后
     * @param  [type] $store_user_id 用户编号
     */
    public function getOutList($dataType='all',$shop_id='',$store_user_id='',$search='' )
    {
        // 筛选条件
        $filter = [];
        if(!empty($store_user_id)){
            $filter['store_user_id'] = $store_user_id;
        }else{
            $filter['store_user_id'] = ['>',0];
        }         
        // 订单数据类型
        switch ((string)$dataType) {
            case 'all'; //全部
                break;
            case 'payment'; //待付款
                $filter['pay_status'] = ['<>',20];
                $filter['order_status'] = 10;
                break;
            case 'collection'; //待收款
                $filter['pay_status'] = 30;
                $filter['delivery_status'] = ['>',10];
                $filter['receipt_status'] = 10;
                $filter['order_status'] = 10;
                break;
            case 'shop';//待接单
                $filter['order_status'] = 10;
                $filter['pay_status'] = ['>',10];
                $filter['shop_status'] = 10;
                break;
            case 'delivery';//待配送
                $filter['order_status'] = 10;
                $filter['shop_status'] = ['>',10];
                $filter['delivery_status'] = ['<',30];
                break;
            case 'receipt';//待收货
                $filter['order_status'] = 10;
                $filter['delivery_status'] = 30;
                $filter['receipt_status'] = 10;
                break;
            case 'cancel';//被取消
                $filter['order_status'] = 20;
                break;
            case 'complete';//已完成
                $filter['order_status'] = 30;
                break;
            case 'comment';//待评价
                $filter['is_cmt'] = 0;
                $filter['order_status'] = 30;
                break;
            case 'refund';//全部退款
                $filter['order_status'] = 40;
                break;
            case 'refund10';//待退款
                $filter['refund_status'] = 10;
                $filter['order_status'] = 40;
                break;
            case 'refund20';//已退款
                $filter['refund_status'] = 20;
                $filter['order_status'] = 40;
                break;
        }
        return $this->useGlobalScope(false)
            ->with(['goods.image','shop','table','comment','delivery'])
            ->where($filter)
            ->order(['create_time' => 'desc'])
            ->paginate(15, false, [
                'query' => Request::instance()->request()
            ]); 
    }

    /**
     * 订单详情
     */
    public static function detail($order_id)
    {
		return self::useGlobalScope(false)
			->with(['goods' => ['image', 'spec', 'goods'], 'address', 'shop', 'table','delivery'])
			->where(['order_id' => $order_id])
			->find();
    }

    /**
     * 订单详情 
     */
    public static function getOrderWhere($where=[])
    {
        return self::useGlobalScope(false)->where($where)->find();
    }
	
	/**
     * 订单详情 - 回调
     */
    public static function getOrder($order_id)
    {
		return self::useGlobalScope(false)->with(['delivery'])->where(['order_id' => $order_id])->find();
    }
	
	/**
     * 根据第三方配送单号 - 配送回调
     */
    public static function getDetail($delivery_id)
    {
		return self::useGlobalScope(false)->where(['delivery_id' => $delivery_id])->find();
    }
	
	/**
     * 退款处理
     */
    public function refund($is_refund)
    {
    	if($this->refund_price > $this->pay_price){
			$this->error = '退款金额不可大于实付金额';
            return false;
    	}
		//同意退款
		if($is_refund==1){
			// 开启事务
	        Db::startTrans();
	        try {
				//判断支付方式
				if(empty($this->transaction_id)){
					//余额变动
					$user = User::detail($this->user_id);
					$user->wallet = ['inc',$this->refund_price];//返回余额
					if($this->refund_price > $user->pay){
						$pay = $user->pay;
					}else{
						$pay = $this->refund_price;
					}
					$user->pay = ['dec', $pay];//扣减消费金额
					if($this->refund_price * 100 > $user->score){
						$score = $user->score;
					}else{
						$score = $this->refund_price * 100;
					}
					$user->score = ['dec', $score];//扣减积分
					$user->save();
					//增加流水记录
					//更新商品库存、销量
                    $this->save([
                        'refund_status' => 20,
                        'refund_time' => time()
                    ]);
                    //发送退款申请状态提醒订阅消息
                    post_tpl('refund',$this->order_id);
				}else{
					//微信退款处理
                    $values = SettingModel::getItem('payment');
                    $config = $values['wx'];
                    $wxapp = Wxapp::getWxappCache();
                    $config['app_id'] = $wxapp['app_id'];
					$WxPay = new WxPay($config);
					$result = $WxPay->refund($this->order_no,$this->transaction_id,$this->pay_price,$this->refund_price,$this->refund_desc);
					if($result){
						$this->error = $result;
						return false;
					}
				}
				Db::commit();
	            return true;
	        } catch (\Exception $e) {
	            Db::rollback();
	        }
	        return false;

		}
        //拒绝退款
		// 开启事务
        Db::startTrans();
        try {
			$goods = $this->goods;
			$this->save([
				'order_status' => 10,
				'refund_status' => 10,
				'refund_price' => 0
			]);
			$new = [];
			for($n=0;$n<sizeof($goods);$n++){
				$new[$n]['order_goods_id'] = $goods[$n]['order_goods_id'];
				$new[$n]['refund_num'] = 0;
				$new[$n]['refund_price'] = 0;
			}
			$model = new OrderGoods;
			$model->saveAll($new);
			//发送退款申请状态提醒订阅消息
			post_tpl('refund',$this->order_id);
			Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }

    /**
     * 确认收到用户付款（后付费用户）
     */
    public function collection()
    {
        return $this->save([
            'pay_status' => 20,
            'pay_time' => time()
        ]) !== false;
    }

    /**
     * 设置配送状态
     */
    public function setDeliveryStatus($data=[])
    {
        $delivery = [
            'delivery_status' => $data['delivery_status'],
            'delivery_time' => time()
        ];
        if($data['delivery_status'] == 20){
            if($data['shop_clerk_id'] == ''){
                $this->error = '请选择骑手';
                return false; 
            }
            $clerk = ShopClerk::detail($data['shop_clerk_id']);
            post_tpl('horseman',$this->order_id);//骑手接单提醒
            $delivery['linkman'] = $clerk['real_name'];
            $delivery['phone'] = $clerk['mobile'];
            if(!isset($this->delivery['linkman'])){
                $delivery['order_no'] = $this->order_no;
                $delivery['shop_id'] = $this->shop_id;
                $delivery['wxapp_id'] = $this->wxapp_id;
                $this->save([
                    'shop_status' => 30,
                    'shop_time' => time(),
                    'delivery_status' => 20
                ]);
                return $this->delivery()->save($delivery);
            }
        }
        if($data['delivery_status'] == 40){
            post_tpl('delivery',$this->order_id);//骑手配送订阅消息
        }
        if($data['delivery_status'] == 50){
            $delivery['status'] = 30;
        }
        return $this->delivery->save($delivery);
    }

    /**
     * 确认发货
     */
    public function setDelivery($company='',$delivery_no='')
    {
        //外卖配送
        if($this->order_mode['value'] == 20){
            if($this->delivery_status['value']==10){
                $data = [
                    'shop_status' => 30,
                    'shop_time' => time(),
                    'delivery_status' => 20
                ];
                if($company == 10){
                    if($this->shop['is_grab']['value'] == 0){
                        $dev = [
                            'linkman' => $this->shop['linkman'],
                            'phone' => $this->shop['phone'],
                            'delivery_status' => 30  //已到店
                        ];
                        //骑手接单提醒
                        post_tpl('horseman',$this->order_id);
                    }else{
                        //抢单通知
                        sand_grab_msg($this);
                    }
                }
                if($company == 20){
                    //$sf = new ShunFeng($order_id);
                    //$sf->delivery();
                    //return true;
                    $dev = [
                        'linkman' => $this->shop['linkman'],
                        'phone' => $this->shop['phone'],
                        'delivery_status' => 30  //已到店
                    ];
                }
                if($company == 30){
                    //$dd = new Dada($order_id);
                    //$dd->delivery();
                    $dev = [
                        'linkman' => $this->shop['linkman'],
                        'phone' => $this->shop['phone'],
                        'delivery_status' => 30  //已到店
                    ];
                }
                if($company == 40){
                    $uu = new Uu($this);
                    $pre = $uu->preOrder();
                    if($pre['return_code']=='ok'){
                        $wxapp = Wxapp::where('wxapp_id',$this->wxapp_id)->find();
                        $user = StoreUser::get($wxapp['store_user_id']);
                        //判断商家余额
                        if($user['wallet'] < $pre['need_paymoney']){
                            //发送余额不足模板消息************************
                            $this->error = '账户余额不足';
                            return false; 
                        }
                        $result = $uu->addOrder($pre);
                        if($result['return_code']=='ok'){
                            $dev = [
                                'price' => $pre['need_paymoney'], //配送费
                                'distance' => $pre['distance']  //配送距离
                            ];
                        }else{
                            $this->error = $result['return_msg'];
                            return false; 
                        }
                    }else{
                        $this->error = $result['return_msg'];
                        return false; 
                    } 
                }
                $dev['order_no'] = $this->order_no;
                $dev['company'] = $company;
                $dev['delivery_time'] = time();
                $dev['shop_id'] = $this->shop_id;
                $dev['wxapp_id'] = $this->wxapp_id;
                $this->delivery()->save($dev);
            }else{
                //外卖配送完毕
                $this->delivery->save([
                    'delivery_status' => 50,
                    'delivery_time' => time(),
                    'status' => 30
                ]);
                $data = [
                    'delivery_status' => 30
                ];
            }
        }else{
            //判断堂食排号点餐
            if($this->order_mode['value']==10 AND !empty($this->row_no)){
                if($device = Calling::getDevice($this->shop_id)){
                    if($device['is_open']['value']==1){
                        $device->push('rows',$device['device_name'],$this->row_no);//叫号
                    }
                }
            }
            //非外卖配送
            $data = [
                'delivery_status' => 30
            ];
            if($this->order_mode['value'] == 10 AND $this->row_no == ''){
                //发送模板消息，订单完成通知
                post_tpl('finish',$this->order_id);
            }else{
                //发送模板消息，取餐提醒通知
                post_tpl('take',$this->order_id);
            }   
        }
        $data['delivery_time'] = time();
        return $this->save($data);
    }
	
	/**
     * 商家接单
     */
    public function setShopStatus()
    {
        //发送商家接单订阅消息
        post_tpl('receive',$this->order_id);
        return $this->save([
			'shop_status' => 20,
            'shop_time' => time()
        ]);
    }

	/**
     * 门店现金统计
     */
    public static function getCashCount($shop_id,$star,$end)
    {
    	$self = new static;
		 // 筛选条件
        $filter = [];
        $filter['shop_id'] = $shop_id;
		$wxapp_id = $self::$wxapp_id;
		$wxapp_id > 0 && $filter['wxapp_id'] = $wxapp_id;
		$filter['pay_status'] = ['=',20];
        $count = array();
		$count['pay_price'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where('order_status','<>',20)->where($filter)->sum('pay_price');
		$count['refund_price'] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where('order_status',40)->where('refund_status',30)->where($filter)->sum('refund_price');
		$count['reach_price'] = $count['pay_price'] - $count['refund_price'];
        $count['reach_price'] = $count['reach_price']-round($count['reach_price']*0.006,2);
		return $count;
    }

	/**
     * 根据时间段统计数量
     */
    public static function getDateCount($shop)
    {
		$self = new static;
		 // 筛选条件
        $filter = [];
        $shop['shop_id'] > 0 && $filter['shop_id'] = $shop['shop_id'];
		$wxapp_id = $self::$wxapp_id;
		$wxapp_id > 0 && $filter['wxapp_id'] = $wxapp_id;
		$filter['pay_status'] = ['=',20];
        $count = array();
		$count['count'] = self::useGlobalScope(false)->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->count();
		$count['pay_price'] = self::useGlobalScope(false)->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->sum('pay_price');
		$count['total_price'] = self::useGlobalScope(false)->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->sum('total_price');
		$count['activity_price'] = self::useGlobalScope(false)->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->sum('activity_price');
		$count['express_price'] = self::useGlobalScope(false)->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->sum('express_price');
		$count['pack_price'] = self::useGlobalScope(false)->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->sum('pack_price');
		$count['ware_price'] = self::useGlobalScope(false)->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->sum('ware_price');
		$count['tang'] = self::useGlobalScope(false)->where(['order_mode'=>10])->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->count();
		$count['qu'] = self::useGlobalScope(false)->where(['order_mode' =>30])->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->count();
		$count['wai'] = self::useGlobalScope(false)->where(['order_mode' =>20])->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->count();
		$count['refund'] = self::useGlobalScope(false)->where(['order_status' => 40])->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->count();
		$count['refund_price'] = self::useGlobalScope(false)->where(['order_status'=>40,'refund_status'=>30])->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->sum('refund_price');
		return $count;
    }

     /**
     * 获取订单分类统计
     */
	public function getOrderClassCount($dataType='all',$shop_id='',$user_id='',$store_user_id='')
    {
        // 筛选条件
        $filter = [];
        !empty($shop_id) && $filter['shop_id'] = $shop_id;
		!empty($user_id) && $filter['user_id'] = $user_id;
        !empty($store_user_id) && $filter['store_user_id'] = $store_user_id;
        // 订单数据类型
        switch ((string)$dataType) {
            case 'all'; //全部
                break;
            case 'payment'; //待付款
                $filter['pay_status'] = ['<>',20];
                $filter['order_status'] = 10;
                break;
            case 'collection'; //待收款
                $filter['pay_status'] = 30;
                $filter['delivery_status'] = ['>',10];
                $filter['receipt_status'] = 10;
                $filter['order_status'] = 10;
                break;
            case 'shop';//待接单
                $filter['order_status'] = 10;
                $filter['pay_status'] = ['>',10];
                $filter['shop_status'] = 10;
                break;
            case 'delivery';//待配送
                $filter['order_status'] = 10;
                $filter['shop_status'] = ['>',10];
                $filter['delivery_status'] = 10;
                break;
            case 'receipt';//待收货
                $filter['order_status'] = 10;
                $filter['delivery_status'] = 30;
                $filter['receipt_status'] = 10;
                break;
            case 'cancel';//被取消
                $filter['order_status'] = 20;
                break;
            case 'complete';//已完成
                $filter['order_status'] = 30;
                break;
            case 'comment';//待评价
                $filter['is_cmt'] = 0;
                $filter['order_status'] = 30;
                break;
            case 'refund';//全部退款
                $filter['order_status'] = 40;
                break;
            case 'refund10';//待退款
                $filter['refund_status'] = 10;
                $filter['order_status'] = 40;
                break;
            case 'refund20';//已退款
                $filter['refund_status'] = 20;
                $filter['order_status'] = 40;
                break;
        }
        return $this->where($filter)->count();
    }

	/**
     * 根据条件统计数量
     */
    public static function getCount($shop_id=0)
    {
		$self = new static;
		 // 筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
		$wxapp_id = $self::$wxapp_id;
		$wxapp_id > 0 && $filter['wxapp_id'] = $wxapp_id;
        $count = array();
		//全部
		$count[0]['count'] = self::useGlobalScope(false)->where('pay_status','=',20)->where($filter)->count();
		//待退款
		$count[0]['refund'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_status' => 40,'refund_status'=>10])->where($filter)->count();
		$count[0]['refund_price'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_status' => 40,'refund_status'=>10])->where($filter)->sum('refund_price');
		//今天
		$star = strtotime(date('Y-m-d 00:00:00',time()));
		$count[1]['count'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where($filter)->count();
		$count[1]['pay_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where($filter)->sum('pay_price');
		$count[1]['total_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where($filter)->sum('total_price');
		$count[1]['activity_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where($filter)->sum('activity_price');
		$count[1]['express_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where($filter)->sum('express_price');
		$count[1]['pack_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where($filter)->sum('pack_price');
		$count[1]['ware_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where($filter)->sum('ware_price');
		$count[1]['tang'] = self::useGlobalScope(false)->where(['pay_status'=>20,'order_mode'=>10])->where('create_time','>',$star)->where($filter)->count();
		$count[1]['qu'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_mode' =>30])->where('create_time','>',$star)->where($filter)->count();
		$count[1]['wai'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_mode' =>20])->where('create_time','>',$star)->where($filter)->count();
		$count[1]['refund'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_status' => 40])->where('create_time','>',$star)->where($filter)->count();
		$count[1]['refund_price'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_status' => 40,'refund_status'=>30])->where('create_time','>',$star)->where($filter)->sum('refund_price');
		//昨天
		$star = strtotime("-1 day");
		$end = strtotime(date('Y-m-d 00:00:00',time()));
		$count[2]['count'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[2]['pay_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('pay_price');
		$count[2]['total_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('total_price');
		$count[2]['activity_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('activity_price');
		$count[2]['express_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('express_price');
		$count[2]['pack_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('pack_price');
		$count[2]['ware_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('ware_price');
		$count[2]['tang'] = self::useGlobalScope(false)->where(['pay_status'=>20,'order_mode'=>10])->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[2]['qu'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_mode' =>30])->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[2]['wai'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_mode' =>20])->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[2]['refund'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_status' => 40])->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[2]['refund_price'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_status' => 40,'refund_status'=>30])->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('refund_price');
		//前天
		$star = strtotime("-2 day");
		$end = strtotime("-1 day");
		$count[3]['count'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[3]['pay_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('pay_price');
		//-4天
		$star = strtotime("-3 day");
		$end = strtotime("-2 day");
		$count[4]['count'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[4]['pay_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('pay_price');
		//-5天
		$star = strtotime("-4 day");
		$end = strtotime("-3 day");
		$count[5]['count'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[5]['pay_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('pay_price');
		//-6天
		$star = strtotime("-5 day");
		$end = strtotime("-4 day");
		$count[6]['count'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[6]['pay_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('pay_price');
		//-7天
		$star = strtotime("-6 day");
		$end = strtotime("-5 day");
		$count[7]['count'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[7]['pay_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('pay_price');
		//本月起至时间 - 月度统计 
		$end = mktime(0,0,0,date('m'),1,date('y')); 
		$count[8]['count'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$end)->where($filter)->count();
		$count[8]['pay_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$end)->where($filter)->sum('pay_price');
		$count[8]['total_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$end)->where($filter)->sum('total_price');
		$count[8]['activity_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$end)->where($filter)->sum('activity_price');
		$count[8]['express_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$end)->where($filter)->sum('express_price');
		$count[8]['pack_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$end)->where($filter)->sum('pack_price');
		$count[8]['ware_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$end)->where($filter)->sum('ware_price');
		$count[8]['tang'] = self::useGlobalScope(false)->where(['pay_status'=>20,'order_mode'=>10])->where('create_time','>',$end)->where($filter)->count();
		$count[8]['qu'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_mode' =>30])->where('create_time','>',$end)->where($filter)->count();
		$count[8]['wai'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_mode' =>20])->where('create_time','>',$end)->where($filter)->count();
		$count[8]['refund'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_status' => 40])->where('create_time','>',$end)->where($filter)->count();
		$count[8]['refund_price'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_status' => 40,'refund_status'=>30])->where('create_time','>',$end)->where($filter)->sum('refund_price');
		//上月开始  
		$star = mktime(0,0,0,date('m')-1,1,date('y')); 
		$count[9]['count'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[9]['pay_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('pay_price');
        $count[9]['total_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('total_price');
		$count[9]['activity_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('activity_price');
		$count[9]['express_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('express_price');
		$count[9]['pack_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('pack_price');
		$count[9]['ware_price'] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('ware_price');
		$count[9]['tang'] = self::useGlobalScope(false)->where(['pay_status'=>20,'order_mode'=>10])->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[9]['qu'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_mode' =>30])->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[9]['wai'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_mode' =>20])->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[9]['refund'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_status' => 40])->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
		$count[9]['refund_price'] = self::useGlobalScope(false)->where(['pay_status' => 20,'order_status' => 40,'refund_status'=>30])->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('refund_price');
		return $count;
    }

}
