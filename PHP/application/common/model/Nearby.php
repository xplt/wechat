<?php
namespace app\common\model;
/**
 * 附近小程序模型
 */
class Nearby extends BaseModel
{
    protected $name = 'nearby';
	
	/**
     * 状态
     */
    public function getStatusAttr($value)
    {
        $status = [1 => '审核中', 2 => '审核失败', 3 => '审核通过'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取列表
     */
    public function getList()
    {
        return $this->order('nearby_id','desc')->select();
    }
	
	/**
     * 根据条件获取详情 - 回调接口
     */
    public static function getDetail($whrer)
    {
       return self::useGlobalScope(false)->where($whrer)->find();
    }
}
