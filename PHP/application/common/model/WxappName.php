<?php
namespace app\common\model;

/**
 * 微信小程序名称模型
 */
class WxappName extends BaseModel
{
    protected $name = 'wxapp_name';
	
	/**
     * 状态
     */
    public function getStatusAttr($value)
    {
        $status = ['审核中','设置成功','被驳回'];
        return ['text' => $status[$value], 'value' => $value];
    }
	
	/**
     * 详情
     */
	public static function detail($where=[],$wxapp_id = null)
    {
		$self = new static;
		is_null($wxapp_id) && $wxapp_id = $self::$wxapp_id;
        $wxapp_id > 0 && $where['wxapp_id'] = $wxapp_id;
        return $self->useGlobalScope(false)->where($where)->find();
    }
}
