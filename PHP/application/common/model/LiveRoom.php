<?php
namespace app\common\model;
use think\Request;

/**
 * 小程序直播间模型
 */
class LiveRoom extends BaseModel
{
    protected $name = 'live_room';


    /**
     * 关联门店表
     */
    public function shop()
    {
        return $this->belongsTo('Shop');
    }
	
	/**
     * 获取器: 转义数组格式
     */
    public function getGoodsAttr($value)
    {
        return json_decode($value, true);
    }

    /**
     * 修改器: 转义成json格式
     */
    public function setGoodsAttr($value)
    {
        return json_encode($value);
    }
	
	/**
     * 开播时间
     */
    public function getStartTimeAttr($value)
    {
        return [
			'value' => $value,
			'text' => date('Y-m-d'.' H:i:s',$value)
		];
    }
	
	/**
     * 直播间状态
     */
    public function getLiveStatusAttr($value)
    {
        $status = [0 => '待同步', 101 => '直播中',102 => '未开始',103 => '已结束',104 => '禁播',105 => '暂停中',106 => '异常',107 => '已过期'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 直播类型
     */
    public function getTypeAttr($value)
    {
        $status = ['手机直播','推流设备直播'];
        return ['text' => $status[$value], 'value' => $value];
    }
	
	/**
     * 画面尺寸
     */
    public function getScreenTypeAttr($value)
    {
        $status = ['竖屏','横屏'];
        return ['text' => $status[$value], 'value' => $value];
    }
	
	/**
     * 点赞
     */
    public function getCloseLikeAttr($value)
    {
        $status = ['开启','关闭'];
        return ['text' => $status[$value], 'value' => $value];
    }
	
	/**
     * 货架
     */
    public function getCloseGoodsAttr($value)
    {
        $status = ['开启','关闭'];
        return ['text' => $status[$value], 'value' => $value];
    }
	
	/**
     * 评论
     */
    public function getCloseCommentAttr($value)
    {
        $status = ['开启','关闭'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取列表
     */
    public function getList()
    {
        // 筛选条件
        //$filter = [];
        //$shop_id > 0 && $filter['shop_id'] = $shop_id;

        // 排序规则
        $sort = ['sort', 'live_room_id' => 'desc'];
		
        // 执行查询
        return $this->order($sort)
			->paginate(15, false, [
                'query' => Request::instance()->request()
            ]);
    }

    /**
     * 获取详情
     */
    public static function detail($live_room_id)
    {
        return self::get($live_room_id, ['shop']);
    }
	
	/**
     * 获取房间
     */
    public static function getRoom($roomid)
    {
        return self::where(['roomid' =>$roomid])->with(['shop'])->find();
    }

}
