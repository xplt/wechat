<?php
namespace app\common\model;
use think\Request;

/**
 * 小程序插件模型
 */
class WebWxappPlugins extends BaseModel
{
    protected $name = 'web_wxapp_plugins';

    /**
     * 获取列表
     */
    public function getList()
    {
        // 执行查询
        $list = $this->useGlobalScope(false)
			->order(['web_wxapp_plugins_id'=>'desc'])
            ->paginate(15, false, [
                'query' => Request::instance()->request()
            ]);
        return $list;
    }
    /**
     * 获取详情
     */
    public static function detail($web_wxapp_plugins_id)
    {
        return self::useGlobalScope(false)->where(['web_wxapp_plugins_id'=>$web_wxapp_plugins_id])->find();
    }
	
	/**
     * 获取详情
     */
    public static function getPlugins($where)
    {
        return self::useGlobalScope(false)->where($where)->find();
    }
}
