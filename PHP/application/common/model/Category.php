<?php
namespace app\common\model;
use think\Cache;

/**
 * 商品分类模型
 */
class Category extends BaseModel
{
    protected $name = 'category';

    /**
     * 分类图片
     */
    public function image()
    {
        return $this->hasOne('uploadFile', 'file_id', 'image_id');
    }

    /**
     * 所有分类
     */
    public static function getALL($shop_id,$wxapp_id)
    {
        $model = new static;
        empty($wxapp_id) && $wxapp_id = $model::$wxapp_id;
        if (!Cache::get('category_' . $wxapp_id . '_' . $shop_id)) {
            $data = $model->with(['image'])->order(['sort' => 'asc'])->where(['shop_id' => $shop_id])->select();
            $all = !empty($data) ? $data->toArray() : [];
            $tree = [];
            foreach ($all as $first) {
                if ($first['parent_id'] != 0) continue;
                $twoTree = [];
                foreach ($all as $two) {
                    if ($two['parent_id'] != $first['category_id']) continue;
                    $threeTree = [];
                    foreach ($all as $three)
                        $three['parent_id'] == $two['category_id']
                        && $threeTree[$three['category_id']] = $three;
                    !empty($threeTree) && $two['child'] = $threeTree;
                    $twoTree[$two['category_id']] = $two;
                }
                if (!empty($twoTree)) {
                    array_multisort(array_column($twoTree, 'sort'), SORT_ASC, $twoTree);
                    $first['child'] = $twoTree;
                }
                $tree[$first['category_id']] = $first;
            }
            Cache::set('category_' . $wxapp_id . '_' . $shop_id, compact('all', 'tree'));
        }
        return Cache::get('category_' . $wxapp_id . '_' . $shop_id);
    }

    /**
     * 获取所有分类
     */
    public static function getCacheAll($shop_id,$wxapp_id='')
    {
        return self::getALL($shop_id,$wxapp_id)['all'];
    }

    /**
     * 获取所有分类(树状结构)
     */
    public static function getCacheTree($shop_id,$wxapp_id='')
    {
        return self::getALL($shop_id,$wxapp_id)['tree'];
    }

}
