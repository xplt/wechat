<?php
namespace app\common\model;
use think\Request;

/**
 * 商品模型
 */
class Goods extends BaseModel
{
    protected $name = 'goods';
    protected $append = ['goods_sales','thumbnail','stock'];
	
	/**
     * 商品缩略图
     */
    public function getThumbnailAttr($value, $data)
    {
        return web_url().'uploads/'.$data['goods_id'].'.jpg';
    }

    /**
     * 商品库存
     */
    public function getStockAttr($value, $data)
    {
    
        $list = (new GoodsSpec)->where('goods_id',$data['goods_id'])->select();

        $value = '';
        for($n=0;$n<sizeof($list);$n++){
            if($n==0){
                $value = (string)$list[$n]['stock_num'];
            }else{
                $value .= '<br>' . (string)$list[$n]['stock_num'];
            }
        }
        return $value;
    }

    /**
     * 计算显示销量 (初始销量 + 实际销量)
     */
    public function getGoodsSalesAttr($value, $data)
    {
        return $data['sales_initial'] + $data['sales_actual'];
    }

    /**
     * 关联商品分类表
     */
    public function category()
    {
        return $this->belongsTo('Category');
    }

    /**
     * 关联商品规格表
     */
    public function spec()
    {
        return $this->hasMany('GoodsSpec')->order(['goods_spec_id' => 'asc']);
    }

    /**
     * 关联商品规格关系表
     */
    public function specRel()
    {
        return $this->belongsToMany('SpecValue', 'GoodsSpecRel');
    }

    /**
     * 关联商品图片表
     */
    public function image()
    {
        return $this->hasMany('GoodsImage')->order(['id' => 'asc']);
    }

    /**
     * 显示状态
     */
    public function getGoodsStatusAttr($value)
    {
        $status = [10 => '上架', 20 => '下架'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 是否推荐
     */
    public function getIsRecommendAttr($value)
    {
        $status = ['否', '是'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取商品列表
     */
    public function getList($shop_id = 0,$status = 0, $category_id = 0, $search = '', $sortType = 'all', $sortPrice = false)
    {
        // 筛选条件
        $filter = [];
        $category_id > 0 && $filter['category_id'] = $category_id;
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
		$status > 0 && $filter['goods_status'] = $status;
        !empty($search) && $filter['goods_name'] = ['like', '%' . trim($search) . '%'];

        // 排序规则
        $sort = [];
        if ($sortType === 'all') {
            $sort = ['goods_sort', 'goods_id' => 'desc'];
        } elseif ($sortType === 'sales') {
            $sort = ['goods_sales' => 'desc'];
        } elseif ($sortType === 'price') {
            $sort = $sortPrice ? ['goods_max_price' => 'desc'] : ['goods_min_price'];
        }
        // 商品表名称
        $tableName = $this->getTable();
        // 多规格商品 最高价与最低价
        $GoodsSpec = new GoodsSpec;
        $minPriceSql = $GoodsSpec->field(['MIN(goods_price)'])
            ->where('goods_id', 'EXP', "= `$tableName`.`goods_id`")->buildSql();
        $maxPriceSql = $GoodsSpec->field(['MAX(goods_price)'])
            ->where('goods_id', 'EXP', "= `$tableName`.`goods_id`")->buildSql();
        // 执行查询
        $list = $this->field(['*', '(sales_initial + sales_actual) as goods_sales',
            "$minPriceSql AS goods_min_price",
            "$maxPriceSql AS goods_max_price"
        ])->with(['category', 'image.file', 'spec', 'spec.image'])
            ->where('is_delete', '=', 0)
            ->where($filter)
            ->order($sort)
            ->paginate(15, false, [
                'query' => Request::instance()->request()
            ]);
        return $list;
    }

    /**
     * 获取商品列表 - DIY组件
     */
    public static function getAll($shop_id = 0,$sortType = 'all',$limit=null)
    {
        // 筛选条件
        $filter = [];
        $filter['goods_status'] = 10;
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        if($sortType == 'recommend'){
            $filter['is_recommend'] = 1;//推荐商品
        }
        // 排序规则
        $sort = [];
        if ($sortType === 'all') {
            $sort = ['sales_actual', 'goods_id' => 'desc'];
        } elseif ($sortType === 'sales') {
            $sort = ['goods_sales' => 'desc'];
        } elseif ($sortType === 'new') {
            $sort = ['goods_id' => 'desc'];
        }

        // 执行查询
        return self::with(['category', 'image.file', 'spec', 'spec.image'])
            ->where('is_delete', '=', 0)
            ->where($filter)
            ->order($sort)
            ->limit($limit)
            ->select();
    }

    /**
     * 猜您喜欢
     */
    public function getBestList($shop_id = 0,$limit=10)
    {
        // 筛选条件
        $filter = [];
        $filter['goods_status'] = 10;
        $filter['is_delete'] = 0;
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        return $this->with(['spec', 'category', 'image.file'])
            ->where($filter)
            ->order(['sales_initial' => 'desc', 'goods_sort' => 'asc'])
            ->limit($limit)
            ->select();
    }

    /**
     * 获取商品详情
     */
    public static function detail($goods_id)
    {
        return self::get($goods_id, ['category', 'image.file', 'spec', 'spec.image', 'spec_rel.spec']);
    }


    /**
     * 商品多规格信息
     */
    public function getGoodsSku($goods_sku_id)
    {
        $goodsSkuData = array_column($this['spec']->toArray(), null, 'spec_sku_id');
        if (!isset($goodsSkuData[$goods_sku_id])) {
            return false;
        }
        $goods_sku = $goodsSkuData[$goods_sku_id];
        // 多规格文字内容
        $goods_sku['goods_attr'] = '';
        if ($this['spec_type'] == 20) {
            $attrs = explode('_', $goods_sku['spec_sku_id']);
            $spec_rel = array_column($this['spec_rel']->toArray(), null, 'spec_value_id');
            foreach ($attrs as $specValueId) {
                if(!empty($goods_sku['goods_attr'])){
                    $goods_sku['goods_attr'] .= ',';
                }
                $goods_sku['goods_attr'] .= $spec_rel[$specValueId]['spec']['spec_name'] . ':'
                    . $spec_rel[$specValueId]['spec_value'];
            }
        }
        return $goods_sku;
    }
	
	/**
     * 更新商品库存销量
     */
    public function updateStockSales($goodsList)
    {
        // 整理批量更新商品销量
        $goodsSave = [];
        // 批量更新商品规格：sku销量、库存
        $goodsSpecSave = [];
        foreach ($goodsList as $goods) {
            $goodsSave[] = [
                'goods_id' => $goods['goods_id'],
                'sales_actual' => ['inc', $goods['total_num']]
            ];
            $specData = [
                'goods_spec_id' => $goods['goods_spec_id'],
                'goods_sales' => ['inc', $goods['total_num']]
            ];
            // 付款减库存
            if ($goods['deduct_stock_type'] == 20) {
                $specData['stock_num'] = ['dec', $goods['total_num']];
            }
            $goodsSpecSave[] = $specData;
        }
        // 更新商品总销量
        $this->allowField(true)->isUpdate()->saveAll($goodsSave);
        // 更新商品规格库存
        (new GoodsSpec)->isUpdate()->saveAll($goodsSpecSave);
    }
	
	/**
     * 获取商品总数
     */
    public static function getCount($shop_id=0)
    {
		 // 筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
		$wxapp_id = self::$wxapp_id;
		$wxapp_id > 0 && $filter['wxapp_id'] = $wxapp_id;
        return self::useGlobalScope(false)->where($filter)->count();
    }
	
	/**
     * 设置商品上下架
     */
    public function status()
    {
		$this->goods_status['value'] == 10 ? $this->goods_status = 20 :$this->goods_status = 10;
		return $this->save() !== false;
    }
}
