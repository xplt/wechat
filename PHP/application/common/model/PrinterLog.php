<?php
namespace app\common\model;

/**
 * 打印记录模型
 */
class PrinterLog extends BaseModel
{
    protected $name = 'printer_log';

	/**
     * 获取信息 - 回调接口
     */
    public static function getTaskid($task_id)
    {
        return self::useGlobalScope(false)->where(['task_id' => $task_id])->find();
    }
}
