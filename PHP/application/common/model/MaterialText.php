<?php
namespace app\common\model;

/**
 * 微信公众号菜单模型
 */
class MaterialText extends BaseModel
{
    protected $name = 'material_text';
	protected $append = ['file_path'];
	
	/**
     * 获取图片完整路径
    */ 
    public function getFilePathAttr($value,$data)
    {
        return web_url() . 'uploads/' . $data['file_name'];
    }
	
	/**
     * 获取列表
    */
	public function getList($text_no,$name=''){
		$list = $this->where(['text_no' => $text_no])
			->order('id','asc')
			->select();
		$list[0]['name'] = $name;
		return json_encode($list);
	}	

	/**
     * 添加
     */
    public function add($data,$text_no)
    {
		//重组数据
		for($n=0;$n<sizeof($data);$n++){
			$data[$n]['id'] = $n;
			$data[$n]['text_no'] = $text_no;
		}
        return $this->allowField(true)->saveAll($data) !== false;
    }
	
	/**
     * 添加
     */
    public function edit($data)
    {
        return $this->allowField(true)->saveAll($data) !== false;
    }
}
