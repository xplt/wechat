<?php
namespace app\common\model;
use think\Request;

/**
 * 模板模型
 */
class Template extends BaseModel
{
    protected $name = 'template';

	/**
     * 小程序类型
     */
    public function getAppTypeAttr($value)
    {
		$app_type = WebEdition::getAppType($value);
        return ['text' => $app_type['name'], 'value' => $app_type['app_type']];
    }

    /**
     * 获取列表
     */
    public function getList()
    {
        // 执行查询
        $list = $this->useGlobalScope(false)
			->order('template_id','desc')
            ->paginate(15, false, [
                'query' => Request::instance()->request()
            ]);
        return $list;
    }

    /**
     * 获取详情
     */
    public static function detail($template_id)
    {
        return self::useGlobalScope(false)->where(['template_id' => $template_id])->find();
    }
	
	/**
     * 获取最新模板
     */
    public static function getNew($app_type)
    {
		$where = [
			'app_type' => $app_type
		];
		return self::useGlobalScope(false)
			->where($where)
			->order('template_id','desc')
			->find();
    }

}
