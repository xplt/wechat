<?php
namespace app\common\model;
use think\Cache;

/**
 * 系统设置模型
 */
class WebSet extends BaseModel
{
    protected $name = 'web_set';
    protected $createTime = false;

    /**
     * 获取器: 转义数组格式
     */
    public function getValuesAttr($value)
    {
        return json_decode($value, true);
    }

    /**
     * 修改器: 转义成json格式
     */
    public function setValuesAttr($value)
    {
        return json_encode($value);
    }

    /**
     * 获取指定项设置
     */
    public static function getItem($key)
    {
        $data = self::getAll();
        if($key == 'web'){
        	$version = get_version();
        	$data['web']['values']['version'] = $version['version']['web'];
        }
        return isset($data[$key]) ? $data[$key]['values'] : [];
    }

    /**
     * 获取设置项信息
     */
    public static function detail($key)
    {
        return self::useGlobalScope(false)->where(['key' => $key])->find();
    }

    /**
     * 全局缓存: 系统设置
     */
    public static function getAll()
    {
        $self = new static;
        if (!$data = Cache::get('web_set_hema')) {
            $data = array_column(collection($self::useGlobalScope(false)->select())->toArray(), null, 'key');
            Cache::set('web_set_hema', $data);
        }
        return array_merge_multiple($self->defaultData(), $data);
    }

    /**
     * 默认配置
     */
    public function defaultData()
    {
        return [
			'web' => [
                'key' => 'web',
                'describe' => '站点设置',
                'values' => [
					'name' => '河马云店',//网站名称
					'domain' => '',	//网站域名
					'icp' => '',	//备案号
					'address' => '',//地址
					'keywords' => '',	//关键字
					'description' => '',	//描述
					'company' => '',//公司名称
					'phone' => '',//联系电话
					'open_id' => '',	//管理员微信ID
					'wxmap' => '',	//微信地图KEY
					'copyright' => '©河马云店'	//小程序显示版权
				],
            ],
            'wxweb' => [
                'key' => 'wxweb',
                'describe' => '网站应用',
                'values' => [
					'app_id' => '',	//公众号APPID
					'app_secret' => '',	//密钥
				],
            ],
			'wxapp' => [
                'key' => 'wxapp',
                'describe' => '商家助手',
                'values' => [
					'app_name' => '河马云店',	//标题名称
					'app_id' => '',	//公众号APPID
					'app_secret' => '',	//密钥
					'share_title' => '河马云店',
					'share_image' => api_url().'assets/images/no_share.jpg',//分享图片
					'logo' => api_url().'assets/images/store_logo.jpg',//分享图片
					'copyright' => '©河马科技'//小程序显示版权
				],
            ],
            'outapp' => [
                'key' => 'outapp',
                'describe' => '外卖平台',
                'values' => [
                	'wx' => [
                		'app_id' => '',	//APPID
						'app_secret' => '',	//密钥
                	]
				],
            ],
            'solve' => [
                'key' => 'solve',
                'describe' => '结算设置',
                'values' => [
                	'mode' => 'fixed',	//结算方式 ratio=比例提拥 ，fixed=固定金额
					'value' => '0'	//数值
				],
            ],
			'wxpay' => [
                'key' => 'wxpay',
                'describe' => '微信支付服务商设置',
                'values' => [
					'app_id' => '',	//公众号APPID
					'mchid' => '',	//商户号
					'apikey' => '',	//密钥
					'apiv3key' => '',	//APIv3密钥
					'cert_id' => '',
					'cert_pem' => '',
					'key_pem' => ''
				],
            ],
            'payment' => [
                'key' => 'payment',
                'describe' => '支付设置',
                'values' => [
                    'wx' => [
                        'mchid' => '',//商户号
                        'apikey' => '',//密钥
                        'cert_pem' => '', //apiclient_cert.pem 证书
                        'key_pem' => '', //apiclient_key.pem 密钥
                        'cash_mode' => 'fixed',//用户提现手续费模式 fixed=固定 ratio=比例 
                        'cash_fee' => 0,//手续费数值
                    ]
                ]
            ],
            'register' => [
                'key' => 'register',
                'describe' => '注册设置',
                'values' => [
                    'prefix' => 'hema', //账号前缀
                    'examine' => 0,  //快速注册小程序审核费
                    'wxapp_auth' => 0, //快速注册小程序超管是否审核
                    'out_auth' => 0   //外卖平台入驻超管是否审核
                ],
            ],
			'delivery' => [
                'key' => 'delivery',
                'describe' => '配送接口',
                'values' => [
					'uu' => [
						'api_url' => 'https://openapi.uupt.com/v2_0/',//请求接口
						'app_id' => '',//应用ID
						'app_key' => '',//应用密钥
						'open_id' => ''//账户ID
					],
					'sf' => [
						'api_url' => 'https://commit-openic.sf-express.com',//请求接口
						'app_key' => '',//开发者ID
						'app_secret' => ''//开发者密钥
					],
					'dada' => [
						'api_url' => 'https://newopen.imdada.cn',//请求接口
						'app_key' => '',//开发者ID
						'app_secret' => '',//开发者密钥
						'source_id' => '' //商户号
					]
				],
            ],
            'calling' => [
                'key' => 'calling',
                'describe' => '云呼叫',
                'values' => [
					'api_url' => '',	//请求接口
					'secret_id' => '',	//密钥
					'secret_key' => '',	//密钥
				],
            ],
			'printer' => [
                'key' => 'printer',
                'describe' => '打印设备',
                'values' => [
					//对对机
					'ddj' => [
						'api_url' => 'http://www.open.mstching.com',//请求接口
						'app_key' => '',//开发者ID
						'app_secret' => ''//开发者密钥
					],
					//飞鹅
					'feie' => [
						'api_url' => 'http://api.feieyun.cn/Api/Open/',//请求接口
						'app_key' => '',//开发者ID
						'app_secret' => ''//开发者密钥
					],
					//易联云
					'yilian' => [
						'api_url' => 'https://open-api.10ss.net/',//请求接口
						'app_key' => '',//开发者ID
						'app_secret' => '',//开发者密钥
						'access_token' => ''//access_token
					],
				],
            ],
			'tplmsg' => [
                'key' => 'tplmsg',
                'describe' => '模板消息',
                'values' => [
					'new_order' => [	//新订单通知
						'template_id' => ''
					],
					'examine' => [	//审核状态通知
						'template_id' => ''
					],
					'balance' => [	//账户资金变动提醒
						'template_id' => ''
					],
					'apply' => [	//申请受理通知
						'template_id' => ''
					],
					'deduction' => [	//扣费失败通知
						'template_id' => ''
					],
					'testing' => [	//试用申请成功通知
						'template_id' => ''
					],
					'refund' => [	//退款发起通知
						'template_id' => ''
					],
                    'grab' => [   //骑手抢单通知
                        'template_id' => ''
                    ],
				],
            ],
			
        ];
    }

}
