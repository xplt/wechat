<?php
namespace app\common\model;

/**
 * 口味选项模型
 */
class Flavor extends BaseModel
{
    protected $name = 'flavor';
	protected $append = ['checked'];
	
	/**
     * 选项状态
     */
    public function getCheckedAttr($value)
    {
        return false;
    }

    /**
     * 列表
     */
    public function getList($wxapp_id='')
    {
        // 筛选条件
        $filter = [];
        !empty($wxapp_id) && $filter['wxapp_id'] = $wxapp_id;
        return $this->where($filter)->order(['sort','flavor_id' => 'desc'])->select();
    }

    /**
     * 详情
     */
    public static function detail($flavor_id)
    {
        return self::get($flavor_id);
    }
}
