<?php
namespace app\common\model;
use think\Request;

/**
 * 用户详情模型
 */
class StoreDetail extends BaseModel
{
    protected $name = 'store_detail';

    /**
     * 营业执照
     */
    public function getLicenseCopyAttr($value)
    {
        if($value){
            $url = api_url() . 'uploads/apply/' . $value;
        }else{
            $url = api_url() . 'assets/images/no_pic.jpg';
        }
        return ['text' => '营业执照', 'value' => $value, 'url' => $url];
    }
    /**
     * 身份证(正面)
     */
    public function getIdCardCopyAttr($value)
    {
        if($value){
            $url = api_url() . 'uploads/apply/' . $value;
        }else{
            $url = api_url() . 'assets/images/no_pic.jpg';
        }
        return ['text' => '身份证(正面)', 'value' => $value, 'url' => $url];
    }

    /**
     * 身份证(反面)
     */
    public function getIdCardNationalAttr($value)
    {
        if($value){
            $url = api_url() . 'uploads/apply/' . $value;
        }else{
            $url = api_url() . 'assets/images/no_pic.jpg';
        }
        return ['text' => '身份证(反面)', 'value' => $value, 'url' => $url];
    }
    /**
     * 特殊资质
     */
    public function getQualificationsAttr($value)
    {
        if($value){
            $url = api_url() . 'uploads/apply/' . $value;
        }else{
            $url = api_url() . 'assets/images/no_pic.jpg';
        }
        return ['text' => '特殊资质', 'value' => $value, 'url' => $url];
    }

	/**
     * 获取列表
	 */
    public function getList()
    {
		$where = [];
        // 执行查询
        return $this->useGlobalScope(false)
			->where($where)
            ->order('store_detail_id','desc')
            ->paginate(15, false, ['query' => Request::instance()->request()]);
    }

	/**
     * 获取详情信息
     */
    public static function detail($store_detail_id)
    {
        return self::useGlobalScope(false)->where(['store_detail_id' => $store_detail_id])->find();
    }

    /**
     * 获取详情信息
     */
    public static function getDetail($where = [])
    {
        return self::useGlobalScope(false)->where($where)->find();
    }

    /**
     * 详情
     */
    public static function getStoreDetail($store_user_id)
    {
        $self = new static;
        $data = $self->useGlobalScope(false)->where(['store_user_id' => $store_user_id])->find();
        if($data){
            return $data;
        }
        return $self->defaultData();
    }
    
    /**
     * 初始数据
     */
    public function defaultData()
    {
        return [
            'license_copy' => [
                'url' => '',//营业执照
                'value' =>''
            ],
            'merchant_name' => '',       //营业执照名称
            'license_number' => '',      //统一社会信用代码
            'id_card_copy' => [
                'url' => '',//身份证正面
                'value' =>''
            ],
            'id_card_national' => [
                'url' => '',//身份证反面
                'value' =>''
            ],
            'id_card_name' => '',      //证件姓名
            'id_card_number' => '', //证件号码
            'qualifications' => [
                'url' => '',//特殊资质
                'value' =>''
            ],
            'legal_persona_wechat' => '',       //微信号
            'mobile_phone' => '',       //联系电话
            'contact_email' => '',      //邮箱
            'bank_name' => '',      //开户银行全称
            'account_number' => '' //银行账号
        ];
    }
}
