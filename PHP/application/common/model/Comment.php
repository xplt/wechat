<?php
namespace app\common\model;
use think\Request;

/**
 * 评论模型类
 */
class Comment extends BaseModel
{
    protected $name = 'comment';
	protected $append = ['total'];
	
	/**
     * 用户评分
     */
    public function getTotalAttr($value, $data)
    {
        $value = ($data['serve']+$data['speed']+$data['flavor']+$data['ambient'])/4;
		return $value;
    }
	
    /**
     * 关联用户
	*/
    public function user()
    {
        return $this->belongsTo('User');
    }

    /**
     * 关联平台用户
    */
    public function store()
    {
        return $this->belongsTo('StoreUser');
    }
	
	/**
     * 关联订单
    */
    public function order()
    {
        return $this->belongsTo('Order');
    }
	
	/**
     * 关联门店
    */
    public function shop()
    {
        return $this->belongsTo('Shop');
    }

	
	/**
     * 显示状态
     */
    public function getIsShowAttr($value)
    {
		$status = [0 => '隐藏', 1 => '显示'];
        return ['text' => $status[$value], 'value' => $value];
    }
	
	/**
     * 获取列表
    */
    public function getList($shop_id=0,$user_id=0)
    {
		 // 筛选条件
        $filter = [];
		$shop_id>0 && $filter['shop_id'] = $shop_id;
		$user_id>0 && $filter['user_id'] = $user_id;
		if($shop_id>0){
			$filter['is_show'] = 1;
		}
        $list = $this->with(['user','order','shop'])
            ->where($filter)
            ->order(['comment_id' => 'desc'])
            ->paginate(15, false, ['query' => Request::instance()->request()]);
        return $list;
    }

    /**
     * 获取列表 --- 外卖平台
    */
    public function getOutList($shop_id=0)
    {
         // 筛选条件
        $filter = [];
        $shop_id>0 && $filter['shop_id'] = $shop_id;
        $filter['store_user_id'] = ['>',0];
        if($shop_id>0){
            $filter['is_show'] = 1;
        }
        $list = $this->with(['store','order','shop'])
            ->where($filter)
            ->order(['comment_id' => 'desc'])
            ->paginate(15, false, ['query' => Request::instance()->request()]);
        return $list;
    }
	
	/**
     * 详情
     */
    public static function detail($comment_id)
    {
        return self::get($comment_id,['user','order','shop']);
    }
	
	/**
     * 评分计算
     */
    public function score($shop_id=0)
    {
        $where = [];
		$shop_id > 0 && $where['shop_id'] = $shop_id;
		$serve = $this->where($where)->avg('serve');
		$speed = $this->where($where)->avg('speed');
		$flavor = $this->where($where)->avg('flavor');
		$ambient = $this->where($where)->avg('ambient');
        $serve <=0 && $serve = 5;
        $speed <=0 && $speed = 5;
        $flavor <=0 && $flavor = 5;
        $ambient <=0 && $ambient = 5;
		$all = ($serve+$speed+$flavor+$ambient)/4;
		return [
			'all' => round($all,2),
			'serve' => round($serve,2),
			'speed' => round($speed,2),
			'flavor' => round($flavor,2),
			'ambient' => round($ambient,2)
		];
    }
	
    /**
     * 根据条件统计数量
     */
    public static function getCount($shop_id=0)
    {
        $self = new static;
		 // 筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
		$wxapp_id = $self::$wxapp_id;
		$wxapp_id > 0 && $filter['wxapp_id'] = $wxapp_id;
        $count = array();
        //全部
        $count[0] = self::useGlobalScope(false)->where($filter)->count();
        //今天
        $star = strtotime(date("Y-m-d"),time());
        $count[1] = self::useGlobalScope(false)->where('create_time','>',$star)->where($filter)->count();
        //昨天
        $star = strtotime("-1 day");
        $end = strtotime(date("Y-m-d"),time());
        $count[2] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
        //前天
        $star = strtotime("-2 day");
        $end = strtotime("-1 day");
        $count[3] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
        //-4天
        $star = strtotime("-3 day");
        $end = strtotime("-2 day");
        $count[4] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
        //-5天
        $star = strtotime("-4 day");
        $end = strtotime("-3 day");
        $count[5] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
        //-6天
        $star = strtotime("-5 day");
        $end = strtotime("-4 day");
        $count[6] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
        //-7天
        $star = strtotime("-6 day");
        $end = strtotime("-5 day");
        $count[7] = self::useGlobalScope(false)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
        return $count;
    }

}
