<?php
namespace app\common\model;
use think\Cache;

/**
 * 小程序版本分类模型
 */
class WebEdition extends BaseModel
{
    protected $name = 'web_edition';
	
	/**
     * 格式化页面数据（读取的时候对数据转换）
     */
    public function getBuySingleAttr($value)
    {
        return json_decode($value, true);
    }
	

    /**
     * 自动转换data为json格式(修改器，保存de时候操作)
     */
    public function setBuySingleAttr($value)
    {
        return json_encode($value);
    }
	
	/**
     * 格式化页面数据（读取的时候对数据转换）
     */
    public function getRenewSingleAttr($value)
    {
        return json_decode($value, true);
    }
	

    /**
     * 自动转换data为json格式(修改器，保存de时候操作)
     */
    public function setRenewSingleAttr($value)
    {
        return json_encode($value);
    }
	
	/**
     * 格式化页面数据（读取的时候对数据转换）
     */
    public function getBuyManyAttr($value)
    {
        return json_decode($value, true);
    }
	

    /**
     * 自动转换data为json格式(修改器，保存de时候操作)
     */
    public function setBuyManyAttr($value)
    {
        return json_encode($value);
    }
	
	/**
     * 格式化页面数据（读取的时候对数据转换）
     */
    public function getRenewManyAttr($value)
    {
        return json_decode($value, true);
    }
	

    /**
     * 自动转换data为json格式(修改器，保存de时候操作)
     */
    public function setRenewManyAttr($value)
    {
        return json_encode($value);
    }

    /**
     * 所有分类
     */
    public static function getALL($agent_id = -1)
    {
		$where = [];
		$agent_id >= 0 && $where['agent_id'] = $agent_id;
        $model = new static;
        return $model->useGlobalScope(false)
			->where($where)
			->order(['sort','web_edition_id' => 'desc'])
			->select();
    }
	
	/**
     * 获取详情
     */
    public static function detail($web_edition_id)
    {
        return self::useGlobalScope(false)->where(['web_edition_id' => $web_edition_id])->find();
    }
	
	/**
     * 根据识别号获取详情
     */
    public static function getAppType($app_type,$agent_id='')
    {
		$where = [];
		$where['app_type'] = $app_type;
		!empty($agent_id) && $where['agent_id'] = $agent_id;
        return self::useGlobalScope(false)->where($where)->find();
    }
}
