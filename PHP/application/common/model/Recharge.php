<?php
namespace app\common\model;
use think\Request;

/**
 * 用户充值模型
 */
class Recharge extends BaseModel
{
    protected $name = 'recharge';

    /**
     * 关联套餐表
     */
    public function plan()
    {
        return $this->hasOne('RechargePlan','recharge_plan_id','recharge_plan_id');
    }
	
	/**
	 * 关联门店
	 */
	public function shop()
	{
	    return $this->hasOne('Shop', 'shop_id', 'shop_id');
	}
	
	/**
     * 关联用户表
     */
    public function user()
    {
        return $this->hasOne('User','user_id','user_id');
    }
	
	/**
     * 付款状态
     */
    public function getPayStatusAttr($value)
    {
		$status = [10 => '待付款', 20 => '已付款'];
        return ['text' => $status[$value], 'value' => $value];
    }
	
	/**
     * 充值方式
     */
    public function getModeAttr($value)
    {
		$status = [10 => '自助充值', 20 => '后台充值'];
        return ['text' => $status[$value], 'value' => $value];
    }
	
	/**
     * 获取列表
     */
    public function getList()
    {
        // 执行查询
        $list = $this->with(['plan','user','shop'])
            ->order('recharge_id','desc')
            ->paginate(15, false, ['query' => Request::instance()->request()]);
        return $list;
    }

    /**
     * 订单详情
     */
    public static function detail($recharge_id)
    {
        return self::get($recharge_id, ['plan','user','shop']);
    }
	
	/**
     * 根据时间段统计数量
     */
    public static function getDateCount($shop)
    {
		$self = new static;
		 // 筛选条件
        $filter = [];
        $shop['shop_id'] > 0 && $filter['shop_id'] = $shop['shop_id'];
		$wxapp_id = $self::$wxapp_id;
		$wxapp_id > 0 && $filter['wxapp_id'] = $wxapp_id;
		$filter['pay_status'] = ['=',20];
		$count = self::useGlobalScope(false)->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->sum('money');
		return $count;
    }
	
	/**
     * 根据条件统计数量
     */
    public static function getCount($shop_id=0)
    {
		$self = new static;
		 // 筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
		$wxapp_id = $self::$wxapp_id;
		$wxapp_id > 0 && $filter['wxapp_id'] = $wxapp_id;
        $count = array();
		//全部
		$count[0] = self::useGlobalScope(false)->where('pay_status','=',20)->where($filter)->sum('money');
		//今天
		$star = strtotime(date("Y-m-d"),time());
		$count[1] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where($filter)->sum('money');
		//昨天
		$star = strtotime("-1 day");
		$end = strtotime(date("Y-m-d"),time());
		$count[2] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('money');
		//前天
		$star = strtotime("-2 day");
		$end = strtotime("-1 day");
		$count[3] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('money');
		//-4天
		$star = strtotime("-3 day");
		$end = strtotime("-2 day");
		$count[4] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('money');
		//-5天
		$star = strtotime("-4 day");
		$end = strtotime("-3 day");
		$count[5] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('money');
		//-6天
		$star = strtotime("-5 day");
		$end = strtotime("-4 day");
		$count[6] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('money');
		//-7天
		$star = strtotime("-6 day");
		$end = strtotime("-5 day");
		$count[7] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('money');
		
		//本月起至时间 - 月度统计 
		$end = mktime(0,0,0,date('m'),1,date('y')); 
		$count[8] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$end)->where($filter)->sum('money');
		
		//上月开始  
		$star = mktime(0,0,0,date('m')-1,1,date('y'));
		$count[9] = self::useGlobalScope(false)->where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('money');
		
        return $count;
    }
	
}
