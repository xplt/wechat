<?php
namespace app\common\model;
use app\common\library\mstching\PrintHelper;
use app\common\library\mstching\Feie;
use app\common\library\mstching\YiLian;

/**
 * 云打印机模型
 */
class Printer extends BaseModel
{
    protected $name = 'printer';
	
	/**
     * 品牌
     */
    public function getBrandAttr($value)
    {
        $status = [10 => '对对机', 20 => '飞鹅', 30 => '易联云'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 开关
     */
    public function getIsOpenAttr($value)
    {
        $status = [0 => '关闭', 1 => '开启'];
        return ['text' => $status[$value], 'value' => $value];
    }
	
	/**
	 * 关联门店
	 */
	public function shop()
	{
	    return $this->hasOne('Shop', 'shop_id', 'shop_id');
	}
	
	/**
     * 设备状态 - 自动完成
	 * 返回数据格式："}{"State":0,"Code":200,"Message":"成功"}
	 *State:状态值(-1错误 0正常 1缺纸 2温度保护报警 3 忙碌 4 离线)
     */
    public function getUuidAttr($value,$data)
    {

		$Message = '正常';
		if($data['brand']==10){
			$helper = new PrintHelper();
			$Message = $helper->getDeviceState($value);//查询设备状态	
		}
		if($data['brand']==20){
			$client = new Feie();
			$Message = $client->queryPrinterStatus($value);//查询设备状态	
		}
		if($data['brand']==30){
			$client = new YiLian();
			$Message = $client->queryPrinterStatus($value);//查询设备状态	
		}
        return ['text' => $Message, 'value' => $value];
    }

    /**
     * 获取打印机列表
     */
    public function getList($shop_id='',$is_open=-1)
    {
		// 筛选条件
        $filter = [];
        !empty($shop_id) && $filter['shop_id'] = $shop_id;
        $is_open >= 0 && $filter['is_open'] = $is_open;
        return $this->with(['shop'])->where($filter)->order('printer_id','desc')->select();
    }
	
    /**
     * 获取打印机详情
     */
    public static function detail($printer_id)
    {
        return self::get($printer_id,['shop']);
    }
	/**
	 * 打印小票
	 * $data 打印的数据
	 * $p_n 打印数量
	 * $tpl 模板类型 0=订单模板 1=退单模板
	 */
	public function gotoPrint($data, $tpl=0)
	{
		//获取门店打印机列表
		if($list = $this->getList($data['shop_id'],1)){
			//如果有设备
			for($n=0;$n<sizeof($list);$n++){
				//对对机
				if($list[$n]['brand']['value']==10){
					$helper = new PrintHelper();
					$helper->printContent($list[$n]['uuid']['value'],$list[$n]['open_user_id'],$data,$list[$n]['printer_num'],$list[$n]['model'],$tpl);
				}
				//飞鹅
				if($list[$n]['brand']['value']==20){
					$client = new Feie();
					$client->printMsg($data,$list[$n]['open_user_id'],$list[$n]['printer_num'],$tpl);
				}
				//易联云
				if($list[$n]['brand']['value']==30){
					$client = new YiLian();
					$client->printMsg($data,$list[$n]['open_user_id'],$list[$n]['printer_num'],$tpl);
				}
			}
		}
        return 'ok';
	}
}
