<?php
namespace app\common\model;
use think\Request;
/**
 * 公众号关键字回复模型
 */
class Keyword extends BaseModel
{
    protected $name = 'keyword';

    /**
     * 获取器: 转义数组格式
     */
    public function getContentAttr($value)
    {
        return json_decode($value, true);
    }

    /**
     * 修改器: 转义成json格式
     */
    public function setContentAttr($value)
    {
        return json_encode($value);
    }

    /**
     * 获取列表
     */
    public function getList($is_open=-1)
    {
        $wxapp_id = self::$wxapp_id;
        empty($wxapp_id) && $wxapp_id = 0;
        // 筛选条件
        $filter = [];
        $is_open >= 0 && $filter['is_open'] = $is_open;
        $filter['wxapp_id'] = $wxapp_id;
		// 执行查询
        return $this->useGlobalScope(false)
            ->where($filter)
            ->order('keyword_id','desc')
            ->paginate(15, false, ['query' => Request::instance()->request()]);
    }

    /**
     * 获取详情
     */
    public static function detail($keyword_id)
    {
        $wxapp_id = self::$wxapp_id;
        empty($wxapp_id) && $wxapp_id = 0;
        $filter = [
            'keyword_id' => $keyword_id,
            'wxapp_id' => $wxapp_id
        ];
        return self::useGlobalScope(false)->where($filter)->find();
    }
	
	/**
     * 根据关键字查询
    */
    public static function getKey($key,$wxapp_id = null)
    {
        is_null($wxapp_id) && $wxapp_id = self::$wxapp_id;
        empty($wxapp_id) && $wxapp_id = 0;
        // 筛选条件
        $filter = [];
        $filter['is_open'] = 1;
        $filter['keyword'] = $key;
        $filter['wxapp_id'] = $wxapp_id;
        return self::useGlobalScope(false)->where($filter)->find();
    }

    /**
     * 添加
     */
    public function add(array $data)
    {
        $data['wxapp_id'] = self::$wxapp_id;
        $data['wxapp_id'] < 1 && $data['wxapp_id'] = 0;
        if($data['type'] == 'text'){
            $data['content']['media_id'] = ''; 
            $data['content']['title'] = '';
            $data['content']['url'] = '';
            $data['content']['hurl'] = '';
            $data['content']['picurl'] = '';
        }else{
            if(!$material = Material::mediaId($data['content']['media_id'])){
                $this->error = '素材不存在';
                return false; 
            }
            //图片消息
            if($data['type'] == 'image' OR $data['type'] == 'voice'){
                $data['content']['description'] = ''; 
                $data['content']['title'] = '';
                $data['content']['url'] = '';
                $data['content']['hurl'] = '';
                $data['content']['picurl'] = '';
            }
            //视频消息
            if($data['type'] == 'video'){
                //获取视频素材内容
                $data['content']['title'] = $material['name'];
                $data['content']['description'] = $material['introduction'];
                $data['content']['url'] = '';
                $data['content']['hurl'] = '';
                $data['content']['picurl'] = '';
            }
            //图文消息
            if($data['type'] == 'news'){
                //获取视频素材内容
                $data['content']['picurl'] = $material['url'];
                $data['content']['url'] = $data['content']['url'] ? $data['content']['url'] : web_url();
                $data['content']['hurl'] = '';
            }
        }
        return $this->allowField(true)->save($data);
    }

    /**
     * 编辑
     */
    public function edit($data)
    {
        if($data['type'] == 'text'){
            $data['content']['media_id'] = ''; 
            $data['content']['title'] = '';
            $data['content']['url'] = '';
            $data['content']['hurl'] = '';
            $data['content']['picurl'] = '';
        }else{
            if(!$material = Material::mediaId($data['content']['media_id'])){
                $this->error = '素材不存在';
                return false; 
            }
            //图片消息
            if($data['type'] == 'image' OR $data['type'] == 'voice'){
                $data['content']['description'] = ''; 
                $data['content']['title'] = '';
                $data['content']['url'] = '';
                $data['content']['hurl'] = '';
                $data['content']['picurl'] = '';
            }
            //视频消息
            if($data['type'] == 'video'){
                //获取视频素材内容
                $data['content']['title'] = $material['name'];
                $data['content']['description'] = $material['introduction'];
                $data['content']['url'] = '';
                $data['content']['hurl'] = '';
                $data['content']['picurl'] = '';
            }
            //图文消息
            if($data['type'] == 'news'){
                //获取视频素材内容
                $data['content']['picurl'] = $material['url'];
                $data['content']['url'] = $data['content']['url'] ? $data['content']['url'] : web_url();
                $data['content']['hurl'] = '';
            }
        }
        return $this->allowField(true)->save($data) !== false;
    }

    /**
     * 删除
     */
    public function remove()
    {
        return $this->delete();
    }
    
    /**
     * 更新状态
     */
    public function isOpen()
    {
        $this->is_open ? $this->is_open=0 : $this->is_open=1;
        return $this->save();
    }
}
