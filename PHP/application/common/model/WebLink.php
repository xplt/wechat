<?php
namespace app\common\model;
use think\Cache;
/**
 * 友情链接模型
 */
class WebLink extends BaseModel
{
    protected $name = 'web_link';

    /**
     * 所有分类
     */
    public static function getALL()
    {
        $model = new static;
        if (!Cache::get('web_link_hema')) {
            $link = $model->useGlobalScope(false)->order(['sort' => 'asc'])->select();
            Cache::set('web_link_hema', $link);
        }
        return Cache::get('web_link_hema');
    }
}
