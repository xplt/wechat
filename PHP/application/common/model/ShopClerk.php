<?php
namespace app\common\model;
use think\Request;

/**
 * 店员模型
 */
class ShopClerk extends BaseModel
{
    protected $name = 'shop_clerk';

	/**
     * 关联门店表
     */
    public function shop()
    {
        return $this->belongsTo('Shop');
    }
	
    /**
     * 身份
     */
    public function getStatusAttr($value)
    {
        $status = [10 => '店员', 20 => '店长', 30 => '配送'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取列表
     */
    public function getList($shop_id='',$search='')
    {
        //筛选条件
        $filter = [];
        !empty($shop_id) && $filter['shop_id'] = $shop_id;
        if(!empty($search)){
            //判断是否是手机号
            if(isMobile($search)){
                $filter['mobile'] = $search;
            }else{
                $filter['real_name'] = $search;
            }
        }
        // 排序规则
        $sort = [];
        $sort = ['shop_clerk_id' => 'desc'];
        // 执行查询
        $list = $this->with(['shop'])
            ->where($filter)
            ->order($sort)
            ->paginate(15, false, ['query' => Request::instance()->request()]);
        return $list;
    }
	
	/**
	* API 获取列表
	*/
	public function getLists($shop_id=0,$status=0,$wxapp_id = null){
		if (is_null($wxapp_id)) {
            $wxapp_id = self::$wxapp_id;
        }
		$filter['wxapp_id'] = $wxapp_id;
		$shop_id > 0 && $filter['shop_id'] = $shop_id;
        $status > 0 && $filter['status'] = $status;
		return $this->useGlobalScope(false)->where($filter)->order('shop_clerk_id','desc')->select();
	}

    /**
     * 获取详情
     */
    public static function detail($where)
    {
        return self::get($where, ['shop']);
    }
	
	/**
     * 条件获取详情
     */
    public static function getClerk($where=[])
    {
        return self::useGlobalScope(false)->where($where)->find();
    }

    /**
     * 添加
     */
    public function add(array $data)
    {
        if(!isMobile($data['mobile'])){
            $this->error = '手机号码错误';
            return false;   
        }
        //验证手机号是否存在
        if($this->useGlobalScope(false)->where(['mobile' => $data['mobile']])->count()){
            $this->error = '手机号码被占用';
            return false;
        }
        $data['pwd'] = hema_hash($data['mobile']);
        $data['wxapp_id'] = self::$wxapp_id;
        return $this->allowField(true)->save($data);
    }

    /**
     * 编辑
     */
    public function edit($data)
    {
        //如果修改了手机号码，密码重置为新手机号码
        if($this['mobile'] != $data['mobile']){
            if(!isMobile($data['mobile'])){
                $this->error = '手机号码错误';
                return false;   
            }
            if($this->useGlobalScope(false)->where(['mobile' => $data['mobile']])->count()){
                $this->error = '手机号码被占用';
                return false;
            }
            $data['pwd'] = hema_hash($data['mobile']);
        }
        return $this->allowField(true)->save($data) !== false;
    }

    /**
     * 删除
     */
    public function remove()
    {
        return $this->delete();
    }
	
	/**
     * 新增默认店长
     */
    public function insertDefault($shop_id,$data,$wxapp_id)
    {
		// 添加默认店长
		return $this->save([
			'shop_id' => $shop_id,
			'real_name' => $data['linkman'],
			'mobile' => $data['phone'],
			'pwd' => hema_hash($data['phone']),
			'status' => 20,
			'wxapp_id' => $wxapp_id,
		]);
    }
}
