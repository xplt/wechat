<?php
namespace app\common\model;
use think\Request;

/**
 * 优惠活动模型
 */
class Activity extends BaseModel
{
    protected $name = 'activity';

    /**
     * 关联门店
     */
    public function shop()
    {
        return $this->hasOne('Shop', 'shop_id', 'shop_id');
    }
		
	/**
     * 优惠券类型
     */
    public function getActivityTypeAttr($value)
    {
		$status = [10 => '满减', 20 => '首单立减'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 显示颜色
     */
    public function getColorAttr($value)
    {
        $status = ['info' => '无','error' => '红', 'warning' => '黄','primary' => '蓝','success' => '绿'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取列表
     */
    public function getList($shop_id=0)
    {
        $filter = [];
        $shop_id>0 && $filter['shop_id'] = $shop_id;
        // 执行查询
        return $this->with(['shop'])
            ->where($filter)
            ->order('activity_id','desc')
            ->select();
    }

    /**
     * 获取详情
     */
    public static function detail($activity_id)
    {
        return self::with(['shop'])
            ->where(['activity_id' => $activity_id])
            ->find();
    }
	
	/**
     * 统计活动数量
     */
    public function getCount($shop_id=0)
    {
        $filter = [];
        $shop_id>0 && $filter['shop_id'] = $shop_id;
        return $this->where($filter)->count();
    }
	
}
