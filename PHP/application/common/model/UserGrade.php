<?php
namespace app\common\model;
use think\Request;
/**
 * 用户等级
 */
class UserGrade extends BaseModel
{
    protected $name = 'user_grade';

    /**
     * 获取用户等级列表
     */
    public function getList()
    {
        // 执行查询
        $list = $this->order(['sort' => 'asc'])
            ->paginate(15, false, [
                'query' => Request::instance()->request()
            ]);
        return $list;
    }
	

    /**
     * 等级详情
     */
    public static function detail($user_grade_id)
    {
        return self::get($user_grade_id);
    }
	
	/**
     * 获取所有 - api接口
     */
    public static function getAll($showNum=null)
    {
		$sort = ['score' => 'asc'];
        return self::order($sort)->limit($showNum)->select();
    }

    /**
     * 新增默认用户等级
     */
    public function insertDefault($wxapp_id)
    {
        return $this->save([
            'name' => '普通会员',
            'wxapp_id' => $wxapp_id,
        ]);
    }
}
