<?php
namespace app\common\model;
use think\Model;
use think\Cache;

/**
 * 门店分类模型
 */
class ShopCategory extends Model
{
    protected $name = 'shop_category';

    /**
     * 分类图片
     */
    public function image()
    {
        return $this->hasOne('uploadFile', 'file_id', 'image_id');
    }

    /**
     * 所有分类
     */
    public static function getALL()
    {
        $model = new static;
        if (!Cache::get('shop_category_hema')) {
            $data = $model->with(['image'])->order(['sort', 'shop_category_id' => 'desc'])->select();
            $all = !empty($data) ? $data->toArray() : [];
            $tree = [];
            foreach ($all as $first) {
                if ($first['parent_id'] != 0) continue;
                $twoTree = [];
                foreach ($all as $two) {
                    if ($two['parent_id'] != $first['shop_category_id']) continue;
                    $threeTree = [];
                    foreach ($all as $three)
                        $three['parent_id'] == $two['shop_category_id']
                        && $threeTree[$three['shop_category_id']] = $three;
                    !empty($threeTree) && $two['child'] = $threeTree;
                    $twoTree[$two['shop_category_id']] = $two;
                }
                if (!empty($twoTree)) {
                    array_multisort(array_column($twoTree, 'sort'), SORT_ASC, $twoTree);
                    $first['child'] = $twoTree;
                }
                $tree[$first['shop_category_id']] = $first;
            }
            Cache::set('shop_category_hema', compact('all', 'tree'));
        }
        return Cache::get('shop_category_hema');
    }

    /**
     * 获取所有分类
     */
    public static function getCacheAll()
    {
        return self::getALL()['all'];
    }

    /**
     * 获取所有分类(树状结构)
     */
    public static function getCacheTree()
    {
        return self::getALL()['tree'];
    }

}
