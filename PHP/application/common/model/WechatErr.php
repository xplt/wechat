<?php
namespace app\common\model;

/**
 * 微信反码语言模型
 */
class WechatErr extends BaseModel
{
    protected $name = 'wechat_err';


    /**
     * 获取详情
     */
    public static function detail($errcode)
    {
        return self::useGlobalScope(false)->where(['errcode' => $errcode])->find();
    }
}
