<?php
namespace app\common\model;
use think\Cache;

/**
 * 首页菜单模型
 */
class WebMenu extends BaseModel
{
    protected $name = 'web_menu';
	
	/**
     * 状态
     */
    public function getStatusAttr($value)
    {
        $status = ['隐藏','显示'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 所有分类
     */
    public static function getALL()
    {
        $model = new static;
        if (!Cache::get('web_menu_hema')) {
            $data = $model->useGlobalScope(false)->order(['sort' => 'asc'])->select();
            $all = !empty($data) ? $data->toArray() : [];
            $tree = [];
            foreach ($all as $first) {
                if ($first['parent_id'] != 0) continue;
                $twoTree = [];
                foreach ($all as $two) {
                    if ($two['parent_id'] != $first['web_menu_id']) continue;
                    $threeTree = [];
                    foreach ($all as $three)
                        $three['parent_id'] == $two['web_menu_id']
                        && $threeTree[$three['web_menu_id']] = $three;
                    !empty($threeTree) && $two['child'] = $threeTree;
                    $twoTree[$two['web_menu_id']] = $two;
                }
                if (!empty($twoTree)) {
                    array_multisort(array_column($twoTree, 'sort'), SORT_ASC, $twoTree);
                    $first['child'] = $twoTree;
                }
                $tree[$first['web_menu_id']] = $first;
            }
            Cache::set('web_menu_hema', compact('all', 'tree'));
        }
        return Cache::get('web_menu_hema');
    }

    /**
     * 获取所有
     */
    public static function getCacheAll()
    {
        return self::getALL()['all'];
    }

    /**
     * 获取所有(树状结构)
     */
    public static function getCacheTree()
    {
        return self::getALL()['tree'];
    }
	
	public static function detail($where)
    {
        return self::useGlobalScope(false)->order(['sort' => 'asc'])->where($where)->find();
    }

}
