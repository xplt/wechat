<?php
namespace app\common\model;
/**
 * 服务类目模型
 */
class CategoryServe extends BaseModel
{
    protected $name = 'category_serve';

    /**
     * 获取列表
     */
    public function getList()
    {
        $list = $this->order('category_serve_id','desc')->select();
		//获取已设置的所有类目
		if($result = getCategory()){
			$audit_status = [1 => '审核中', 2 => '审核不通过', 3 => '审核通过'];
			for($n=0;$n<sizeof($list);$n++){
				$list[$n]['audit_status'] = [
					'value' => 0,
					'text' => '未知'
				];
				for($m=0;$m<sizeof($result['categories']);$m++){
					if($result['categories'][$m]['first']==$list[$n]['first'] AND $result['categories'][$m]['second']==$list[$n]['second']){
						$list[$n]['audit_status'] = [
							'value' => $result['categories'][$m]['audit_status'],
							'text' => $audit_status[$result['categories'][$m]['audit_status']]
						];	
					}
				}
			}
		}
		return $list;
    }
}
