<?php
namespace app\common\model;
use think\Request;
/**
 * 小程序体验者模型
 */
class TestUser extends BaseModel
{
    protected $name = 'test_user';

    /**
     * 所有
     */
    public function getList()
    {
		 // 执行查询
        $list = $this->order(['test_user_id' => 'desc'])
            ->paginate(15, false, ['query' => Request::instance()->request()]);
        return $list;
    }
}
