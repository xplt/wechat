<?php
namespace app\common\model;

/**
 * 商品规格模型
 */
class GoodsSpec extends BaseModel
{
    protected $name = 'goods_spec';

    /**
     * 规格图片
     */
    public function image()
    {
        return $this->hasOne('uploadFile', 'file_id', 'image_id');
    }

}
