<?php
namespace app\common\model;
use app\common\exception\BaseException;
use think\Request;

/**
 * 图文资讯模型
 */
class Article extends BaseModel
{
    protected $name = 'article';
    protected $append = ['views','view_time'];

    /**
     * 计算显示阅读量 (虚拟阅读量 + 实际阅读量)
     */
    public function getViewsAttr($value, $data)
    {
        return $data['virtual_views'] + $data['actual_views'];
    }

    /**
     * HTML实体转换回普通字符
     */
    public function getArticleContentAttr($value)
    {
        return htmlspecialchars_decode($value);
    }

    /**
     * 格式化创建时间
     */
    public function getViewTimeAttr($value, $data)
    {
        return date('Y-m-d', $data['create_time']);
    }

    /**
     * 关联分类表
     */
    public function category()
    {
        return $this->belongsTo('ArticleCategory');
    }

    /**
     * 封面图片
     */
    public function image()
    {
        return $this->hasOne('uploadFile', 'file_id', 'image_id');
    }

    /**
     * 显示方式
     */
    public function getShowTypeAttr($value)
    {
        $status = [10 => '小图', 20 => '大图'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 显示状态
     */
    public function getArticleStatusAttr($value)
    {
        $status = ['隐藏','显示'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 信息列表
     */
    public function getList($shop_id = 0,$status = '', $article_category_id = 0)
    {
        // 筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        $article_category_id > 0 && $filter['article_category_id'] = $article_category_id;
		!empty($status) && $filter['article_status'] = $status;
        // 排序规则
        $sort = ['article_sort', 'article_id' => 'desc'];
        // 执行查询
        $list = $this->with(['category', 'image'])
            ->where($filter)
            ->order($sort)
            ->paginate(15, false, [
                'query' => Request::instance()->request()
            ]);
        return $list;
    }

    /**
     * 获取所有 - api组件接口
     */
    public static function getAll($shop_id = 0,$category_id = 0,$showNum=null)
    {
        $filter = [];
        $filter['article_status'] = 1;
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        $category_id > 0 && $filter['article_category_id'] = $article_category_id;
        $sort = ['article_sort', 'article_id' => 'desc'];
        return self::with(['category', 'image'])->where($filter)->order($sort)->limit($showNum)->select();
    }

    /**
     * 获取详情
     */
    public static function detail($article_id)
    {
        if (!$model = self::get($article_id, ['category', 'image'])) {
            throw new BaseException(['msg' => '文章不存在']);
        }
        // 累积阅读数
        $model->setInc('actual_views', 1);
        return $model;
    }

	/**
     * 设置状态
     */
    public function status()
    {
		$this->article_status['value'] ? $this->article_status = 0 : $this->article_status = 1;
		return $this->save() !== false;
    }
}
