<?php
namespace app\common\model;
/**
 * 附近小程序 -类目模型
 */
class NearbyCategory extends BaseModel
{
    protected $name = 'nearby_category';
	
	/**
     * 状态
     */
    public function getStatusAttr($value)
    {
        $status = [1 => '审核中', 2 => '审核失败', 3 => '审核通过'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取列表
     */
    public function getList()
    {
        return $this->order('nearby_category_id','desc')->select();
    }
	
	/**
     * 根据条件获取详情 - 回调接口
     */
    public static function getDetail($whrer)
    {
       return self::useGlobalScope(false)->where($whrer)->find();
    }
	
	/**
     * 默认数据
     */
    public static function defaultData()
    {
		return [
			'first' =>[
				'id' => 383,
				'name' => '美食饮品',
				'children' => [
					384 =>[
						'id' => 384,
						'name' => '江浙菜'
					],
					385 =>[
						'id' => 385,
						'name' => '粤菜'
					],
					386 =>[
						'id' => 386,
						'name' => '川菜'
					],
					387 =>[
						'id' => 387,
						'name' => '湘菜'
					],
					388 =>[
						'id' => 388,
						'name' => '东北菜'
					],
					389 =>[
						'id' => 389,
						'name' => '徽菜'
					],
					390 =>[
						'id' => 390,
						'name' => '闽菜'
					],
					391 =>[
						'id' => 391,
						'name' => '鲁菜'
					],
					392 =>[
						'id' => 392,
						'name' => '台湾菜'
					],
					393 =>[
						'id' => 393,
						'name' => '西北菜'
					],
					394 =>[
						'id' => 394,
						'name' => '东南亚菜'
					],
					395 =>[
						'id' => 395,
						'name' => '西餐'
					],
					396 =>[
						'id' => 396,
						'name' => '日韩菜'
					],
					397 =>[
						'id' => 397,
						'name' => '火锅'
					],
					398 =>[
						'id' => 398,
						'name' => '清真菜'
					],
					399 =>[
						'id' => 399,
						'name' => '小吃快餐'
					],
					400 =>[
						'id' => 400,
						'name' => '海鲜'
					],
					401 =>[
						'id' => 401,
						'name' => '烧烤'
					],
					402 =>[
						'id' => 402,
						'name' => '自助餐'
					],
					404 =>[
						'id' => 404,
						'name' => '茶餐厅'
					],
					405 =>[
						'id' => 405,
						'name' => '美食'
					],
					549 =>[
						'id' => 549,
						'name' => '面包甜点'
					],
					550 =>[
						'id' => 550,
						'name' => '咖啡厅'
					],
					551 =>[
						'id' => 551,
						'name' => '茶馆'
					],
					552 =>[
						'id' => 552,
						'name' => '奶茶'
					],
					553 =>[
						'id' => 553,
						'name' => '茶饮果汁'
					]
				]
			]
		];
	}
}
