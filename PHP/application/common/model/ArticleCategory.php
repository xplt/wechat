<?php
namespace app\common\model;
use think\Cache;

/**
 * 图文资讯分类模型
 */
class ArticleCategory extends BaseModel
{
    protected $name = 'article_category';

    /**
     * 所有分类
     */
    public function getList()
    {
        if (!Cache::get('article_category_' . self::$wxapp_id)) {
            $list = $this->order([
                'sort', 
                'article_category_id'  => 'desc'
            ])->select();
            Cache::set('article_category_' . self::$wxapp_id, $list);
        }
        return Cache::get('article_category_' . self::$wxapp_id);
    }

    /**
     * 获取详情
     */
    public static function detail($article_category_id)
    {
        return self::get($article_category_id);
    }

}
