<?php
namespace app\common\model;
use think\Request;

/**
 * 免费吃商品模型
 */
class FreeGoods extends BaseModel
{
    protected $name = 'free_goods';

    /**
     * 关联门店表
     */
    public function shop()
    {
        return $this->belongsTo('Shop');
    }
	
	/**
     * 关联产品表
     */
    public function goods()
    {
        return $this->belongsTo('Goods');
    }

    /**
     * 获取商品列表
     */
    public function getList($shop_id = 0)
    {
        // 筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        // 执行查询
        $list = $this->with(['shop', 'goods', 'goods.image.file'])
            ->where($filter)
            ->paginate(15, false, [
                'query' => Request::instance()->request()
            ]);
        return $list;
    }

    /**
     * 获取商品详情
     */
    public static function detail($free_goods_id)
    {
        return self::get($free_goods_id, ['shop', 'goods', 'goods.image.file']);
    }
}
