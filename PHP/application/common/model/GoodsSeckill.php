<?php
namespace app\common\model;
use think\Request;

/**
 * 秒杀商品模型
 */
class GoodsSeckill extends BaseModel
{
    protected $name = 'goods_seckill';
    protected $append = ['goods_sales'];

    /**
     * 关联商品表
     */
    public function goods()
    {
        return $this->belongsTo('Goods');
    }

    /**
     * 计算显示销量 (初始销量 + 实际销量)
     */
    public function getGoodsSalesAttr($value, $data)
    {
        return $data['sales_initial'] + $data['sales_actual'];
    }

    /**
     * 显示状态
     */
    public function getGoodsStatusAttr($value)
    {
        $status = [10 => '上架', 20 => '下架'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取商品列表
     */
    public function getList($shop_id = 0,$status = null, $search = '')
    {
        // 筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
		$status > 0 && $filter['goods_status'] = $status;
        !empty($search) && $filter['goods_name'] = ['like', '%' . trim($search) . '%'];

        // 排序规则
        $sort = ['goods_sort', 'goods_seckill_id' => 'desc'];

        // 执行查询
        $list = $this->with(['goods', 'goods.image.file'])
            ->where($filter)
            ->order($sort)
            ->paginate(15, false, [
                'query' => Request::instance()->request()
            ]);
        return $list;
    }

    /**
     * 获取详情
     */
    public static function detail($goods_seckill_id)
    {
        return self::get($goods_seckill_id, []);
    }

    /**
     * 获取商品列表 - DIY组件
     */
    public static function getAll($shop_id = 0,$limit=null,$sortType = 'all')
    {
        // 筛选条件
        $filter = [];
        $filter['goods_status'] = 10;
        $shop_id > 0 && $filter['shop_id'] = $shop_id;

        // 排序规则
        $sort = [];
        if ($sortType === 'all') {
            $sort = ['goods_sort', 'goods_seckill_id' => 'desc'];
        } elseif ($sortType === 'sales') {
            $sort = ['goods_sales' => 'desc'];
        } elseif ($sortType === 'new') {
            $sort = ['goods_seckill_id' => 'desc'];
        }
        // 执行查询
        return self::with(['goods', 'goods.image.file', 'goods.spec', 'goods.spec.image'])
            ->where($filter)
            ->order($sort)
            ->paginate($limit, false, [
                'query' => Request::instance()->request()
            ]);
    }

	
	/**
     * 更新商品库存销量
     */
    public function updateStockSales($goodsList)
    {
        // 整理批量更新商品销量
        $goodsSave = [];
        // 批量更新商品规格：sku销量、库存
        $goodsSpecSave = [];
        foreach ($goodsList as $goods) {
            $goodsSave[] = [
                'goods_id' => $goods['goods_id'],
                'sales_actual' => ['inc', $goods['total_num']]
            ];
            $specData = [
                'goods_spec_id' => $goods['goods_spec_id'],
                'goods_sales' => ['inc', $goods['total_num']]
            ];
            // 付款减库存
            if ($goods['deduct_stock_type'] == 20) {
                $specData['stock_num'] = ['dec', $goods['total_num']];
            }
            $goodsSpecSave[] = $specData;
        }
        // 更新商品总销量
        $this->allowField(true)->isUpdate()->saveAll($goodsSave);
        // 更新商品规格库存
        (new GoodsSpec)->isUpdate()->saveAll($goodsSpecSave);
    }
	
	/**
     * 设置商品上下架
     */
    public function status()
    {
		$this->goods_status['value'] == 10 ? $this->goods_status = 20 :$this->goods_status = 10;
		return $this->save() !== false;
    }
}
