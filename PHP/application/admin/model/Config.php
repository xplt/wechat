<?php
namespace app\admin\model;
use app\common\model\Config as ConfigModel;
/**
 * 站点配置模型
 */
class Config extends ConfigModel
{
	
	/**
     * 编辑
     */
    public function edit($data)
    {
    	startTicket($data['app_id'],$data['app_secret']);
        return $this->allowField(true)->save($data) !== false;
    }
	
}
