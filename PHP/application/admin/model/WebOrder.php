<?php
namespace app\admin\model;
use app\common\model\WebOrder as WebOrderModel;

/**
 * 站点订单模型
 */
class WebOrder extends WebOrderModel
{
    
	/**
     * 删除
     */
    public function remove()
    {
		if($this['pay_status']['value']==20){
			$this->error = '支付成功的不可删除';
			return false;
		}
        return $this->delete();
    }

}

