<?php
namespace app\admin\model;
use app\common\model\WebSet as WebSetModel;
use app\common\library\mstching\YiLian;
use think\Cache;

/**
 * 系统设置模型
 */
class WebSet extends WebSetModel
{
    /**
     * 设置项描述
     */
    private $describe = [
        'web' => '站点设置',
		'wxweb' => '网站应用',
		'delivery' => '配送接口',
        'calling' => '云呼叫',
		'wxapp' => '商户手机端',
		'wxpay' => '微信支付服务商设置',
        'payment' => '支付设置',
		'printer' => '打印设备',
		'tplmsg' => '模板消息',
        'outapp'=> '外卖平台',
        'solve' => '结算设置',
        'register' => '注册设置'
    ];

    /**
     * 更新系统设置
     */
    public function edit($key, $values)
    {
		//打印机配置
		if($key == 'printer'){
			//易联云获取token
			if((!empty($values['yilian']['app_key'])) AND (!empty($values['yilian']['app_secret']))){
				$client = new YiLian();
				$values['yilian']['access_token'] = $client->getToken($values['yilian']['app_key'],$values['yilian']['app_secret']);
			}
		}
        $model = self::detail($key) ?: $this;
        // 删除系统设置缓存
        Cache::rm('web_set_hema');
        return $model->save([
            'key' => $key,
            'describe' => $this->describe[$key],
            'values' => $values,
        ]) !== false;
    }
	
}
