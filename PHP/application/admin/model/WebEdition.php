<?php
namespace app\admin\model;
use app\common\model\WebEdition as WebEditionModel;

/**
 * 小程序版本分类模型
 */
class WebEdition extends WebEditionModel
{
    /**
     * 添加新记录
     */
    public function add($data)
    {
        return $this->allowField(true)->save($data);
    }

    /**
     * 编辑记录
     */
    public function edit($data)
    {
        return $this->allowField(true)->save($data) !== false;
    }

    /**
     * 删除分类
     */
    public function remove()
    {
        // 判断是否发布了模板
        if ($tplCount = (new Template)->useGlobalScope(false)->where(['app_type'=>$this['app_type']])->count()) {
			$this->error = '该版本下存在' . $tplCount . '个已发布的模板，不允许删除';
            return false;
        }
        // 判断是否创建了小程序
        if ($wxappCount = (new Wxapp)->useGlobalScope(false)->where(['app_type'=>$this['app_type']])->count()) {
			$this->error = '该版本下存在' . $wxappCount . '个小程序，不允许删除';
            return false;
        }
        return $this->delete();
    }

}
