<?php
namespace app\admin\model;
use app\common\model\StoreUser as StoreUserModel;
use think\Session;
use think\Db;
/**
 * 用户模型
 */
class StoreUser extends StoreUserModel
{
	/**
     * 管理员用户登录
     */
    public function login($data)
    {
		//体验用户
		if($data['user_name'] =='test' AND $data['password']=='test'){
			// 保存登录状态
			Session::set('hema_admin', [
				'user' => [
					'store_user_id' => 0,
					'user_name' => '体验用户',
                    'avatarUrl' => web_url() . 'assets/images/head.jpg'
				],
				'is_login' => true,
			]);
			return true;
		}
		$filter = [
            'user_name' => $data['user_name'],
            'password' => hema_hash($data['password']),
			'is_agent' => 2
        ];
        // 验证用户名密码是否正确
		if (!$user = $this->useGlobalScope(false)->where($filter)->find()){
			$this->error = '登录失败, 用户名或密码错误';
            return false;
		}
        // 保存登录状态
        Session::set('hema_admin', [
            'user' => [
				'store_user_id' => $user['store_user_id'],
                'user_name' => $user['user_name'],
                'avatarUrl' => $user['avatarUrl']
            ],
            'is_login' => true,
        ]);
        return true;
    }
	
	/**
     * 更新当前管理员信息
     */
    public function renew($data)
    {		
        if ($data['password'] !== $data['password_confirm']) {
            $this->error = '确认密码不正确';
            return false;
        }
		$data['password'] = hema_hash($data['password']);
        // 更新管理员信息
        return $this->allowField(true)->save($data) !== false;
    }
}
