<?php
namespace app\admin\model;
use app\common\model\OutPage as OutPageModel;
use think\Db;

/**
 * 外卖平台diy页面模型
 */
class OutPage extends OutPageModel
{

    /**
     * 更新页面数据
     */
    public function edit($page_data)
    {
        return $this->save(compact('page_data')) !== false;
    }
	
	/**
     * 添加
     */
    public function add(array $page_data)
    {
		$data['page_data'] = $page_data;
        return $this->allowField(true)->save($data) !== false;
    }

    /**
     * 删除
     */
    public function remove()
    {
        return $this->delete() !== false;
    }
}
