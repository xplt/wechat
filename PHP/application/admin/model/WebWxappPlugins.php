<?php
namespace app\admin\model;
use app\common\model\WebWxappPlugins as WebWxappPluginsModel;

/**
 * 小程序插件模型
 */
class WebWxappPlugins extends WebWxappPluginsModel
{
    /**
     * 添加
     */
    public function add(array $data)
    {
		return $this->allowField(true)->save($data);
    }
	
    /**
     * 编辑
     */
    public function edit($data)
    {
		return $this->allowField(true)->save($data) !== false;
    }


    /**
     * 删除
     */
    public function remove()
    {
        return $this->delete();
    }

}
