<?php
namespace app\admin\model;
use think\Model;

/**
 * 文件库分组模型
 */
class UploadGroup extends Model
{
	protected $name = 'upload_group';

    /**
     * 分组详情
     */
    public static function detail($group_id) {
        return self::get($group_id);
    }
	
    /**
     * 获取列表记录
     */
    public function getList($group_type = 'image')
    {
        return $this->where('wxapp_id',0)->where(compact('group_type'))->order(['sort' => 'asc'])->select();
    }

    /**
     * 添加新记录
     */
    public function add($data)
    {
        $data['sort'] = 100;
        return $this->save($data);
    }

    /**
     * 更新记录
     */
    public function edit($data)
    {
        return $this->allowField(true)->save($data) !== false;
    }

    /**
     * 删除记录
     */
    public function remove()
    {
        // 更新该分组下的所有文件
        $model = new UploadFile;
        $model->where('wxapp_id',0)->where(['group_id' => $this['group_id']])->update(['group_id' => 0]);
        return $this->delete();
    }

}
