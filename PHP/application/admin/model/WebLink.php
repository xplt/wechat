<?php
namespace app\admin\model;
use app\common\model\WebLink as WebLinkModel;
use think\Cache;

/**
 * 友情链接模型
 */
class WebLink extends WebLinkModel
{
    /**
     * 添加
     */
    public function add($data)
    {
        $this->deleteCache();
        return $this->allowField(true)->save($data);
    }

    /**
     * 编辑
     */
    public function edit($data)
    {
        $this->deleteCache();
        return $this->allowField(true)->save($data);
    }

    /**
     * 删除
     */
    public function remove()
    {
        $this->deleteCache();
        return $this->delete();
    }

    /**
     * 删除缓存
     */
    private function deleteCache()
    {
        return Cache::rm('web_link_hema');
    }

}
