<?php
namespace app\admin\model;
use app\common\model\WebMenu as WebMenuModel;
use think\Cache;

/**
 * 首页菜单模型
 */
class WebMenu extends WebMenuModel
{
    /**
     * 添加
     */
    public function add($data)
    {
        $this->deleteCache();
        return $this->allowField(true)->save($data);
    }

    /**
     * 编辑
     */
    public function edit($data)
    {
        $this->deleteCache();
        return $this->allowField(true)->save($data);
    }

    /**
     * 删除
     */
    public function remove($web_menu_id)
    {
        // 判断是否存在子菜单
        if ((new self)->where(['parent_id' => $web_menu_id])->count()) {
            $this->error = '该菜单下存在子菜单，请先删除';
            return false;
        }
        $this->deleteCache();
        return $this->delete();
    }

    /**
     * 删除缓存
     */
    private function deleteCache()
    {
        return Cache::rm('web_menu_hema');
    }

}
