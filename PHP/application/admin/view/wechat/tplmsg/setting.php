<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
					<div class="tips am-margin-bottom-sm am-u-sm-12">
					    <div class="pre">
					        <p>模板消息类目设置为“餐饮/餐饮”和“IT科技/互联网|电子商务”，<a href="https://mp.weixin.qq.com/" target="_blank">去设置获取模板ID</a></p>
					    </div>
					</div>
					<div class="widget-body">
                        <fieldset>
							<div class="widget-head am-cf">
								<div class="widget-title am-fl">模板消息配置</div>
							</div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">
									新订单通知 <span class="tpl-form-line-small-title">模板 ID</span>
								</label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="setting[new_order][template_id]"
                                           value="<?= $values['new_order']['template_id']?>">
                                    <small>模板编号：OPENTM417875155</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">
									申请状态更新通知 <span class="tpl-form-line-small-title">模板 ID</span>
								</label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="setting[examine][template_id]"
                                           value="<?= $values['examine']['template_id']?>">
                                    <small>模板编号：OPENTM414769250</small>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">
									账户资金变动提醒 <span class="tpl-form-line-small-title">模板 ID</span>
								</label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="setting[balance][template_id]"
                                           value="<?= $values['balance']['template_id']?>">
                                    <small>模板编号：OPENTM415437052</small>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">
									申请受理通知 <span class="tpl-form-line-small-title">模板 ID</span>
								</label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="setting[apply][template_id]"
                                           value="<?= $values['apply']['template_id']?>">
                                    <small>模板编号：OPENTM416620550</small>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">
									扣费失败通知 <span class="tpl-form-line-small-title">模板 ID</span>
								</label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="setting[deduction][template_id]"
                                           value="<?= $values['deduction']['template_id']?>">
                                    <small>模板编号：OPENTM414769357</small>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">
									试用申请成功通知 <span class="tpl-form-line-small-title">模板 ID</span>
								</label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="setting[testing][template_id]"
                                           value="<?= $values['testing']['template_id']?>">
                                    <small>模板编号：OPENTM406071616</small>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">
									退款发起通知 <span class="tpl-form-line-small-title">模板 ID</span>
								</label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="setting[refund][template_id]"
                                           value="<?= $values['refund']['template_id']?>">
                                    <small>模板编号：OPENTM413423705</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">
                                    抢单提示（骑手） <span class="tpl-form-line-small-title">模板 ID</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="setting[grab][template_id]"
                                           value="<?= $values['grab']['template_id']?>">
                                    <small>模板编号：OPENTM207709328</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        $('#my-form').formPost();

    });
</script>
