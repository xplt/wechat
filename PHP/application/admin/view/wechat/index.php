<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" method="post">
                    <div class="widget-body">
                        <fieldset>
						<?php if($wechat['app_id']): ?>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">公众号信息</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">切换/更新授权 </label>
                                <div class="am-u-sm-9">
									<div class="am-btn-group am-btn-group-xs">
										<a class="am-btn am-btn-default am-btn-secondary am-radius"
										   href="<?= $admin['user']['store_user_id']==0?'#':url('wechat/auth')?>">
											<span class="am-icon-retweet am-icon-sm"></span> <?= $admin['user']['store_user_id']==0?'测试用户不可切换':'切换/更新授权'?>
										</a>
									</div>
                                    <small>谨慎操作，切换后会清除相关数据</small>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">头像 </label>
                                <div class="am-u-sm-9">
                                    <a href="<?= $wechat['head_img'] ? $wechat['head_img'] : 'assets/images/wechat.png' ?>" title="点击查看大图" target="_blank">
                                        <img style="border-radius: 50%;" src="<?= $wechat['head_img'] ? $wechat['head_img'] : 'assets/images/wechat.png' ?>" width="72" height="72" alt="">
                                    </a>
									
									<a href="<?= $wechat['qrcode_url'] ?>" title="点击查看大图" target="_blank">
                                        <img style="margin-left:50px;" src="<?= $wechat['qrcode_url'] ?>" width="72" height="72" alt="">
                                    </a>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">昵称 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $wechat['app_name'] ?>" disabled="disabled">
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">AppID </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $admin['user']['store_user_id']==0?'*****************************':$wechat['app_id']?>" disabled="disabled">
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">账号主体 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $wechat['principal_name']?>" disabled="disabled">
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">原始ID </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $wechat['user_name']?>" disabled="disabled">
                                </div>
                            </div>
						<?php else:?>
							<div class="widget-head am-cf">
                                <div class="widget-title am-fl">您还没有绑定公众号</div>
                            </div>
							<div class="widget-body am-cf">
								<div style="background:#eeeeee;margin-left:100px;border-radius:10px;padding:20px;" class="am-u-sm-6 am-u-md-6 am-u-lg-3">
									<div style="text-align:center;">我已有微信公众号</div>
									<div style="margin-top:20px;text-align:center;">
										<a class="am-btn am-btn-default am-btn-success" href="<?= url('wechat/auth')?>">
                                            一键绑定
                                        </a>
									</div>
								</div>
								<div style="background:#eeeeee;margin-left:100px;border-radius:10px;padding:20px;" class="am-u-sm-6 am-u-md-6 am-u-lg-3">
									<div style="text-align:center;">我还没有微信公众号</div>
									<div style="margin-top:20px;text-align:center;">
										<a class="am-btn am-btn-default am-btn-success" href="https://mp.weixin.qq.com" target="_blank">
                                            我去注册
                                        </a>
									</div>
								</div>
								<div style="" class="am-u-sm-6 am-u-md-6 am-u-lg-3">
								</div>
							</div>
						<?php endif; ?>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
