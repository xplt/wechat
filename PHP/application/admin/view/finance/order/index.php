<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">支付账务流水</div>
                </div>
                <div class="widget-body am-fr">
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>订单号</th>
								<th>事务ID</th>
                                <th>账务类型</th>
                                <th>扣付金额</th>
								<th>扣付状态</th>
								<th>款项用途</th>
								<th>扣付时间</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle">
                                        <p class="item-title"><?= $item['order_no']?></p>
                                    </td>
									<td class="am-text-middle"><?= $item['affair_id'] ?></td>
                                    <td class="am-text-middle"><?= $item['order_type']['text'] ?></td>
									<td class="am-text-middle"><?= $item['pay_price'] ?></td>
                                    <td class="am-text-middle"><?= $item['pay_status']['text'] ?></td>
									<td class="am-text-middle"><?= $item['purpose'] ?></td>
									<td class="am-text-middle"><?= $item['pay_time']?date('Y-m-d h:i:s',$item['pay_time']):'--' ?></td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="7" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

