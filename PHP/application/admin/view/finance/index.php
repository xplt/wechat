<div class="page-home row-content am-cf">
    <!-- 统计 -->
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12 am-margin-bottom">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title">财务统计</div>
                </div>
                <div class="widget-body am-cf">
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-3">
                        <div class="widget-card card__blue am-cf">
                            <div class="card-header">账户总额</div>
                            <div class="card-body">
                                <div class="card-value"><?=$count['store']['money']['web']?></div>
                                <div class="card-description">当前账户总额度</div>
                                <span class="card-icon iconfont iconcaiwuguanli"></span>
                            </div>
                        </div>
                    </div>

                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-3">
                        <div class="widget-card card__red am-cf">
                            <div class="card-header">商户总额</div>
                            <div class="card-body">
                                <div class="card-value"><?=$count['store']['money']['store']?></div>
                                <div class="card-description">当前商户充值总额度</div>
                                <span class="card-icon iconfont icondianpu"></span>
                            </div>
                        </div>
                    </div>

                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-3">
                        <div class="widget-card card__violet am-cf">
                            <div class="card-header">代理总额</div>
                            <div class="card-body">
                                <div class="card-value"><?=$count['store']['money']['agent']?></div>
                                <div class="card-description">当前代理分红总额度</div>
                                <span class="card-icon iconfont iconfenxiao"></span>
                            </div>
                        </div>
                    </div>

                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-3">
                        <div class="widget-card card__primary am-cf">
                            <div class="card-header">提现总额</div>
                            <div class="card-body">
                                <div class="card-value"><?=$count['order']['all']['take']?></div>
                                <div class="card-description">当前提现总额度</div>
                                <span class="card-icon iconfont iconqiandai"></span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
	<!-- 订单统计 -->
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12 am-margin-bottom">
            <div class="widget am-cf">
                <div class="widget-head">
                    <div class="widget-title">交易统计</div>
                </div>
                <div class="widget-body am-cf">
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日充值额度</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['today']['money']['recharge']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order']['today2']['money']['recharge']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月充值额度</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['month']['money']['recharge']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order']['month2']['money']['recharge']?></div>
                            </div>
                        </div>
                    </div>
                   <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日扣费额度</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['today']['money']['deduct']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order']['today2']['money']['deduct']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月扣费额度</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['month']['money']['deduct']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order']['month2']['money']['deduct']?></div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日分红额度</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['today']['money']['bonus']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order']['today2']['money']['bonus']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月分红额度</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['month']['money']['bonus']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order']['month2']['money']['bonus']?></div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日提现额度</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['today']['money']['take']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order']['today2']['money']['take']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月提现额度</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['month']['money']['take']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order']['month2']['money']['take']?></div>
                            </div>
                        </div>
                    </div>
					<div class="am-u-sm-2">
					</div>
					<div class="am-u-sm-2">
					</div>
                </div>
            </div>
        </div>
    </div>
	<div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="help-block am-text-center">
                <small><?= $web['name']?>&nbsp;&nbsp;版权所有</small>
            </div>
        </div>
    </div>
</div>