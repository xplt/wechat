<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf"><?= $title?></div>
                </div>
                <div class="widget-body am-fr">
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>WXAPP_ID</th>
                                <th>LOGO</th>
                                <th>名称</th>
                                <th>联系电话</th>
								<th>应用类型</th>
                                <th>授权状态</th>
								<th>所属代理</th>
								<th>注册来源</th>
								<th>自定版权</th>
								<th>到期时间</th>
                                <th>管理管理</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['wxapp_id'] ?></td>
                                    <td class="am-text-middle">
                                        <a href="<?= $item['head_img']?>" title="点击查看大图" target="_blank">
                                            <img src="<?= $item['head_img']?>" width="50" height="50" alt="小程序图片">
                                        </a>
                                    </td>
                                    <td class="am-text-middle">
                                        <p class="item-title"><?= $item['app_name']?$item['app_name']:'--' ?></p>
                                    </td>
                                    <td class="am-text-middle"><?= $item['phone']?$item['phone']:'--' ?></td>
									<td class="am-text-middle"><?= $item['app_type']['text'].' - '.$item['shop_mode']['text'] ?></td>
                                    <td class="am-text-middle">
										<span class="<?= $item['is_empower']['value'] === 0 ? 'x-color-red'
                                                : 'x-color-green' ?>">
                                            <?= $item['is_empower']['text'] ?>
                                         </span>
									</td>
									<td class="am-text-middle">
										<span class="<?= $item['agent_id'] ? 'x-color-red'
                                                : 'x-color-green' ?>">
                                            <?= $item['agent_id']?$item['agent_id']:'平台' ?>
                                         </span>
									</td>
									<td class="am-text-middle">
                                            <span class="j-source am-badge x-cur-p  <?= $item['source']['value'] == 20 ? ' am-badge-success'
                                                : ' am-badge-warning' ?>" 
                                                 data-ids="<?= $item['wxapp_id'] ?>"
                                                 data-status="<?= $item['source']['value'] ?>">
                                            <?= $item['source']['text'] ?>
                                            </span>
                                    </td>
									<td class="am-text-middle">
										<span class="j-copyright am-badge x-cur-p  <?= $item['is_copyright']['value'] == 1 ? ' am-badge-success'
											: ' am-badge-warning' ?>" 
											 data-ids="<?= $item['wxapp_id'] ?>"
											 data-status="<?= $item['is_copyright']['value'] ?>">
										<?= $item['is_copyright']['text'] ?>
										</span>
                                    </td>
									<td class="am-text-middle"><?= $item['expire_time']['text'] ?> </td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
											<a class="j-genewal tpl-table-black-operation-green"
                                                href="javascript:void(0);"
                                                data-id="<?= $item['wxapp_id'] ?>"
												data-expire="<?= $item['expire_time']['text'] ?>">
												<i class="am-icon-clock-o"></i> 续期
                                            </a>
											<a href="<?= url('wxapp/appLogin',['wxapp_id' => $item['wxapp_id']])?>" target="_blank">
                                                <i class="am-icon-user"></i> 登录
                                            </a>
                                            <a href="javascript:;" class="item-del tpl-table-black-operation-del"
                                               data-ids="<?= $item['wxapp_id'] ?>">
                                                <i class="am-icon-trash"></i> 删除
                                            </a>
                                            <?php if($item['new_tpl']):?>
                                            <a href="javascript:;" class="item-upgrade tpl-table-black-operation-primary"
                                               data-ids="<?= $item['wxapp_id'] ?>"
                                               data-status="<?= $item['new_tpl']['value'] ?>">
                                                <i class="am-icon-cloud-upload"></i> <?= $item['new_tpl']['text']?>
                                            </a>
                                            <?php endif;?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="11" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{include file="wxapp/tpl/tp_genewal" /}}
<script src="assets/plugins/vue/vue.min.js"></script>

<script>
    $(function () {
		
		/**
         * 变更到期时间操作
         */
        $('.j-genewal').on('click', function () {
            var data = $(this).data();
            $.showModal({
                title: '变更到期时间操作'
                , area: '460px'
                , content: template('tpl-genewal', data)
                , uCheck: true
                , success: function ($content) {
                }
                , yes: function ($content) {
                    $content.find('form').myAjaxSubmit({
                        url: "<?= url('wxapp/genewal')?>",
                        data: {
                            wxapp_id: data.id,
                        }
                    });
                    return true;
                }
            });
        });

		// 自定义版权设置
        $('.j-copyright').click(function () {
            var data = $(this).data();
			var msg = '确定要' + (parseInt(data.status) === 1 ? '关闭' : '开启') + '自定义版权？';
            var url = "<?= url('wxapp/copyright') ?>";
			$('.j-copyright').del('wxapp_id', url,msg);
                

        });
		
		// 切换注册来源
        $('.j-source').click(function () {
            var data = $(this).data();
			var msg = '确定要设置为'+(parseInt(data.status) === 10 ? '平台' : '自主')+'注册？';
            var url = "<?= url('wxapp/source') ?>";
			$('.j-source').del('wxapp_id', url,msg);
        });

        // 升级操作
        $('.item-upgrade').click(function () {
            var data = $(this).data();
            if(parseInt(data.status) === 0){
                layer.msg('该状态不允许操作', {time: 1500, anim: 6});
                return false;
            }
            var url = "<?= url('wxapp/upgrade') ?>";
            $('.item-upgrade').del('wxapp_id', url,'确定要发布新版本');
        });

        // 删除元素
        var url = "<?= url('wxapp/delete') ?>";
        $('.item-del').del('wxapp_id', url);

    });
</script>

