<div class="page-home row-content am-cf">
    <!-- 统计 -->
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12 am-margin-bottom">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title">站点统计</div>
                </div>
                <div class="widget-body am-cf">
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-3">
                        <div class="widget-card card__blue am-cf">
                            <div class="card-header">用户总量</div>
                            <div class="card-body">
                                <div class="card-value"><?=$count['store']['all']['all']?></div>
                                <div class="card-description">当前用户总数量</div>
                                <span class="card-icon iconfont iconyonghuguanli"></span>
                            </div>
                        </div>
                    </div>

                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-3">
                        <div class="widget-card card__red am-cf">
                            <div class="card-header">应用总量</div>
                            <div class="card-body">
                                <div class="card-value"><?=$count['wxapp']['all']['all']?></div>
                                <div class="card-description">当前小程序总数量</div>
                                <span class="card-icon iconfont iconweixinxiaochengxu"></span>
                            </div>
                        </div>
                    </div>

                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-3">
                        <div class="widget-card card__violet am-cf">
                            <div class="card-header">订单总量</div>
                            <div class="card-body">
                                <div class="card-value"><?=$count['order']['all']['all']?></div>
                                <div class="card-description">当前支付订单总数量</div>
                                <span class="card-icon iconfont icondingdanguanli"></span>
                            </div>
                        </div>
                    </div>

                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-3">
                        <div class="widget-card card__primary am-cf">
                            <div class="card-header">设备总量</div>
                            <div class="card-body">
                                <div class="card-value"><?=$count['printer']['all']['all']?></div>
                                <div class="card-description">当前打印机总数量</div>
                                <span class="card-icon iconfont icondayinji"></span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
	<!-- 订单统计 -->
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12 am-margin-bottom">
            <div class="widget am-cf">
                <div class="widget-head">
                    <div class="widget-title">订单统计</div>
                </div>
                <div class="widget-body am-cf">
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日充值订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['today']['recharge']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order']['today2']['recharge']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月充值订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['month']['recharge']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order']['month2']['recharge']?></div>
                            </div>
                        </div>
                    </div>
                   <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日扣费订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['today']['deduct']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order']['today2']['deduct']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月扣费订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['month']['deduct']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order']['month2']['deduct']?></div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日分红订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['today']['bonus']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order']['today2']['bonus']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月分红订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['month']['bonus']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order']['month2']['bonus']?></div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日提现订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['today']['take']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order']['today2']['take']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月提现订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order']['month']['take']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order']['month2']['take']?></div>
                            </div>
                        </div>
                    </div>
					<div class="am-u-sm-2">
					</div>
					<div class="am-u-sm-2">
					</div>
                </div>
            </div>
        </div>
    </div>

    <!-- 近七日走势 -->
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12 am-margin-bottom">
            <div class="widget am-cf">
                <div class="widget-head">
                    <div class="widget-title">近七日走势</div>
                </div>
                <div class="widget-body am-cf">
                    <div id="echarts-trade" class="widget-echarts"></div>
                </div>
            </div>
        </div>
    </div>
	<div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="help-block am-text-center">
                <small><?= $web['name']?>&nbsp;&nbsp;版权所有</small>
            </div>
        </div>
    </div>
</div>
<script src="assets/plugins/echarts/echarts.min.js"></script>
<script src="assets/plugins/echarts/echarts-walden.js"></script> 
<script type="text/javascript">

    /**
     * 近七日交易走势
     * @type {HTMLElement}
     */
    var dom = document.getElementById('echarts-trade');
    echarts.init(dom, 'walden').setOption({
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['用户量', '应用量', '订单量']
        },
        toolbox: {
            show: true,
            showTitle: false,
            feature: {
                mark: {show: true},
                magicType: {show: true, type: ['line', 'bar']}
            }
        },
        calculable: true,
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: ["<?= date("Y-m-d")?>","<?= date("Y-m-d",strtotime("-1 day"))?>","<?= date("Y-m-d",strtotime("-2 day"))?>","<?= date("Y-m-d",strtotime("-3 day"))?>","<?= date("Y-m-d",strtotime("-4 day"))?>","<?= date("Y-m-d",strtotime("-5 day"))?>","<?= date("Y-m-d",strtotime("-6 day"))?>"]        
		},
        yAxis: {
            type: 'value'
        },
        series: [
            {
                name: '用户量',
                type: 'line',
                data: [<?= $count['store']['today']['all'] ?>,<?= $count['store']['today2']['all'] ?>,<?= $count['store']['today3']['all'] ?>,<?= $count['store']['today4']['all'] ?>,<?= $count['store']['today5']['all'] ?>,<?= $count['store']['today6']['all'] ?>,<?= $count['store']['today7']['all'] ?>]
			},
            {
                name: '应用量',
                type: 'line',
                data: [<?= $count['wxapp']['today']['all'] ?>,<?= $count['wxapp']['today2']['all'] ?>,<?= $count['wxapp']['today3']['all'] ?>,<?= $count['wxapp']['today4']['all'] ?>,<?= $count['wxapp']['today5']['all'] ?>,<?= $count['wxapp']['today6']['all'] ?>,<?= $count['wxapp']['today7']['all'] ?>]
			},
            {
                name: '订单量',
                type: 'line',
                data: [<?= $count['order']['today']['all'] ?>,<?= $count['order']['today2']['all'] ?>,<?= $count['order']['today3']['all'] ?>,<?= $count['order']['today4']['all'] ?>,<?= $count['order']['today5']['all'] ?>,<?= $count['order']['today6']['all'] ?>,<?= $count['order']['today7']['all'] ?>]
			}
        ]
    }, true);

</script>