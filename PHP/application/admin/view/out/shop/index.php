<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">门店列表</div>
                </div>
                <div class="widget-body am-fr">
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>门店信息</th>
                                <th>联系方式</th>
                                <th>所属分类</th>
                                <th>门店状态</th>
                                <th>创建时间</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle">
                                        <div class="goods-detail">
                                            <div class="goods-image">
                                                <img src="<?= $item['logo']['file_path'] ?>" width="50" height="50" alt="门店图片">
                                            </div>
                                            <div class="goods-info dis-flex flex-dir-column flex-x-center">
                                                <p class="goods-title"><?= $item['shop_name'] ?></p>
                                                <p class="goods-title">ID：<?= $item['shop_id'] ?></p>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="am-text-middle">
                                        <p class="goods-title">联系人：<?= $item['linkman'] ?></p>
                                        <p class="goods-title">电 话 ：<?= $item['phone'] ?></p>
                                        <p class="goods-title">区 域 ：<?= $item['province'].'-'.$item['city'].'-'.$item['district'] ?></p>
                                    </td>
                                    <td class="am-text-middle"><?= $item['category']['name'] ?></td>
                                    <td class="am-text-middle">
                                            <span class="j-state am-badge x-cur-p  <?= $item['out_show']['value'] ? ' am-badge-success'
                                                : ' am-badge-warning' ?>" 
                                                 data-ids="<?= $item['shop_id'] ?>"
                                                 data-status="<?= $item['out_show']['value'] ?>">
                                            <?= $item['out_show']['text'] ?>
                                            </span>
                                    </td>
                                    <td class="am-text-middle"><?= $item['create_time'] ?></td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="5" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        
        // 切换状态
        $('.j-state').click(function () {
            var data = $(this).data();
            var msg = '确定要在平台'+(parseInt(data.status) === 1 ? '隐藏' : '显示')+'吗？';
            var url = "<?= url('out.shop/show') ?>";
            $('.j-state').del('shop_id', url,msg);
        });

    });
</script>

