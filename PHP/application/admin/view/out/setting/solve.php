<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">订单佣金结算设置</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">结算模式 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="solve[mode]" value="ratio" data-am-ucheck
                                            <?= $values['mode'] == 'ratio' ? 'checked':'' ?>>
                                        比例提佣
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="solve[mode]" value="fixed" data-am-ucheck 
                                        <?= $values['mode'] == 'fixed' ? 'checked':'' ?>>
                                        固定金额
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 数值 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" min="0" class="tpl-form-input" name="solve[value]"
                                           value="<?= $values['value']?>" required>
                                    <small>设置为0则不收取佣金。当设置为10时，如设置为“比例提佣”则为提取订单金额的10‰（千分之），如设置为“固定金额”则为10元</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#my-form').formPost();
    });
</script>
