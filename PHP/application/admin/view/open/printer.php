<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" method="post">
                    <div class="widget-body">
                        <fieldset>
							<div class="widget-head am-cf">
                                <div class="widget-title am-fl">对对机开放平台对接</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    请求接口 <span class="tpl-form-line-small-title">APIURL</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="printer[ddj][api_url]"
                                           value="<?= $values['ddj']['api_url']?>">
                                    <small>默认API接口：https://www.open.mstching.com</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    开发者ID <span class="tpl-form-line-small-title">APPID</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="printer[ddj][app_key]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['ddj']['app_key']?>">
									<small>我还没有开发者账号 <a href="http://www.mstching.com/home/open"  target="_blank">去注册</a></small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    开发者KEY <span class="tpl-form-line-small-title">SECRET</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="printer[ddj][app_secret]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['ddj']['app_secret']?>">
                                </div>
                            </div>
							<div class="widget-head am-cf">
                                <div class="widget-title am-fl">飞鹅开放平台对接</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    请求接口 <span class="tpl-form-line-small-title">APIURL</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="printer[feie][api_url]"
                                           value="<?= $values['feie']['api_url']?>">
                                    <small>默认API接口：http://api.feieyun.cn/Api/Open/</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    开发者ID <span class="tpl-form-line-small-title">USER</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="printer[feie][app_key]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['feie']['app_key']?>">
									<small>我还没有开发者账号 <a href="http://www.feieyun.com/open/"  target="_blank">去注册</a></small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    开发者KEY <span class="tpl-form-line-small-title">UKEY</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="printer[feie][app_secret]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['feie']['app_secret']?>">
                                </div>
                            </div>
							<div class="widget-head am-cf">
                                <div class="widget-title am-fl">易联云开放平台对接</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    请求接口 <span class="tpl-form-line-small-title">APIURL</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="printer[yilian][api_url]"
                                           value="<?= $values['yilian']['api_url']?>">
                                    <small>默认API接口：https://open-api.10ss.net/</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    应用ID <span class="tpl-form-line-small-title">APPID</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="printer[yilian][app_key]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['yilian']['app_key']?>">
									<small>我还没有开发者账号 <a href="https://dev.yilianyun.net/"  target="_blank">去注册</a> -> 创建“自有应用”</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    应用密钥 <span class="tpl-form-line-small-title">SECRET</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="printer[yilian][app_secret]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['yilian']['app_secret']?>">
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    TOKEN <span class="tpl-form-line-small-title">AccessToken</span>
                                </label>
                                <div class="am-u-sm-9">
									<input name="printer[yilian][access_token]" type="hidden" 
										value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['yilian']['access_token']?>">
                                    <input type="text" class="tpl-form-input" 
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['yilian']['access_token']?>" disabled>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {

        $('#my-form').formPost();

    });
</script>
