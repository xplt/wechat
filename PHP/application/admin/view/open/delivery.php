<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
							<div class="widget-head am-cf">
                                <div class="widget-title am-fl">UU配送开放平台对接</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    请求接口 <span class="tpl-form-line-small-title">API_URL</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="delivery[uu][api_url]"
                                           value="<?= $values['uu']['api_url']?>">
								</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    应用ID <span class="tpl-form-line-small-title">APP_ID</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="delivery[uu][app_id]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['uu']['app_id']?>">
									<small>我还没有 <a href="https://open.uupt.com/"  target="_blank">注册</a></small>
								</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    应用密钥 <span class="tpl-form-line-small-title">APP_KEY</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="delivery[uu][app_key]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['uu']['app_key']?>">
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    账户ID <span class="tpl-form-line-small-title">OPEN_ID</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="delivery[uu][open_id]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['uu']['open_id']?>">
                                </div>
                            </div>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">顺丰同城开放平台对接</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    请求接口 <span class="tpl-form-line-small-title">API_URL</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="delivery[sf][api_url]"
                                           value="<?= $values['sf']['api_url']?>">
								</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    开发者ID <span class="tpl-form-line-small-title">DevId</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="delivery[sf][app_key]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['sf']['app_key']?>">
									<small>我还没有开发者账号 <a href="http://commit-openic.sf-express.com/"  target="_blank">注册</a></small>
								</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    开发者KEY <span class="tpl-form-line-small-title">DevKey</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="delivery[sf][app_secret]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['sf']['app_secret']?>">
                                </div>
                            </div>
							<div class="widget-head am-cf">
                                <div class="widget-title am-fl">达达配送开放平台对接</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    请求接口 <span class="tpl-form-line-small-title">API_URL</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="delivery[dada][api_url]"
                                           value="<?= $values['dada']['api_url']?>">
								</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    开发者ID <span class="tpl-form-line-small-title">AppKey</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="delivery[dada][app_key]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['dada']['app_key']?>">
									<small>我还没有开发者账号 <a href="https://newopen.imdada.cn/"  target="_blank">注册</a></small>
								</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    开发者KEY <span class="tpl-form-line-small-title">AppSecret</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="delivery[dada][app_secret]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['dada']['app_secret']?>">
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    商户号 <span class="tpl-form-line-small-title">SourceId</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="delivery[dada][source_id]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['dada']['source_id']?>">
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        $('#my-form').formPost();
    });
</script>
