<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" method="post">
                    <div class="widget-body">
                         <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">微信开放平台网站应用 - 参数对接</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require"> AppID </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="wxweb[app_id]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['app_id'] ?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require"> AppSecret </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="wxweb[app_secret]"
                                           value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['app_secret'] ?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {

        $('#my-form').formPost();

    });
</script>

