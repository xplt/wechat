<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">用户提现申请记录</div>
                </div>
                <div class="widget-body am-fr">
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>订单号</th>
                                <th>提现金额</th>
								<th>提现状态</th>
                                <th>事务ID</th>
								<th>申请时间</th>
                                <th>管理操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle">
                                        <p class="item-title"><?= $item['order_no']?></p>
                                    </td>
									<td class="am-text-middle"><?= $item['pay_price'] ?></td>
                                    <td class="am-text-middle"><?= $item['pay_status']['text'] ?></td>
                                    <td class="am-text-middle"><?= $item['affair_id'] ?></td>
									<td class="am-text-middle"><?= $item['create_time'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="javascript:;" class="item-cash tpl-table-black-operation-green"
                                               data-ids="<?= $item['web_order_id'] ?>">
                                                <i class="am-icon-info"></i> 同意申请
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="6" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        // 删除元素
        var url = "<?= url('apply.order/cash') ?>";
        $('.item-cash').del('web_order_id', url,'确认同意用户的提现申请？');

    });
</script>

