<div class="row-content am-cf">
    <div class="row layui-form layui-form-pane">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">实名认证列表</div>
                </div>
                <div class="widget-body am-fr">
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>认证编号</th>
                                <th>工商资料</th>
                                <th>个人资料</th>
                                <th>联系方式</th>
                                <th>认证类型</th>
                                <th>申请状态</th>
                                <th>申请时间</th>
                                <th>管理操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['store_apply_id'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="goods-detail">
                                            <div class="goods-image">
                                                <a href="<?= $item['details']['license_copy']['url']?>" target="_blank">
                                                <img src="<?= $item['details']['license_copy']['url']?>" alt="">
                                                </a>
                                            </div>
                                            <div class="goods-info dis-flex flex-dir-column flex-x-center">
                                                <p class="goods-title"><?= $item['details']['merchant_name'] ?: '--'?></p>
                                                <p class="goods-title">代码：<?= $item['details']['license_number'] ?: '--'?></p>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="am-text-middle">
                                        <div class="goods-detail">
                                            <div class="goods-image">
                                            <a href="<?= $item['details']['license_copy']['url']?>" target="_blank">
                                                <img src="<?= $item['details']['id_card_copy']['url']?>" alt="">
                                            </a>
                                            </div>
                                            <div class="goods-info dis-flex flex-dir-column flex-x-center">
                                                <p class="goods-title"><?= $item['details']['id_card_name'] ?: '--'?></p>
                                                <p class="goods-title">号码：<?= $item['details']['id_card_number'] ?: '--'?></p>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="am-text-middle">
                                        <div class="goods-detail">
                                            <div class="goods-info dis-flex flex-dir-column flex-x-center">
                                                <p class="goods-title">会员ID：<?= $item['store_user_id'] ?: '--'?></p>
                                                <p class="goods-title">手机：<?= $item['details']['mobile_phone'] ?: '--'?></p>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="am-text-middle"><?= $item['auth_mode']['text'] ?></td>
                                    <td class="am-text-middle"><?= $item['apply_status']['text'] ?></td>
                                    <td class="am-text-middle"><?= $item['create_time'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a class=" tpl-table-black-operation-green"  href="<?= url('apply/detail',['store_apply_id' => $item['store_apply_id']])?>">
                                                <i class="am-icon-info"></i> 认证
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="8" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>