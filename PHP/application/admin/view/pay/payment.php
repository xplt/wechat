<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">微信支付设置</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">
                                    商户号 <span class="tpl-form-line-small-title">MCH_ID</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="payment[wx][mchid]"
									value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['wx']['mchid'] ?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">
                                    支付密钥 <span class="tpl-form-line-small-title">API_KEY</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="payment[wx][apikey]"
									value="<?= $admin['user']['store_user_id']==0?'*****************************':$values['wx']['apikey'] ?>" required>
                                </div>
                            </div>
							<div class="am-form-group am-padding-top">
								<label class="am-u-sm-3 am-form-label">
									apiclient_cert.pem
								</label>
								<div class="am-u-sm-9">
									 <textarea rows="3" name="payment[wx][cert_pem]"><?= $admin['user']['store_user_id']==0?'*****************************':$values['wx']['cert_pem'] ?></textarea>
									<small>退款/提现功能必填：使用文本编辑器打开apiclient_cert.pem文件，将文件的全部内容复制进来</small>
								</div>
							</div>
							<div class="am-form-group">
								<label class="am-u-sm-3 am-form-label">
									apiclient_key.pem
								</label>
								<div class="am-u-sm-9">
									 <textarea rows="3" name="payment[wx][key_pem]"><?= $admin['user']['store_user_id']==0?'*****************************':$values['wx']['key_pem'] ?></textarea>
									<small>退款/提现功能必填：使用文本编辑器打开apiclient_key.pem文件，将文件的全部内容复制进来</small>
								</div>
							</div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">用户提现手续费模式 </label>
                                <div class="am-u-sm-9">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="payment[wx][cash_mode]" value="ratio" data-am-ucheck
                                            <?= $values['wx']['cash_mode'] == 'ratio' ? 'checked':'' ?>>
                                        比例提取
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="payment[wx][cash_mode]" value="fixed" data-am-ucheck 
                                        <?= $values['wx']['cash_mode'] == 'fixed' ? 'checked':'' ?>>
                                        固定金额
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require"> 手续费数值 </label>
                                <div class="am-u-sm-9">
                                    <input type="number" min="0" class="tpl-form-input" name="payment[wx][cash_fee]"
                                           value="<?= $values['wx']['cash_fee']?>" required>
                                    <small>设置为0则不收取手续费。当设置为10时，如设置为“比例提取”则为提取订单金额的10‰（千分之），如设置为“固定金额”则为10元</small>
                                </div>
                            </div>

                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {

        $('#my-form').formPost();

    });
</script>
