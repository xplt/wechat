<script src="assets/plugins/layui/layui.js"></script>
<div class="row-content am-cf">
    <div class="row layui-form layui-form-pane">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">商户列表</div>
                </div>
                <div class="widget-body am-fr">
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>商户ID</th>
                                <th>微信头像</th>
                                <th>微信昵称</th>
								<th>公众号</th>
								<th>钱包余额</th>
                                <th>应用数量</th>
                                <th>性别</th>
                                <th>国家</th>
                                <th>省份</th>
                                <th>城市</th>
								<th>注册时间</th>
								<th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['store_user_id'] ?></td>
                                    <td class="am-text-middle">
                                        <a href="<?= $item['avatarUrl'] ? $item['avatarUrl'] : 'assets/store/img/head-no.png' ?>" title="点击查看大图" target="_blank">
                                            <img src="<?= $item['avatarUrl'] ? $item['avatarUrl'] : 'assets/store/img/head-no.png' ?>" width="72" height="72" alt="">
                                        </a>
                                    </td>
                                    <td class="am-text-middle"><?= $item['nickName'] ?: '--' ?></td>	
                                    <td class="am-text-middle">
                                        <input type="checkbox" <?= $item['wechat_open_id']? ' checked ' : ' ' ?> lay-skin="switch" disabled>
                                    </td>
									<td class="am-text-middle"><?= $item['wallet'] ?></td>
									<td class="am-text-middle"><?= $item['wxapp'] ?>个</td>
                                    <td class="am-text-middle"><?= $item['gender'] ?></td>
                                    <td class="am-text-middle"><?= $item['country'] ?: '--' ?></td>
                                    <td class="am-text-middle"><?= $item['province'] ?: '--' ?></td>
                                    <td class="am-text-middle"><?= $item['city'] ?: '--' ?></td>
									<td class="am-text-middle"><?= $item['create_time'] ?></td>
									<td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <?php if($item['details']):?>
                                            <a class=" tpl-table-black-operation-green"  href="<?= url('user/details',['store_detail_id' => $item['details']['store_detail_id']])?>" target="_blank">
                                                <i class="am-icon-info"></i> 详情
                                            </a>
                                            <?php endif;?>
											<a href="<?= url('user/login',['store_user_id' => $item['store_user_id']])?>" target="_blank">
                                                <i class="am-icon-user"></i> 登录
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="12" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    layui.use(['form']);
</script>