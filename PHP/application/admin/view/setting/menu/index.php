<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">前端菜单</div>
                </div>
                <div class="widget-body am-fr">
					<div class="tips am-margin-bottom-sm am-u-sm-12">
                        <div class="pre">
                            <p> 页面别名：为模板页名称，不能随便修改</p>
                        </div>
                    </div>
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-6">
                        <div class="am-form-group">
                            <div class="am-btn-toolbar">
                                <div class="am-btn-group am-btn-group-xs">
                                    <a class="am-btn am-btn-default am-btn-success am-radius"
                                       href="<?= url('setting.menu/add') ?>">
                                        <span class="am-icon-plus"></span> 新增
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black ">
                            <thead>
                            <tr>
                                <th>菜单ID</th>
                                <th>菜单名称</th>
                                <th>排序</th>
								<th>状态</th>
                                <th>添加时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($list)): foreach ($list as $first): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $first['web_menu_id'] ?></td>
                                    <td class="am-text-middle"><?= $first['name'] ?></td>
                                    <td class="am-text-middle"><?= $first['sort'] ?></td>
									<td class="am-text-middle">
                                        <span class="j-status am-badge x-cur-p  <?= $first['status']['value'] == 10 ? ' am-badge-success'
                                        : ' am-badge-warning' ?>" 
                                         data-ids="<?= $first['web_menu_id'] ?>"
                                         data-status="<?= $first['status']['value'] ?>">
                                            <?= $first['status']['text'] ?>
                                        </span>
                                    </td>
                                    <td class="am-text-middle"><?= $first['create_time'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="<?= url('setting.menu/edit',
                                                ['web_menu_id' => $first['web_menu_id']]) ?>">
                                                <i class="am-icon-pencil"></i> 编辑
                                            </a>
                                            <a href="javascript:;" class="item-del tpl-table-black-operation-del"
                                               data-ids="<?= $first['web_menu_id'] ?>">
                                                <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <?php if (isset($first['child'])): foreach ($first['child'] as $two): ?>
                                    <tr>
                                        <td class="am-text-middle"><?= $two['web_menu_id'] ?></td>
                                        <td class="am-text-middle">　-- <?= $two['name'] ?></td>
                                        <td class="am-text-middle"><?= $two['sort'] ?></td>
										<td class="am-text-middle">
                                            <span class="j-status am-badge x-cur-p  <?= $two['status']['value'] == 10 ? ' am-badge-success'
                                        : ' am-badge-warning' ?>" 
                                         data-ids="<?= $two['web_menu_id'] ?>"
                                         data-status="<?= $two['status']['value'] ?>">
                                                <?= $two['status']['text'] ?>
                                            </span>
										</td>
                                        <td class="am-text-middle"><?= $two['create_time'] ?></td>
                                        <td class="am-text-middle">
                                            <div class="tpl-table-black-operation">
                                                <a href="<?= url('setting.menu/edit',
                                                    ['web_menu_id' => $two['web_menu_id']]) ?>">
                                                    <i class="am-icon-pencil"></i> 编辑
                                                </a>
                                                <a href="javascript:;" class="item-del tpl-table-black-operation-del"
                                                   data-ids="<?= $two['web_menu_id'] ?>">
                                                    <i class="am-icon-trash"></i> 删除
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php if (isset($two['child'])): foreach ($two['child'] as $three): ?>
                                        <tr>
                                            <td class="am-text-middle"><?= $three['web_menu_id'] ?></td>
                                            <td class="am-text-middle">　　　-- <?= $three['name'] ?></td>
											<td class="am-text-middle">
                                                <span class="j-status am-badge x-cur-p  <?= $three['status']['value'] == 10 ? ' am-badge-success'
                                        : ' am-badge-warning' ?>" 
                                         data-ids="<?= $three['web_menu_id'] ?>"
                                         data-status="<?= $three['status']['value'] ?>">
                                                    <?= $three['status']['text'] ?>
                                                </span>
											</td>
                                            <td class="am-text-middle"><?= $three['create_time'] ?></td>
                                            <td class="am-text-middle">
                                                <div class="tpl-table-black-operation">
                                                    <a href="<?= url('setting.menu/edit',
                                                        ['web_menu_id' => $three['web_menu_id']]) ?>">
                                                        <i class="am-icon-pencil"></i> 编辑
                                                    </a>
                                                    <a href="javascript:;" class="item-del tpl-table-black-operation-del"
                                                       data-ids="<?= $three['web_menu_id'] ?>">
                                                        <i class="am-icon-trash"></i> 删除
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; endif; ?>
                                <?php endforeach; endif; ?>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="6" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        // 切换状态
        $('.j-status').click(function () {
            var data = $(this).data();
            var msg = '确定要'+(parseInt(data.status) === 1 ? '隐藏' : '显示')+'该菜单吗？';
            var url = "<?= url('setting.menu/status') ?>";
            $('.j-status').del('web_menu_id', url,msg);
        });
        // 删除元素
        var url = "<?= url('setting.menu/delete') ?>";
        $('.item-del').del('web_menu_id', url);
    });
</script>

