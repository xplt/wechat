<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">添加菜单</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">菜单名称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="menu[name]"
                                           value="" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">上级菜单 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="menu[parent_id]"
                                            data-am-selected="{searchBox: 1, btnSize: 'sm'}">
                                        <option value="0">顶级菜单</option>
                                        <?php if (isset($list)): foreach ($list as $first): ?>
                                            <option value="<?= $first['web_menu_id'] ?>">
                                                <?= $first['name'] ?></option>
                                        <?php endforeach; endif; ?>
                                    </select>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">页面标题 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="menu[title]"
                                           value="" required>
                                </div>
                            </div>
							<div class="am-form-group">
								<label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">模板名称 </label>
								<div class="am-u-sm-9 am-u-end">
									<input type="text" class="tpl-form-input" name="menu[key]"
										   value="" required>
									<small>二级栏目取名规则：上级名称_模板名称</small>
								</div>
							</div>
							<div class="am-form-group">
								<label class="am-u-sm-3 am-u-lg-2 am-form-label">链接地址 </label>
								<div class="am-u-sm-9 am-u-end">
									<input type="text" class="tpl-form-input" name="menu[url]"
										   value="" >
									<small>链接到外部页面必填，站内留空</small>
								</div>
							</div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">页面描述 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="menu[description]"
                                           value="" >
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">SEO关键字 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <textarea class="" rows="3" placeholder="请输入SEO关键字"
                                              name="menu[keywords]"></textarea>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">菜单排序 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" class="tpl-form-input" name="menu[sort]"
                                           value="100" required>
                                    <small>数字越小越靠前</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {

        $('#my-form').formPost();

    });
</script>
