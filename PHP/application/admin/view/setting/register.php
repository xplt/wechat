<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" method="post">
                    <div class="widget-body">
                        <fieldset>
							<div class="widget-head am-cf">
                                <div class="widget-title am-fl">注册设置</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">账号前缀 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="register[prefix]"
                                           value="<?= $values['prefix'] ?>" required>
                                    <small>商户账号自动生成，账号前是否添加该字符串（必须为数字或字母），规则：前缀+商户ID号</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">注册小程序审核费 </label>
                                <div class="am-u-sm-9">
                                    <input type="number" min="0" class="tpl-form-input" name="register[examine]"
                                           value="<?= $values['examine'] ?>" required>
									<small>用户通过快速通道注册小程序是否收取审核费，设置0为不收取。必须是整数，单位元。</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">注册小程序免审核 </label>
                                <div class="am-u-sm-9">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="register[wxapp_auth]" value="0" data-am-ucheck 
                                        <?= $values['wxapp_auth'] == 0?'checked':''?>>
                                        关闭
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="register[wxapp_auth]" value="1" data-am-ucheck 
                                        <?= $values['wxapp_auth'] == 1?'checked':''?>>
                                        开启
                                    </label>
                                    <div class="help-block">
                                        <small>关闭后需要超管端审核提审</small>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">平台商家入驻免审核 </label>
                                <div class="am-u-sm-9">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="register[out_auth]" value="0" data-am-ucheck 
                                        <?= $values['out_auth'] == 0?'checked':''?>>
                                        关闭
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="register[out_auth]" value="1" data-am-ucheck 
                                        <?= $values['out_auth'] == 1?'checked':''?>>
                                        开启
                                    </label>
                                    <div class="help-block">
                                        <small>关闭后需要超管端审核提审</small>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        $('#my-form').formPost();

    });
</script>
