<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-4 am-u-md-4 am-u-lg-4">
			<div class="widget am-cf">
				<div class="widget-body">
					<fieldset>
						<div class="widget-head am-cf">
							<div class="widget-title am-fl">授权信息</div>
						</div>
						<div class="am-form-group">
							<div class="am-scrollable-horizontal">
								当前域名：<?= isset($hema['authorize']['web_domain'])?$hema['authorize']['web_domain']:'--'?><br>
								是否授权：<?= isset($hema['authorize']['status'])?$hema['authorize']['status']:'--'?><br>
								到期时间：<?= isset($hema['authorize']['expires_in'])?$hema['authorize']['expires_in']:'--'?><br>
								搭建时间：<?= isset($hema['authorize']['create_time'])?$hema['authorize']['create_time']:'--'?>
							</div>
						</div>
					</fieldset>
				</div>
			</div>			
		</div>
		<div class="am-u-sm-4 am-u-md-4 am-u-lg-4">
			<div class="widget am-cf">
				<div class="widget-body">
					<fieldset>
						<div class="widget-head am-cf">
							<div class="widget-title am-fl">联系我们</div>
						</div>
						<div class="am-form-group">
							<div class="am-scrollable-horizontal">
								联系电话：<?= isset($hema['contact']['tel'])?$hema['contact']['tel']:'--'?><br>
								授权购买：<?= isset($hema['contact']['qq'])?$hema['contact']['qq']:'--'?><br>
								技术QQ群：<?= isset($hema['contact']['qun'])?$hema['contact']['qun']:'--'?> <br><br>
							</div>
						</div>
					</fieldset>
				</div>
			</div>			
		</div>
		<div class="am-u-sm-4 am-u-md-4 am-u-lg-4">
			<div class="widget am-cf">
				<div class="widget-body">
					<fieldset>
						<div class="widget-head am-cf">
							<div class="widget-title am-fl">最新版本</div>
						</div>
						<div class="am-form-group"> 
							<div class="am-scrollable-horizontal">
								站点版本：<?= isset($hema['version']['web'])?$hema['version']['web']:'--'?><br>
								接口版本：<?= isset($hema['version']['api'])?$hema['version']['api']:'--'?><br>
								助手版本：<?= isset($hema['version']['store'])?$hema['version']['store']:'--'?> <br>
								<br>
							</div>
						</div>
					</fieldset>
				</div>
			</div>			
		</div>
	</div>
</div>
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
				<div class="widget-body">
					<fieldset>
						<div class="widget-head am-cf">
							<div class="widget-title am-fl">服务器信息</div>
						</div>
						<div class="am-form-group">
							<div class="am-scrollable-horizontal">
								<table class="am-table am-table-centered">
									<tbody>
									<tr>
										<th width="30%">参数</th>
										<th>值</th>
										<th></th>
									</tr>
									<?php if (isset($server)): foreach ($server as $item): ?>
										<tr class="<?= isset($statusClass) ? $statusClass[$item['status']] : '' ?>">
											<td><?= $item['name'] ?></td>
											<td><?= $item['value'] ?> </td>
											<td><?= $item['status'] !== 'normal' ? $item['remark'] : '' ?> </td>
										</tr>
									<?php endforeach; endif; ?>
									</tbody>
								</table>
							</div>
						</div>

						<div class="widget-head am-cf">
							<div class="widget-title am-fl">PHP环境要求</div>
						</div>
						<div class="am-form-group">
							<div class="am-scrollable-horizontal">
								<table class="am-table am-table-centered">
									<tbody>
									<tr>
										<th width="30%">选项</th>
										<th>要求</th>
										<th>状态</th>
										<th></th>
									</tr>
									<?php if (isset($phpinfo)): foreach ($phpinfo as $item): ?>
										<tr class="<?= isset($statusClass) ? $statusClass[$item['status']] : '' ?>">
											<td><?= $item['name'] ?></td>
											<td><?= $item['value'] ?> </td>
											<td>
												<?php if ($item['status'] !== 'danger'): ?>
													<i class="am-icon-check x-color-green"></i>
												<?php else: ?>
													<i class="am-icon-times x-color-red"></i>
												<?php endif; ?>
											</td>
											<td><?= $item['status'] !== 'normal' ? $item['remark'] : '' ?> </td>
										</tr>
									<?php endforeach; endif; ?>
									</tbody>
								</table>
							</div>
						</div>

						<div class="widget-head am-cf">
							<div class="widget-title am-fl">目录权限监测</div>
						</div>
						<div class="am-form-group">
							<div class="am-scrollable-horizontal">
								<table class="am-table am-table-centered">
									<tbody>
									<tr>
										<th width="30%">名称</th>
										<th class="am-text-left">路径</th>
										<th>状态</th>
										<th></th>
									</tr>
									<?php if (isset($writeable)): foreach ($writeable as $item): ?>
										<tr class="<?= isset($statusClass) ? $statusClass[$item['status']] : '' ?>">
											<td><?= $item['name'] ?></td>
											<td class="am-text-left"><?= $item['value'] ?> </td>
											<td>
												<?php if ($item['status'] !== 'danger'): ?>
													<i class="am-icon-check x-color-green"></i>
												<?php else: ?>
													<i class="am-icon-times x-color-red"></i>
												<?php endif; ?>
											</td>
											<td><?= $item['status'] !== 'normal' ? $item['remark'] : '' ?> </td>
										</tr>
									<?php endforeach; endif; ?>
									</tbody>
								</table>
							</div>
						</div>

					</fieldset>
				</div>
            </div>
        </div>
    </div>
</div>
