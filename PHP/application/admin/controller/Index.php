<?php
namespace app\admin\controller;
use app\admin\model\Wxapp as WxappModel;
use app\admin\model\StoreUser as StoreUserModel;
use app\admin\model\WebOrder as WebOrderModel;

/**
 * 后台首页
 */
class Index extends Controller
{
    /**
     * 首页统计
     */
    public function index()
    {
		$count = array();
		$count['wxapp'] = WxappModel::getCount();		//小程序
		$count['store'] = StoreUserModel::getCount();	//用户统计
		$count['order'] = WebOrderModel::getCount();	//订单统计
		$count['printer'] = 0;	//打印机统计
		return $this->fetch('index', compact('count'));
    }

}
