<?php
namespace app\admin\controller;
use app\admin\model\StoreUser as StoreUserModel;
use app\admin\model\StoreDetail as StoreDetailModel;
use think\Session;

/**
 * 商户管理控制器
 */
class User extends Controller
{
    /**
     * 会员列表
     */
    public function index()
    {
		$model = new StoreUserModel;
		$list = $model->getList();
		return $this->fetch('index', compact('list'));
    }
    /**
     * 商户列表
     */
    public function store()
    {
		$model = new StoreUserModel;
		$list = $model->getList('store');
		return $this->fetch('store', compact('list'));
    }
    /**
     * 代理列表
     */
    public function agent()
    {
		$model = new StoreUserModel;
		$list = $model->getList('agent');
		return $this->fetch('agent', compact('list'));
    }

    /**
     * 用户资料
     */
    public function auth()
    {
		$model = new StoreDetailModel;
		$list = $model->getList();
		return $this->fetch('auth', compact('list'));
    }
	
	/**
     * 一键登录
     */
    public function login($store_user_id,$is_agent='')
    {
		//全局判断
		if($err = checking()){
			return $this->error($err);
		}
		if(empty($is_agent)){
			$name = 'hema_store';
			$url = '/user.php';
		}else{
			$name = 'hema_agent';
			$url = '/agent.php';
		}
		$user = StoreUserModel::detail($store_user_id);
		Session::set($name, [
			'user' => [
				'store_user_id' => $user['store_user_id'],
				'user_name' => $user['user_name'],
				'agent_id' => $user['agent_id'],
                'avatarUrl' => $user['avatarUrl']
			],
			'is_login' => true,
			'is_admin' => true,
		]);
		$this->redirect($url);
    }
	
	/**
     * 更新当前管理员信息
     */
    public function renew()
    {
		//全局判断
		if($err = checking()){
			return $this->error($err);
		}
        $model = StoreUserModel::detail($this->admin_id);
        if (!$this->request->isAjax()) {
			return $this->fetch('renew', compact('model'));            
        }
		if ($model->renew($this->postData('user'))) {
			return $this->renderSuccess('更新成功');
		}
		return $this->renderError($model->getError() ?: '更新失败');
    }

    /**
     * 代理切换
     */
    public function status($store_user_id)
    {
		//全局判断
		if($err = checking()){
			return $this->renderError($err);
		}
        //详情
        $model = StoreUserModel::detail($store_user_id);
		if($model['is_agent']['value']==2){
			return $this->renderError('管理身份不可切换');
		}
		$model->is_agent['value']==1 ? $model->is_agent = 0 : $model->is_agent = 1;
		$model->save();
        return $this->renderSuccess('操作成功');
    }
}
