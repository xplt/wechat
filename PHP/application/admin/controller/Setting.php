<?php
namespace app\admin\controller;
use app\admin\model\WebSet as WebSetModel;

/**
 * 站点配置
 */
class Setting extends Controller
{
	/**
     * 站点设置
     */
    public function web()
    {
        return $this->updateEvent('web');
    }

	/**
     * 商户手机端设置
     */
    public function wxapp()
    {
        return $this->updateEvent('wxapp');
    }

    /**
     * 注册设置
     */
    public function register()
    {
        return $this->updateEvent('register');
    }

    /**
     * 更新设置事件
     */
    private function updateEvent($key)
    {
        if (!$this->request->isAjax()) {
            $values = WebSetModel::getItem($key);
            return $this->fetch($key, compact('values'));
        }
		//全局判断
		if($err = checking()){
			return $this->renderError($err);
		}
        $model = new WebSetModel;
        if ($model->edit($key,$this->postData($key))) {
            return $this->renderSuccess('更新成功');
        }
        return $this->renderError('更新失败');
    }

}
