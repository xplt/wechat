<?php
namespace app\admin\controller;
use app\admin\model\WebSet as WebSetModel;

/**
 * 微信支付配置
 */
class Pay extends Controller
{
    /**
     * 支付设置
     */
    public function payment()
    {
        return $this->updateEvent('payment');
    }

    /**
     * 微信支付服务商设置
     */
    public function wxpay()
    {
        return $this->updateEvent('wxpay');
    }

    /**
     * 更新设置事件
     */
    private function updateEvent($key)
    {
        if (!$this->request->isAjax()) {
            $values = WebSetModel::getItem($key);
            return $this->fetch($key, compact('values'));
        }
        //全局判断
        if($err = checking()){
            return $this->renderError($err);
        }
        $model = new WebSetModel;
        if($model->edit($key, $this->postData($key))) {
            return $this->renderSuccess('设置成功');
        }
        return $this->renderError('设置失败');
    }

}
