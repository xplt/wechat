<?php
namespace app\admin\controller;
use app\admin\model\Wxapp as WxappModel;
use think\Cache;

/**
 * 商户管理控制器
 */
class Wxapp extends Controller
{
    /**
     * 全部小程序列表
     */
    public function index()
    {
        $model = new WxappModel;
        $list = $model->getList();
		$title = '全部小程序列表';
        return $this->fetch('index', compact('list','title'));
    }
	
	/**
     * 已授权列表
     */
    public function normal()
    {
        $model = new WxappModel;
        $list = $model->getList(0,0,1);
        $title = '已授权小程序列表';
        return $this->fetch('index', compact('list','title'));
    }
	
	/**
     * 将到期列表
     */
    public function warn()
    {
        $model = new WxappModel;
        $list = $model->getList(0,1,1);
        $title = '将到期小程序列表';
        return $this->fetch('index', compact('list','title'));
    }
	
	/**
     * 已到期列表
     */
    public function ends()
    {
        $model = new WxappModel;
        $list = $model->getList(0,2,1);
        $title = '已到期小程序列表';
        return $this->fetch('index', compact('list','title'));
    }

    /**
     * 删除
     */
    public function delete($wxapp_id)
    {
		//全局判断
		if($err = checking()){
			return $this->renderError($err);
		}
        $model = new WxappModel;
         if ($model->deleteBatch($wxapp_id)) {
			return $this->renderSuccess('删除成功');
        }
		$error = $model->getError() ?: '删除失败';
		return $this->renderError($error);
    }
	
	/**
     * 自定义小程序版权开关
     */
    public function copyright($wxapp_id)
    {
		//全局判断
		if($err = checking()){
			return $this->renderError($err);
		}
        //详情
        $model = WxappModel::getWxapp(['wxapp_id' => $wxapp_id]);
		$model->is_copyright['value']==0 ? $model->is_copyright = 1 : $model->is_copyright = 0;
		$model->save();
		Cache::rm('wxapp_' . $wxapp_id);
        return $this->renderSuccess('操作成功');
    }
	
	/**
     * 注册来源切换
     */
    public function source($wxapp_id)
    {
		//全局判断
		if($err = checking()){
			return $this->renderError($err);
		}
        //详情
        $model = WxappModel::getWxapp(['wxapp_id' => $wxapp_id]);
		$model->source['value']==10 ? $model->source = 20 : $model->source = 10;
		$model->save();
		Cache::rm('wxapp_' . $wxapp_id);
        return $this->renderSuccess('操作成功');
    }
    
    /**
     * 发布小程序新模板
     */
    public function upgrade($wxapp_id)
    {
        //全局判断
        if($err = checking()){
            return $this->renderError($err);
        }
        //详情
        $model = WxappModel::getWxapp(['wxapp_id' => $wxapp_id]);
        if($model->publish()){
            return $this->renderSuccess('发布成功');
        }
        $error = $model->getError() ?: '发布失败';
        return $this->renderError($error);
    }
	
	/**
     * 管理员执行小程序登录
     */
    public function appLogin($wxapp_id)
    {	
		//全局判断
		if($err = checking()){
			return $this->error($err);
		}
		$model = WxappModel::getWxapp(['wxapp_id' => $wxapp_id]);
        if ($model->appLogin()) {
            $this->redirect('/index.php?s=/store/index/index');
        }
        $this->error('登录错误', 'wxapp/index');
    }
	
	/**
     * 变更到期时间
     */
    public function genewal($wxapp_id)
    {
		//全局判断
		if($err = checking()){
			return $this->renderError($err);
		}
        if (!$this->request->isAjax()) {
            return $this->renderError('非法操作');
        }
		$model = WxappModel::getWxapp(['wxapp_id' => $wxapp_id]);
        if ($model->genewal($this->postData('wxapp'))) {
            return $this->renderSuccess('操作成功', url('wxapp/index'));
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }


}
