<?php
namespace app\admin\controller\wechat;
use app\admin\controller\Controller;
use app\admin\model\WebSet as WebSetModel;

/**
 * 公众号模板消息
 */
class Tplmsg extends Controller
{
   /**
    * 更新设置事件
    */
   public function setting()
   {
	   if (!$this->request->isAjax()) {
		   $values = WebSetModel::getItem('tplmsg');
		   return $this->fetch('setting', compact('values'));
	   }
	   //全局验证
		if($err = checking()){
			return $this->renderError($err);
		}
       $model = new WebSetModel;
       if ($model->edit('tplmsg',$this->postData('setting'))) {
           return $this->renderSuccess('更新成功');
       }
       return $this->renderError('更新失败');
   }
}
