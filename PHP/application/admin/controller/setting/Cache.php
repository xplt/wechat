<?php
namespace app\admin\controller\setting;
use app\admin\controller\Controller;
use think\Cache as Driver;

/**
 * 清理缓存
 */
class Cache extends Controller
{
    /**
     * 清理缓存
     */
    public function clear($isForce = false)
    {
        if ($this->request->isAjax()) {
            $data = $this->postData('cache');
            $this->rmCache($data['keys'], isset($data['isForce']) ? !!$data['isForce'] : false);
            return $this->renderSuccess('操作成功');
        }
        return $this->fetch('clear', [
            'cacheList' => $this->getCacheKeys(),
            'isForce' => !!$isForce ?: config('app_debug'),
        ]);
    }

    /**
     * 删除缓存
     */
    private function rmCache($keys, $isForce = false)
    {
        if ($isForce === true) {
            Driver::clear();
        } else {
            $cacheList = $this->getCacheKeys();
            foreach (array_intersect(array_keys($cacheList), $keys) as $key) {
                Driver::has($cacheList[$key]['key']) && Driver::rm($cacheList[$key]['key']);
            }
        }
    }

    /**
     * 获取缓存索引数据
     */
    private function getCacheKeys()
    {
        return [
			'user' => [
                'key' => 'web_set_hema',
                'name' => '站点设置'
            ],
			'menu' => [
                'key' => 'web_menu_hema',
                'name' => '站点菜单'
            ],
			'shop' => [
                'key' => 'shop_category_hema',
                'name' => '门店分类'
            ],
            'link' => [
                'key' => 'web_link_hema',
                'name' => '友情链接'
            ],
        ];
    }

}
