<?php
namespace app\admin\controller\setting;
use app\admin\controller\Controller;
use app\admin\model\WebLink as WebLinkModel;
/**
 * 友情链接
 */
class Links extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
		$model = new WebLinkModel;
        $list = $model->getALL();
        return $this->fetch('index', compact('list'));
    }

    /**
     * 删除
     */
    public function delete($web_link_id)
    {
		//全局验证
		if($err = checking()){
			return $this->renderError($err);
		}
        $model = WebLinkModel::get($web_link_id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 添加
     */
    public function add()
    {
        $model = new WebLinkModel;
        if (!$this->request->isAjax()) {
            $list = $model->getALL();
            return $this->fetch('add', compact('list'));
        }
		//全局验证
		if($err = checking()){
			return $this->renderError($err);
		}
        // 新增记录
        if ($model->add($this->postData('link'))) {
            return $this->renderSuccess('添加成功', url('setting.links/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 编辑
     */
    public function edit($web_link_id)
    {
        $model = WebLinkModel::get($web_link_id);
        if (!$this->request->isAjax()) {
            return $this->fetch('edit', compact('model'));
        }
		//全局验证
		if($err = checking()){
			return $this->renderError($err);
		}
        // 更新记录
        if ($model->edit($this->postData('link'))) {
            return $this->renderSuccess('更新成功', url('setting.links/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

}
