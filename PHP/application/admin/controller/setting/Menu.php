<?php
namespace app\admin\controller\setting;
use app\admin\controller\Controller;
use app\admin\model\WebMenu as WebMenuModel;
use think\Cache;
/**
 * 首页菜单
 */
class Menu extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
        $model = new WebMenuModel;
        $list = $model->getCacheTree();
        return $this->fetch('index', compact('list'));
    }

    /**
     * 删除
     */
    public function delete($web_menu_id)
    {
		//全局验证
		if($err = checking()){
			return $this->renderError($err);
		}
        $model = WebMenuModel::get($web_menu_id);
        if (!$model->remove($web_menu_id)) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 添加
     */
    public function add()
    {
        $model = new WebMenuModel;
        if (!$this->request->isAjax()) {
            $list = $model->getCacheTree();
            return $this->fetch('add', compact('list'));
        }
		//全局验证
		if($err = checking()){
			return $this->renderError($err);
		}
        // 新增记录
        if ($model->add($this->postData('menu'))) {
            return $this->renderSuccess('添加成功', url('setting.menu/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 编辑
     */
    public function edit($web_menu_id)
    {
        $model = WebMenuModel::get($web_menu_id);
        if (!$this->request->isAjax()) {
            $list = $model->getCacheTree();
            return $this->fetch('edit', compact('model', 'list'));
        }
		//全局验证
		if($err = checking()){
			return $this->renderError($err);
		}
        // 更新记录
        if ($model->edit($this->postData('menu'))) {
            return $this->renderSuccess('更新成功', url('setting.menu/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
	
	/**
     * 状态编辑
     */
    public function status($web_menu_id)
    {
		//全局验证
		if($err = checking()){
			return $this->renderError($err);
		}
        $model = WebMenuModel::get($web_menu_id);
		$model->status['value']==1 ? $model->status = 0 : $model->status = 1;
		$model->save();
		Cache::rm('web_menu_hema');
        return $this->renderSuccess('更新成功', url('setting.menu/index'));
    }

}
