<?php
namespace app\admin\controller;
use app\admin\model\Config as ConfigModel;
use app\admin\model\WebSet as WebSetModel;

/**
 * 开放平台对接
 */
class Open extends Controller
{
    /**
     * 微信开放平台第三方平台设置
     */
    public function wxopen()
    {
        $model = ConfigModel::detail();
        if ($this->request->isAjax()) {
			//全局判断
			if($err = checking()){
				return $this->renderError($err);
			}
            if ($model->edit($this->postData('config'))){
				return $this->renderSuccess('更新成功');
			}
            return $this->renderError('更新失败');
        }
        return $this->fetch('wxopen', compact('model'));
    }
	
	/**
     * 微信开放平台网站应用设置
     */
    public function wxweb()
    {
        return $this->updateEvent('wxweb');
    }
	
	/**
     * 配送开放平台设置
     */
    public function delivery()
    {
        return $this->updateEvent('delivery');
    }

    /**
     * 打印机开放平台设置
     */
    public function printer()
    {
        return $this->updateEvent('printer');
    }

    /**
     * 云叫号开放平台设置
     */
    public function calling()
    {
        return $this->updateEvent('calling');
    }

    /**
     * 更新设置事件
     */
    private function updateEvent($key)
    {
        if (!$this->request->isAjax()) {
            $values = WebSetModel::getItem($key);
            return $this->fetch($key, compact('values'));
        }
		//全局判断
		if($err = checking()){
			return $this->renderError($err);
		}
        $model = new WebSetModel;
        if ($model->edit($key,$this->postData($key))) {
            return $this->renderSuccess('更新成功');
        }
        return $this->renderError('更新失败');
    }

}
