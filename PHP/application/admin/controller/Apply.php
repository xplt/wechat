<?php
namespace app\admin\controller;
use app\admin\model\StoreApply as StoreApplyModel;
use app\admin\model\StoreDetail as StoreDetailModel;

/**
 * 用户认证申请控制器
 */
class Apply extends Controller
{
    /**
     * 入住申请列表
     */
    public function out()
    {
		$model = new StoreApplyModel;
		$list = $model->getList(10);
		return $this->fetch('out', compact('list'));
    }
    /**
     * 小程序注册列表
     */
    public function wxapp()
    {
        $model = new StoreApplyModel;
        $list = $model->getList(50);
        return $this->fetch('wxapp', compact('list'));
    }
    /**
     * 支付申请列表
     */
    public function pay()
    {
		$model = new StoreApplyModel;
		$list = $model->getList(20);
		return $this->fetch('pay', compact('list'));
    }
    /**
     * 代理申请列表
     */
    public function agent()
    {
		$model = new StoreApplyModel;
		$list = $model->getList(30);
		return $this->fetch('agent', compact('list'));
    }

    /**
     * 用户资料
     */
    public function auth()
    {
		$model = new StoreApplyModel;
		$list = $model->getList(40);
		return $this->fetch('auth', compact('list'));
    }

    /**
     * 详情
    */
    public function detail($store_apply_id)
    {
        $model = StoreApplyModel::detail($store_apply_id);
        if (!$this->request->isAjax()) {
            return $this->fetch('detail', compact('model'));
        }
        //全局验证
        if($err = checking()){
            return $this->error($err);
        }
        $data = $this->postData('apply');
        if($data['apply_status']==40 AND empty($data['reject'])){
            return $this->renderError('请输入驳回原因');
        }
        $url = [10 => 'out',20 => 'pay',30 => 'agent',40 => 'auth',50 => 'wxapp'];
        // 更新记录
        if ($model->edit($data)) {
            return $this->renderSuccess('更新成功', url('apply/'.$url[$model['apply_mode']['value']]));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
    
}
