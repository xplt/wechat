<?php
namespace app\admin\controller;
use app\admin\model\StoreUser as StoreUserModel;
use app\admin\model\WebOrder as WebOrderModel;

/**
 * 财务管理首页
 */
class Finance extends Controller
{
    /**
     * 首页统计
     */
    public function index()
    {
		$count = array();
		$count['store'] = StoreUserModel::getCount();	//用户统计
		$count['order'] = WebOrderModel::getCount();	//订单统计
		return $this->fetch('index', compact('count'));
    }
}
