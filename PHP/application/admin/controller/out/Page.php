<?php
namespace app\admin\controller\out;
use app\admin\controller\Controller;
use app\admin\model\OutPage as OutPageModel;

/**
 * 页面管理
 */
class Page extends Controller
{
    /**
     * 获取页面列表
     */
    public function index()
    {
        $model = new OutPageModel;
        $list = $model->getList();
        return $this->fetch('index', compact('list'));
    }

    /**
     * 添加
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
			$temp = OutPageModel::temp()['json'];
            $jsonData = OutPageModel::page()['json'];
			$opts['catgory'] = '';//Category::getCacheTree();//获取商品分类
			$opts['sharingCatgory'] ='';
			$opts['articleCatgory'] = ''; //获取图文分类
			$opts = json_encode($opts); //转换成json格式
            return $this->fetch('add', compact('temp','jsonData','opts'));
        }
        if($msg = checking(1)){
            return $this->renderError($msg);
        }
        $model = new OutPageModel;
        if ($model->add($this->postData('data'))) {
            return $this->renderSuccess('添加成功', url('out.page/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 编辑
     */
    public function edit($page_id)
    {
        // 详情
        $model = OutPageModel::detail($page_id);
        if (!$this->request->isAjax()) {
			$temp = OutPageModel::temp()['json'];
			$opts['catgory'] = '';//Category::getCacheTree();//获取商品分类
			$opts['sharingCatgory'] ='';
            $opts['articleCatgory'] = ''; //获取图文分类
			$opts = json_encode($opts); //转换成json格式
            $jsonData = $model['page_data']['json'];
            return $this->fetch('edit', compact('temp','jsonData','opts'));
        }
        $data = $this->postData('data');
        if (!$model->edit($data)) {
            return $this->renderError('更新失败');
        }
        return $this->renderSuccess('更新成功');
    }

    /**
     * 删除
     */
    public function delete($page_id)
    {
        if($msg = checking(1)){
            return $this->renderError($msg);
        }
        $model = OutPageModel::get($page_id);
        if (!$model->remove()) {
            return $this->renderError('删除失败');
        }
        return $this->renderSuccess('删除成功');
    }
}
