<?php
namespace app\admin\controller\out\shop;
use app\admin\controller\Controller;
use app\admin\model\ShopCategory as ShopCategoryModel;

/**
 * 门店分类控制器
 */
class Category extends Controller
{
    /**
     * 分类列表
     */
    public function index()
    {
        $model = new ShopCategoryModel;
        $list = $model->getCacheTree();
        return $this->fetch('index', compact('list'));
    }

    /**
     * 删除分类
     */
    public function delete($shop_category_id)
    {
        $model = ShopCategoryModel::get($shop_category_id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 添加分类
     */
    public function add()
    {
        $model = new ShopCategoryModel;
        if (!$this->request->isAjax()) {
            return $this->fetch('add');
        }
        // 新增记录
        if ($model->add($this->postData('category'))) {
            return $this->renderSuccess('添加成功', url('out.shop.category/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 编辑
     */
    public function edit($shop_category_id)
    {
        // 模板详情
        $model = ShopCategoryModel::get($shop_category_id, ['image']);
        if (!$this->request->isAjax()) {
            return $this->fetch('edit', compact('model'));
        }
        // 更新记录
        if ($model->edit($this->postData('category'))) {
            return $this->renderSuccess('更新成功', url('out.shop.category/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

}
