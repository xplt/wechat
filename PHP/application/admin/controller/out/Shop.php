<?php
namespace app\admin\controller\out;
use app\admin\controller\Controller;
use app\admin\model\Shop as ShopModel;

/**
 * 门店控制器
 */
class Shop extends Controller
{
    /**
     * 门店列表
     */
    public function index()
    {
        $model = new ShopModel;
        $list = $model->outList();
        return $this->fetch('index', compact('list'));
    }

    /**
     * 状态编辑
     */
    public function show($shop_id)
    {
        $model = ShopModel::get($shop_id);
        $model->out_show['value'] ? $model->out_show = 0 : $model->out_show = 1;
        $model->save();
        return $this->renderSuccess('更新成功', url('out.shop/index'));
    }
}
