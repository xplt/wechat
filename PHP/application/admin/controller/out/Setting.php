<?php
namespace app\admin\controller\out;
use app\admin\controller\Controller;
use app\admin\model\WebSet as WebSetModel;

/**
 * 外卖设置
 */
class Setting extends Controller
{
   /**
     * APP对接设置
     */
    public function outapp()
    {
        return $this->updateEvent('outapp');
    }
    
    /**
     * 结算设置
     */
    public function solve()
    {
        return $this->updateEvent('solve');
    }

    /**
     * 更新设置事件
     */
    private function updateEvent($key)
    {
        if (!$this->request->isAjax()) {
            $values = WebSetModel::getItem($key);
            return $this->fetch($key, compact('values'));
        }
        //全局判断
        if($err = checking()){
            return $this->renderError($err);
        }
        $model = new WebSetModel;
        if ($model->edit($key,$this->postData($key))) {
            return $this->renderSuccess('更新成功');
        }
        return $this->renderError('更新失败');
    }
}
