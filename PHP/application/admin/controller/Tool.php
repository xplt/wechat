<?php
namespace app\admin\controller;
use app\admin\model\Order as OrderModel;
use app\admin\model\Config as ConfigModel;
use app\admin\model\Wechat as WechatModel;
use app\admin\model\Wxapp as WxappModel;
use app\admin\model\Setting as SettingModel;
use app\admin\model\WebSet as WebSetModel;
use think\Session;

/**
 * 工具接口
 */

class Tool extends Controller
{
	public function v407(){
		echo 'ok';
		if($value = WebSetModel::detail('openweb')){
			$wxweb = $value->toArray();
			$model = new WebSetModel;
			$model->save([
				'key' => 'wxweb',
	            'describe' => '网站应用',
	            'values' => $wxweb['values']
			]);
			$value->delete();
		}
		deldir('./runtime/');
	}
	public function v406(){
		echo 'ok';
		if($value = WebSetModel::detail('pay')){
			$pay = $value->toArray();
			$model = new WebSetModel;
			$model->save([
				'key' => 'wxpay',
	            'describe' => '微信支付服务商设置',
	            'values' => $pay['values']
			]);
			$value->delete();
		}
		$value = WebSetModel::getItem('wxapp');
		$model = new WebSetModel;
		$model->save([
			'key' => 'payment',
            'describe' => '支付设置',
            'values' => [
            	'wx' => [
                    'mchid' => $value['mchid'],//商户号
                    'apikey' => $value['apikey'],//密钥
                    'cert_pem' => '', //apiclient_cert.pem 证书
                    'key_pem' => '' //apiclient_key.pem 密钥
                ]
            ]
		]);
		$model = new WxappModel;
		$wxapp = $model->where(['mchid' => ['<>','']])->select();
		for($n=0;$n<sizeof($wxapp);$n++){
			$model = new SettingModel;
			$model->save([
				'key' => 'payment',
	            'describe' => '支付设置',
	            'values' => [
	            	'wx' => [
                        'is_sub' => $wxapp[$n]['is_sub']['value'], //是否为特约商户
                        'mchid' => $wxapp[$n]['mchid'],//商户号
                        'apikey' => $wxapp[$n]['apikey'],//密钥
                        'cert_pem' => $wxapp[$n]['cert_pem'], //apiclient_cert.pem 证书
                        'key_pem' => $wxapp[$n]['key_pem'] //apiclient_key.pem 密钥
                    ],
                    'postpaid' => [0,0,0]
	            ],
	            'wxapp_id' => $wxapp[$n]['wxapp_id']
			]);
		}
		$config = ConfigModel::detail();
		if($config['wechat_appid']==''){
			return true;
		}
		if(!$wechat = WechatModel::detail()){
			$wechat = new WechatModel;
		}
		$wechat->save([
			'app_name' => $config['wechat_nick'],
			'head_img' => $config['wechat_logo'],
			'qrcode_url' => $config['wechat_qrcode'],
			'user_name' => $config['wechat_id'],
			'app_id' => $config['wechat_appid'],
			'principal_name' => $config['wechat_principal'],
			'access_token' => $config['wechat_access_token'],
			'expires_in' => $config['wechat_at_expires'],
			'authorizer_refresh_token' => $config['wechat_refresh_token']
		]);
		deldir('./runtime/');
	}
	public function v404(){
		$mode = new OrderModel;
		$mode->isUpdate(true)->save(['receipt_status' => 20], ['receipt_status' => 30]);
	}
	/**
     * 清除session（当前作用域）
     */
    public function clear_session()
    {
		Session::clear();
	}
}
