<?php
namespace app\admin\controller\apply;
use app\admin\controller\Controller;
use app\admin\model\WebOrder as WebOrderModel;

/**
 * 站点订单管理
 */
class Order extends Controller
{
    /**
     * 提现列表
     */
    public function index()
    {
        $model = new WebOrderModel;
        $list = $model->getList('','',40);
        return $this->fetch('index', compact('list'));
    }

    /**
     * 同意提现
    */
    public function cash($web_order_id)
    {
        //全局验证
        if($err = checking()){
            return $this->error($err);
        }
        $model = WebOrderModel::detail($web_order_id);
        if ($model->cash()) {
            return $this->renderSuccess('操作成功');
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }	
}
