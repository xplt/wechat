<?php
namespace app\admin\controller\finance;
use app\admin\controller\Controller;
use app\admin\model\WebOrder as WebOrderModel;

/**
 * 站点订单管理
 */
class Order extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
        $model = new WebOrderModel;
        $list = $model->getList();
        return $this->fetch('index', compact('list'));
    }	
}
