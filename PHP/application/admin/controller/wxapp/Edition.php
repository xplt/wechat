<?php
namespace app\admin\controller\wxapp;
use app\admin\controller\Controller;
use app\admin\model\WebEdition as WebEditionModel;

/**
 * 小程序版本分类控制器
 */
class Edition extends Controller
{
    /**
     * 分类列表
     */
    public function index()
    {
		$list = WebEditionModel::getAll(0);
        return $this->fetch('index', compact('list'));
    }

    /**
     * 删除分类
     */
    public function delete($web_edition_id)
    {
		//全局验证
		if($err = checking()){
			return $this->renderError($err);
		}
        $model = WebEditionModel::detail($web_edition_id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 添加分类
     */
    public function add()
    {
        $model = new WebEditionModel;
        if (!$this->request->isAjax()) {
            return $this->fetch('add');
        }
        //全局验证
		if($err = checking()){
			return $this->renderError($err);
		}
        if ($model->add($this->postData('edition'))) {
            return $this->renderSuccess('添加成功', url('wxapp.edition/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 编辑
     */
    public function edit($web_edition_id)
    {
        // 详情
        $model = WebEditionModel::detail($web_edition_id);
        if (!$this->request->isAjax()) {
            return $this->fetch('edit', compact('model'));
        }
        //全局验证
		if($err = checking()){
			return $this->renderError($err);
		}
        if ($model->edit($this->postData('edition'))) {
            return $this->renderSuccess('更新成功', url('wxapp.edition/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

}
