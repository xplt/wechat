<?php
namespace app\admin\controller\wxapp;
use app\admin\controller\Controller;
use app\admin\model\WebWxappPlugins as WebWxappPluginsModel;

/**
 * 小程序插件管理
 */
class Plugins extends Controller
{
	/**
     * 列表
     */
    public function index()
    {
        $model = new WebWxappPluginsModel;
        $list = $model->getList();
        return $this->fetch('index', compact('list'));
    }

    /**
     * 删除
     */
    public function delete($web_wxapp_plugins_id)
    {
		//全局验证
		if($err = checking()){
			return $this->renderError($err);
		}
        $model = WebWxappPluginsModel::detail($web_wxapp_plugins_id);
        if (!$model->remove()) {
            return $this->renderError('删除失败');
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 添加
     */
    public function add()
    {
        $model = new WebWxappPluginsModel;
        if (!$this->request->isAjax()) {
            return $this->fetch('add');
        }
		//全局验证
		if($err = checking()){
			return $this->renderError($err);
		}
        // 新增记录
        if ($model->add($this->postData('plugins'))) {
            return $this->renderSuccess('添加成功', url('wxapp.plugins/index'));
        }
        return $this->renderError('添加失败');
    }

    /**
     * 编辑
     */
    public function edit($web_wxapp_plugins_id)
    {
        $model = WebWxappPluginsModel::detail($web_wxapp_plugins_id);
        if (!$this->request->isAjax()) {
            return $this->fetch('edit', compact('model'));
        }
		//全局验证
		if($err = checking()){
			return $this->renderError($err);
		}
        // 更新记录
        if ($model->edit($this->postData('plugins'))) {
            return $this->renderSuccess('更新成功', url('wxapp.plugins/index'));
        }
        return $this->renderError('更新失败');
    }
}
