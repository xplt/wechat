<?php
return [
    'index' => [
        'name' => '首页',
        'icon' => 'iconshouye',
        'index' => 'index/index',
    ],
    'apply' => [
        'name' => '用户申请',
        'icon' => 'iconrenzheng',
        'index' => 'apply/out',
        'submenu' => [
			[
                'name' => '入驻申请',
                'index' => 'apply/out',
                'uris' => [
                    'apply/out',
                ],
            ],
            [
                'name' => '小程序注册',
                'index' => 'apply/wxapp',
                'uris' => [
                    'apply/wxapp',
                ],
            ],
            [
                'name' => '支付申请',
                'index' => 'apply/pay',
                'uris' => [
                    'apply/pay',
                ],
            ],
            [
                'name' => '代理申请',
                'index' => 'apply/agent',
                'uris' => [
                    'apply/agent',
                ],
            ],
            [
                'name' => '实名认证',
                'index' => 'apply/auth',
                'uris' => [
                    'apply/auth',
                ],
            ],
            [
                'name' => '提现申请',
                'index' => 'apply.order/index',
                'uris' => [
                    'apply.order/index',
                    'apply.order/cash',
                ],
            ],
		]
    ],
    'wxapp' => [
        'name' => '小程序',
        'icon' => 'iconweixinxiaochengxu',
        'color' => '#36b313',
        'index' => 'wxapp/index',
        'submenu' => [
			[
                'name' => '小程序管理',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '全部小程序',
						'index' => 'wxapp/index',
						'uris' => [
							'wxapp/index',
							'wxapp/appLogin',
							'wxapp/delete'
						],
                    ],
                    [
                        'name' => '已授权',
						'index' => 'wxapp/normal',
						'uris' => [
							'wxapp/normal',
							'wxapp/appLogin',
							'wxapp/delete'
						],
                    ],
					[
                        'name' => '将到期',
						'index' => 'wxapp/warn',
						'uris' => [
							'wxapp/warn',
							'wxapp/appLogin',
							'wxapp/delete'
						],
                    ],
					[
                        'name' => '已到期',
						'index' => 'wxapp/ends',
						'uris' => [
							'wxapp/ends',
							'wxapp/appLogin',
							'wxapp/delete'
						],
                    ],
                ]
            ],
			[
                'name' => '模板管理',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '最新草稿',
                        'index' => 'wxapp.template/draft',
						'uris' => [
							'wxapp.template/draft',
							'wxapp.template/addTemplate'
						],
                    ],
                    [
                        'name' => '模板库',
                        'index' => 'wxapp.template/template',
						'uris' => [
							'wxapp.template/template',
							'wxapp.template/deleteTemplate'
						],
                    ],
					[
                        'name' => '模板推送',
                        'index' => 'wxapp.template/index',
						'uris' => [
							'wxapp.template/index',
							'wxapp.template/add',
							'wxapp.template/edit',
							'wxapp.template/delete'
						],
                    ],
                ]
            ],
			[
                'name' => '版本管理',
                'index' => 'wxapp.edition/index',
                'uris' => [
                    'wxapp.edition/index',
					'wxapp.edition/add',
                    'wxapp.edition/edit',
					'wxapp.edition/delete'
                ],
            ],
			[
                'name' => '插件管理',
                'index' => 'wxapp.plugins/index',
                'uris' => [
                    'wxapp.plugins/index',
					'wxapp.plugins/add',
                    'wxapp.plugins/edit',
					'wxapp.plugins/delete'
                ],
            ],
        ],
    ],
    'wechat' => [
        'name' => '公众号',
        'icon' => 'iconweixingongzhonghao',
		'color' => '#36b313',
        'index' => 'wechat/index',
        'submenu' => [
			[
                'name' => '基础信息',
                'index' => 'wechat/index',
				'urls' => [
					'wechat/index',
				] 
            ],
			[
                'name' => '菜单设置',
                'index' => 'wechat/menus',
				'urls' => [
					'wechat/menu',
				] 
            ],
			[
				'name' => '模板消息',
				'index' => 'wechat.tplmsg/setting',
				'uris' => [
					'wechat.tplmsg/setting'
				],
			],
			[
				'name' => '群发消息',
				'index' => 'wechat.send/index',
				'uris' => [
					'wechat.send/index',
					'wechat.send/add',
                    'wechat.send/edit',
					'wechat.send/delete'
				],
			],
			[
                'name' => '素材管理',
                'active' => true,
				'submenu' => [
					[
						'name' => '图文素材',
						'index' => 'wechat.material.text/index',
						'urls' => [
							'wechat.material.text/index',
							'wechat.material.text/add',
							'wechat.material.text/edit',
							'wechat.material.text/delete'
						]
					],
					[
						'name' => '图片素材',
						'index' => 'wechat.material.image/index',
						'urls' => [
							'wechat.material.image/index',
							'wechat.material.image/add',
							'wechat.material.image/edit',
							'wechat.material.image/delete'
						]
					],	
					[
						'name' => '语音素材',
						'index' => 'wechat.material.voice/index',
						'urls' => [
							'wechat.material.voice/index',
							'wechat.material.voice/add',
							'wechat.material.voice/edit',
							'wechat.material.voice/delete'
						]
					],
					[
						'name' => '视频素材',
						'index' => 'wechat.material.video/index',
						'urls' => [
							'wechat.material.video/index',
							'wechat.material.video/add',
							'wechat.material.video/edit',
							'wechat.material.video/delete'
						]
					],
				]
            ],
			[
                'name' => '智能回复',
                'active' => false,
                'submenu' => [
					[
						'name' => '被关注回复',
						'index' => 'wechat/subscribe',
							'urls' => [
								'wechat/subscribe',
							]
					],
					[
						'name' => '关键字回复',
						'index' => 'wechat.keyword/index',
						'uris' => [
							'wechat.keyword/index',
							'wechat.keyword/add',
							'wechat.keyword/edit',
							'wechat.keyword/delete',
						],
					],
					
                ]
            ],
        ],
    ],
	'pay' => [
		'name' => '支付设置',
		'icon' => 'iconcaiwuguanli',
		'color' => '#36b313',
		'index' => 'pay/payment',
		'submenu' => [
			[
				'name' => '支付设置',
				'index' => 'pay/payment',
				'uris' => [
					'pay/payment',
				],
			],
            [
                'name' => '服务商设置',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '微信支付',
                        'index' => 'pay/wxpay'
                    ],
                    
                ]
            ],
		]
	],
	'out' => [
		'name' => '外卖平台',
		'icon' => 'iconwaimai',
		'color' => '#f37b1d',
		'index' => 'out.shop/index',
		'submenu' => [
			[
                'name' => '门店管理',
                'active' => true,
                'submenu' => [
					[
						'name' => '门店列表',
						'index' => 'out.shop/index',
							'urls' => [
								'out.shop/index',
							]
					],
					[
						'name' => '门店分类',
						'index' => 'out.shop.category/index',
						'uris' => [
							'out.shop.category/index',
							'out.shop.category/add',
							'out.shop.category/edit',
							'out.shop.category/delete',
						],
					],
					
                ]
            ],
            [
                'name' => '页面管理',
                'index' => 'out.page/index',
                'uris' => [
                    'out.page/index',
                    'out.page/add',
                    'out.page/edit',
                    'out.page/delete',
                ],
            ],
            [
                'name' => '平台设置',
                'active' => false,
                'submenu' => [
                    [
                        'name' => 'APP对接',
                        'index' => 'out.setting/outapp',
                        'uris' => [
                            'out.setting/outapp',
                        ],
                    ],
                    [
                        'name' => '结算设置',
                        'index' => 'out.setting/solve',
                        'uris' => [
                            'out.setting/solve',
                        ],
                    ],
                    
                ]
            ],
		]
	],
	'user' => [
        'name' => '用户管理',
        'icon' => 'iconyonghuguanli',
        'index' => 'user/index',
        'submenu' => [
			[
                'name' => '会员列表',
                'index' => 'user/index',
                'uris' => [
                    'user/index',
                ],
            ],
            [
                'name' => '商户列表',
                'index' => 'user/store',
                'uris' => [
                    'user/store',
                ],
            ],
            [
                'name' => '代理列表',
                'index' => 'user/agent',
                'uris' => [
                    'user/agent',
                ],
            ],
            [
                'name' => '用户资料',
                'index' => 'user/auth',
                'uris' => [
                    'user/auth',
                ],
            ],
		]
    ],
    'finance' => [
        'name' => '账务管理',
        'icon' => 'icontongji',
        'index' => 'finance/index',
        'submenu' => [
			[
                'name' => '账务统计',
                'index' => 'finance/index',
                'uris' => [
                    'finance/index',
                ],
            ],
			[
                'name' => '订单流水',
                'index' => 'finance.order/index',
                'uris' => [
                    'finance.order/index',
                ],
            ],
		]
    ],
	'open' => [
        'name' => '开放平台',
        'icon' => 'iconkaifangpingtai',
        'index' => 'open/wxopen',
		'submenu' => [
            [
                'name' => '微信平台',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '第三方应用',
                        'index' => 'open/wxopen',
                        'urls' => [
                            'open/wxopen'
                        ]
                    ],
                    [
                        'name' => '网站应用',
                        'index' => 'open/wxweb',
                        'urls' => [
                            'open/wxweb'
                        ]
                    ],
                ]
            ],
            [
                'name' => '第三方配送',
                'index' => 'open/delivery',
                'urls' => [
                    'open/delivery'
                ]
            ],
            [
                'name' => '云打印机',
                'index' => 'open/printer',
                'urls' => [
                    'open/printer',
                ]
            ],
            [
                'name' => '云叫号器',
                'index' => 'open/calling',
                'urls' => [
                    'open/calling',
                ]
            ],
           
		]
    ],	
    'setting' => [
        'name' => '设置',
        'icon' => 'iconshezhi',
        'index' => 'setting/web',
        'submenu' => [
			[
                'name' => '站点设置',
                'index' => 'setting/web',
				'urls' => [
					'setting/web'
				]
            ],
			[
                'name' => '助手设置',
                'index' => 'setting/wxapp',
				'urls' => [
					'setting/wxapp'
				]
            ],
            [
                'name' => '注册设置',
                'index' => 'setting/register',
                'urls' => [
                    'setting/register'
                ]
            ],
			[
                'name' => '前端菜单',
                'index' => 'setting.menu/index',
				'urls' => [
					'setting.menu/index',
					'setting.menu/add',
					'setting.menu/edit',
					'setting.menu/delete'
				]
            ],
			[
                'name' => '友情链接',
                'index' => 'setting.links/index',
				'urls' => [
					'setting.links/index',
					'setting.links/add',
					'setting.links/edit',
					'setting.links/delete'
				]
            ],
			/*
            [
                'name' => '角色类目',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '超级管理端',
                        'index' => 'setting.rules.admin/index',
						'urls' => [
							'setting.rules.admin/index',
							'setting.rules.admin/add',
							'setting.rules.admin/edit',
							'setting.rules.admin/delete'
						]
                    ]
                ]
            ],
			*/
            [
                'name' => '其他',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '清理缓存',
                        'index' => 'setting.cache/clear'
                    ],
					[
                        'name' => '环境监测',
                        'index' => 'setting.science/index'
                    ]
                ]
            ],
        ],
    ],
];
