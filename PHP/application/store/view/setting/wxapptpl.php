<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">微信小程序订阅消息设置</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">商家接单通知 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $values['receive']?>" disabled>
                                    <input type="hidden" name="wxapptpl[receive]" disabled>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">骑手取货通知 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $values['horseman']?>" disabled>
                                    <input type="hidden" name="wxapptpl[horseman]" disabled>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">订单配送通知 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $values['delivery']?>" disabled>
                                    <input type="hidden" name="wxapptpl[delivery]" disabled>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">取餐提醒 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $values['take']?>" disabled>
									<input type="hidden" name="wxapptpl[take]" disabled>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">订单完成通知 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $values['finish']?>" disabled>
									<input type="hidden" name="wxapptpl[finish]" disabled>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">退款状态通知 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $values['refund']?>" disabled>
									<input type="hidden" name="wxapptpl[refund]" disabled>
                                </div>
                            </div>
							<!-- 事件功能 -->
							<div class="am-modal am-modal-loading am-modal-no-btn" tabindex="-1" id="my-modal-loading"></div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">一键设置
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        $('#my-form').formPost();

    });
</script>
