<!-- 模板：用户充值 -->
<script id="tpl-recharge" type="text/template">
    <div class="am-padding-xs am-padding-top-sm">
        <form id="my-form" class="am-form tpl-form-line-form" method="post" action="">
            <div class="j-tabs am-tabs">
                <ul class="am-tabs-nav am-nav am-nav-tabs">
                    <li class="am-active"><a href="#tab1">充值余额</a></li>
                    <li><a href="#tab2">充值积分</a></li>
                </ul>
                <div class="am-tabs-bd am-padding-xs">
                    <div class="am-tab-panel am-padding-0 am-active" id="tab1">
                        <div class="am-form-group">
                            <label class="am-u-sm-3 am-form-label">当前余额</label>
                            <div class="am-u-sm-8 am-u-end">
                                <div class="am-form--static">{{ balance }}</div>
                            </div>
                        </div>
                        <div class="am-form-group">
                            <label class="am-u-sm-3 am-form-label">充值方式</label>
                            <div class="am-u-sm-8 am-u-end">
                                <label class="am-radio-inline">
                                    <input type="radio" name="recharge[balance][mode]"
                                           value="inc" data-am-ucheck checked> 增加
                                </label>
                                <label class="am-radio-inline">
                                    <input type="radio" name="recharge[balance][mode]" 
									value="dec" data-am-ucheck> 减少
                                </label>
                                <label class="am-radio-inline">
                                    <input type="radio" name="recharge[balance][mode]" 
									value="final" data-am-ucheck> 最终金额
                                </label>
                            </div>
                        </div>
                        <div class="am-form-group">
                            <label class="am-u-sm-3 am-form-label form-require">变更金额</label>
                            <div class="am-u-sm-8 am-u-end">
                                <input type="number" min="0" class="tpl-form-input"
                                       placeholder="请输入要变更的金额" name="recharge[balance][money]" 
									   value="" required>
                            </div>
                        </div>
                        <?php if($store['is_admin'] AND $store['wxapp']['shop_mode']['value']==20):?>
						<div class="am-form-group">
                            <label class="am-u-sm-3 am-form-label form-require">选择门店</label>
                            <div class="am-u-sm-8 am-u-end">
                                <select name="recharge[balance][shop_id]" required >
									<option value="">请选择门店</option>
									<?php if (isset($shop)): foreach ($shop as $item): ?>
										<option value="<?= $item['shop_id'] ?>"><?= $item['shop_name'] ?>
										</option>
									<?php endforeach; endif; ?>
								</select>
                            </div>
                        </div>
                        <?php endif;?>
                        <div class="am-form-group">
                            <label class="am-u-sm-3 am-form-label">备注</label>
                            <div class="am-u-sm-8 am-u-end">
                                <textarea rows="2" name="recharge[balance][remark]" placeholder="请输入管理员备注"
                                          class="am-field-valid"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="am-tab-panel am-padding-0" id="tab2">
                        <div class="am-form-group">
                            <label class="am-u-sm-3 am-form-label">当前积分</label>
                            <div class="am-u-sm-8 am-u-end">
                                <div class="am-form--static">{{ points }}</div>
                            </div>
                        </div>
                        <div class="am-form-group">
                            <label class="am-u-sm-3 am-form-label">充值方式</label>
                            <div class="am-u-sm-8 am-u-end">
                                <label class="am-radio-inline">
                                    <input type="radio" name="recharge[points][mode]"
                                           value="inc" data-am-ucheck checked>增加
                                </label>
                                <label class="am-radio-inline">
                                    <input type="radio" name="recharge[points][mode]" value="dec" 
									data-am-ucheck>减少
                                </label>
                                <label class="am-radio-inline">
                                    <input type="radio" name="recharge[points][mode]" value="final" 
									data-am-ucheck>最终积分
                                </label>
                            </div>
                        </div>
                        <div class="am-form-group">
                            <label class="am-u-sm-3 am-form-label form-require">变更数量</label>
                            <div class="am-u-sm-8 am-u-end">
                                <input type="number" min="0" class="tpl-form-input"
                                       placeholder="请输入要变更的数量" name="recharge[points][value]" 
									   value="" required>
                            </div>
                        </div>
                        <?php if($store['is_admin'] AND $store['wxapp']['shop_mode']['value']==20):?>
						<div class="am-form-group">
                            <label class="am-u-sm-3 am-form-label form-require">选择门店</label>
                            <div class="am-u-sm-8 am-u-end">
                                <select name="recharge[points][shop_id]" required >
									<option value="">请选择门店</option>
									<?php if (isset($shop)): foreach ($shop as $item): ?>
										<option value="<?= $item['shop_id'] ?>"><?= $item['shop_name'] ?>
										</option>
									<?php endforeach; endif; ?>
								</select>
                            </div>
                        </div>
                        <?php endif;?>
                        <div class="am-form-group">
                            <label class="am-u-sm-3 am-form-label">备注</label>
                            <div class="am-u-sm-8 am-u-end">
                                <textarea rows="2" name="recharge[points][remark]" placeholder="请输入管理员备注"
                                          class="am-field-valid"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</script>