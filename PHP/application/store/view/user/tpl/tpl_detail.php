<!-- 模板：会员信息详情 -->
<script id="tpl-detail" type="text/template">
    <div class="am-padding-xs am-padding-top">
        <div class="am-tab-panel am-padding-0 am-active">
            <div>用户编号：{{ id }}</div>
			<div>微信编号：{{ open_id }}</div>
			<div>手机号码：{{ mobile }}</div>
			<div>推 荐 人 ：{{ recommender }}</div>
			<div>佣金收入：{{ commission }}</div>
			<div>注册时间：{{ create_time }}</div>
        </div>
    </div>
</script>