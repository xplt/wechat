<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">余额充值记录</div>
                </div>
                <div class="widget-body am-fr">
                    <!-- 工具栏 -->
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>编号</th>
                                <th>头像</th>
                                <th>昵称</th>
                                <th>订单号</th>
                                <th>充值门店</th>
								<th>充值方式</th>
                                <th>套餐名称</th>
                                <th>支付金额</th>
                                <th>赠送金额</th>
                                <th>支付状态</th>
                                <th>付款时间</th>
                            </tr>
                            </thead>
                            <tbody>
								<?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
									<td class="am-text-middle"><?= $item['recharge_id'] ?></td>
                                    <td class="am-text-middle">
                                        <a href="<?= $item['user']['avatarUrl'] ?>" title="点击查看大图" target="_blank">
                                            <img src="<?= $item['user']['avatarUrl']?>" width="50" height="50" alt="">
                                        </a>
                                    </td>
                                    <td class="am-text-middle"><?= $item['user']['nickName'] ?></td>
                                    <td class="am-text-middle"><?= $item['order_no'] ?></td>
									<td class="am-text-middle"><?= $item['shop']['shop_name'] ?></td>
                                    <td class="am-text-middle"><?= $item['mode']['text'] ?></td>
									<td class="am-text-middle"><?= $item['recharge_plan_id']==0?'--':$item['plan']['plan_name'] ?></td>
                                    <td class="am-text-middle"><?= $item['money'] ?></td>
                                    <td class="am-text-middle"><?= $item['gift_money']==0?'--':$item['gift_money'] ?></td>
									<td class="am-text-middle"><?= $item['pay_status']['text'] ?></td>
									<td class="am-text-middle"><?= $item['pay_time']?date("Y-m-d H:i",$item['pay_time']):'--' ?></td>
                                </tr>
                                <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="10" class="am-text-center">暂无记录</td>
                                </tr>
								<?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
