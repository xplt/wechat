<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">用户收货地址</div>
                </div>
                <div class="widget-body am-fr">
                    <!-- 工具栏 -->
                    <div class="page_toolbar am-margin-bottom am-cf">
                        <form class="toolbar-form" action="">
                            <input type="hidden" name="s" value="/store/user.addres/index">
                            <div class="am-u-sm-12">
                                <div class="am fr">
                                    <div class="am-form-group am-fl">
                                        <div class="am-input-group am-input-group-sm tpl-form-border-form">
                                            <input type="text" class="am-form-field" name="search" placeholder="请输入姓名/手机号！" value="<?= $search ?>">
                                            <div class="am-input-group-btn">
                                                <button class="am-btn am-btn-default am-icon-search" type="submit"></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>编号ID</th>
                                <th>微信信息</th>
                                <th>收货姓名</th>
								<th>联系电话</th>
                                <th>省份</th>
                                <th>城市</th>
                                <th>区/县</th>
                                <th>门牌号</th>
                                <th>位置坐标</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['address_id'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="goods-detail">
                                            <div class="goods-image">
                                                <img src="<?= $item['user']['avatarUrl'] ? $item['user']['avatarUrl'] : 'assets/store/img/head-no.png' ?>" alt="">
                                            </div>
                                            <div class="goods-info dis-flex flex-dir-column flex-x-center">
                                                <p class="goods-title"><?= $item['user']['nickName'] ?: '--' ?></p>
                                                <p class="goods-title">ID：<?= $item['user_id'] ?></p>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="am-text-middle"><?= $item['name'] ?></td>
									<td class="am-text-middle"><?= $item['phone'] ?></td>
									<td class="am-text-middle"><?= $item['province'] ?></td>
									<td class="am-text-middle"><?= $item['city'] ?></td>
									<td class="am-text-middle"><?= $item['district'] ?></td>
                                    <td class="am-text-middle"><?= $item['detail'] ?></td>
                                    <td class="am-text-middle"><?= $item['location']?></td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>