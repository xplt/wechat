<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">分销设置</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">是否开启分销 </label>
                                <div class="am-u-sm-9">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="dealer[is_open]" value="1"
                                               data-am-ucheck <?= $values['is_open'] == 1 ? 'checked' : '' ?>
                                               required>
                                        开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="dealer[is_open]" value="0"
                                               data-am-ucheck <?= $values['is_open'] == 0 ? 'checked' : '' ?>>
                                        关闭
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">一级佣金比例 </label>
                                <div class="am-u-sm-9">
                                    <input type="number" min="0" max="100" class="tpl-form-input" name="dealer[first_money]"
                                           value="<?= $values['first_money'] ?>">
                                    <small>佣金比例范围 0% - 100%，填写参数时不要加“%”</small>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">二级级佣金比例 </label>
                                <div class="am-u-sm-9">
                                    <input type="number" min="0" max="100" class="tpl-form-input" name="dealer[second_money]"
                                           value="<?= $values['second_money'] ?>">
                                    <small>佣金比例范围 0% - 100%，填写参数时不要加“%”</small>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">最低提现额度 </label>
                                <div class="am-u-sm-9">
                                    <input type="number" class="tpl-form-input" name="dealer[min_money]"
                                           value="<?= $values['min_money'] ?>">
									<small>低于该参数不可提现</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        $('#my-form').formPost();

    });
</script>
