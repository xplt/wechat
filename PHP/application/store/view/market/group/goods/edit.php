<link rel="stylesheet" href="assets/store/css/hema.goods.css">
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">编辑商品信息</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 商品信息 </label>
                                <div class="am-u-sm-9 am-u-md-6 am-u-lg-5 am-u-end">
                                    <div class="goods-detail">
                                        <div class="goods-image">
                                            <img src="<?= $model['goods']['image'][0]['file_path']?>" alt="">
                                        </div>
                                        <div class="goods-info dis-flex flex-dir-column flex-x-center">
                                            <p class="goods-title"><?= $model['goods']['goods_name']?></p>
                                            <p class="goods-title">ID：<?= $model['goods']['goods_id']?></p>
                                            <input type="hidden" name="goods[goods_id]" value="<?= $model['goods_id']?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <!-- 商品单规格 -->
                            <?php if($model['goods']['spec_type'] == 10 ):?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">单买价格 </label>
                                <div class="am-u-sm-9 am-u-md-6 am-u-lg-5 am-u-end">
                                    <input type="number" class="tpl-form-input" value="<?= $model['goods']['spec'][0]['goods_price']?>" disabled >
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">拼团售价 </label>
                                <div class="am-u-sm-9 am-u-md-6 am-u-lg-5 am-u-end">
                                    <input type="number" class="tpl-form-input" name="goods[spec][group_price]"
                                           value="<?= $model['goods']['spec'][0]['group_price'] ?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">拼团库存数量 </label>
                                <div class="am-u-sm-9 am-u-md-6 am-u-lg-5 am-u-end">
                                    <input type="number" class="tpl-form-input" name="goods[spec][group_stock_num]"
                                           value="<?= $model['goods']['spec'][0]['group_stock_num'] ?>" required>
                                </div>
                            </div>
                            <?php else:?>
                            <!-- 商品多规格 -->
                             <div class="am-form-group am-padding-top">
                                    <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">商品规格 </label>
                                    <div id="many-app" v-cloak class="goods-spec-many am-u-sm-9 am-u-end"
                                         style="display: block">
                                        <div class="goods-spec-box style-simplify">
                                            <!-- 商品多规格sku信息 -->
                                            <div v-if="spec_list.length > 0" class="goods-sku am-scrollable-horizontal">
                                                <!-- sku 批量设置 -->
                                                <div class="spec-batch am-form-inline">
                                                    <div class="am-form-group">
                                                        <input type="number" v-model="batchData.group_price"
                                                               placeholder="拼团价格">
                                                    </div>
                                                    <div class="am-form-group">
                                                        <input type="number" min="0" v-model="batchData.group_stock_num"
                                                               placeholder="拼团库存">
                                                    </div>
                                                    <div class="am-form-group">
                                                        <button @click="onSubmitBatchData" type="button"
                                                                class="am-btn am-btn-sm am-btn-secondaryam-radius">批量设置
                                                        </button>
                                                    </div>
                                                </div>
                                                <!-- sku table -->
                                                <table class="spec-sku-tabel am-table am-table-bordered am-table-centered
                                     am-margin-bottom-xs am-text-nowrap">
                                                    <tbody>
                                                    <tr>
                                                        <th v-for="item in spec_attr">{{ item.group_name }}</th>
                                                        <th>商家编码</th>
                                                        <th>单买价格</th>
                                                        <th>商品库存</th>
                                                        <th class="form-require">
                                                            秒杀价格
                                                        </th>
                                                        <th class="form-require">
                                                            秒杀库存
                                                        </th>
                                                    </tr>
                                                    <tr v-for="(item, index) in spec_list">
                                                        <td v-for="td in item.rows" class="td-spec-value am-text-middle"
                                                            :rowspan="td.rowspan">
                                                            {{ td.spec_value }}
                                                        </td>
                                                        <td>{{ item.form.goods_no ? item.form.goods_no : '--' }}</td>
                                                        <td>{{ item.form.goods_price }}</td>
                                                        <td>{{ item.form.stock_num }}</td>
                                                        <td>
                                                            <input type="number" min="0" class="ipt-w80"
                                                                   name="group_price"
                                                                   v-model="item.form.group_price" required>
                                                        </td>
                                                        <td>
                                                            <input type="number" min="0" class="ipt-w80"
                                                                   name="group_stock_num"
                                                                   v-model="item.form.group_stock_num" required>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <div class="help-block">
                                                    <small>注：拼团库存为独立库存，与主商品库存不同步</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif;?>
                            <div class="am-form-group am-padding-top">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">库存计算方式 </label>
                                <div class="am-u-sm-9 am-u-md-6 am-u-lg-5 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="goods[deduct_stock_type]" value="10" data-am-ucheck
                                            <?= $model['deduct_stock_type'] == 10?'checked':'' ?> >
                                        下单减库存
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="goods[deduct_stock_type]" value="20" data-am-ucheck
                                            <?= $model['deduct_stock_type'] == 20?'checked':'' ?> >
                                        付款减库存
                                    </label>
                                </div>
                            </div>

                            <div class="am-form-group am-padding-top">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 允许单买 </label>
                                <div class="am-u-sm-9 am-u-md-6 am-u-lg-5 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="goods[is_alone]" value="1" data-am-ucheck
                                               <?= $model['is_alone']['value'] == 1?'checked':'' ?>>
                                        是
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="goods[is_alone]" value="0" data-am-ucheck 
                                        <?= $model['is_alone']['value'] == 0?'checked':'' ?>>
                                        否
                                    </label>
                                    <div class="help-block">
                                        <small>是否允许用户选择不拼团单独购买，如果允许单买，请务必设置好单买价</small>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 成团人数 </label>
                                <div class="am-u-sm-9 am-u-md-6 am-u-lg-5 am-u-end">
                                    <input type="number" min="2" class="tpl-form-input" name="goods[people]"
                                           value="<?= $model['people']?>" required>
                                    <small>拼团成员的总人数，最低2人</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 初始销量 </label>
                                <div class="am-u-sm-9 am-u-md-6 am-u-lg-5 am-u-end">
                                    <input type="number" min="0" class="tpl-form-input" name="goods[sales_initial]"
                                           value="<?= $model['sales_initial']?>">
                                </div>
                            </div>
                            <div class="am-form-group am-padding-top">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 商品状态 </label>
                                <div class="am-u-sm-9 am-u-md-6 am-u-lg-5 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="goods[goods_status]" value="10" data-am-ucheck
                                             <?= $model['goods_status']['value'] == 10?'checked':'' ?> >
                                        上架
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="goods[goods_status]" value="20" data-am-ucheck 
                                        <?= $model['goods_status']['value'] == 20?'checked':'' ?>>
                                        下架
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">排序 </label>
                                <div class="am-u-sm-9 am-u-md-6 am-u-lg-5 am-u-end">
                                    <input type="number" min="0" class="tpl-form-input" name="goods[goods_sort]"
                                           value="<?= $model['goods_sort']?>" required>
                                    <small>数字越小越靠前</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-sm am-btn-secondary"> 提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="assets/store/js/goods.spec.js"></script>
<script src="assets/plugins/vue/vue.min.js"></script>
<script>
    $(function () {

        // 注册商品多规格组件
        var specMany = new GoodsSpec({
            el: '#many-app',
            baseData:<?= $specData?>  
        });

        /**
         * 表单验证提交
         */
        $('#my-form').formPost({
            // 获取多规格sku数据
            buildData: function () {
                var specData = specMany.appVue.getData();
                return {
                    goods: {
                        spec_many: {
                            spec_list: specData.spec_list
                        }
                    }
                };
            }
        });
    });
</script>