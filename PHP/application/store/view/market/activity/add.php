<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">发布优惠活动</div>
                            </div>
                            <?php if($store['is_admin'] AND $store['wxapp']['shop_mode']['value']==20):?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">选择门店 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="activity[shop_id]" required
                                            data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'请选择门店'}">
                                        <option value=""></option>
                                        <?php if (isset($shoplist)): foreach ($shoplist as $item): ?>
                                            <option value="<?= $item['shop_id'] ?>"><?= $item['shop_name'] ?></option>
                                        <?php endforeach; endif; ?>
                                    </select>
                                    <small class="am-margin-left-xs">
                                        <a href="<?= url('shop/index') ?>">去添加</a>
                                    </small>
                                </div>
                            </div>
                            <?php endif;?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">活动名称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="activity[name]"
                                           value="" required>
                                    <small>例如：满100减10</small>
                                </div>
                            </div>
                            <div class="am-form-group" data-x-switch>
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">活动类型 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="activity[activity_type]" value="10" checked
                                               data-am-ucheck
                                               data-switch-box="switch-activity_type"
                                               data-switch-item="activity_type_10">
                                        满减
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="activity[activity_type]" value="20"
                                               data-am-ucheck
                                               data-switch-box="switch-activity_type"
                                               data-switch-item="activity_type_20">
                                        首单立减
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">减免金额 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" class="tpl-form-input" name="activity[reduce_price]"
                                           value="10" placeholder="请输入减免金额" required>
                                </div>
                            </div>
							<div class="am-form-group switch-activity_type activity_type_10">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">最低消费 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" min="0" class="tpl-form-input" name="activity[min_price]"
                                           value="100" placeholder="请输入最低消费金额" required>
									<small>设置说明：0=不限制最低消费</small>
                                </div>
                            </div>
                            <div class="am-form-group" data-x-switch>
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">显示样式 </label>
                                <div class="am-u-sm-9 am-u-end">
                                <label class="am-radio-inline">
                                        <input type="radio" name="activity[color]" value="info" data-am-ucheck> 无
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="activity[color]" value="error" data-am-ucheck> 红
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="activity[color]" value="warning" checked data-am-ucheck> 黄
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="activity[color]" value="primary" data-am-ucheck> 蓝
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="activity[color]" value="success" data-am-ucheck> 绿
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {

        // swith切换
        var $mySwitch = $('[data-x-switch]');
        $mySwitch.find('[data-switch-item]').click(function () {
            var $mySwitchBox = $('.' + $(this).data('switch-box'));
            $mySwitchBox.hide().filter('.' + $(this).data('switch-item')).show();
        });

        $('#my-form').formPost();

    });
</script>