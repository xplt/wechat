<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">免费吃设置</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">是否开启 </label>
                                <div class="am-u-sm-9">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="free[is_open]" value="1"
                                               data-am-ucheck <?= $values['is_open'] == 1 ? 'checked' : '' ?>
                                               required>
                                        开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="free[is_open]" value="0"
                                               data-am-ucheck <?= $values['is_open'] == 0 ? 'checked' : '' ?>>
                                        关闭
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">活动标题 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="free[title]" placeholder="请输入活动标题" 
                                           value="<?= $values['title'] ?>" required>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">活动规则 </label>
                                <div class="am-u-sm-9">
                                    <textarea class="" rows="3" placeholder="请输入活动规则" name="free[detail]" required><?= $values['detail'] ?></textarea>
                                </div>
                            </div>
							<div class="am-form-group am-padding-top">
                                <label class="am-u-sm-3 am-form-label form-require"> 活动时间 </label>
                                <div class="am-u-sm-9 am-u-md-6 am-u-lg-5 am-u-end">
                                    <div class="am-input-group">
                                        <input type="text" class="j-laydate-start am-form-field"
                                               name="free[start_time]" value="<?= $values['start_time'] ?>" 
                                               placeholder="开始时间">
                                        <span class="am-input-group-label am-input-group-label__center">至</span>
                                        <input type="text" class="j-laydate-end am-form-field"
                                               name="free[end_time]" value="<?= $values['end_time'] ?>" 
                                               placeholder="结束时间">
                                    </div>
                                    <div class="help-block">
                                        <small>活动的开始时间必须大于当前时间，结束时间必须大于开始时间。<br>日期格式：2020-05-25 08:30:00</small>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="assets/plugins/laydate/laydate.js"></script>
<script>
    $(function () {
		
		// 时间选择器
        laydate.render({
            elem: '.j-laydate-start'
            , type: 'datetime'
        });

        // $('.j-laydate-start').blur()
        // $activeTimeInput.blur()


        // 时间选择器
        laydate.render({
            elem: '.j-laydate-end'
            , type: 'datetime'
        });
		
        $('#my-form').formPost();

    });
</script>
