<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">砍价商品列表 - <?= $shop_name?></div>
                </div>
                <div class="widget-body am-fr">
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-6">
                        <div class="am-form-group">
                            <div class="am-btn-toolbar">
                                <div class="am-btn-group am-btn-group-xs">
                                    <a class="am-btn am-btn-default am-btn-success am-radius"
                                       href="<?= url('market.bargain.goods/add',['shop_id'=>$shop_id,'step'=>1]) ?>">
                                        <span class="am-icon-plus"></span> 新增
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>砍价ID</th>
                                <th>商品信息</th>
                                <th>自砍一刀</th>
                                <th>底价购买</th>
                                <th>帮砍人数</th>
                                <th>实际销量</th>
								<th>商品状态</th>
                                <th>排序</th>
                                <th>添加时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['goods_bargain_id'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="goods-detail">
                                            <div class="goods-image">
                                                <img src="<?= $item['goods']['image'][0]['file_path']?>" alt="">
                                            </div>
                                            <div class="goods-info dis-flex flex-dir-column flex-x-center">
                                                <p class="goods-title"><?= $item['goods']['goods_name']?></p>
                                                <p class="goods-title">ID：<?= $item['goods']['goods_id']?></p>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="am-text-middle"><?= $item['is_self_cut']['text'] ?></td>
                                    <td class="am-text-middle"><?= $item['is_floor_buy']['text'] ?></td>
                                    <td class="am-text-middle"><?= $item['people'] ?></td>
									<td class="am-text-middle"><?= $item['sales_actual'] ?></td>
                                    <td class="am-text-middle">
                                        <span class="j-status am-badge x-cur-p  <?= $item['goods_status']['value'] == 10 ? ' am-badge-success'
                                            : ' am-badge-warning' ?>" 
                                             data-ids="<?= $item['goods_bargain_id'] ?>"
                                             data-status="<?= $item['goods_status']['value'] ?>">
                                        <?= $item['goods_status']['text'] ?>
                                        </span>
                                    </td>
                                    <td class="am-text-middle"><?= $item['goods_sort'] ?></td>
                                    <td class="am-text-middle"><?= $item['create_time'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="<?= url('market.bargain.goods/edit',
                                                ['goods_bargain_id' => $item['goods_bargain_id'],'shop_id'=>$shop_id]) ?>">
                                                <i class="am-icon-pencil"></i> 编辑
                                            </a>
                                            <a href="javascript:;" class="item-del tpl-table-black-operation-del"
                                               data-ids="<?= $item['goods_bargain_id'] ?>">
                                                <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="10" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        // 切换上架状态
        $('.j-status').click(function () {
            var data = $(this).data();
            var msg = '确定要'+(parseInt(data.status) === 10 ? '下架' : '上架')+'该商品？';
            var url = "<?= url('market.bargain.goods/status') ?>";
            $('.j-status').del('goods_bargain_id', url,msg);
        });
        // 删除元素
        var url = "<?= url('market.bargain.goods/delete',['shop_id'=>$shop_id]) ?>";
        $('.item-del').del('goods_bargain_id', url);

    });
</script>

