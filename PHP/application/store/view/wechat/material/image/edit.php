<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">修改素材名称</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">素材名称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" placeholder="请输入素材名称" class="tpl-form-input" name="material[name]" value="<?= $model['name']?>" required>
                                </div>
                            </div>
							<div class="am-form-group">
								<label class="am-u-sm-3 am-u-lg-2 am-form-label am-text-xs"></label>
								<div class="am-u-sm-8 am-u-end">
									<div class="am-form-file">
										<div class="uploader-list am-cf">
											<div class="file-item">
												<a href="<?= 'uploads/'.$model['file_name'] ?>" title="点击查看大图" target="_blank">
													<img src="<?= 'uploads/'.$model['file_name'] ?>">
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交</button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        $('#my-form').formPost();

    });
</script>
