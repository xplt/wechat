<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="renderer" content="webkit"/>
    <link rel="stylesheet" href="assets/plugins/amazeui/amazeui.min.css"/>
    <link rel="stylesheet" href="assets/store/css/hema.app.css"/>
    <script src="assets/plugins/jquery/jquery.min.js"></script>
    <title>用户列表</title>
</head>
<body class="select-data">
<!-- 工具栏 -->
<div class="page_toolbar am-margin-bottom-xs am-cf">
    <form class="toolbar-form" action="">
        <input type="hidden" name="s" value="/store/data.user/lists">
        <div class="am-u-sm-12">
            <div class="am fr">
                <div class="am-form-group am-fl">
                    <select name="user_grade_id" data-am-selected="{btnSize: 'sm', placeholder: '请选择会员等级'}">
                        <option value=""></option>
                    </select>
                </div>
                <div class="am-form-group am-fl">
                    <select name="gender" data-am-selected="{btnSize: 'sm', placeholder: '请选择性别'}">
                        <option value=""></option>
                        <option value="-1">全部</option>
                        <option value="1">男</option>
                        <option value="2">女</option>
                        <option value="0">未知</option>
                    </select>
                </div>
                <div class="am-form-group am-fl">
                    <div class="am-input-group am-input-group-sm tpl-form-border-form">
                        <input type="text" class="am-form-field" name="nickName"
                               placeholder="请输入微信昵称"
                               value="">
                        <div class="am-input-group-btn">
                            <button class="am-btn am-btn-default am-icon-search"
                                    type="submit"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="am-scrollable-horizontal am-u-sm-12">
    <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black am-text-nowrap">
        <thead>
			<tr>
				<th>
					<label class="am-checkbox">
						<input data-am-ucheck data-check="all" type="checkbox">
					</label>
				</th>
				<th>用户ID</th>
				<th>微信头像</th>
				<th>微信昵称</th>
				<th>用户余额</th>
				<th>会员等级</th>
				<th>消费金额</th>
				<th>性别</th>
				<th>注册时间</th>
			</tr>
        </thead>
        <tbody>
			<?php if (!$list->isEmpty()): foreach ($list as $item): ?>
            <tr>
                <td class="am-text-middle">
                    <label class="am-checkbox">
                        <input data-am-ucheck data-check="item" data-params='<?= $item['params']?>' type="checkbox">
                    </label>
                </td>
				<td class="am-text-middle"><?= $item['user_id']?></td>
                <td class="am-text-middle">
					<a href="<?= $item['avatarUrl'] ? $item['avatarUrl'] : 'assets/store/img/head-no.png' ?>" title="点击查看大图" target="_blank">
						<img src="<?= $item['avatarUrl'] ? $item['avatarUrl'] : 'assets/store/img/head-no.png' ?>" width="72" height="72" alt="">
					</a>
				</td>
                <td class="am-text-middle"><?= $item['nickName'] ?: '--' ?></td>
                <td class="am-text-middle"><?= $item['wallet']?></td>
                <td class="am-text-middle"><?= $item['mobile']?$item['grade']['name']:'暂未开通' ?></td>
                <td class="am-text-middle"><?= $item['pay']?></td>
                <td class="am-text-middle"><?= $item['gender']?></td>
                <td class="am-text-middle"><?= $item['create_time']?></td>
            </tr>
            <?php endforeach; else: ?>
            <tr>
                <td colspan="8" class="am-text-center">暂无记录</td>
            </tr>
            <?php endif; ?>  
        </tbody>
    </table>
</div>
<div class="am-u-lg-12 am-cf">
    <div class="am-fr"><?= $list->render() ?> </div>
    <div class="am-fr pagination-total am-margin-right">
		<div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
    </div>
</div>
<script src="assets/plugins/amazeui/amazeui.min.js"></script>
<script>

    /**
     * 获取已选择的数据
     */
    function getSelectedData() {
        var data = [];
        $('input[data-check=item]:checked').each(function () {
            data.push($(this).data('params'));
        });
        return data;
    }

    $(function () {

        // 全选框元素
        var $checkAll = $('input[data-check=all]')
            , $checkItem = $('input[data-check=item]')
            , itemCount = $checkItem.length;

        // 复选框: 全选和反选
        $checkAll.change(function () {
            $checkItem.prop('checked', this.checked);
        });

        // 复选框: 子元素
        $checkItem.change(function () {
            if (!this.checked) {
                $checkAll.prop('checked', false);
            } else {
                var checkedItemNum = $checkItem.filter(':checked').length;
                checkedItemNum === itemCount && $checkAll.prop('checked', true);
            }
        });

    });
</script>
</body>
</html>