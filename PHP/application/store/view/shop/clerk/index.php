<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title a m-cf">店员列表</div>
                </div>
                <div class="widget-body am-fr">
					<div class="tips am-margin-bottom-sm am-u-sm-12">
                        <div class="pre">
                            <p> 注：店长可以登录<a href="/shop.php" target="_blank">门店管理端</a>，账号密码为手机号，如修改店长手机号则密码初始为新手机号</p>
                        </div>
                    </div>
                    <!-- 工具栏 -->
                    <div class="page_toolbar am-margin-bottom am-cf">
                        <form class="toolbar-form" action="">
                            <input type="hidden" name="s" value="/store/shop.clerk/index">
                            <div class="am-u-sm-12 am-u-md-3">
                                <div class="am-form-group">
                                    <div class="am-btn-group am-btn-group-xs">
										<a class="am-btn am-btn-default am-btn-success"
										   href="<?= url('shop.clerk/add')?>">
											<span class="am-icon-plus"></span> 新增
										</a>
                                    </div>
                                </div>
                            </div>
                            <div class="am-u-sm-12 am-u-md-9">
                                <div class="am fr">
                                    <?php if($store['is_admin'] AND $store['wxapp']['shop_mode']['value']==20):?>
                                    <div class="am-form-group am-fl">
                                        <select name="shop_id"
                                                data-am-selected="{btnSize: 'sm', placeholder: '所属门店'}">
                                            <option value=""></option>
											<?php if (isset($catgory)): foreach ($catgory as $item): ?>
												<option value="<?= $item['shop_id'] ?>"><?= $item['shop_name'] ?></option>
											<?php endforeach; endif; ?>
                                        </select>
                                    </div>
                                    <?php endif;?>
                                    <div class="am-form-group am-fl">
                                        <div class="am-input-group am-input-group-sm tpl-form-border-form">
                                            <input type="text" class="am-form-field" name="search"
                                                   placeholder="请输入店员姓名/手机号"
                                                   value="">
                                            <div class="am-input-group-btn">
                                                <button class="am-btn am-btn-default am-icon-search"
                                                        type="submit"></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>店员ID</th>
                                <th>所属门店</th>
                                <th>店员姓名</th>
                                <th>店员手机号</th>
                                <th>店员身份</th>
                                <th>添加时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['shop_clerk_id'] ?></td>
                                    <td class="am-text-middle">
                                        <p class="item-title"><?= $item['shop']['shop_name'] ?></p>
                                    </td>
                                    <td class="am-text-middle"><?= $item['real_name'] ?></td>
                                    <td class="am-text-middle"><?= $item['mobile'] ?></td>
                                    <td class="am-text-middle">
                                            <span class="am-badge x-cur-p  <?= $item['status']['value']==20 ? ' am-badge-warning'
                                                : ' am-badge-success' ?>">
                                            <?= $item['status']['text'] ?>
                                            </span>
                                    </td>
                                    <td class="am-text-middle"><?= $item['create_time'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="<?= url('shop.clerk/edit',
                                                ['shop_clerk_id' => $item['shop_clerk_id']]) ?>">
                                                <i class="am-icon-pencil"></i> 编辑
                                            </a>
                                            <a href="javascript:;" class="item-del tpl-table-black-operation-del"
                                               data-ids="<?= $item['shop_clerk_id'] ?>">
                                                <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="7" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        // 删除元素
        var url = "<?= url('shop.clerk/delete')?>";
        $('.item-del').del('shop_clerk_id', url);

    });
</script>

