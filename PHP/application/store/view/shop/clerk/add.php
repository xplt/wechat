<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">添加店员</div>
                            </div>
                            <?php if($store['is_admin'] AND $store['wxapp']['shop_mode']['value']==20):?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 所属门店 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="clerk[shop_id]" 
										data-am-selected="{ btnSize: 'sm', placeholder:'请选择', maxHeight: 400}" required>
                                        <option value=""></option>
										<?php if (isset($catgory)): foreach ($catgory as $item): ?>
                                            <option value="<?= $item['shop_id'] ?>">
                                                <?= $item['shop_name'] ?></option>
                                        <?php endforeach; endif; ?>
                                    </select>
									<small class="am-margin-left-xs">
							            <a href="<?= url('shop/index') ?>">去添加</a>
							        </small>
                                </div>
                            </div>
                            <?php endif;?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 店员姓名 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="clerk[real_name]"
                                           placeholder="请输入店员姓名" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 手机号 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="clerk[mobile]"
                                           placeholder="请输入手机号" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 身份 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="clerk[status]" value="10" data-am-ucheck
                                               checked>
                                        店员
                                    </label>
                                    <?php if($store['is_admin']):?>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="clerk[status]" value="20" data-am-ucheck>
                                        店长
                                    </label>
                                    <?php endif;?>
									<label class="am-radio-inline">
                                        <input type="radio" name="clerk[status]" value="30" data-am-ucheck>
                                        配送
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        $('#my-form').formPost();

    });
</script>