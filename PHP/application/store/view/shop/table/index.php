<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">餐桌列表</div>
                </div>
                <div class="widget-body am-fr">
                    <!-- 工具栏 -->
                    <div class="page_toolbar am-margin-bottom am-cf">
                        <form class="toolbar-form" action="">
                            <input type="hidden" name="s" value="/store/shop.table/index">
                            <div class="am-u-sm-12 am-u-md-3">
                                <div class="am-form-group">
                                    <div class="am-btn-group am-btn-group-xs">
                                        <a class="am-btn am-btn-default am-btn-success"
                                           href="<?= url('shop.table/add')?>">
                                            <span class="am-icon-plus"></span> 新增
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="am-u-sm-12 am-u-md-9">
                                <div class="am fr">
                                    <?php if($store['is_admin'] AND $store['wxapp']['shop_mode']['value']==20):?>
                                    <div class="am-form-group am-fl">
                                        <select name="shop_id"
                                                data-am-selected="{btnSize: 'sm', placeholder: '所属门店'}">
                                            <option value=""></option>
                                            <?php if (isset($catgory)): foreach ($catgory as $item): ?>
                                                <option value="<?= $item['shop_id'] ?>"><?= $item['shop_name'] ?></option>
                                            <?php endforeach; endif; ?>
                                        </select>
                                    </div>
                                    <?php endif;?>
                                    <div class="am-form-group am-fl">
                                        <div class="am-input-group am-input-group-sm tpl-form-border-form">
                                            <input type="text" class="am-form-field" name="search"
                                                   placeholder="请输入餐桌/包间号"
                                                   value="">
                                            <div class="am-input-group-btn">
                                                <button class="am-btn am-btn-default am-icon-search"
                                                        type="submit"></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>餐桌ID</th>
                                <th>二维码</th>
                                <th>餐桌/包间</th>
                                <th>容纳人数</th>
                                <th>最低消费</th>
								<th>餐具费/人</th>
								<th>状态</th>
                                <th>门店</th>
                                <th>排序</th>
                                <th>添加时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($list): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['table_id'] ?></td>
									<td class="am-text-middle">
										<a href="assets/qrcode/table/<?= $item['table_id'] ?>.png"
                                           title="点击获取二维码" target="_blank">
                                            <img src="assets/store/img/2code.png" width="50" height="50" alt="餐桌包间二维码">
                                        </a>
									</td>
                                    <td class="am-text-middle">
                                        <p class="item-title"><?= $item['table_name'] ?></p>
                                    </td>
                                    <td class="am-text-middle"><?= $item['people'] ?></td>
									<td class="am-text-middle"><?= $item['consume'] ?></td>
									<td class="am-text-middle"><?= $item['ware_price'] ?></td>
                                    <td class="am-text-middle">
                                            <span class="<?= $item['status']['value'] == 10 ? 'x-color-green'
                                                : 'x-color-red' ?>">
                                            <?= $item['status']['text'] ?>
                                            </span>
                                    </td>
                                    <td class="am-text-middle"><?= $item['shop']['shop_name'] ?></td>
									<td class="am-text-middle"><?= $item['sort'] ?></td>
                                    <td class="am-text-middle"><?= $item['create_time'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="<?= url('shop.table/edit',
                                                ['table_id' => $item['table_id']]) ?>">
                                                <i class="am-icon-pencil"></i> 编辑
                                            </a>
                                            <a href="javascript:;" class="item-del tpl-table-black-operation-del"
                                               data-ids="<?= $item['table_id'] ?>">
                                                <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="11" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        // 删除元素
        var url = "<?= url('shop.table/delete') ?>";
        $('.item-del').del('table_id', url);

    });
</script>

