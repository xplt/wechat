<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">图文资讯列表 - <?= $shop_name?></div>
                </div>
                <div class="widget-body am-fr">
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-6">
                        <div class="am-form-group">
                            <div class="am-btn-toolbar">
                                <div class="am-btn-group am-btn-group-xs">
                                    <a class="am-btn am-btn-default am-btn-success am-radius"
                                       href="<?= url('shop.article/add',['shop_id'=>$shop_id]) ?>">
                                        <span class="am-icon-plus"></span> 新增
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>资讯ID</th>
                                <th>封面图片</th>
                                <th>资讯标题</th>
                                <th>资讯分类</th>
                                <th>实际阅读量</th>
								<th>显示类型</th>
                                <th>显示状态</th>
                                <th>显示排序</th>
                                <th>添加时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['article_id'] ?></td>
                                    <td class="am-text-middle">
                                        <a href="<?= $item['image']['file_path'] ?>"
                                           title="点击查看大图" target="_blank">
                                            <img src="<?= $item['image']['file_path'] ?>"
                                                 width="50" height="50" alt="封面图片">
                                        </a>
                                    </td>
                                    <td class="am-text-middle">
                                        <p class="item-title"><?= $item['article_title'] ?></p>
                                    </td>
                                    <td class="am-text-middle"><?= $item['category']['name'] ?></td>
                                    <td class="am-text-middle"><?= $item['actual_views'] ?></td>
                                    <td class="am-text-middle">
                                            <span class="<?= $item['show_type']['value'] == 10 ? 'x-color-green'
                                                : 'x-color-red' ?>">
                                            <?= $item['show_type']['text'] ?>
                                            </span>
                                    </td>
									<td class="am-text-middle">
										<span class="j-status am-badge x-cur-p  <?= $item['article_status']['value'] == 10 ? ' am-badge-success'
											: ' am-badge-warning' ?>" 
											 data-ids="<?= $item['article_id'] ?>"
											 data-status="<?= $item['article_status']['value'] ?>">
										<?= $item['article_status']['text'] ?>
										</span>
                                    </td>
									<td class="am-text-middle"><?= $item['article_sort'] ?></td>
                                    <td class="am-text-middle"><?= $item['create_time'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="<?= url('shop.article/edit',
                                                ['article_id' => $item['article_id'],'shop_id'=>$shop_id]) ?>">
                                                <i class="am-icon-pencil"></i> 编辑
                                            </a>
                                            <a href="javascript:;" class="item-del tpl-table-black-operation-del"
                                               data-ids="<?= $item['article_id'] ?>">
                                                <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="10" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
		
		// 切换状态
        $('.j-status').click(function () {
            var data = $(this).data();
			var msg = '确定要'+(parseInt(data.status) === 1 ? '隐藏' : '显示')+'该资讯？';
            var url = "<?= url('shop.article/status') ?>";
			$('.j-status').del('article_id', url,msg);
        });

        // 删除元素
        var url = "<?= url('shop.article/delete',['shop_id'=>$shop_id]) ?>";
        $('.item-del').del('article_id', url);

    });
</script>

