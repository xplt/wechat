<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">门店列表</div>
                </div>
                <div class="widget-body am-fr">
                    <div class="tips am-margin-bottom-sm am-u-sm-12">
                        <div class="pre">
                            <p>  如门店二维码无法打开获取，请编辑该门店重新保存下会重新生成最新二维码</p>
                        </div>
                    </div>
					<!-- 工具栏 -->
					<?php if($store['wxapp']['shop_mode']['value']==20):?>
                    <div class="page_toolbar am-margin-bottom-xs am-cf">
                        <div class="am-u-sm-12 am-u-md-3">
							<div class="am-form-group">
								<div class="am-btn-group am-btn-group-xs">
									<a class="am-btn am-btn-default am-btn-success" href="<?= url('shop/add') ?>">
										<span class="am-icon-plus"></span> 新增
									</a>
								</div>
							</div>
						</div>
                    </div>
					<?php endif;?>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>门店ID</th>
                                <th>门店LOGO</th>
                                <th>门店名称</th>
                                <th>营业时间</th>
                                <th>联系人</th>
                                <th>联系电话</th>
								<th>二维码</th>
								<th>门店状态</th>
                                <th>创建时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['shop_id'] ?></td>
                                    <td class="am-text-middle">
                                        <a href="<?= $item['logo']['file_path'] ?>" title="点击查看大图" target="_blank">
                                            <img src="<?= $item['logo']['file_path'] ?>" width="50" height="50" alt="门店图片">
                                        </a>
                                    </td>
                                    <td class="am-text-middle">
                                        <p class="item-title"><?= $item['shop_name'] ?></p>
                                    </td>
                                    <td class="am-text-middle"><?= $item['shop_hours'] ?></td>
                                    <td class="am-text-middle"><?= $item['linkman'] ?></td>
                                    <td class="am-text-middle"><?= $item['phone'] ?></td>
									<td class="am-text-middle">
										<a href="assets/qrcode/shop/<?= $item['shop_id']?>.png" title="点击查看大图" target="_blank">
											<i class="iconfont iconerweima" style="font-size:20px;"></i>
										</a>
									</td>
                                    <td class="am-text-middle">
                                            <span class="j-state am-badge x-cur-p  <?= $item['status']['value'] ? ' am-badge-success'
                                                : ' am-badge-warning' ?>" 
                                                 data-ids="<?= $item['shop_id'] ?>"
                                                 data-status="<?= $item['status']['value'] ?>">
                                            <?= $item['status']['text'] ?>
                                            </span>
                                    </td>
                                    <td class="am-text-middle"><?= $item['create_time'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="<?= url('shop/edit',
                                                ['shop_id' => $item['shop_id']]) ?>">
                                                <i class="am-icon-pencil"></i> 编辑
                                            </a>
                                            <a href="javascript:;" class="item-del tpl-table-black-operation-del"
                                               data-ids="<?= $item['shop_id'] ?>">
                                                <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="10" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
		
		// 切换状态
        $('.j-state').click(function () {
            var data = $(this).data();
			var msg = '确定要'+(parseInt(data.status) === 1 ? '暂停营业' : '开启营业')+'吗？';
            var url = "<?= url('shop/status') ?>";
			$('.j-state').del('shop_id', url,msg);
        });
        // 删除元素
        var url = "<?= url('shop/delete') ?>";
        $('.item-del').del('shop_id', url);

    });
</script>

