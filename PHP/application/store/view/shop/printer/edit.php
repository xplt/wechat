<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">编辑设备</div>
                            </div>
                            <?php if($store['is_admin'] AND $store['wxapp']['shop_mode']['value']==20):?>
							<div class="am-form-group">
							    <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">选择门店 </label>
							    <div class="am-u-sm-9 am-u-end">
							        <select name="printer[shop_id]" required
							                data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'请选择门店'}">
							            <option value=""></option>
							            <?php if (isset($shoplist)): foreach ($shoplist as $item): ?>
							                <option value="<?= $item['shop_id'] ?>" 
											<?= $model['shop_id'] == $item['shop_id'] ? 'selected' : '' ?>><?= $item['shop_name'] ?>
											</option>
							            <?php endforeach; endif; ?>
							        </select>
							    </div>
							</div>
                            <?php endif;?>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">设备品牌 </label>
                                <div class="am-u-sm-9 am-u-end" style="line-height: 40px;vertical-align: middle;"><?= $model['brand']['text'] ?></div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">云端账号 </label>
                                <div class="am-u-sm-9 am-u-end" style="line-height: 40px;vertical-align: middle;"><?= $model['open_user_id'] ?></div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">设备编号 </label>
                                <div class="am-u-sm-9 am-u-end" style="line-height: 40px;vertical-align: middle;"><?= $model['uuid']['value'] ?></div>
                            </div>
							<?php if($model['brand']['value']!=10):?>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">设备密钥 </label>
                                <div class="am-u-sm-9 am-u-end" style="line-height: 40px;vertical-align: middle;"><?= $model['key'] ?></div>
                            </div>
							<?php endif;?>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">设备状态 </label>
                                <div class="am-u-sm-9 am-u-end" style="line-height: 40px;vertical-align: middle;">
									<span class="<?= $model['uuid']['text'] == '正常' ? 'x-color-green'
                                                : 'x-color-red' ?>">
                                            <?= $model['uuid']['text'] ?>
                                    </span>
								</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">设备名称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="printer[printer_name]"
                                           value="<?= $model['printer_name'] ?>" placeholder="请为设备自定义个名称" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">打印份数 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" class="tpl-form-input" name="printer[printer_num]" value="<?= $model['printer_num'] ?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">是否开启 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="printer[is_open]" value="1" data-am-ucheck 
                                        <?= $model['is_open']['value']==1?'checked':'' ?>>
                                        开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="printer[is_open]" value="0" data-am-ucheck 
                                        <?= $model['is_open']['value']==0?'checked':'' ?>>
                                        关闭
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        $('#my-form').formPost();

    });
</script>
