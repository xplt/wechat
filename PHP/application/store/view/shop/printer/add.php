<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">添加打印机</div>
                            </div>
                            <?php if($store['is_admin'] AND $store['wxapp']['shop_mode']['value']==20):?>
							<div class="am-form-group">
							    <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">选择门店 </label>
							    <div class="am-u-sm-9 am-u-end">
							        <select name="printer[shop_id]" required
							                data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'请选择门店'}">
							            <option value=""></option>
							            <?php if (isset($shoplist)): foreach ($shoplist as $item): ?>
							                <option value="<?= $item['shop_id'] ?>"><?= $item['shop_name'] ?></option>
							            <?php endforeach; endif; ?>
							        </select>
									<small class="am-margin-left-xs">
							            <a href="<?= url('shop/index') ?>">去添加</a>
							        </small>
							    </div>
							</div>
                            <?php endif;?>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">设备品牌 </label>
                                <div class="am-u-sm-9 am-u-end">
									<select v-model="selected" name="printer[brand]" style="width:200px;" required>
                                        <option value="">请选择品牌</option>
                                        <template>
											<option :value="10">对对机</option>
											<option :value="20">飞鹅</option>
											<option :value="30">易联云</option>
                                        </template>
									</select>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">设备名称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="printer[printer_name]" placeholder="请为设备自定义个名称" required>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">设备编号 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="printer[uuid]"
                                         placeholder="请输入设备编号" required>
                                </div>
                            </div>
							<div class="am-form-group" v-if="selected!=10">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">设备密钥 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="printer[key]"
                                         placeholder="请输入设备密钥" required>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">流量卡号码 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" class="tpl-form-input" name="printer[phone]">
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">打印份数 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" class="tpl-form-input" name="printer[printer_num]" value="1" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">是否开启 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="printer[is_open]" value="1" data-am-ucheck
                                               checked>
                                        开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="printer[is_open]" value="0" data-am-ucheck>
                                        关闭
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="assets/plugins/vue/vue.min.js"></script>
<script>
    $(function () {
		
		// 渲染页面
		new Vue({
			el: '#my-form',
			data: {
				selected:10
			}
		});

        $('#my-form').formPost();

    });
</script>
