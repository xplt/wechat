<link rel="stylesheet" href="assets/plugins/layui/css/layui.css"/>
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">编辑门店</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 门店名称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="shop[shop_name]"
                                           placeholder="请输入门店名称" value="<?= $model['shop_name']?>" required>
                                </div>
                            </div>
                            <?php if($app_type == 'food'):?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 门店分类 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="shop[shop_category_id]" 
                                        data-am-selected="{ btnSize: 'sm', placeholder:'请选择', maxHeight: 400}" required>
                                        <option value=""></option>
                                        <?php if (isset($catgory)): foreach ($catgory as $item): ?>
                                            <option value="<?= $item['shop_category_id'] ?>" <?= $model['shop_category_id'] == $item['shop_category_id'] ? 'selected' : '' ?>>
                                                <?= $item['name'] ?></option>
                                        <?php endforeach; endif; ?>
                                    </select>
                                </div>
                            </div>
                            <?php endif;?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label"> 门店logo </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <div class="am-form-file">
                                            <button type="button"
                                                    class="upload-file am-btn am-btn-secondary am-radius">
                                                <i class="am-icon-cloud-upload"></i> 选择图片
                                            </button>
                                            <div class="uploader-list am-cf">
												<div class="file-item">
													<img src="<?= $model['logo']['file_path'] ?>">
													<input type="hidden" name="shop[logo_image_id]"
															   value="<?= $model['logo_image_id'] ?>">
													<i class="iconfont iconshanchu file-item-delete"></i>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label"> 门头照片 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <div class="am-form-file">
                                            <button type="button"
                                                    class="upload-file-front am-btn am-btn-secondary am-radius">
                                                <i class="am-icon-cloud-upload"></i> 选择图片
                                            </button>
                                            <div class="uploader-list am-cf">
												<div class="file-item">
													<img src="<?= $model['front']['file_path'] ?>">
													<input type="hidden" name="shop[front_id]"
															   value="<?= $model['front_id'] ?>">
													<i class="iconfont iconshanchu file-item-delete"></i>
												</div>
                                            </div>
                                        </div>
										<div class="help-block am-margin-top-sm">
                                            <small>尺寸750x300像素以上，大小2M以下</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group am-padding-top">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 联系人 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="shop[linkman]"
                                           placeholder="请输入门店联系人" value="<?= $model['linkman']?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 联系电话 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="shop[phone]"
                                           placeholder="请输入门店联系电话" value="<?= $model['phone']?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 营业时间 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="shop[shop_hours]" id="shop_hours" 
                                           placeholder="请选择营业时间" value="<?= $model['shop_hours']?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 详细地址 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="shop[address]"
                                           placeholder="请输入详细地址" value="<?= $model['address']?>" required>
									<small>不用填写省、市、区/县</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 门店坐标 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-block">
                                        <input type="text" style="background: none !important;" id="coordinate"
                                               class="tpl-form-input" name="shop[coordinate]" placeholder="请选择门店坐标" 
											   value="<?= $model['coordinate']?>" readonly="" required>
                                    </div>
                                    <div class="am-block am-padding-top-xs">
                                        <iframe id="map" src="index.php?s=/store/shop/getpoint"
                                                width="915"
                                                height="610"></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group am-padding-top">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label"> 门店简介 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <textarea class="am-field-valid" rows="5" placeholder="请输入门店简介"
                                              name="shop[summary]"><?= $model['summary']?></textarea>
                                </div>
                            </div>
                            <?php if($store['wxapp']['app_type']['value'] == 'food'):?>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">堂食模式 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="shop[tang_mode]" value="1" data-am-ucheck 
										<?= $model['tang_mode'] == 1?'checked':''?>>
                                        选桌点餐
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="shop[tang_mode]" value="2" data-am-ucheck 
										<?= $model['tang_mode'] == 2?'checked':''?>>
                                        排号点餐
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 自动接单 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="shop[is_order]" value="0" data-am-ucheck 
                                        <?= $model['is_order']['value'] == 0?'checked':''?>>
                                        关闭
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="shop[is_order]" value="1" data-am-ucheck 
                                        <?= $model['is_order']['value'] == 1?'checked':''?>>
                                        开启
                                    </label>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 自动配送 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input v-model="checked" type="radio" name="shop[is_delivery]" value="0" 
											<?= $model['is_delivery']['value'] == 0?'checked':''?> data-am-ucheck>
                                        关闭
                                    </label>
                                    <label class="am-radio-inline">
                                        <input v-model="checked" type="radio" name="shop[is_delivery]" value="1" 
										<?= $model['is_delivery']['value'] == 1?'checked':''?> data-am-ucheck>
                                        开启
                                    </label>
									<div class="help-block">
                                        <small>用户下单后是否自动接受订单</small>
                                    </div>
                                </div>
                            </div>
							<div v-if="checked == 1" class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 配送公司 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="shop[delivery_id]" value="10" 
										<?= $model['delivery_id'] == 10?'checked':''?> data-am-ucheck>
                                        商家自配
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="shop[delivery_id]" value="40" 
										<?= $model['delivery_id'] == 40?'checked':''?> data-am-ucheck>
                                        UU跑腿
                                    </label>
									<label class="am-radio-inline">
                                        <input type="radio" name="shop[delivery_id]" value="20"  
										<?= $model['delivery_id'] == 20?'checked':''?> data-am-ucheck>
                                        顺丰同城
                                    </label>
									<label class="am-radio-inline">
                                        <input type="radio" name="shop[delivery_id]" value="30" 
										<?= $model['delivery_id'] == 30?'checked':''?> data-am-ucheck>
                                        达达快送
                                    </label>
                                    <div class="help-block">
                                        <small>外卖订单有效，请咨询服务商后选择设置</small>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 骑手抢单 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="shop[is_grab]" value="0" data-am-ucheck <?= $model['is_grab']['value'] == 0?'checked':''?>>
                                        关闭
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="shop[is_grab]" value="1" data-am-ucheck <?= $model['is_grab']['value'] == 1?'checked':''?>>
                                        开启
                                    </label>
                                    <div class="help-block">
                                        <small>商家自配有效，开启后需要骑手接单。关闭后配送员为店长</small>
                                    </div>
                                </div>
                            </div>
                            <?php endif;?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">顺丰门店ID </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="shop[sf_shop_id]"
                                           value="<?= $model['sf_shop_id']?>">
                                    <small>使用顺丰配送必须配置该项</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">达达门店ID </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="shop[dada_shop_id]"
                                           value="<?= $model['dada_shop_id']?>">
                                    <small>使用达达配送必须配置该项</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require"> 门店状态 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="shop[status]" value="1" data-am-ucheck
                                               <?= $model['status']['value'] == 1?'checked':''?>>
                                        营业中
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="shop[status]" value="0" data-am-ucheck 
                                        <?= $model['status']['value'] == 0?'checked':''?>>
                                        歇业中
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">显示排序 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" class="tpl-form-input" name="shop[sort]"
                                           value="<?= $model['sort']?>" required>
                                    <small>数字越小越靠前</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 图片文件列表模板 -->
{{include file="layouts/_template/tpl_file_item" /}}
<!-- 文件库弹窗 -->
{{include file="layouts/_template/file_library" /}}
<script src="assets/plugins/layui/layui.js" charset="utf-8"></script>
<script src="assets/plugins/vue/vue.min.js"></script>
<script>
	
	layui.use('laydate', function(){
	  var laydate = layui.laydate;
	  //时间范围
	  laydate.render({
		elem: '#shop_hours'
		,format: 'HH:mm'
		,type: 'time'
		,range: true
	  });
	});

    /**
     * 设置坐标
     */
    function setCoordinate(value) {
        var $coordinate = $('#coordinate');
        $coordinate.val(value);
        // 触发验证
        $coordinate.trigger('change');
    }
	
    $(function () {
		new Vue({
		  el: '#my-form',
		  data: {
			checked:<?= $model['is_delivery']['value']?>
		  }
		});
        // 选择图片
        $('.upload-file').selectImages({
            name: 'shop[logo_image_id]'
        });
		// 选择图片
        $('.upload-file-front').selectImages({
            name: 'shop[front_id]'
        });

        $('#my-form').formPost();

    });
</script>
