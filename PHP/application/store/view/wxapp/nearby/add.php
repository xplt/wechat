<link rel="stylesheet" href="assets/plugins/layui/css/layui.css"/>
<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">添加展示门店</div>
                            </div>
                            <div class="tips am-margin-bottom-sm am-u-sm-12">
                                <div class="pre">
                                    <p> 提醒：请确保门店信息补充完整
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">选择门店 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <select name="nearby[shop_id]" required
                                            data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'请选择门店'}">
                                        <option value=""></option>
                                        <?php if (isset($shoplist)): foreach ($shoplist as $item): ?>
                                            <option value="<?= $item['shop_id'] ?>"><?= $item['shop_name'] ?>
                                            </option>
                                        <?php endforeach; endif; ?>
                                    </select>
                                    <small class="am-margin-left-xs">
                                        <a href="<?= url('shop/index') ?>">去添加</a>
                                    </small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">营业执照 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <button type="button"
                                                class="upload-file-qualification upload-but am-btn am-btn-secondary am-radius">
                                            <i class="am-icon-cloud-upload"></i> 选择图片
                                        </button>
                                        <div class="uploader-list am-cf">
                                        </div>
                                    </div>
                                    <div class="help-block am-margin-top-sm">
                                        <small>营业执照（与小程序主体一致），大小2M以下</small>
                                    </div>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">主体名称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input name="nearby[company_name]" type="text" class="tpl-form-input" required >
									<small>营业执照名称</small> 
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">主体编号 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input name="nearby[credential]" type="text" class="tpl-form-input" required >
									<small>对应营业执照的统一信用编号</small> 
                                </div>
                            </div>
							<!-- 事件功能 -->
							<div class="am-modal am-modal-loading am-modal-no-btn" tabindex="-1" id="my-modal-loading"></div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 图片文件列表模板 -->
{{include file="layouts/_template/tpl_file_item_filename" /}}
<!-- 文件库弹窗 -->
{{include file="layouts/_template/file_library" /}}
<script src="assets/plugins/layui/layui.js" charset="utf-8"></script>
<script>
	
	layui.use('laydate', function(){
	  var laydate = layui.laydate;
	  //时间范围
	  laydate.render({
		elem: '#hour'
		,format: 'HH:mm'
		,type: 'time'
		,range: true
	  });
	});
    $(function () {
		$(document).ready(function(){$("#ca").hide();});
        // 门头照片
        $('.upload-file').selectImages({
            name: 'nearby[pic_list]'
        });
        // 营业执照
        $('.upload-file-qualification').selectImages({
            name: 'nearby[qualification_list]'
        });

        $('#my-form').formPost();

    });
</script>
