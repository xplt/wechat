<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">添加附近门店展示类目</div>
                            </div>
                             <div class="am-form-group">
							    <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">选择类目 </label>
							    <div class="am-u-sm-9 am-u-end">
							        <select name="category[id]" required
							                data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder:'请选择类目'}">
							            <option value=""></option>
							            <?php if (isset($category)): foreach ($category['first']['children'] as $item): ?>
							                <option value="<?= $item['id'] ?>"><?= $item['name'] ?>
											</option>
							            <?php endforeach; endif; ?>
							        </select>
							    </div>
							</div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">食品经营许可证 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <button type="button"
                                                class="upload-file am-btn am-btn-secondary am-radius">
                                            <i class="am-icon-cloud-upload"></i> 选择图片
                                        </button>
                                        <div class="uploader-list am-cf">
                                        </div>
                                    </div>
                                </div>
                            </div>
							<!-- 事件功能 -->
							<div class="am-modal am-modal-loading am-modal-no-btn" tabindex="-1" id="my-modal-loading"></div>
							<div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 图片文件列表模板 -->
{{include file="layouts/_template/tpl_file_item_filename" /}}
<!-- 文件库弹窗 -->
{{include file="layouts/_template/file_library" /}}

<script>
    $(function () {
        // 选择图片
        $('.upload-file').selectImages({
            name: 'category[file_name]'
			, multiple: true
        });

        $('#my-form').formPost();

    });
</script>
