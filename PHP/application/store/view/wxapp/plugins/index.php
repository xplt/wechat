<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">微信小程序插件</div>
                </div>
                <div class="widget-body am-fr">
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>插件ID</th>
                                <th>插件头像</th>
                                <th>插件名称</th>
                                <th>插件版本</th>
                                <th>插件状态</th>
                                <th>更新时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['web_wxapp_plugins_id'] ?></td>
                                    <td class="am-text-middle">
                                        <a href="<?= $item['headimgurl'] ?>"
                                           title="点击查看大图" target="_blank">
                                            <img src="<?= $item['headimgurl'] ?>"
                                                 width="50" height="50" alt="插件图片">
                                        </a>
                                    </td>
                                    <td class="am-text-middle">
                                        <p class="item-title"><?= $item['nickname'] ?></p>
                                    </td>
                                    <td class="am-text-middle"><?= $item['user_version'] ?></td>
									<td class="am-text-middle">
                                            <span class="<?= $item['status']['value'] ? 'x-color-green'
                                                : 'x-color-red' ?>">
                                            <?= $item['status']['text'] ?>
                                            </span>
                                    </td>
                                    <td class="am-text-middle"><?= $item['update_time'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
										<?php if($item['status']['value']==2):?>
											<a href="javascript:;" class="item-del tpl-table-black-operation-del"
                                               data-ids="<?= $item['plugin_appid'] ?>">
                                                <i class="am-icon-trash"></i> 卸载
                                            </a>
										<?php else:?>
											<a href="javascript:;" class="item-add"
                                               data-ids="<?= $item['plugin_appid'] ?>">
                                                <i class="am-icon-gears"></i> 安装
                                            </a>
										<?php endif;?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="7" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        // 卸载
        var url = "<?= url('wxapp.plugins/delete') ?>";
        $('.item-del').del('plugin_appid', url,'确定要卸载吗？');
		// 安装
        var url = "<?= url('wxapp.plugins/add') ?>";
        $('.item-add').del('plugin_appid', url,'确定要安装吗？');

    });
</script>

