<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">设置业务域名</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">业务域名 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="wxapp[webview_domain]"
                                           value="<?= $model['webview_domain']?>" required>
										<small>
                                            业务域名必须在开放平台进行配置<br>
                                            填写格式如 https://www.hemaphp.com。（建议协议头加S：https://）<br>
                                            最多可以添加100个业务域名，多个用分号（;）隔开
                                        </small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {

        $('#my-form').formPost();

    });
</script>
