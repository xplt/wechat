<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" method="post">
                    <div class="widget-body">
                        <fieldset>
						<?php if($wxapp['is_empower']['value']== 0): ?>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">绑定微信小程序</div>
                            </div>
							<div class="widget-body am-cf">
								<div style="background:#eeeeee;margin-left:100px;border-radius:10px;padding:20px;" class="am-u-sm-6 am-u-md-6 am-u-lg-3">
									<div style="text-align:center;">我已有微信小程序</div>
									<div style="margin-top:20px;text-align:center;">
										<a class="am-btn am-btn-default am-btn-success" href="<?= url('wxapp/auth')?>">
                                            一键授权
                                        </a>
									</div>
								</div>
								<div style="background:#eeeeee;margin-left:100px;border-radius:10px;padding:20px;" class="am-u-sm-6 am-u-md-6 am-u-lg-3">
									<div style="text-align:center;">
									<?php 
									if(!$auth){
										echo '我还没有微信小程序';
									}else{
										if($auth['pay_status']['value']==10){
											echo '现在状态：<code>待付审核费</code>';
										}else{
											echo '现在状态：<code>'.$auth['apply_status']['text'].'</code>';
										}
									}?>
									</div>
									<div style="margin-top:20px;text-align:center;">
										<a class="am-btn am-btn-default am-btn-success" href="<?= $auth ? '/index.php?s=/user/apply/index' : url('wxapp.register/app')?>">
                                            <?= $auth ? '查看详情':'快速注册'?>
                                        </a>
									</div>
								</div>
								<div style="" class="am-u-sm-6 am-u-md-6 am-u-lg-3">
									
								</div>
								
							</div>
						<?php else:?>
							<div class="tips am-margin-bottom-sm am-u-sm-12">
		                        <div class="pre">
		                            <p> 提醒：发布小程序前必须完成初始化配置。配置项目 1.小程序头像 2.小程序名称 3.功能介绍 4.服务器域名 5.服务类目<br>
		                            </p>
		                        </div>
		                    </div>
							<div class="widget-head am-cf">
                                <div class="widget-title am-fl">小程序信息</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">切换/更新授权 </label>
                                <div class="am-u-sm-9">
									<div class="am-btn-group am-btn-group-xs">
										<a class="am-btn am-btn-default am-btn-secondary am-radius" href="<?= url('wxapp/auth')?>">
											<span class="am-icon-retweet am-icon-sm"></span> 切换/更新授权
										</a>
									</div>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">同步公众平台设置 </label>
                                <div class="am-u-sm-9">
									<div class="am-btn-group am-btn-group-xs">
										<a href="javascript:;" class="item-del am-btn am-btn-default am-btn-secondary am-radius" data-ids="<?= $wxapp['app_id'] ?>">
											<span class="am-icon-refresh am-icon-sm"></span> 一键同步
										</a>
									</div>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">小程序头像 </label>
                                <div class="am-u-sm-9">
                                    <a href="<?= $wxapp['head_img'] ? $wxapp['head_img'] : 'assets/store/img/wechatapp.png' ?>" title="点击查看大图" target="_blank">
                                        <img style="border-radius: 50%;" src="<?= $wxapp['head_img'] ? $wxapp['head_img'] : 'assets/store/img/wechatapp.png' ?>" width="72" height="72" alt="">
                                    </a>
									<?php if($wxapp['qrcode_url']):?>
									<a href="<?= $wxapp['qrcode_url']?>" title="线上二维码" target="_blank">
                                        <img style="margin-left:50px;" src="<?= $wxapp['qrcode_url']?>" width="72" height="72" alt="">
                                    </a>
									<?php endif;?>
									<a href="<?= url('wxapp/testCode',['wxapp_id' => $wxapp['wxapp_id']])?>" title="体验版本" target="_blank">
                                        <img style="margin-left:50px;" src="assets/store/img/code_test.png" width="72" height="72" alt="">
                                    </a>
									<div class="help-block am-margin-top-sm">
										<small>
										<?php if(isset($infor['head_image_info'])): ?>
											每月可修改<?= $infor['head_image_info']['modify_quota']?>次，
											本月还可修改<?= $infor['head_image_info']['modify_quota']-$infor['head_image_info']['modify_used_count']?>次。
										<?php endif; ?>
											<a href="<?= url('wxapp/sethead') ?>">修改</a>
										</small>
									</div>
                                </div>
								
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">小程序名称 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $wxapp['app_name'] ?>" disabled="disabled">
									<small>友情提醒：名称设置后不可修改<a href="<?= url('wxapp.name/setting') ?>">设置</a></small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">
                                    AppID <span class="tpl-form-line-small-title">小程序ID</span>
                                </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $wxapp['app_id'] ?>" disabled="disabled">
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">小程序介绍 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="wxapp[signature]" value="<?= $wxapp['signature'] ?>" required>
										<small>
										<?php if(isset($infor['signature_info'])): ?>
											每月可修改<?= $infor['signature_info']['modify_quota']?>次，
											本月还可修改<?= $infor['signature_info']['modify_quota']-$infor['signature_info']['modify_used_count']?>次。
										<?php endif; ?>
										</small>									
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">服务器域名 </label>
                                <div class="am-u-sm-9">
									<input type="text" class="tpl-form-input" name="wxapp[api_domain]" value="<?= $wxapp['api_domain'] ?>" required>
									<small>
										请确保上面域名列表至少有一个在下方域名列表中，多个以“;”隔开，否则无法正常使用小程序。<br>
										最新域名列表：<?= $wxapp['serve_domain'] ?><br>
									</small>
                                </div>
                            </div>
							<div class="widget-head am-cf">
                                <div class="widget-title am-fl">其它设置</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">商家客服电话 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="wxapp[phone]"
                                           value="<?= $wxapp['phone'] ?>">
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">收藏提醒 </label>
                                <div class="am-u-sm-9">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="wxapp[is_collection]" value="1" data-am-ucheck
                                            <?= $wxapp['is_collection'] ? 'checked' : '' ?>> 开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="wxapp[is_collection]" value="0" data-am-ucheck
                                            <?= $wxapp['is_collection'] ? '' : 'checked' ?>> 关闭
                                    </label>
                                    <div class="help-block">
                                        <small>收藏提醒显示位置：小程序首页面顶端位置。</small>
                                    </div>
                                </div>
                            </div>
							<?php if($wxapp['is_copyright']['value']==1):?>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">小程序版权 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" name="wxapp[copyright]"
                                           value="<?= $wxapp['copyright'] ?>">
									<small>该参数在小程序页面低端显示</small>
                                </div>
                            </div>
							<?php endif;?>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
						<?php endif; ?>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 图片文件列表模板 -->
{{include file="layouts/_template/tpl_file_item" /}}

<!-- 文件库弹窗 -->
{{include file="layouts/_template/file_library" /}}

<script>
    $(function () {
		
		// 选择图片
        $('.upload-file').selectImages({
            name: 'wxapp[share_image]'
        });
		
		// 同步设置
        var url = "<?= url('wxapp/update') ?>";
        $('.item-del').del('app_id', url,'确定要同步吗？');

        $('#my-form').formPost();

    });
</script>
