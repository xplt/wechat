<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">小程序类目列表</div>
                </div>
                <div class="widget-body am-fr">
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-6">
                        <div class="am-form-group">
                            <div class="am-btn-toolbar">
                                <div class="am-btn-group am-btn-group-xs">
                                    <a class="am-btn am-btn-default am-btn-success am-radius"
                                       href="<?= url('wxapp.category/add') ?>">
                                        <span class="am-icon-plus"></span> 申请
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped tpl-table-black ">
                            <thead>
                            <tr>
								<th>编号ID</th>
                                <th>一级类目ID</th>
                                <th>一级类目名称</th>
                                <th>二级类目ID</th>
                                <th>二级类目名称</th>
								<th>审核状态</th>
								<th>创建时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
									<td class="am-text-middle"><?= $item['category_serve_id'] ?></td>
                                    <td class="am-text-middle"><?= $item['first'] ?></td>
                                    <td class="am-text-middle"><?= $item['first_name'] ?></td>
                                    <td class="am-text-middle"><?= $item['second'] ?></td>
                                    <td class="am-text-middle"><?= $item['second_name'] ?></td>
									<td class="am-text-middle"><?= $item['audit_status']['text'] ?></td>
									<td class="am-text-middle"><?= $item['create_time'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="javascript:;" class="item-del tpl-table-black-operation-del"
                                               data-ids="<?= $item['category_serve_id']?>" >
                                                <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="8" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        // 删除元素
        var url = "<?= url('wxapp.category/delete') ?>";
        $('.item-del').del('category_serve_id', url);

    });
</script>

