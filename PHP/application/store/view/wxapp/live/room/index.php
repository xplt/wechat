<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">小程序直播间列表</div>
                </div>
                <div class="widget-body am-fr">
					<div class="tips am-margin-bottom am-u-sm-12">
                        <div class="pre">
                            <p class="am-padding-bottom-xs"> 小程序直播使用说明：</p>
							<p>1. 微信开放接口暂不支持与微信端的同步修改和删除。</p>
                            <p>2. 非本平台注册的小程序，登录 <a href="https://mp.weixin.qq.com/" target="_blank">小程序公众平台</a>，进行修改配置。</p>
                        </div>
                    </div>
					<div class="page_toolbar am-margin-bottom am-cf">
						<div class="am-u-sm-12">
							<div class="am-form-group">
								<div class="am-btn-toolbar">
									<div class="am-btn-group am-btn-group-xs">
										<a class="j-refresh am-btn am-btn-default am-btn-primary am-radius"
											   href="javascript:void(0);">
											<span class="am-icon-refresh"></span> 同步直播间
										</a>
										<a class="am-btn am-btn-default am-btn-success am-radius"
										   href="<?= url('wxapp.live.room/add') ?>">
											<span class="am-icon-plus"></span> 新增
										</a>
									</div>
								</div>
							</div>
						</div>
                    </div>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>房间编号</th>
                                <th>房间名称</th>
                                <th>主播昵称</th>
								<th>开播时间</th>
                                <th>直播状态</th>
								<th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['roomid'] ?></td>
                                    <td class="am-text-middle">
                                        <p class="item-title"><?= $item['name'] ?></p>
                                    </td>
                                    <td class="am-text-middle"><?= $item['anchor_name'] ?></td>
									<td class="am-text-middle"><?= $item['start_time']['text'] ?></td>
                                    <td class="am-text-middle"><?= $item['live_status']['text'] ?> </td>
									<td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
											<!--
                                            <a href="<?= url('wxapp.live.room/edit',
                                                ['live_room_id' => $item['live_room_id']]) ?>">
                                                <i class="am-icon-pencil"></i> 编辑
                                            </a>
											-->
                                            <a href="javascript:;" class="item-del tpl-table-black-operation-del"
                                               data-id="<?= $item['live_room_id'] ?>">
                                                <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="6" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        // 删除元素
        var url = "<?= url('wxapp.live.room/delete') ?>";
        $('.item-del').del('live_room_id', url);
		
		/**
         * 同步直播间
         */
        $('.j-refresh').on('click', function () {
            var url = "index.php?s=/store/wxapp.live.room/refresh";
            var load = layer.load();
            $.post(url, {}, function (result) {
                result.code === 1 ? $.show_success(result.msg, result.url)
                    : $.show_error(result.msg);
                layer.close(load);
            });
        });

    });
</script>

