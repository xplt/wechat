<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">修改直播间</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">直播类型 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="room[type]" value="0" data-am-ucheck 
											<?= $model['type']['value']==0?'checked':''?>>
                                        手机直播
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="room[type]" value="1" data-am-ucheck 
										<?= $model['type']['value']==1?'checked':''?>>
                                        推流设备直播
                                    </label>
                                </div>
                            </div>
							<div class="screen_type am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">画面尺寸 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="room[screen_type]" value="0" data-am-ucheck
                                               <?= $model['screen_type']['value']==0?'checked':''?>>
                                        竖屏
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="room[screen_type]" value="1" data-am-ucheck 
										<?= $model['screen_type']['value']==1?'checked':''?>>
                                        横屏
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">房间名称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="room[name]"
                                           value="<?= $model['name'] ?>" maxlength='17' required>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">开播时间 </label>
                                <div class="am-u-sm-9 am-u-end">
									<select name="room[time]" required
									data-am-selected="{searchBox: 1, maxHeight: 200, btnSize: 'sm',  placeholder:'请选择'}">
                                        <option value=""></option>
									  <option value="9">9:00</option>
									  <option value="10">10:00</option>
									  <option value="11">11:00</option>
									  <option value="12">12:00</option>
									  <option value="13">13:00</option>
									  <option value="14">14:00</option>
									  <option value="15">15:00</option>
									  <option value="16">16:00</option>
									  <option value="17">17:00</option>
									  <option value="18">18:00</option>
									  <option value="19">19:00</option>
									  <option value="20">20:00</option>
									  <option value="21">21:00</option>
									  <option value="22">22:00</option>
									</select>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">主播昵称 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="room[anchor_name]"
                                           value="<?= $model['anchor_name'] ?>" maxlength='15' required>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">主播微信号 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input" name="room[anchor_wechat]"
                                           value="<?= $model['anchor_wechat'] ?>" required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">分享卡片封面 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <button type="button"
                                                class="upload-file-share am-btn am-btn-secondary am-radius">
                                            <i class="am-icon-cloud-upload"></i> 选择图片
                                        </button>
                                        <div class="uploader-list am-cf">
                                        </div>
                                    </div>
                                    <div class="help-block am-margin-top-sm">
                                        <small>建议尺寸：800*640，图片大小不得超过1M（上传前自检），否则出错</small>
                                    </div>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">直播间背景墙 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <button type="button"
                                                class="upload-file-cover am-btn am-btn-secondary am-radius">
                                            <i class="am-icon-cloud-upload"></i> 选择图片
                                        </button>
                                        <div class="uploader-list am-cf">
                                        </div>
                                    </div>
                                    <div class="help-block am-margin-top-sm">
                                        <small>建议尺寸：1080*1920，图片大小不得超过2M（上传前自检），否则出错</small>
                                    </div>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">点赞 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="room[close_like]" value="0" data-am-ucheck
                                               <?= $model['close_like']['value']==0?'checked':''?> >
                                        开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="room[close_like]" value="1" data-am-ucheck 
										<?= $model['close_like']['value']==1?'checked':''?>>
                                        关闭
                                    </label>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">评论 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="room[close_comment]" value="0" data-am-ucheck
                                               <?= $model['close_comment']['value']==0?'checked':''?>>
                                        开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="room[close_comment]" value="1" data-am-ucheck 
										<?= $model['close_comment']['value']==1?'checked':''?>>
                                        关闭
                                    </label>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">商品货架 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="room[close_goods]" value="0" data-am-ucheck
                                               <?= $model['close_goods']['value']==0?'checked':''?>>
                                        开启
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="room[close_goods]" value="1" data-am-ucheck 
										<?= $model['close_goods']['value']==1?'checked':''?>>
                                        关闭
                                    </label>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">显示排序 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" class="tpl-form-input" name="room[sort]"
                                           value="<?= $model['sort'] ?>" required>
                                    <small>数字越小越靠前</small>
                                </div>
                            </div>
							<!-- 事件功能 -->
							<div class="am-modal am-modal-loading am-modal-no-btn" tabindex="-1" id="my-modal-loading"></div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- 图片文件列表模板 -->
{{include file="layouts/_template/tpl_file_item_filename" /}}

<!-- 文件库弹窗 -->
{{include file="layouts/_template/file_library" /}}

<script>
    $(function () {
		$('.screen_type').hide();
        // 选择图片
        $('.upload-file-share').selectImages({
            name: 'room[share_name]'
        });
		// 选择图片
        $('.upload-file-cover').selectImages({
            name: 'room[cover_name]'
        });
        // 切换
        $('input:radio[name="room[type]"]').change(function (e) {
            var $screen_type = $('.screen_type');
            if (e.currentTarget.value === '1') {
                $screen_type.show();
            } else {
                $screen_type.hide();
            }
        });

        $('#my-form').formPost();

    });
</script>
