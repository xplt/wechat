<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">出售中的商品 - <?= $shop_name?></div>
                </div>
                <div class="widget-body am-fr">
                    <!-- 工具栏 -->
                    <div class="page_toolbar am-margin-bottom am-cf">
                        <form class="toolbar-form" action="">
                            <input type="hidden" name="s" value="/store/goods/index">
                            <input type="hidden" name="shop_id" value="<?= $shop_id?>">
                            <div class="am-u-sm-12 am-u-md-3">
                                <div class="am-form-group">
                                    <div class="am-btn-group am-btn-group-xs">
                                        <a class="am-btn am-btn-default am-btn-success"
                                           href="<?= url('goods/add',['shop_id'=>$shop_id]) ?>">
                                            <span class="am-icon-plus"></span> 新增
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="am-u-sm-12 am-u-md-9">
                                <div class="am fr">
                                    <div class="am-form-group am-fl">
                                        <select name="category_id"
                                                data-am-selected="{searchBox: 1, btnSize: 'sm',  placeholder: '请选择商品分类', maxHeight: 400}">
                                            <option value=""></option>
                                            <option value="0">全部分类</option>
                                            <?php if (isset($category)): foreach ($category as $first): ?>
                                                <option value="<?= $first['category_id'] ?>"
                                                    <?= $category_id == $first['category_id'] ? 'selected' : '' ?>>
                                                    <?= $first['name'] ?></option>
                                                <?php if (isset($first['child'])): foreach ($first['child'] as $two): ?>
                                                    <option value="<?= $two['category_id'] ?>"
                                                        <?= $category_id == $two['category_id'] ? 'selected' : '' ?>>
                                                        　　<?= $two['name'] ?></option>
                                                <?php endforeach; endif; ?>
                                            <?php endforeach; endif; ?>
                                        </select>
                                    </div>
                                    <div class="am-form-group am-fl">
                                        <div class="am-input-group am-input-group-sm tpl-form-border-form">
                                            <input type="text" class="am-form-field" name="search"
                                                   placeholder="请输入商品名称"
                                                   value="">
                                            <div class="am-input-group-btn">
                                                <button class="am-btn am-btn-default am-icon-search"
                                                        type="submit"></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>商品ID</th>
                                <th>商品图片</th>
                                <th>商品名称</th>
                                <th>商品分类</th>
                                <th>实际销量</th>
								<th>包装费</th>
                                <th>商品库存</th>
								<th>商品规格</th>
                                <th>商品状态</th>
                                <th>显示排序</th>
                                <th>添加时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['goods_id'] ?></td>
                                    <td class="am-text-middle">
                                        <a href="<?= $item['image'][0]['file_path'] ?>"
                                           title="点击查看大图" target="_blank">
                                            <img src="<?= $item['image'][0]['file_path'] ?>"
                                                 width="50" height="50" alt="商品图片">
                                        </a>
                                    </td>
                                    <td class="am-text-middle">
                                        <p class="item-title"><?= $item['goods_name'] ?></p>
                                    </td>
                                    <td class="am-text-middle"><?= isset($item['category']['name'])?$item['category']['name']:'暂无' ?></td>
                                    <td class="am-text-middle"><?= $item['sales_actual'] ?></td>
                                    <td class="am-text-middle"><?= $item['pack_price'] ?></td>
                                    <td class="am-text-middle"><?= $item['stock'] ?></td>
                                    <td class="am-text-middle">
                                            <span class="<?= $item['spec_type'] == 10 ? 'x-color-green'
                                                : 'x-color-red' ?>">
                                            <?= $item['spec_type'] == 10 ? '单规格':'多规格' ?>
                                            </span>
                                    </td>
									<td class="am-text-middle">
										<span class="j-status am-badge x-cur-p  <?= $item['goods_status']['value'] == 10 ? ' am-badge-success'
											: ' am-badge-warning' ?>" 
											 data-ids="<?= $item['goods_id'] ?>"
											 data-status="<?= $item['goods_status']['value'] ?>">
										<?= $item['goods_status']['text'] ?>
										</span>
                                    </td>
									<td class="am-text-middle"><?= $item['goods_sort'] ?></td>
                                    <td class="am-text-middle"><?= $item['create_time'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="<?= url('goods/edit',
                                                ['goods_id' => $item['goods_id'],'shop_id'=>$shop_id]) ?>">
                                                <i class="am-icon-pencil"></i> 编辑
                                            </a>
                                            <a href="javascript:;" class="item-del tpl-table-black-operation-del"
                                               data-ids="<?= $item['goods_id'] ?>">
                                                <i class="am-icon-trash"></i> 删除
                                            </a>
											<?php if($store['is_admin'] AND $store['wxapp']['shop_mode']['value']==20):?>
											<a class="tpl-table-black-operation-green" href="<?= url('goods/copys',
                                                ['goods_id' => $item['goods_id']]) ?>">
                                                <i class="am-icon-clipboard"></i> 一键复制
                                            </a>
											<?php endif;?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="12" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
		
		// 切换上架状态
        $('.j-status').click(function () {
            var data = $(this).data();
			var msg = '确定要'+(parseInt(data.status) === 10 ? '下架' : '上架')+'该商品？';
            var url = "<?= url('goods/status') ?>";
			$('.j-status').del('goods_id', url,msg);
        });

        // 删除元素
        var url = "<?= url('goods/delete',['shop_id'=>$shop_id]) ?>";
        $('.item-del').del('goods_id', url);

    });
</script>

