<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title><?= $web['name']?></title>
	<meta name="author" content="<?= $web['name']?>">
	<meta name="keywords" content="<?= $web['keywords']?>">
	<meta name="description" content="<?= $web['description']?>">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="referrer" content="never">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" type="image/png" href="assets/icon.png"/>
    <link rel="stylesheet" href="assets/plugins/amazeui/amazeui.min.css"/>
    <link rel="stylesheet" href="assets/store/css/hema.app.css"/>
    <script src="assets/store/js/lang.js"></script>
    <script src="assets/plugins/jquery/jquery.min.js"></script>
	<link rel="stylesheet" href="assets/plugins/iconfont/iconfont.css">
    <script src="assets/plugins/iconfont/iconfont.js"></script>
    <script>
        BASE_URL = '<?= isset($base_url) ? $base_url : '' ?>';
        STORE_URL = '<?= isset($store_url) ? $store_url : '' ?>';
    </script>
</head>

<body data-type="">
<div class="am-g tpl-g">
    <!-- 头部 -->
    <header class="tpl-header">
        <!-- 右侧内容 -->
        <div class="tpl-header-fluid">
            <!-- 侧边切换 -->
            <div class="am-fl tpl-header-button switch-button">
                <i class="iconfont iconzhedie"></i>
            </div>
            <!-- 刷新页面 -->
            <div class="am-fl tpl-header-button refresh-button">
                <i class="iconfont iconshuaxin"></i>
            </div>
            <!-- 其它功能-->
            <div class="am-fr tpl-header-navbar">
                <ul>
                    <!-- 欢迎语 -->
                    <li class="am-text-sm">
                        <a href="<?= $store['is_admin'] ? '/user.php' : url('shop.clerk/renew') ?>">
							<img  src="<?= $store['user']['avatarUrl'] ?>" /> <?= $store['user']['user_name'] ?>
                        </a>
                    </li>
					<!-- 版本 -->
                    <li class="am-text-sm">
                        <a href="#">
                            <i class="iconfont iconmendian"></i> 
                            <?php 
                                if($store['is_admin']){
                                    echo $store['wxapp']['app_type']['text'] . ' - ' . $store['wxapp']['shop_mode']['text'];
                                }else{
                                    echo $store['shop']['shop_name'];
                                }
                            ?>
                        </a>
                    </li>
					<!-- 到期 -->
                    <li class="am-text-sm">
                        <a href="#">
                            <i class="iconfont icondengpao"></i> 
							<?= $store['wxapp']['expire_time']['day'] ?>天到期
                        </a>
                    </li>
					<!-- 消息 -->
                    <li class="am-text-sm">
                        <a href="#">
                            <i class="iconfont iconlingdang"></i> 消息
                        </a>
                    </li>
                    <!-- 返回首页 -->
                    <li class="am-text-sm">
                        <a href="/index.php">
                            <i class="iconfont iconshouye1"></i> 首页
                        </a>
                    </li>
                    <!-- 退出 -->
                    <li class="am-text-sm">
                        <a href="index.php?s=/index/login/logout">
                            <i class="iconfont icontuichu"></i> 退出
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <!-- 侧边导航栏 -->
    <div class="left-sidebar dis-flex">
        <?php $menus = $menus ?: []; ?>
        <?php $group = $group ?: 0; ?>
        <!-- 一级菜单 -->
        <ul class="sidebar-nav">
            <li class="sidebar-nav-heading"><?= $store['wxapp']['app_name']?$store['wxapp']['app_name']:$web['name'] ?></li>
            <?php foreach ($menus as $key => $item): ?>
                <li class="sidebar-nav-link">
                    <a href="<?= isset($item['index']) ? url($item['index']) : 'javascript:void(0);' ?>"
                       class="<?= $item['active'] ? 'active' : '' ?>">
                        <?php if (isset($item['is_svg']) && $item['is_svg'] === true): ?>
                            <svg class="icon sidebar-nav-link-logo" aria-hidden="true">
                                <use xlink:href="#<?= $item['icon'] ?>"></use>
                            </svg>
                        <?php else: ?>
                            <i class="iconfont sidebar-nav-link-logo <?= $item['icon'] ?>"
                               style="<?= isset($item['color']) ? "color:{$item['color']};" : '' ?>"></i>
                        <?php endif; ?>
                        <?= $item['name'] ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <!-- 子级菜单-->
        <?php $second = isset($menus[$group]['submenu']) ? $menus[$group]['submenu'] : []; ?>
        <?php if (!empty($second)) : ?>
            <ul class="left-sidebar-second">
                <li class="sidebar-second-title"><?= $menus[$group]['name'] ?></li>
                <li class="sidebar-second-item">
                    <?php foreach ($second as $item) : ?>
                        <?php if (!isset($item['submenu'])): ?>
                            <!-- 二级菜单-->
                            <a href="<?= url($item['index']) ?>" class="<?= $item['active'] ? 'active' : '' ?>">
                                <?= $item['name']; ?>
                            </a>
                        <?php else: ?>
                            <!-- 三级菜单-->
                            <div class="sidebar-third-item">
                                <a href="javascript:void(0);"
                                   class="sidebar-nav-sub-title <?= $item['active'] ? 'active' : '' ?>">
                                    <i class="iconfont icongengduo"></i>
                                    <?= $item['name']; ?>
                                </a>
                                <ul class="sidebar-third-nav-sub">
                                    <?php foreach ($item['submenu'] as $third) : ?>
                                        <li>
                                            <a class="<?= $third['active'] ? 'active' : '' ?>"
                                               href="<?= url($third['index']) ?>">
                                                <?= $third['name']; ?></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </li>
            </ul>
        <?php endif; ?>
    </div>

    <!-- 内容区域 start -->
    <div class="tpl-content-wrapper <?= empty($second) ? 'no-sidebar-second' : '' ?>">
        {__CONTENT__}
    </div>
    <!-- 内容区域 end -->

</div>
<script src="assets/plugins/layer/layer.js"></script>
<script src="assets/plugins/jquery/jquery.form.min.js"></script>
<script src="assets/plugins/amazeui/amazeui.min.js"></script>
<script src="assets/store/js/webuploader.html5only.js"></script>
<script src="assets/store/js/art-template.js"></script>
<script src="assets/store/js/hema.app.js"></script>
<script src="assets/store/js/file.library.js"></script>
</body>

</html>
