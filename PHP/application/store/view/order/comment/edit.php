<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" method="post">
                    <div class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">编辑评价数据</div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">服务评分 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" min="1" max="5" class="tpl-form-input" name="cmt[serve]"
                                           value="<?= $model['serve'] ?>" required>
									<small>最小值1，最大5</small>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">速度评分 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" min="1" max="5" class="tpl-form-input" name="cmt[speed]"
                                           value="<?= $model['speed'] ?>" required>
									<small>最小值1，最大5</small>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">口味评分 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" min="1" max="5" class="tpl-form-input" name="cmt[flavor]"
                                           value="<?= $model['flavor'] ?>" required>
									<small>最小值1，最大5</small>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">环境评分 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="number" min="1" max="5" class="tpl-form-input" name="cmt[ambient]"
                                           value="<?= $model['ambient'] ?>" required>
									<small>最小值1，最大5</small>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">评论内容 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <textarea class="" rows="3" placeholder="请输入评论内容" name="cmt[content]"><?= $model['content'] ?></textarea>
                                </div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">回复内容 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <textarea class="" rows="3" placeholder="请输入回复内容" name="cmt[reply]"><?= $model['reply'] ?></textarea>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">是否显示 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <label class="am-radio-inline">
                                        <input type="radio" name="cmt[is_show]" value="1" data-am-ucheck
                                            <?= $model['is_show']['value'] == 1 ? 'checked' : '' ?> >
                                        是
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" name="cmt[is_show]" value="0" data-am-ucheck
                                            <?= $model['is_show']['value'] == 0 ? 'checked' : '' ?> >
                                        否
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        $('#my-form').formPost();

    });
</script>
