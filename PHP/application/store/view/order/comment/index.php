<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">订单评价列表</div>
                </div>
                <div class="widget-body am-fr">
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>编号</th>
								<th>用户头像</th>
								<th>昵称</th>
                                <th>服务</th>
                                <th>速度</th>
                                <th>口味</th>
                                <th>环境</th>
                                <th>评论</th>
								<th>门店</th>
                                <th>状态</th>
                                <th>评价时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle">
                                        <p class="item-title"><?= $item['comment_id'] ?></p>
                                    </td>
									 <td class="am-text-middle">
                                        <a href="<?= $item['user']['avatarUrl'] ? $item['user']['avatarUrl'] : 'assets/store/img/head-no.png' ?>" title="点击查看大图" target="_blank">
                                            <img src="<?= $item['user']['avatarUrl'] ? $item['user']['avatarUrl'] : 'assets/store/img/head-no.png' ?>" width="72" height="72" alt="">
                                        </a>
                                    </td>
                                    <td class="am-text-middle"><?= $item['user']['nickName'] ?: '--' ?></td>
                                    <td class="am-text-middle"><?= $item['serve']?></td>
                                    <td class="am-text-middle"><?= $item['speed'] ?></td>
                                    <td class="am-text-middle"><?= $item['flavor'] ?></td>
									<td class="am-text-middle"><?= $item['ambient'] ?></td>
									<td class="am-text-middle"><?= $item['content'] ?: '--' ?></td>
									<td class="am-text-middle"><?= $item['shop']['shop_name'] ?></td>
                                    <td class="am-text-middle">
										<span class="j-show am-badge x-cur-p  <?= $item['is_show']['value'] ? ' am-badge-success'
											: ' am-badge-warning' ?>" 
											 data-ids="<?= $item['comment_id'] ?>"
											 data-status="<?= $item['is_show']['value'] ?>">
										<?= $item['is_show']['text'] ?>
										</span>
                                    </td>
                                    <td class="am-text-middle"><?= $item['create_time'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="<?= url('order.comment/edit',
                                                ['comment_id' => $item['comment_id']]) ?>">
                                                <i class="am-icon-pencil"></i> 编辑
                                            </a>
											<a class="tpl-table-black-operation-green" href="<?= url('order/detail', ['order_id' => $item['order_id']]) ?>">
                                                <i class="am-icon-file-text"></i> 订单
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="12" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
		
		// 切换显示状态
        $('.j-show').click(function () {
            var data = $(this).data();
			var msg = '确定要'+(parseInt(data.status) === 1 ? '隐藏' : '显示')+'该评论？';
            var url = "<?= url('order.comment/show') ?>";
			$('.j-show').del('comment_id', url,msg);
        });
    });
</script>

