<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">预约订桌订单列表</div>
                </div>
                <div class="widget-body am-fr">
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>编号ID</th>
								<th>门店</th>
                                <th>联系人</th>
                                <th>电话</th>
                                <th>人数</th>
                                <th>餐桌/包间</th>
								<th>约定时间</th>
                                <th>状态</th>
                                <th>创建时间</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['pact_id'] ?></td>
                                    <td class="am-text-middle"><?= $item['shop']['shop_name'] ?></td>
                                    <td class="am-text-middle">
                                        <p class="item-title"><?= $item['linkman'] ?></p>
                                    </td>
                                    <td class="am-text-middle"><?= $item['phone'] ?></td>
                                    <td class="am-text-middle"><?= $item['people'] ?></td>
                                    <td class="am-text-middle"><?= $item['table']['table_name'] ?></td>
									<td class="am-text-middle"><?= date('Y-m-d 00:00:00',$item['pact_time']) ?></td>
									<td class="am-text-middle">
                                            <span class="j-state am-badge x-cur-p  <?= $item['status']['value'] == 10 ? ' am-badge-success'
                                                : ' am-badge-warning' ?>" 
                                                 data-id="<?= $item['pact_id'] ?>"
                                                 data-status="<?= $item['status']['value'] ?>">
                                            <?= $item['status']['text'] ?>
                                            </span>
                                    </td>
                                    <td class="am-text-middle"><?= $item['create_time'] ?></td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
		
		// 状态
        $('.j-state').click(function () {
            // 验证权限
            if (!"1") {
                return false;
            }
            var data = $(this).data();
            layer.confirm('确定要' + (parseInt(data.status) === 10 ? '终止' : '恢复') + '该预约吗？'
                , {title: '友情提示'}
                , function (index) {
                    $.post("index.php?s=/store/order.pact/status"
                        , {pact_id: data.id}
                        , function (result) {
                            result.code === 1 ? $.show_success(result.msg, result.url)
                                : $.show_error(result.msg);
                        });
                    layer.close(index);
                });

        });
    });
</script>

