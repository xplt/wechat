<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-body  am-margin-bottom-lg">
                    <div class="widget__order-detail am-u-sm-12">
                        <?php
                        // 计算当前步骤位置
                        $progress = 2;
                        $detail['pay_status']['value'] > 10 && $progress += 1;
                        $detail['delivery_status']['value'] == 30 && $progress += 1;
                        $detail['receipt_status']['value'] == 20 && $progress += 1;
                        $detail['order_status']['value'] == 30 && $progress += 1;
                        ?>
                        <ul class="order-detail-progress progress-<?= $progress ?>">
                            <li>
                                <span>下单时间</span>
                                <div class="tip"><?= $detail['create_time'] ?></div>
                            </li>
                            <li>
                                <span>付款时间</span>
                                <?php if ($detail['pay_status']['value'] > 10): ?>
                                    <div class="tip">
                                        <?= date('Y-m-d H:i:s', $detail['pay_time']) ?>
                                    </div>
                                <?php endif; ?>
                            </li>
                            <li>
                                <span>配送时间</span>
                                <?php if ($detail['delivery_status']['value'] == 30): ?>
                                    <div class="tip">
                                        <?= date('Y-m-d H:i:s', $detail['delivery_time']) ?>
                                    </div>
                                <?php endif; ?>
                            </li>
                            <li>
                                <span>收货时间</span>
                                <?php if ($detail['receipt_status']['value'] == 20): ?>
                                    <div class="tip">
                                        <?= date('Y-m-d H:i:s', $detail['receipt_time']) ?>
                                    </div>
                                <?php endif; ?>
                            </li>
                            <li>
                                <span><?= $detail['order_status']['value'] == 40 ? '退款时间' : '完成时间'?></span>
                                <?php if ($detail['order_status']['value'] == 30): ?>
                                    <div class="tip">
                                        <?= date('Y-m-d H:i:s', $detail['receipt_time']) ?>
                                    </div>
                                <?php elseif($detail['refund_status']['value'] == 20): ?>
									<div class="tip">
                                        <?= date('Y-m-d H:i:s', $detail['refund_time']) ?>
                                    </div>
								<?php endif; ?>
                            </li>
                        </ul>
                    </div>

                    <div class="widget-head am-cf">
                        <div class="widget-title am-fl">基本信息</div>
                    </div>
                    <div class="am-scrollable-horizontal">
                        <table class="regional-table am-table am-table-bordered am-table-centered
                            am-text-nowrap am-margin-bottom-xs">
                            <tbody>
                            <tr>
                                <th>订单号</th>
                                <th>付款信息</th>
                                <th>买家信息</th>
                                <th>买家留言</th>
                                <th>口味要求</th>
                                <th>交易状态</th>
                            </tr>
                            <tr>
                                <td><?= $detail['order_no'] ?></td>
                                <td>
                                    <p>￥<?= $detail['pay_price'] ?></p>
                                    <p class="am-link-muted">(优惠额：-￥<?= $detail['activity_price'] ?>)</p>
                                    <?php if($detail['pack_price']>0):?>
                                        <p class="am-link-muted">(包装费：+￥<?= $detail['pack_price'] ?>)</p>
                                    <?php endif;?>
                                    <?php if($detail['express_price']>0):?>
                                        <p class="am-link-muted">(配送费：+￥<?= $detail['express_price'] ?>)</p>
                                    <?php endif;?>
                                    <?php if($detail['ware_price']>0):?>
                                        <p class="am-link-muted">(餐具费：+￥<?= $detail['ware_price'] ?>)</p>
                                    <?php endif;?>
                                    <?php if($detail['order_status']['value']==40):?>
                                        <p>退款：-￥<?= $detail['refund_price'] ?></p>
                                    <?php endif;?>
                                </td>
                                <td>
                                    <p><?= $detail['user']['nickName'] ?></p>
                                    <p class="am-link-muted">(用户id：<?= $detail['source']['value']==10?$detail['user']['user_id']:$detail['user']['store_user_id']?>)</p>
                                </td>
                                <td>
                                    <p><?= $detail['message']?:'--' ?></p> 
                                </td>
                                <td>
                                    <p><?= $detail['flavor']?:'--' ?></p> 
                                </td>
                                <td>
                                     <p>付款状态：
                                        <span class="am-badge
                                    <?= $detail['pay_status']['value'] == 20 ? 'am-badge-success' : '' ?>
                                    <?= $detail['pay_status']['value'] == 30 ? 'am-badge-danger' : '' ?>">
                                            <?= $detail['pay_status']['text'] ?></span>
                                    </p>
                                    <p>接单状态：
                                        <span class="am-badge
                                    <?= $detail['shop_status']['value'] > 10 ? 'am-badge-success' : '' ?>">
                                            <?= $detail['shop_status']['text'] ?></span>
                                    </p>
                                    <p>配送状态：
                                        <span class="am-badge
                                    <?= $detail['delivery_status']['value'] == 20 ? 'am-badge-danger' : '' ?>
                                    <?= $detail['delivery_status']['value'] == 30 ? 'am-badge-success' : '' ?>">
                                            <?= $detail['delivery_status']['text'] ?></span>
                                    </p>
                                    <p>收货状态：
                                        <span class="am-badge
                                    <?= $detail['receipt_status']['value'] == 20 ? 'am-badge-success' : '' ?>">
                                            <?= $detail['receipt_status']['text'] ?></span>
                                    </p>
                                    <p>订单状态：
                                        <span class="am-badge
                                    <?= $detail['order_status']['value'] == 10 ? 'am-badge-warning' : '' ?>
                                    <?= $detail['order_status']['value'] == 20 ? 'am-badge-danger' : '' ?>
                                    <?= $detail['order_status']['value'] == 30 ? 'am-badge-success' : '' ?>
                                    <?= $detail['order_status']['value'] == 40 ? 'am-badge-danger' : '' ?>">
                                    <?= $detail['order_status']['value'] == 40 ? $detail['refund_status']['text'] : $detail['order_status']['text'] ?>
                                        </span>
                                    </p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="widget-head am-cf">
                        <div class="widget-title am-fl">商品信息</div>
                    </div>
                    <div class="am-scrollable-horizontal">
                        <table class="regional-table am-table am-table-bordered am-table-centered
                            am-text-nowrap am-margin-bottom-xs">
                            <tbody>
                            <tr>
                                <th>商品名称</th>
                                <th>商品编码</th>
                                <th>单价</th>
                                <th>购买数量</th>
								<th>售后数量</th>
                                <th>商品总价</th>
                            </tr>
                            <?php foreach ($detail['goods'] as $goods): ?>
                                <tr>
                                    <td class="goods-detail am-text-middle">
                                        <div class="goods-image">
                                            <img src="<?= $goods['image'] == null ? $goods['thumbnail'] : $goods['image']['file_path'] ?>" alt="">
                                        </div>
                                        <div class="goods-info">
                                            <p class="goods-title"><?= $goods['goods_name'] ?></p>
                                            <p class="goods-spec am-link-muted">
                                                <?= $goods['goods_attr'] ?>
                                            </p>
                                        </div>
                                    </td>
                                    <td><?= $goods['goods_no'] ?: '--' ?></td>
                                    <td>￥<?= $goods['goods_price'] ?></td>
                                    <td>×<?= $goods['total_num'] ?></td>
									<td><?= $goods['refund_num']?' - ' . $goods['refund_num'] : '--' ?></td>
                                    <td>￥<?= $goods['total_price'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <td colspan="6" class="am-text-right">总计金额：￥<?= $detail['total_price'] ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="widget-head am-cf">
                        <div class="widget-title am-fl">付款信息</div>
                    </div>
                    <div class="am-scrollable-horizontal">
                        <table class="regional-table am-table am-table-bordered am-table-centered
                            am-text-nowrap am-margin-bottom-xs">
                            <tbody>
                            <tr>
                                <th>应付款金额</th>
                                <th>支付方式</th>
                                <th>支付流水号</th>
                                <th>付款状态</th>
                                <th>付款时间</th>
                            </tr>
                            <tr>
                                <td>￥<?= $detail['pay_price'] ?></td>
                                <td>
                                <?php 
                                    if($detail['pay_status']['value'] == 20){
                                        if(empty($detail['transaction_id'])){
                                            echo '余额支付';
                                        }else{
                                            echo '微信支付';
                                        }
                                    }else{
                                        echo '--';
                                    }
                                ?>
                                </td>
                                <td><?= $detail['transaction_id']?:'--'?></td>
                                <td>
                                    <span class="am-badge
                                    <?= $detail['pay_status']['value'] == 20 ? 'am-badge-success' : '' ?>
                                    <?= $detail['pay_status']['value'] == 30 ? 'am-badge-danger' : '' ?>">
                                    <?php 
                                        if($detail['pay_status']['value'] < 30){
                                            echo $detail['pay_status']['text'];
                                        }else{
                                            if($detail['order_mode']['value'] == 10){
                                                echo '餐后支付';
                                            }
                                            if($detail['order_mode']['value'] == 20){
                                                echo '骑手代收';
                                            }
                                            if($detail['order_mode']['value'] == 30){
                                                echo '到店支付';
                                            }
                                        }
                                    ?>
                                    </span>
                                </td>
                                <td>
                                    <?= $detail['pay_time'] ? date('Y-m-d H:i:s', $detail['pay_time']) : '--' ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--收货信息-->
					<?php if($detail['order_mode']['value']==20):?>
                    <div class="widget-head am-cf">
                        <div class="widget-title am-fl">收货信息</div>
                    </div>
                    <div class="am-scrollable-horizontal">
                        <table class="regional-table am-table am-table-bordered am-table-centered
                            am-text-nowrap am-margin-bottom-xs">
                            <tbody>
                            <tr>
                                <th>收货人</th>
                                <th>收货电话</th>
                                <th>收货地址</th>
                            </tr>
                            <tr>
                                <td><?= $detail['address']['name'] ?></td>
                                <td><?= $detail['address']['phone'] ?></td>
                                <td>
                                    <?= $detail['address']['province'] ?>
                                    <?= $detail['address']['city'] ?>
                                    <?= $detail['address']['district'] ?>
                                    <?= $detail['address']['detail'] ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
					<?php endif;?>
                    
                    <!-- 外卖已配送 -->
                    <?php if($detail['order_mode']['value'] == 20 AND $detail['shop_status']['value']==30):?>
                    <div class="widget-head am-cf">
                        <div class="widget-title am-fl">骑手信息</div>
                    </div>
                    <div class="am-scrollable-horizontal">
                        <table class="regional-table am-table am-table-bordered am-table-centered
                        am-text-nowrap am-margin-bottom-xs">
                            <tbody>
                            <tr>
                                <th>骑手姓名</th>
                                <th>联系电话</th>
                                <th>骑手状态</th>
                                <th>配送状态</th>
                                <th>状态时间</th>
                            </tr>
                            <tr>
                                <td><?= $detail['delivery']['linkman'] ?></td>
                                <td><?= $detail['delivery']['phone'] ?></td>
                                <td>
                                     <span class="am-badge
                                    <?= $detail['delivery']['delivery_status']['value'] == 20 ? 'am-badge-warning' : '' ?>
                                    <?= $detail['delivery']['delivery_status']['value'] == 30 ? 'am-badge-warning' : '' ?>
                                    <?= $detail['delivery']['delivery_status']['value'] == 40 ? 'am-badge-warning' : '' ?>
                                    <?= $detail['delivery']['delivery_status']['value'] == 50 ? 'am-badge-success' : '' ?>">
                                            <?= $detail['delivery']['delivery_status']['text'] ?></span>
                                </td>
                                <td>
                                    <span class="am-badge
                                    <?= $detail['delivery']['status']['value'] == 10 ? 'am-badge-warning' : '' ?>
                                    <?= $detail['delivery']['status']['value'] == 20 ? 'am-badge-danger' : '' ?>
                                    <?= $detail['delivery']['status']['value'] == 30 ? 'am-badge-success' : '' ?>">
                                            <?= $detail['delivery']['status']['text'] ?></span>
                                </td>
                                <td>
                                    <?= date('Y-m-d H:i:s', $detail['delivery']['delivery_time']) ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php endif;?>
                    <!-- 退款信息 -->
					<?php if($detail['order_status']['value'] == 40):?>
					<form id="refund" class="my-form am-form tpl-form-line-form" method="post" action="<?= url('order/refund', ['order_id' => $detail['order_id']]) ?>">
						<div class="widget-head am-cf">
                            <div class="widget-title am-fl">退款信息</div>
                        </div>
                        <div class="am-scrollable-horizontal">
                            <table class="regional-table am-table am-table-bordered am-table-centered
                                am-text-nowrap am-margin-bottom-xs">
                                <tbody>
                                <tr>
                                    <th>退款金额</th>
                                    <th>返回账户</th>
                                    <th>退款理由</th>
                                    <th>退款状态</th>
                                    <th>退款时间</th>
                                </tr>
                                <tr>
                                    <td>￥<?= $detail['refund_price'] ?></td>
                                    <td><?= $detail['transaction_id'] ?'微信':'余额' ?></td>
                                    <td><?= $detail['refund_desc'] ?></td>
                                    <td>
                                        <span class="am-badge
                                        <?= $detail['refund_status']['value'] == 20 ? 'am-badge-success' : 'am-badge-warning' ?>">
                                                <?= $detail['refund_status']['text'] ?></span>
                                    </td>
                                    <td>
                                        <?= $detail['refund_time'] ? date('Y-m-d H:i:s', $detail['refund_time']) : '--' ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
						<?php if($detail['refund_status']['value'] ==10): ?>
						<p>
							<div class="am-form-group">
								<label class="am-u-sm-9 am-u-lg-3 am-form-label form-require">等待商家退款操作 </label>
								<div class="am-u-sm-3 am-u-end">
									<label class="am-radio-inline">
										<input type="radio" name="refund[is_refund]" value="1" data-am-ucheck checked >
										同意
									</label>
									<label class="am-radio-inline">
										<input type="radio" name="refund[is_refund]" value="0" data-am-ucheck>
										拒绝
									</label>
								</div>
							</div>
							<div class="am-form-group">
								<div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
									<button type="submit" class="j-submit am-btn am-btn-sm am-btn-secondary">
										确认操作
									</button>
								</div>
							</div>
						</p>
						<?php endif;?>
						</form>
					<?php endif;?>

                    <!-- 确认收款 -->
                    <?php if ($detail['pay_status']['value'] == 30 AND $detail['delivery_status']['value'] == 30 AND $detail['order_status']['value'] == 10): ?>
                    <form id="collection" class="my-form am-form tpl-form-line-form" method="post" action="<?= url('order/collection', ['order_id' => $detail['order_id']]) ?>">
                        <div class="widget-head am-cf">
                            <div class="widget-title am-fl">上餐完毕 - 等待商家收款</div>
                        </div>
                        <div class="am-form-group">
                            <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                <button type="submit" class="j-submit am-btn am-btn-sm am-btn-secondary">确认收款</button>
                            </div>
                        </div>
                    </form>
                    <?php endif; ?>

                    <!-- 商家接单 -->
                    <?php if ($detail['pay_status']['value'] > 10 AND $detail['shop_status']['value'] == 10 AND $detail['order_status']['value'] == 10): ?>
                    <form id="shop" class="my-form am-form tpl-form-line-form" method="post" action="<?= url('order/shop', ['order_id' => $detail['order_id']]) ?>">
                        <div class="widget-head am-cf">
                            <div class="widget-title am-fl"><?= $detail['pay_status']['value']==20 ?'已支付':'餐后支付'?> - 待商家确认接单</div>
                        </div>
                        <div class="am-form-group">
                            <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                <button type="submit" class="j-submit am-btn am-btn-sm am-btn-secondary">确认接单</button>
                            </div>
                        </div>
                    </form>
                    <?php endif; ?>

                    <!-- 订单配送 -->
                    <?php if ($detail['shop_status']['value'] == 20 AND $detail['delivery_status']['value'] == 10 AND $detail['order_status']['value'] == 10): ?>
                    <form id="delivery" class="my-form am-form tpl-form-line-form" method="post" action="<?= url('order/delivery', ['order_id' => $detail['order_id']]) ?>">
                        <div class="widget-head am-cf">
                            <div class="widget-title am-fl"><?= $detail['order_mode']['value'] == 20 ? '等待外卖订单推送给骑手':'待商家发货' ?></div>
                        </div>
                        <?php if($detail['order_mode']['value']==20):?>
                        <div class="am-form-group">
                            <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">选择配送方式 </label>
                            <div class="am-u-sm-9 am-u-end">
                                <label class="am-radio-inline">
                                    <input v-model="company" type="radio" name="delivery[company]" value="10" data-am-ucheck checked>
                                    商家自配
                                </label>
                                <label class="am-radio-inline">
                                    <input v-model="company" type="radio" name="delivery[company]" value="20" data-am-ucheck>
                                     顺丰同城
                                </label>
                                <label class="am-radio-inline">
                                    <input v-model="company" type="radio" name="delivery[company]" value="30" data-am-ucheck>
                                    达达快送
                                </label>
                                <label class="am-radio-inline">
                                    <input v-model="company" type="radio" name="delivery[company]" value="40" data-am-ucheck>
                                    UU跑腿
                                </label>
                            </div>
                        </div>
                        <?php if($detail['shop']['is_grab']['value']==0):?>
                        <div class="am-form-group" v-if="company == 10">
                            <label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">指派骑手 </label>
                            <div class="am-u-sm-9 am-u-end">
                                <select name="delivery[shop_clerk_id]" required  style="width:200px;">
                                    <option value="">请选择配送员</option>
                                    <?php if (isset($clerk)): foreach ($clerk as $item): ?>
                                        <option value="<?= $item['shop_clerk_id'] ?>"><?= $item['real_name'] ?></option>
                                    <?php endforeach; endif; ?>
                                </select>
                            </div>
                        </div>
                        <?php endif; endif; ?>
                        <div class="am-form-group">
                            <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                <button type="submit" class="j-submit am-btn am-btn-sm am-btn-secondary"><?= $detail['order_mode']['value'] == 20 ? '确认推送':'确认发货' ?></button>
                            </div>
                        </div>
                    </form>
                    <?php endif; ?>

                    <!-- 骑手已到店 -->
                    <?php if ($detail['order_mode']['value'] == 20 AND $detail['delivery_status']['value'] == 20 AND $detail['delivery']['delivery_status']['value'] == 20 AND $detail['order_status']['value'] == 10): ?>
                    <form id="dev30" class="my-form am-form tpl-form-line-form" method="post" action="<?= url('order/deliveryStatus', ['order_id' => $detail['order_id']]) ?>">
                        <div class="widget-head am-cf">
                            <div class="widget-title am-fl">待骑手到店取货</div>
                            <input type="hidden" name="delivery[delivery_status]" value="30">
                        </div>
                        <div class="am-form-group">
                            <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                <button type="submit" class="j-submit am-btn am-btn-sm am-btn-secondary">骑手取货</button>
                            </div>
                        </div>
                    </form>
                    <?php endif; ?>

                    <!-- 外卖开始配送 -->
                    <?php if ($detail['order_mode']['value'] == 20 AND $detail['delivery_status']['value'] == 20 AND $detail['delivery']['delivery_status']['value'] == 30 AND $detail['order_status']['value'] == 10): ?>
                    <form id="dev40" class="my-form am-form tpl-form-line-form" method="post" action="<?= url('order/deliveryStatus', ['order_id' => $detail['order_id']]) ?>">
                        <div class="widget-head am-cf">
                            <div class="widget-title am-fl">骑手已取货，等待配送</div>
                            <input type="hidden" name="delivery[delivery_status]" value="40">
                        </div>
                        <div class="am-form-group">
                            <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                <button type="submit" class="j-submit am-btn am-btn-sm am-btn-secondary">开始配送</button>
                            </div>
                        </div>
                    </form>
                    <?php endif; ?>

                    <!-- 外卖送达 -->
                    <?php if ($detail['order_mode']['value'] == 20 AND $detail['delivery_status']['value'] == 20 AND $detail['delivery']['delivery_status']['value'] == 40 AND $detail['order_status']['value'] == 10): ?>
                    <form id="dev50" class="my-form am-form tpl-form-line-form" method="post" action="<?= url('order/delivery', ['order_id' => $detail['order_id']]) ?>">
                        <div class="widget-head am-cf">
                            <div class="widget-title am-fl">外卖已送达待确认</div>
                            <input type="hidden" name="delivery[company]">
                        </div>
                        <div class="am-form-group">
                            <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                <button type="submit" class="j-submit am-btn am-btn-sm am-btn-secondary">确认送达</button>
                            </div>
                        </div>
                    </form>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </div>
</div>
<script src="assets/plugins/vue/vue.min.js"></script>
<script>
    $(function () {
        new Vue({
          el: '.my-form',
          data: {
            company:10
          }
        });
        $('.my-form').formPost();

    });
</script>
