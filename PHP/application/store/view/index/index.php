<div class="page-home row-content am-cf">
    <!-- 商城统计 -->
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12 am-margin-bottom">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title">门店统计</div>
                    <?php if($store['is_admin'] AND $store['wxapp']['shop_mode']['value']==20):?>
					<div class="widget-screen am-cf">
                        <!-- 日期选择器 -->
						<form id="my-form">
                        <div class="yxs-date-editor am-fl">
                            <select id="shop" name="shop_id">
								<option value="">全部门店统计</option>
								<?php if (isset($shop)): foreach ($shop as $item): ?>
									<option value="<?= $item['shop_id'] ?>" <?= $shop_id==$item['shop_id']?'selected':''?>><?= $item['shop_name'] ?>
									</option>
								<?php endforeach; endif; ?>
							</select>
                        </div>
						</form>
                    </div>
                    <?php endif;?>
                </div>
                <div class="widget-body am-cf">
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-3">
                        <div class="widget-card card__blue am-cf">
                            <div class="card-header">商品总量</div>
                            <div class="card-body">
                                <div class="card-value"><?=$count['goods']?></div>
                                <div class="card-description">当前商品总数量</div>
                                <span class="card-icon iconfont iconshangpinguanli"></span>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-3">
                        <div class="widget-card card__red am-cf">
                            <div class="card-header">用户总量</div>
                            <div class="card-body">
                                <div class="card-value"><?=$count['user'][0]?></div>
                                <div class="card-description">当前用户总数量</div>
                                <span class="card-icon iconfont iconyonghuguanli"></span>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-3">
                        <div class="widget-card card__violet am-cf">
                            <div class="card-header">订单总量</div>
                            <div class="card-body">
                                <div class="card-value"><?=$count['order'][0]['count']?></div>
                                <div class="card-description">付款订单总数量</div>
                                <span class="card-icon iconfont icondingdanguanli"></span>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-12 am-u-md-6 am-u-lg-3">
                        <div class="widget-card card__primary am-cf">
                            <div class="card-header">评价总量</div>
                            <div class="card-body">
                                <div class="card-value"><?=$count['comment'][0]?></div>
                                <div class="card-description">订单评价总数量</div>
                                <span class="card-icon iconfont iconhaoping"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 待处理 -->
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12 am-margin-bottom">
            <div class="widget am-cf">
                <div class="widget-head">
                    <div class="widget-title">待处理统计</div>
                </div>
                <div class="widget-body am-cf">
                    <div class="am-u-sm-2">
                        <div class="widget-outline2 dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">待退款数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][0]['refund']?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- 订单统计 -->
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12 am-margin-bottom">
            <div class="widget am-cf">
                <div class="widget-head">
                    <div class="widget-title">数量统计</div>
                </div>
                <div class="widget-body am-cf">
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日堂食订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][1]['tang']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order'][2]['tang']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月堂食订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][8]['tang']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order'][9]['tang']?></div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日自取订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][1]['qu']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order'][2]['qu']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月自取订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][8]['qu']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order'][9]['qu']?></div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日外卖订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][1]['wai']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order'][2]['wai']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月外卖订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][8]['wai']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order'][9]['wai']?></div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日排号订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['pact'][1]['sort']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['pact'][2]['sort']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月排号订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['pact'][8]['sort']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['pact'][9]['sort']?></div>
                            </div>
                        </div>
                    </div>
					<div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日订桌订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['pact'][1]['table']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['pact'][2]['table']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月订桌订单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['pact'][8]['table']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['pact'][9]['table']?></div>
                            </div>
                        </div>
                    </div>
					<div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日退单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?= $count['order'][1]['refund'] ?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?= $count['order'][2]['refund'] ?></div>
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月退单数量</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?= $count['order'][8]['refund'] ?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?= $count['order'][9]['refund'] ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widget-head">
                    <div class="widget-title">金额统计</div>
                </div>
				<div class="widget-body am-cf">
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日订单商品金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][1]['total_price']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order'][2]['total_price']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月订单商品金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][8]['total_price']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order'][9]['total_price']?></div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日订单配送金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][1]['express_price']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order'][2]['express_price']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月订单配送金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][8]['express_price']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order'][9]['express_price']?></div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日订单餐盒金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][1]['pack_price']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order'][2]['pack_price']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月订单餐盒金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][8]['pack_price']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order'][9]['pack_price']?></div>
                            </div>
                        </div>
                    </div>
					<div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日订单调料金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][1]['ware_price']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order'][2]['ware_price']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月订单调料金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][8]['ware_price']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order'][9]['ware_price']?></div>
                            </div>
                        </div>
                    </div>
					<div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日订单优惠金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][1]['activity_price']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order'][2]['activity_price']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月订单优惠金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][8]['activity_price']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order'][9]['activity_price']?></div>
                            </div>
                        </div>
                    </div>
					<div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日订单实收金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][1]['pay_price']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?=$count['order'][2]['pay_price']?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月订单实收金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?=$count['order'][8]['pay_price']?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?=$count['order'][9]['pay_price']?></div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="widget-body am-cf">
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日买单金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;">0</div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：0</div>
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月买单金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;">0</div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：0</div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日充值金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?= $count['recharge'][1] ?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?= $count['recharge'][2] ?></div>
								<div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月充值金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?= $count['recharge'][8] ?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?= $count['recharge'][9] ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-2">
                        <div class="widget-outline dis-flex flex-y-center">
                            <div class="outline-right dis-flex flex-dir-column flex-x-between">
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">今日退款金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?= $count['order'][1]['refund_price'] ?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    昨日：<?= $count['order'][2]['refund_price'] ?></div>
                                <div style="color: rgb(102, 102, 102); font-size: 1.2rem;">本月退款金额</div>
                                <div style="color: rgb(51, 51, 51); font-size: 2.4rem;"><?= $count['order'][8]['refund_price'] ?></div>
                                <div style="color: rgb(153, 153, 153); font-size: 1.2rem;">
                                    上月：<?= $count['order'][9]['refund_price'] ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="am-u-sm-2">
                    </div>
                    <div class="am-u-sm-2">
                    </div>
                    <div class="am-u-sm-2">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 近七日走势 -->
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12 am-margin-bottom">
            <div class="widget am-cf">
                <div class="widget-head">
                    <div class="widget-title">近七日走势</div>
                </div>
                <div class="widget-body am-cf">
                    <div id="echarts-trade" class="widget-echarts"></div>
                </div>
            </div>
        </div>
    </div>
	<div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="help-block am-text-center">
                <small><?= $web['name']?>&nbsp;&nbsp;版权所有</small>
            </div>
        </div>

    </div>
</div>
<script src="assets/plugins/echarts/echarts.min.js"></script>
<script src="assets/plugins/echarts/echarts-walden.js"></script> 
<script type="text/javascript">
		$('#shop').change(function(){
			window.location.href='index.php?s=/store/index/index/shop_id/'+$('#shop').val();
		});	
	
    /**
     * 近七日交易走势
     * @type {HTMLElement}
     */
    var dom = document.getElementById('echarts-trade');
    echarts.init(dom, 'walden').setOption({
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['订单量', '成交额', '用户量', '充值额']
        },
        toolbox: {
            show: true,
            showTitle: false,
            feature: {
                mark: {show: true},
                magicType: {show: true, type: ['line', 'bar']}
            }
        },
        calculable: true,
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: ["<?= date("Y-m-d")?>","<?= date("Y-m-d",strtotime("-1 day"))?>","<?= date("Y-m-d",strtotime("-2 day"))?>","<?= date("Y-m-d",strtotime("-3 day"))?>","<?= date("Y-m-d",strtotime("-4 day"))?>","<?= date("Y-m-d",strtotime("-5 day"))?>","<?= date("Y-m-d",strtotime("-6 day"))?>"]        
		},
        yAxis: {
            type: 'value'
        },
        series: [
            {
                name: '成交额',
                type: 'line',
                data: [<?= $count['order'][1]['pay_price'] ?>,<?= $count['order'][2]['pay_price'] ?>,<?= $count['order'][3]['pay_price'] ?>,<?= $count['order'][4]['pay_price'] ?>,<?= $count['order'][5]['pay_price'] ?>,<?= $count['order'][6]['pay_price'] ?>,<?= $count['order'][7]['pay_price'] ?>]
			},
            {
                name: '订单量',
                type: 'line',
                data: [<?= $count['order'][1]['count'] ?>,<?= $count['order'][2]['count'] ?>,<?= $count['order'][3]['count'] ?>,<?= $count['order'][4]['count'] ?>,<?= $count['order'][5]['count'] ?>,<?= $count['order'][6]['count'] ?>,<?= $count['order'][7]['count'] ?>]
			},
            {
                name: '用户量',
                type: 'line',
                data: [<?= $count['user'][1] ?>,<?= $count['user'][2] ?>,<?= $count['user'][3] ?>,<?= $count['user'][4] ?>,<?= $count['user'][5] ?>,<?= $count['user'][6] ?>,<?= $count['user'][7] ?>]
			},
            {
                name: '充值额',
                type: 'line',
                data: [<?= $count['recharge'][1] ?>,<?= $count['recharge'][2] ?>,<?= $count['recharge'][3] ?>,<?= $count['recharge'][4] ?>,<?= $count['recharge'][5] ?>,<?= $count['recharge'][6] ?>,<?= $count['recharge'][7] ?>]
			}
        ]
    }, true);

</script>