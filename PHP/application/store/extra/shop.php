<?php

return [
    'index' => [
        'name' => '首页',
        'icon' => 'iconshouye',
        'index' => 'index/index',
    ],
    'wxapp' => [
        'name' => '小程序',
        'icon' => 'iconweixinxiaochengxu',
        'color' => '#36b313',
        'index' => 'wxapp/setting',
        'submenu' => [
            [
                'name' => '小程序设置',
                'index' => 'wxapp/setting',
				'urls' => [
					'wxapp/setting',
					'wxapp.register/index',
					'wxapp.register/pay',
					'wxapp.wxpay/reg',
					'wxapp.name/setting',
					'wxapp/sethead',
					'wxapp/servedomain',
					'wxapp/signature',
				]
            ],
			[
				'name' => '发布/升级',
				'index' => 'wxapp.release/index',
					'urls' => [
						'wxapp.release/index',
						'wxapp.release/add',
						'wxapp.release/edit',
						'wxapp.release/delete'
					]
			],
			[
				'name' => '业务域名',
				'index' => 'wxapp/setdomain',
				'uris' => [
					'wxapp/setdomain',
				],
			],
			[
				'name' => '服务类目',
				'index' => 'wxapp.category/index',
				'uris' => [
					'wxapp.category/index',
					'wxapp.category/add',
					'wxapp.category/delete',
				],
			],
			[
				'name' => '订阅消息',
				'index' => 'wxapp.tpl/setting',
				'uris' => [
					'wxapp.tpl/setting',
				],
			],
			[
                'name' => '插件管理',
				'index' => 'wxapp.plugins/index',
				'uris' => [
					'wxapp.plugins/index',
					'wxapp.plugins/add',
					'wxapp.plugins/del',
				],
            ],
			[
				'name' => '体验用户',
				'index' => 'wxapp.test/index',
					'urls' => [
						'wxapp.test/index',
						'wxapp.test/add',
						'wxapp.test/delete'
					]
			],
            [
                'name' => '帮助中心',
                'index' => 'wxapp.help/index',
                'urls' => [
                    'wxapp.help/index',
                    'wxapp.help/add',
                    'wxapp.help/edit',
                    'wxapp.help/delete'
                ]
            ],
			[
                'name' => '装修小程序',
                'active' => true,
                'submenu' => [
                    [
                        'name' => '首页设计',
                        'index' => 'wxapp.page/index'
                    ],
                    [
                        'name' => '分类样式',
                        'index' => 'wxapp.page/category'
                    ],
                    [
                        'name' => '页面链接',
                        'index' => 'wxapp.page/links'
                    ],
                ]
            ],
            [
                'name' => '直播管理',
                'active' => true,
                'submenu' => [
                    [
                        'name' => '直播间管理',
                        'index' => 'wxapp.live.room/index',
						'uris' => [
							'wxapp.live.room/index',
							'wxapp.live.room/add',
							'wxapp.live.room/edit',
							'wxapp.live.room/delete',
						],
                    ],
                ]
            ],
			[
                'name' => '附近小程序',
                'active' => false,
                'submenu' => [
					[
						'name' => '门店类目',
						'index' => 'wxapp.nearby.category/index',
						'uris' => [
							'wxapp.nearby.category/index',
							'wxapp.nearby.category/add',
							'wxapp.nearby.category/delete',
						],
					],
					[
						'name' => '附近展示',
						'index' => 'wxapp.nearby/index',
						'uris' => [
							'wxapp.nearby/index',
							'wxapp.nearby/add',
							'wxapp.nearby/delete',
						],
					],
                ]
            ],
        ],
    ],
    'shop' => [
        'name' => '门店管理',
        'icon' => 'icondianpu',
        'index' => 'shop/index',
		'submenu' => [
		    [
		        'name' => '门店列表',
		        'index' => 'shop/index',
				'urls' => [
				    'shop/index',
				    'shop/add',
				    'shop/edit',
				    'shop/delete'
				]
		    ],
		    [
		        'name' => '店员管理',
		        'index' => 'shop.clerk/index',
				'urls' => [
				    'shop.clerk/index',
				    'shop.clerk/add',
				    'shop.clerk/edit',
				    'shop.clerk/delete'
				]
		    ],
			[
		        'name' => '门店打印机',
		        'index' => 'shop.printer/index',
				'urls' => [
				    'shop.printer/index',
				    'shop.printer/add',
				    'shop.printer/edit',
				    'shop.printer/delete'
				]
		    ],
		    [
                'name' => '图文资讯',
                'active' => false,
                'submenu' => [
                     [
		                'name' => '资讯列表',
		                'index' => 'shop.article/opt',
		                'uris' => [
							'shop.article/opt',
		                    'shop.article/index',
		                    'shop.article/add',
		                    'shop.article/edit',
							'shop.article/delete'
		                ],
		            ],
		            [
		                'name' => '资讯分类',
		                'index' => 'shop.article.category/index',
		                'uris' => [
		                    'shop.article.category/index',
		                    'shop.article.category/add',
		                    'shop.article.category/edit',
							'shop.article.category/delete'
		                ],
		            ]
                ]
            ],
		]
    ],
    'goods' => [
        'name' => '商品管理',
        'icon' => 'iconshangpinguanli',
        'index' => 'goods/opt',
        'submenu' => [
            [
                'name' => '商品列表',
                'index' => 'goods/opt',
                'uris' => [
					'goods/opt',
					'goods/copys',
                    'goods/index',
                    'goods/add',
                    'goods/edit',
					'goods/delete'
                ],
            ],
            [
                'name' => '商品分类',
                'index' => 'goods.category/opt',
                'uris' => [
					'goods.category/opt',
                    'goods.category/index',
                    'goods.category/add',
                    'goods.category/edit',
					'goods.category/delete'
                ],
            ]
        ],
    ],
    'order' => [
        'name' => '订单管理',
        'icon' => 'icondingdanguanli',
        'index' => 'order/all_list',
        'submenu' => [
            [
                'name' => '全部订单',
                'index' => 'order/all_list',
            ],
            [
				'name' => '待发货',
				'index' => 'order/delivery_list',
			],
			[
				'name' => '待完成',
				'index' => 'order/receipt_list',
			],
			[
				'name' => '待付款',
				'index' => 'order/pay_list',
			],
			[
				'name' => '已完成',
				'index' => 'order/complete_list',

			],
			[
				'name' => '已取消',
				'index' => 'order/cancel_list',
			],
			[
				'name' => '退款订单',
				'index' => 'order/tui',
			],
			[
                'name' => '订单评价',
                'index' => 'order.comment/index',
				'uris' => [
					'order.comment/index',
					'order.comment/edit',
				],
            ],
        ]
    ],
    'user' => [
        'name' => '用户管理',
        'icon' => 'iconyonghuguanli',
        'index' => 'user/index',
        'submenu' => [
            [
                'name' => '用户列表',
                'index' => 'user/index',
            ],
			[
                'name' => '等级管理',
                'index' => 'user.grade/index',
				'uris' => [
					'user.grade/index',
					'user.grade/add',
					'user.grade/edit',
					'user.grade/delete',
				],
            ],
			[
                'name' => '充值记录',
                'index' => 'user.recharge/index',
				'uris' => [
					'user.recharge/index'
				],
            ],
            [
                'name' => '收货地址',
                'index' => 'user.addres/index',
				'uris' => [
					'user.addres/index'
				],
            ],
            [
                'name' => '购物车',
                'index' => 'user.cart/index',
				'uris' => [
					'user.cart/index'
				],
            ],
        ]
    ],
	'market' => [
        'name' => '营销管理',
        'icon' => 'iconyingxiao',
        'index' => 'market.activity/index',
        'submenu' => [
			[
                'name' => '优惠活动',
                'index' => 'market.activity/index',
				'uris' => [
					'market.activity/index',
					'market.activity/add',
					'market.activity/edit',
					'market.activity/delete',
				],
            ],
			[
                'name' => '用户充值',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '充值套餐',
                        'index' => 'market.recharge.plan/index',
						'uris' => [
							'market.recharge.plan/index',
							'market.recharge.plan/add',
							'market.recharge.plan/edit',
							'market.recharge.plan/delete',
						],
                    ],
					[
                        'name' => '充值设置',
                        'index' => 'market.recharge/setting',
                    ],
                ]
            ],
			[
                'name' => '分销中心',
                'active' => false,
                'submenu' => [
					[
                        'name' => '分销设置',
                        'index' => 'market.dealer/setting',
						'urls' => [
							'market.dealer/setting',
						]
                    ],
					[
                        'name' => '分销海报',
                        'index' => 'market.dealer/poster',
						'urls' => [
							'market.dealer/poster',
						]
                    ],
                ]
            ],
            [
                'name' => '整点秒杀',
                'active' => false,
                'submenu' => [
					[
                        'name' => '商品管理',
                        'index' => 'market.seckill.goods/opt',
						'urls' => [
							'market.seckill.goods/opt',
							'market.seckill.goods/index',
							'market.seckill.goods/add',
							'market.seckill.goods/edit',
							'market.seckill.goods/delete',
						]
                    ],
                ]
            ],
            [
                'name' => '拼团活动',
                'active' => false,
                'submenu' => [
					[
                        'name' => '商品管理',
                        'index' => 'market.group.goods/opt',
						'urls' => [
							'market.group.goods/opt',
							'market.group.goods/index',
							'market.group.goods/add',
							'market.group.goods/edit',
							'market.group.goods/delete',
						]
                    ],
                ]
            ],
            [
                'name' => '砍价活动',
                'active' => false,
                'submenu' => [
					[
                        'name' => '商品管理',
                        'index' => 'market.bargain.goods/opt',
						'urls' => [
							'market.bargain.goods/opt',
							'market.bargain.goods/index',
							'market.bargain.goods/add',
							'market.bargain.goods/edit',
							'market.bargain.goods/delete',
						]
                    ],
                ]
            ],
		],
    ],
	'statistics' =>[
		'name' => '数据统计',
        'icon' => 'icontongji',
        'index' => 'statistics/opt',
		'urls' => [
			'statistics/opt',
			'statistics/index'
		]
	],
    'setting' => [
        'name' => '设置',
        'icon' => 'iconshezhi',
        'index' => 'setting.cache/clear',
        'submenu' => [
            [
                'name' => '清理缓存',
                'index' => 'setting.cache/clear',
            ],
            [
                'name' => '交易设置',
                'index' => 'setting/trade',
            ],
            [
                'name' => '配送设置',
                'index' => 'setting/delivery',
                'active' => true,
                'submenu' => [
					[
                        'name' => '同城配送',
                        'index' => 'setting/delivery',
						'urls' => [
							'setting/delivery',
						]
                    ],
					[
                        'name' => '配送公司',
                        'index' => '#',
						'urls' => [
							'#',
						]
                    ],
                ]
            ]
        ],
    ],
];
