<?php

return [
    'index' => [
        'name' => '首页',
        'icon' => 'iconshouye',
        'index' => 'index/index',
    ],
    'wxapp' => [
        'name' => '小程序',
        'icon' => 'iconweixinxiaochengxu',
        'color' => '#36b313',
        'index' => 'wxapp/setting',
        'submenu' => [
            [
                'name' => '小程序设置',
                'index' => 'wxapp/setting',
				'urls' => [
					'wxapp/setting',
					'wxapp.register/index',
					'wxapp.register/pay',
					'wxapp.wxpay/reg',
					'wxapp.name/setting',
					'wxapp/sethead',
					'wxapp/servedomain',
					'wxapp/signature',
				]
            ],
			[
				'name' => '发布/升级',
				'index' => 'wxapp.release/index',
					'urls' => [
						'wxapp.release/index',
						'wxapp.release/add',
						'wxapp.release/edit',
						'wxapp.release/delete'
					]
			],
			[
				'name' => '业务域名',
				'index' => 'wxapp/setdomain',
				'uris' => [
					'wxapp/setdomain',
				],
			],
			[
				'name' => '服务类目',
				'index' => 'wxapp.category/index',
				'uris' => [
					'wxapp.category/index',
					'wxapp.category/add',
					'wxapp.category/delete',
				],
			],
			[
                'name' => '插件管理',
				'index' => 'wxapp.plugins/index',
				'uris' => [
					'wxapp.plugins/index',
					'wxapp.plugins/add',
					'wxapp.plugins/del',
				],
            ],
			[
				'name' => '体验用户',
				'index' => 'wxapp.test/index',
					'urls' => [
						'wxapp.test/index',
						'wxapp.test/add',
						'wxapp.test/delete'
					]
			],
            [
                'name' => '帮助中心',
                'index' => 'wxapp.help/index',
                'urls' => [
                    'wxapp.help/index',
                    'wxapp.help/add',
                    'wxapp.help/edit',
                    'wxapp.help/delete'
                ]
            ],
			[
                'name' => '装修小程序',
                'active' => true,
                'submenu' => [
                    [
                        'name' => '首页设计',
                        'index' => 'wxapp.page/index'
                    ],
                    [
                        'name' => '页面链接',
                        'index' => 'wxapp.page/links'
                    ],
                ]
            ],
            [
                'name' => '直播管理',
                'active' => true,
                'submenu' => [
                    [
                        'name' => '直播间管理',
                        'index' => 'wxapp.live.room/index',
						'uris' => [
							'wxapp.live.room/index',
							'wxapp.live.room/add',
							'wxapp.live.room/edit',
							'wxapp.live.room/delete',
						],
                    ],
                ]
            ],
			[
                'name' => '附近小程序',
                'active' => false,
                'submenu' => [
					[
						'name' => '门店类目',
						'index' => 'wxapp.nearby.category/index',
						'uris' => [
							'wxapp.nearby.category/index',
							'wxapp.nearby.category/add',
							'wxapp.nearby.category/delete',
						],
					],
					[
						'name' => '附近展示',
						'index' => 'wxapp.nearby/index',
						'uris' => [
							'wxapp.nearby/index',
							'wxapp.nearby/add',
							'wxapp.nearby/delete',
						],
					],
                ]
            ],
        ],
    ],
    'wechat' => [
        'name' => '公众号',
        'icon' => 'iconweixingongzhonghao',
		'color' => '#36b313',
        'index' => 'wechat/index',
        'submenu' => [
			[
                'name' => '基础信息',
                'index' => 'wechat/index',
				'urls' => [
					'wechat/index',
				] 
            ],
			[
                'name' => '菜单设置',
                'index' => 'wechat/menus',
				'urls' => [
					'wechat/menu',
				] 
            ],
			[
				'name' => '群发消息',
				'index' => 'wechat.send/index',
				'uris' => [
					'wechat.send/index',
					'wechat.send/add',
                    'wechat.send/edit',
					'wechat.send/delete'
				],
			],
			[
                'name' => '素材管理',
                'active' => true,
				'submenu' => [
					[
						'name' => '图文素材',
						'index' => 'wechat.material.text/index',
						'urls' => [
							'wechat.material.text/index',
							'wechat.material.text/add',
							'wechat.material.text/edit',
							'wechat.material.text/delete'
						]
					],
					[
						'name' => '图片素材',
						'index' => 'wechat.material.image/index',
						'urls' => [
							'wechat.material.image/index',
							'wechat.material.image/add',
							'wechat.material.image/edit',
							'wechat.material.image/delete'
						]
					],	
					[
						'name' => '语音素材',
						'index' => 'wechat.material.voice/index',
						'urls' => [
							'wechat.material.voice/index',
							'wechat.material.voice/add',
							'wechat.material.voice/edit',
							'wechat.material.voice/delete'
						]
					],
					[
						'name' => '视频素材',
						'index' => 'wechat.material.video/index',
						'urls' => [
							'wechat.material.video/index',
							'wechat.material.video/add',
							'wechat.material.video/edit',
							'wechat.material.video/delete'
						]
					],
				]
            ],
			[
                'name' => '智能回复',
                'active' => false,
                'submenu' => [
					[
						'name' => '被关注回复',
						'index' => 'wechat/subscribe',
							'urls' => [
								'wechat/subscribe',
							]
					],
					[
						'name' => '关键字回复',
						'index' => 'wechat.keyword/index',
						'uris' => [
							'wechat.keyword/index',
							'wechat.keyword/add',
							'wechat.keyword/edit',
							'wechat.keyword/delete',
						],
					],
					
                ]
            ],
        ],
    ],
    'shop' => [
        'name' => '门店管理',
        'icon' => 'icondianpu',
        'index' => 'shop/index',
		'submenu' => [
		    [
		        'name' => '门店列表',
		        'index' => 'shop/index',
				'urls' => [
				    'shop/index',
				    'shop/add',
				    'shop/edit',
				    'shop/delete'
				]
		    ],
		    [
		        'name' => '店员管理',
		        'index' => 'shop.clerk/index',
				'urls' => [
				    'shop.clerk/index',
				    'shop.clerk/add',
				    'shop.clerk/edit',
				    'shop.clerk/delete'
				]
		    ],
			[
		        'name' => '餐桌管理',
		        'index' => 'shop.table/index',
				'urls' => [
				    'shop.table/index',
				    'shop.table/add',
				    'shop.table/edit',
				    'shop.table/delete'
				]
		    ],
			[
		        'name' => '口味选项',
		        'index' => 'shop.flavor/index',
				'urls' => [
				    'shop.flavor/index',
				    'shop.flavor/add',
				    'shop.flavor/edit',
				    'shop.flavor/delete'
				]
		    ],
		    [
                'name' => '门店设备',
                'active' => false,
                'submenu' => [
                     [
		                'name' => '打印机',
		                'index' => 'shop.printer/index',
		                'uris' => [
		                    'shop.printer/index',
		                    'shop.printer/add',
		                    'shop.printer/edit',
							'shop.printer/delete'
		                ],
		            ],
		            [
		                'name' => '叫号器',
		                'index' => 'shop.calling/index',
		                'uris' => [
		                    'shop.calling/index',
		                    'shop.calling/add',
		                    'shop.calling/edit',
							'shop.calling/delete'
		                ],
		            ],
                ]
            ],
		    [
                'name' => '图文资讯',
                'active' => false,
                'submenu' => [
                     [
		                'name' => '资讯列表',
		                'index' => 'shop.article/opt',
		                'uris' => [
							'shop.article/opt',
		                    'shop.article/index',
		                    'shop.article/add',
		                    'shop.article/edit',
							'shop.article/delete'
		                ],
		            ],
		            [
		                'name' => '资讯分类',
		                'index' => 'shop.article.category/index',
		                'uris' => [
		                    'shop.article.category/index',
		                    'shop.article.category/add',
		                    'shop.article.category/edit',
							'shop.article.category/delete'
		                ],
		            ]
                ]
            ],
		]
    ],
    'goods' => [
        'name' => '商品管理',
        'icon' => 'iconshangpinguanli',
        'index' => 'goods/opt',
        'submenu' => [
            [
                'name' => '商品列表',
                'index' => 'goods/opt',
                'uris' => [
					'goods/opt',
					'goods/copys',
                    'goods/index',
                    'goods/add',
                    'goods/edit',
					'goods/delete'
                ],
            ],
            [
                'name' => '商品分类',
                'index' => 'goods.category/opt',
                'uris' => [
					'goods.category/opt',
                    'goods.category/index',
                    'goods.category/add',
                    'goods.category/edit',
					'goods.category/delete'
                ],
            ]
        ],
    ],
    'order' => [
        'name' => '订单管理',
        'icon' => 'icondingdanguanli',
        'index' => 'order/all_list',
        'submenu' => [
            [
                'name' => '全部订单',
                'index' => 'order/all_list',
				'uris' => [
					'order/all_list'
				],
            ],
            [
				'name' => '待收款',
				'index' => 'order/collection_list',
				'uris' => [
					'order/collection_list'
				],
			],
            [
				'name' => '待接单',
				'index' => 'order/shop_list',
				'uris' => [
					'order/shop_list'
				],
			],
			[
				'name' => '待发货',
				'index' => 'order/delivery_list',
				'uris' => [
					'order/delivery_list'
				],
			],
			[
				'name' => '待收货',
				'index' => 'order/receipt_list',
				'uris' => [
					'order/receipt_list'
				],
			],
			
			[
				'name' => '已完成',
				'index' => 'order/complete_list',
				'uris' => [
					'order/complete_list'
				],

			],
			[
				'name' => '已取消',
				'index' => 'order/cancel_list',
				'uris' => [
					'order/cancel_list'
				],
			],
			[
                'name' => '退款订单',
                'active' => false,
                'submenu' => [
                    [
						'name' => '全部订单',
						'index' => 'order/refund_list',
						'uris' => [
							'order/refund_list'
						],
					],
					[
						'name' => '待退款',
						'index' => 'order/refund10_list',
						'uris' => [
							'order/refund10_list'
						],
					],
					[
						'name' => '已退款',
						'index' => 'order/refund20_list',
						'uris' => [
							'order/refund20_list'
						],
					],
                ]
            ],
			[
                'name' => '预约订单',
                'active' => false,
                'submenu' => [
                    [
						'name' => '排号等座',
						'index' => 'order.pact/sorts',
						'uris' => [
							'order.pact/sorts'
						],
					],
					[
						'name' => '预约订桌',
						'index' => 'order.pact/table',
						'uris' => [
							'order.pact/table'
						],
					],
                ]
            ],
			[
                'name' => '订单评价',
                'index' => 'order.comment/index',
				'uris' => [
					'order.comment/index',
					'order.comment/edit',
				],
            ],
        ]
    ],
    'user' => [
        'name' => '用户管理',
        'icon' => 'iconyonghuguanli',
        'index' => 'user/index',
        'submenu' => [
            [
                'name' => '用户列表',
                'index' => 'user/index',
            ],
			[
                'name' => '等级管理',
                'index' => 'user.grade/index',
				'uris' => [
					'user.grade/index',
					'user.grade/add',
					'user.grade/edit',
					'user.grade/delete',
				],
            ],
			[
                'name' => '充值记录',
                'index' => 'user.recharge/index',
				'uris' => [
					'user.recharge/index'
				],
            ],
            [
                'name' => '收货地址',
                'index' => 'user.addres/index',
				'uris' => [
					'user.addres/index'
				],
            ],
            [
                'name' => '购物车',
                'index' => 'user.cart/index',
				'uris' => [
					'user.cart/index'
				],
            ],
        ]
    ],
	'market' => [
        'name' => '营销管理',
        'icon' => 'iconyingxiao',
        'index' => 'market.activity/index',
        'submenu' => [
			[
                'name' => '优惠活动',
                'index' => 'market.activity/index',
				'uris' => [
					'market.activity/index',
					'market.activity/add',
					'market.activity/edit',
					'market.activity/delete',
				],
            ],
			[
                'name' => '用户充值',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '充值套餐',
                        'index' => 'market.recharge.plan/index',
						'uris' => [
							'market.recharge.plan/index',
							'market.recharge.plan/add',
							'market.recharge.plan/edit',
							'market.recharge.plan/delete',
						],
                    ],
					[
                        'name' => '充值设置',
                        'index' => 'market.recharge/setting',
                    ],
                ]
            ],
			[
                'name' => '免费吃',
                'active' => false,
                'submenu' => [
					[
                        'name' => '活动商品',
                        'index' => 'market.free.goods/opt',
						'urls' => [
							'market.free.goods/opt',
							'market.free.goods/index',
							'market.free.goods/add',
							'market.free.goods/edit',
							'market.free.goods/delete',
						]
                    ],
					[
                        'name' => '活动设置',
                        'index' => 'market.free/setting',
						'urls' => [
							'market.free/setting',
						]
                    ],
                ]
            ],
			[
                'name' => '分销中心',
                'active' => false,
                'submenu' => [
					[
                        'name' => '分销设置',
                        'index' => 'market.dealer/setting',
						'urls' => [
							'market.dealer/setting',
						]
                    ],
					[
                        'name' => '分销海报',
                        'index' => 'market.dealer/poster',
						'urls' => [
							'market.dealer/poster',
						]
                    ],
                ]
            ],
		],
    ],
	'statistics' =>[
		'name' => '数据统计',
        'icon' => 'icontongji',
        'index' => 'statistics/opt',
		'urls' => [
			'statistics/opt',
			'statistics/index'
		]
	],
    'setting' => [
        'name' => '设置',
        'icon' => 'iconshezhi',
        'index' => 'setting.cache/clear',
        'submenu' => [
            [
                'name' => '清理缓存',
                'index' => 'setting.cache/clear',
            ],
            [
                'name' => '支付设置',
                'index' => 'setting/payment',
            ],
            [
                'name' => '交易设置',
                'index' => 'setting/trade',
            ],
            [
                'name' => '配送设置',
                'index' => 'setting/delivery',
            ],
            [
                'name' => '消息设置',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '微信小程序',
                        'index' => 'setting/wxapptpl'
                    ],
                    [
                        'name' => '微信公众号',
                        'index' => 'setting/wechattpl'
                    ],
                ]
            ],
        ],
    ],
];
