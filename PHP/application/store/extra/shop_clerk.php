<?php

return [
    'index' => [
        'name' => '首页',
        'icon' => 'iconshouye',
        'index' => 'index/index',
    ],
    'shop' => [
        'name' => '门店管理',
        'icon' => 'icondianpu',
        'index' => 'shop/edit',
		'submenu' => [
		    [
		        'name' => '门店详情',
		        'index' => 'shop/edit'
		    ],
		    [
		        'name' => '店员管理',
		        'index' => 'shop.clerk/index',
				'urls' => [
				    'shop.clerk/index',
				    'shop.clerk/add',
				    'shop.clerk/edit',
				    'shop.clerk/delete'
				]
		    ],
			[
		        'name' => '门店打印机',
		        'index' => 'shop.printer/index',
				'urls' => [
				    'shop.printer/index',
				    'shop.printer/add',
				    'shop.printer/edit',
				    'shop.printer/delete'
				]
		    ],
		    [
                'name' => '资讯列表',
                'index' => 'shop.article/opt',
                'uris' => [
					'shop.article/opt',
                    'shop.article/index',
                    'shop.article/add',
                    'shop.article/edit',
					'shop.article/delete'
                ]
            ],
		]
    ],
    'goods' => [
        'name' => '商品管理',
        'icon' => 'iconshangpinguanli',
        'index' => 'goods/opt',
        'submenu' => [
            [
                'name' => '商品列表',
                'index' => 'goods/opt',
                'uris' => [
					'goods/opt',
					'goods/copys',
                    'goods/index',
                    'goods/add',
                    'goods/edit',
					'goods/delete'
                ],
            ],
            [
                'name' => '商品分类',
                'index' => 'goods.category/opt',
                'uris' => [
					'goods.category/opt',
                    'goods.category/index',
                    'goods.category/add',
                    'goods.category/edit',
					'goods.category/delete'
                ],
            ]
        ],
    ],
    'order' => [
        'name' => '订单管理',
        'icon' => 'icondingdanguanli',
        'index' => 'order/all_list',
        'submenu' => [
            [
                'name' => '全部订单',
                'index' => 'order/all_list',
            ],
            [
				'name' => '待发货',
				'index' => 'order/delivery_list',
			],
			[
				'name' => '待完成',
				'index' => 'order/receipt_list',
			],
			[
				'name' => '待付款',
				'index' => 'order/pay_list',
			],
			[
				'name' => '已完成',
				'index' => 'order/complete_list',

			],
			[
				'name' => '已取消',
				'index' => 'order/cancel_list',
			],
			[
				'name' => '退款订单',
				'index' => 'order/tui',
			],
			[
                'name' => '订单评价',
                'index' => 'order.comment/index',
				'uris' => [
					'order.comment/index',
					'order.comment/edit',
				],
            ],
        ]
    ],
    'user' => [
        'name' => '用户管理',
        'icon' => 'iconyonghuguanli',
        'index' => 'user/index',
        'submenu' => [
            [
                'name' => '用户列表',
                'index' => 'user/index',
            ],
			[
                'name' => '充值记录',
                'index' => 'user.recharge/index',
				'uris' => [
					'user.recharge/index'
				],
            ],
        ]
    ],
	'statistics' =>[
		'name' => '数据统计',
        'icon' => 'icontongji',
        'index' => 'statistics/opt',
		'urls' => [
			'statistics/opt',
			'statistics/index'
		]
	],
];
