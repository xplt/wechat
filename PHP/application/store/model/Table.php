<?php
namespace app\store\model;
use app\common\model\Table as TableModel;
use think\Db;

/**
 * 餐桌/包间模型
 */
class Table extends TableModel
{
    /**
     * 新增记录
     */
    public function add($data)
    {
		$data['wxapp_id'] = self::$wxapp_id;
        // 开启事务
        Db::startTrans();
        try {
            // 添加
            $this->allowField(true)->save($data);
			//生成餐桌二维码
			if($result = qrCode($data['shop_id'],$this->table_id)){
				$this->error = wx_err_msg($result['errcode']);
                return false;
			}
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }

    /**
     * 更新记录
     */
    public function edit($data)
    {
		// 开启事务
        Db::startTrans();
        try {
            //生成餐桌二维码
            if($this['shop_id']!=$data['shop_id']){
                if($result = qrCode($data['shop_id'],$this->table_id)){
                    $this->error = wx_err_msg($result['errcode']);
                    return false;
                }
            }
			$this->allowField(true)->save($data);
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }

    /**
     * 删除记录
     */
    public function remove() 
	{
		// 开启事务
        Db::startTrans();
        try {
			//删除二维码
			if(file_exists('./assets/qrcode/table/'.$this['table_id'].'.png'))
			{
				unlink('./assets/qrcode/table/'.$this['table_id'].'.png');
			}
            $this->delete();
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }

}
