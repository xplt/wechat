<?php
namespace app\store\model;
use app\common\model\Shop as ShopModel;
use think\Session;
use think\Db;

/**
 * 门店模型
 */
class Shop extends ShopModel
{
    /**
     * 添加
     */
    public function add(array $data)
    {
		$location = getLocation($data['coordinate']);
		if(!$location OR empty($location['province']) OR empty($location['city']) OR empty($location['district'])){
			$this->error = ' 逆地址解析失败，地图KEY配置错误';
            return false;
		}
		$data['province'] = $location['province'];
		$data['city'] = $location['city'];
		$data['district'] = $location['district'];
        $data['wxapp_id'] = self::$wxapp_id;
        // 开启事务
        Db::startTrans();
        try {
            // 添加
            $this->allowField(true)->save($data)!== false;
			//生成门店二维码
			if($result = qrCode($this->shop_id)){
                $this->error = wx_err_msg($result['errcode']);
                return false;
            }
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }
	
	
    /**
     * 编辑
     */
    public function edit($data)
    {
		$location = getLocation($data['coordinate']);
        $data['poi_id'] = $location['poi_id'];
		$data['province'] = $location['province'];
		$data['city'] = $location['city'];
		$data['district'] = $location['district'];
		// 开启事务
        Db::startTrans();
        try {
            // 添加
            $this->allowField(true)->save($data);
			//生成门店二维码
			if($result = qrCode($this->shop_id)){
                $this->error = wx_err_msg($result['errcode']);
                return false;
            }
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }
}
