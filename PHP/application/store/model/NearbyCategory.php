<?php
namespace app\store\model;
use app\common\model\NearbyCategory as NearbyCategoryModel;

/**
 * 附近小程序-类目模型
 */
class NearbyCategory extends NearbyCategoryModel
{
	/**
     * 添加
     */
    public function add($data)
    {
		if (!isset($data['file_name']) || empty($data['file_name'])) {
			$this->error = '请上传《食品经营许可证》';
			return false;
		}
		//上传临时素材
		$media_id = up_temp_material($data['file_name']);
		$category = NearbyCategoryModel::defaultData();
		$data['first_name'] = $category['first']['name'];
		$data['first_id'] = $category['first']['id'];
		$data['second_name'] = $category['first']['children'][$data['id']]['name'];
		$data['second_id'] = $category['first']['children'][$data['id']]['id'];
		$data['wxapp_id'] = self::$wxapp_id;
		//提交到微信端
		$access_token = getAccessToken();
		$url = 'https://api.weixin.qq.com/wxa/nearbyapplycategory?access_token='.$access_token;
		$post = [
			'category_first_id' => $data['first_id'],
			'category_second_id' => $data['first_id'],
			'media_list' => [0 => $media_id]
		];
		$result = json_decode(http_post($url,$post),true);
		if($result['errcode']!=0){
			$this->error = wx_err_msg($result['errcode']);
			return false;
		}
		if(isset($result['audit_id'])){
			$data['audit_id'] = $result['audit_id'];
		}
        return $this->allowField(true)->save($data);
    }

    /**
     * 删除
     */
    public function remove()
    {
		if($this['status']['value']==1){
			$this->error = '状态审核中，不允许删除';
			return false;
		}
        return $this->delete();
    }
}
