<?php
namespace app\store\model;
use app\common\model\Flavor as FlavorModel;

/**
 * 口味选项模型
 */
class Flavor extends FlavorModel
{
    /**
     * 新增
     */
    public function add($data)
    {
        $data['wxapp_id'] = self::$wxapp_id;
        return $this->allowField(true)->save($data);
    }

    /**
     * 更新
     */
    public function edit($data)
    {
        return $this->allowField(true)->save($data) !== false;
    }

    /**
     * 删除
     */
    public function remove() {
        return $this->delete();
    }

}
