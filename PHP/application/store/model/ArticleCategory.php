<?php
namespace app\store\model;
use app\common\model\ArticleCategory as ArticleCategoryModel;
use think\Cache;

/**
 * 图文资讯分类模型
 */
class ArticleCategory extends ArticleCategoryModel
{
    /**
     * 添加新记录
     */
    public function add($data)
    {
        $data['wxapp_id'] = self::$wxapp_id;
        $this->deleteCache();
        return $this->allowField(true)->save($data);
    }

    /**
     * 编辑记录
     */
    public function edit($data)
    {
        $this->deleteCache();
        return $this->allowField(true)->save($data);
    }

    /**
     * 删除分类
     */
    public function remove($article_category_id)
    {
        // 判断是否存在图文信息
        if ($Count = (new Article)->where(['article_category_id'=>$article_category_id])->count()) {
            $this->error = '该分类下存在' . $Count . '条图文信息，不允许删除';
            return false;
        }
        $this->deleteCache();
        return $this->delete();
    }

    /**
     * 删除缓存
     */
    private function deleteCache()
    {
        return Cache::rm('article_category_' . self::$wxapp_id);
    }

}
