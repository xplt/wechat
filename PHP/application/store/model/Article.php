<?php
namespace app\store\model;
use app\common\model\Article as ArticleModel;

/**
 * 图文资讯模型
 */
class Article extends ArticleModel
{
    /**
     * 添加
     */
    public function add(array $data,$shop_id)
    {
        if (!isset($data['image_id']) || empty($data['image_id'])) {
            $this->error = '请上传封面图片';
            return false;
        }
        if (empty($data['article_category_id'])) {
            $this->error = '请选择分类';
            return false;
        }
        $data['shop_id'] = $shop_id;
        $data['article_content'] = isset($data['article_content']) ? $data['article_content'] : '';
        $data['wxapp_id'] = self::$wxapp_id;
        return $this->allowField(true)->save($data);      
    }

    
    /**
     * 编辑
     */
    public function edit($data)
    {
        if (!isset($data['image_id']) || empty($data['image_id'])) {
            $this->error = '请上传封面图片';
            return false;
        }
        if (empty($data['article_category_id'])) {
            $this->error = '请选择分类';
            return false;
        }
        $data['article_content'] = isset($data['article_content']) ? $data['article_content'] : '';
        return $this->allowField(true)->save($data) !== false;   
    }

    /**
     * 删除
     */
    public function remove()
    {
        return $this->delete();
    }
}
