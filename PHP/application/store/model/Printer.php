<?php
namespace app\store\model;
use app\common\model\Printer as PrinterModel;
use app\common\library\mstching\PrintHelper;
use app\common\library\mstching\Feie;
use app\common\library\mstching\YiLian;
use think\Db;

/**
 * 云打印机模型
 */
class Printer extends PrinterModel
{
	
    /**
	 * 用户设备绑定
	 * 返回数据格式：{"OpenUserId":1251,"Code":200,"Message":"成功"}
     */
    public function add($data)
    {
		// 开启事务
        Db::startTrans();
        try {
			//对对机
			if($data['brand']==10){
				if(strlen($data['uuid'])!= 16){
					$this->error = '设备编号应该为16位';
					return false;
				}
				//查询设备是否已经添加
				if($this->detail([
					'uuid' => $data['uuid'],
					'brand' => 10
				])){
					$this->error = '该设备已被添加，不可重复';
					return false;
				}
				//实例化
				$helper = new PrintHelper();
				$userBind = $helper->userBind($data['uuid'], $data['shop_id'], $data['printer_name']);
				if($userBind['Code']!=200){//绑定不成功
					$this->error = $userBind['Message'];
					return false;
				}	
				$data['open_user_id']=$userBind['OpenUserId'];
			}
			//飞鹅
			if($data['brand']==20){
				if(strlen($data['uuid'])!= 9){
					$this->error = '设备编号应该为9位';
					return false;
				}
				if(strlen($data['key'])!= 8){
					$this->error = '设备编号应该为8位';
					return false;
				}
				//查询设备是否已经添加
				if($this->detail([
					'uuid' => $data['uuid'],
					'brand' => 20
				])){
					$this->error = '该设备已被添加，不可重复';
					return false;
				}
				$printerContent = $data['uuid'] . '#' . $data['key'].'#'. $data['printer_name'];
				if(!empty($data['phone'])){
					$printerContent = $printerContent . '#' . $data['phone'];
				}
				$client = new Feie();
				if(!$client->printerAddlist($printerContent)){
					$this->error = '云端添加失败';
					return false;
				}
				$data['open_user_id']=$data['uuid'];
			}
			//易联云
			if($data['brand']==30){
				if(strlen($data['uuid'])!= 10){
					$this->error = '设备编号应该为10位';
					return false;
				}
				if(strlen($data['key'])!= 12){
					$this->error = '设备编号应该为12位';
					return false;
				}
				//查询设备是否已经添加
				if($this->detail([
					'uuid' => $data['uuid'],
					'brand' => 30
				])){
					$this->error = '该设备已被添加，不可重复';
					return false;
				}
				$client = new YiLian();
				if($err = $client->printerAdd($data)){
					$this->error = $err;
					return false;
				}
				$data['open_user_id']=$data['uuid'];
			}
			$data['wxapp_id'] = self::$wxapp_id;
			$this->save($data);
			Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }

    /**
     * 更新
     */
    public function edit($data)
    {
        return $this->save($data) !== false;
    }

    /**
     * 删除
     */
    public function remove() {
		// 开启事务
        Db::startTrans();
        try {
			if($this['brand']['value']==20){
				$client = new Feie();
				if(!$client->printerDelList($this['open_user_id'])){
					$this->error = '云端删除失败';
					return false;
				}
			}
			if($this['brand']['value']==30){
				$client = new YiLian();
				if($err = $client->printerDel($this['open_user_id'])){
					$this->error = $err;
					return false;
				}
			}
			$this->delete();
			Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }
}
