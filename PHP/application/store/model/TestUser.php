<?php
namespace app\store\model;
use app\common\model\TestUser as TestUserModel;

/**
 * 小程序体验者用户模型
 */
class TestUser extends TestUserModel
{
    /**
     * 添加
     */
    public function add($data)
    {
		$access_token = getAccessToken();		
		$url = 'https://api.weixin.qq.com/wxa/bind_tester?access_token='.$access_token;
		$post = ['wechatid' => $data['wechatid']];
		$result = json_decode(http_post($url,$post),true);
		if($result['errcode']!= 0){
			$this->error = wx_err_msg($result['errcode']);
            return false;
		}
		$data['userstr'] = $result['userstr'];
        $data['wxapp_id'] = self::$wxapp_id;
        return $this->allowField(true)->save($data);
    }

    /**
     * 删除
     */
    public function remove()
    {
		$access_token = getAccessToken();		
		$url = 'https://api.weixin.qq.com/wxa/unbind_tester?access_token='.$access_token;
		$post = ['userstr' => $this->userstr];
		$result = json_decode(http_post($url,$post),true);
		if($result['errcode']!= 0){
			$this->error = wx_err_msg($result['errcode']);
            return false;
		}
        return $this->delete();
    }
}
