<?php
namespace app\store\model;
use app\common\model\User as UserModel;
use app\store\model\Recharge as RechargeModel;
//use app\store\model\UserMsg as UserMsgModel;

/**
 * 用户模型
 */
class User extends UserModel
{
	/**
     * 用户充值
	 *$recharge 接收表单数据（数组）
	 *$source 0为充值余额，1为充值积分
    */
    public function recharge($user_id,$recharge,$source)
    {
		//$model = new UserMsgModel;
        if($source){
			//积分操作
			if(empty($recharge["points"]["value"])){
				$this->error = '积分变更数量不可为空';
				return false;
			}
			if(strpos($recharge["points"]["value"],".")){
				$this->error = '积分变更数量必须为整数';
				return false;
			}
			if(empty($recharge["points"]["shop_id"])){
				$this->error = '请选择门店';
				return false;
			}
			/*
			if(empty($recharge["points"]["remark"])){
				$this->error = '备注不可为空';
				return false;
			}*/
			//$msg['num'] = $recharge["points"]["value"];	//操作数值
			//$msg['category'] = 1;	//积分变更
			//$msg['content'] = $recharge["points"]["remark"]; 
			if($recharge["points"]["mode"] == "inc"){
				$this->score = ['inc',$recharge["points"]["value"]];//增加积分
				//$msg['reason'] = 1; 	//充值
			}
			if($recharge["points"]["mode"] == "dec"){
				$this->score = ['dec',$recharge["points"]["value"]];//扣减积分
				//$msg['reason'] = 2;	//扣减
			}
			if($recharge["points"]["mode"] == "final"){
				$this->score = $recharge["points"]["value"];//重置积分
				//$msg['reason'] = 3;	//重置
			}
			//$msg['user_id'] = $user_id;
			//$msg['wxapp_id'] = self::$wxapp_id;
			if(!$this->save()){
				$this->error = '积分操作失败';
				return false;
			}
			return true;
			//return $model->save($msg);
			
		}
		//充值余额
		if(empty($recharge["balance"]["money"])){
			$this->error = '变更金额不可为空';
			return false;
		}
		if(strpos($recharge["balance"]["money"],".")){
			$this->error = '变更金额必须为整数';
			return false;
		}
		if(empty($recharge["balance"]["shop_id"])){
			$this->error = '请选择门店';
			return false;
		}
		/*
		if(empty($recharge["balance"]["remark"])){
			$this->error = '备注不可为空';
			return false;
		}*/
		//$msg['num'] = $recharge["balance"]["money"];	//操作数值
		//$msg['category'] = 2;	//钱包变更
		//$msg['content'] = $recharge["balance"]["remark"]; 
		if($recharge["balance"]["mode"] == "inc"){
			$this->wallet = ['inc',$recharge["balance"]["money"]];//增加
			//增加充值记录
			$rec = new RechargeModel;
			$rec->save([
				'mode' => 20,
				'order_no' => orderNo(),
				'shop_id' => $recharge["balance"]["shop_id"],
				'money' => $recharge["balance"]["money"],
				'pay_status' => 20,
				'pay_time' => time(),
				'user_id' => $user_id,
				'wxapp_id' => self::$wxapp_id
			]);
			//$msg['reason'] = 1; 	//充值
		}
		if($recharge["balance"]["mode"] == "dec"){
			$this->wallet = ['dec',$recharge["balance"]["money"]];//扣减
			//$msg['reason'] = 2;	//扣减
		}
		if($recharge["balance"]["mode"] == "final"){
			$this->wallet = $recharge["balance"]["money"];//重置
			//$msg['reason'] = 3;	//重置
		}
		//$msg['user_id'] = $user_id;
		//$msg['wxapp_id'] = self::$wxapp_id;
		if(!$this->save()){
			$this->error = '金额操作失败';
			return false;
		}
		//return $model->save($msg);
		return true;
    }
}
