<?php
namespace app\store\model;
use app\common\model\FreeGoods as FreeGoodsModel;
use think\Db;

/**
 * 免单商品模型
 */
class FreeGoods extends FreeGoodsModel
{
    /**
     * 添加
     */
    public function add(array $data,$shop_id)
    {
		if (empty($data['goods_id'])) {
            $this->error = '请选择商品';
            return false;
        }
		$data['shop_id'] = $shop_id;
        $data['wxapp_id'] = self::$wxapp_id;
		return $this->allowField(true)->save($data);		
    }

    /**
     * 编辑
     */
    public function edit($data)
    {
        if (empty($data['goods_id'])) {
            $this->error = '请选择商品';
            return false;
        }
		return $this->allowField(true)->save($data) !== false;
    }

    /**
     * 删除
     */
    public function remove()
    {
        return $this->delete();  
    }
}
