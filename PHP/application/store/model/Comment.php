<?php
namespace app\store\model;
use app\common\model\Comment as CommentModel;

/**
 * 用户评论
 */
class Comment extends CommentModel
{
     /**
     * 编辑
     */
    public function edit($data)
    {
		return $this->allowField(true)->save($data) !== false;
    }
}
