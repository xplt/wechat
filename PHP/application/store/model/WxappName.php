<?php
namespace app\store\model;
use app\common\model\WxappName as WxappNameModel;
use think\Cache;

/**
 * 微信小程序名称模型
 */
class WxappName extends WxappNameModel
{
    /**
     * 添加
     */
    public function add($data)
    {
		if (!isset($data['license']) || empty($data['license'])) {
            $this->error = '请选择营业执照1';
            return false;
        }
		$result = $this->setnickname($data);
		if($result['errcode']!=0){
			$this->error = wx_err_msg($result['errcode']);
            return false;
		}
		$data['status'] = 1;//审核成功
		if(isset($result['audit_id']) AND !empty($result['audit_id'])){
			$data['status'] = 0;//需要审核
			$this->error = '设置成功，但需要审核';
			$data['audit_id'] = $result['audit_id'];
		}
		$wxapp = Wxapp::detail();
		$wxapp->app_name = $data['nick_name'];
		$wxapp->save();
        $data['wxapp_id'] = self::$wxapp_id;
		Cache::rm('wxapp_' . self::$wxapp_id);
        return $this->allowField(true)->save($data);
    }

    /**
     * 编辑
     */
    public function edit($data)
    {
        return $this->allowField(true)->save($data);
    }
	
	/**
     * 设置昵称
     */
    private function setnickname($set)
    {
		//上传临时素材
		$license = up_temp_material($set['license']);
		$other1 = '';
		if(isset($set['naming_other_stuff_1']) AND !empty($set['naming_other_stuff_1'])){
			$other1 = up_temp_material($set['naming_other_stuff_1']);
		}
        $access_token = getAccessToken();
		$url = 'https://api.weixin.qq.com/wxa/setnickname?access_token='.$access_token;
		$data = [
			'nick_name' => $set['nick_name'],
			'license' => $license,
			'naming_other_stuff_1' => $other1
		];
		return json_decode(http_post($url,$data),true);
    }
	
	/**
     * 检测名称
     */
    public function check($nick_name)
    {
        $access_token = getAccessToken();
		$url = 'https://api.weixin.qq.com/cgi-bin/wxverify/checkwxverifynickname?access_token='.$access_token;
		$data = ['nick_name' => $nick_name];
		$result = json_decode(http_post($url,$data),true);
		if($result['errcode']==0){
			 if($result['hit_condition']){
				$this->error ='命中关键字：'.$result['wording'];
				return false;
			 }
			$this->error = '可以使用';
            return false;
		}
		$this->error = wx_err_msg($result['errcode']);
        return false;
    }
}
