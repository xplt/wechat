<?php
namespace app\store\model;
use app\common\model\ShopClerk as ShopClerkModel;
use think\Session;

/**
 * 店员模型
 */
class ShopClerk extends ShopClerkModel
{

    /**
     * 店长登录
     */
    public function login($data)
    {
		$filter = [
            'mobile' => $data['user_name'],
            'pwd' => hema_hash($data['password']),
			'status' => 20
        ];
        // 验证用户名密码是否正确
		if($user = $this->useGlobalScope(false)->where($filter)->with(['shop'])->find()){
			$wxapp = Wxapp::detail();
			// 保存登录状态
			Session::set('hema_store', [
				'user' => [
					'store_user_id' => $wxapp['store_user_id'],
					'user_name' => $user['mobile'],
					'agent_id' => $wxapp['agent_id'],
                    'avatarUrl' => $wxapp['head_img'],
                    'shop_id' => $user['shop_id'],
				],
                'shop' => $user['shop']->toArray(),
				'wxapp' => $wxapp->toArray(),
				'is_login' => true,
				'is_admin' => false,
			]);
			return true;
		}else{
			$this->error = '登录失败, 用户名或密码错误';
            return false;
		}
    } 

    /**
     * 修改密码
     */
    public function renew($data)
    {
        //验证旧密码是否正确
        if($this->pwd !== hema_hash($data['password'])){
            $this->error = '旧密码输入错误';
            return false;
        }
        //验证密码长度是否合法
		if(strlen($data['password_new'])<6){
			$this->error = '新密码长度不足6位';
            return false;
		}
		if ($data['password_new'] !== $data['password_confirm']) {
            $this->error = '两次输入的新密码不一致';
            return false;
        }
        // 更新管理员信息
        return $this->save([
            'pwd' => hema_hash($data['password_new'])
        ]) !== false;
    }
}
