<?php
namespace app\store\model;
use app\common\model\Nearby as NearbyModel;

/**
 * 附近小程序模型
 */
class Nearby extends NearbyModel
{
	/**
     * 添加
     */
    public function add($data)
    {
    	$shop = Shop::detail($data['shop_id']);
    	if(!$shop){
    		$this->error = '门店数据获取失败';
			return false;
    	}
    	if(empty($shop['shop_name'])){
    		$this->error = '门店名称不可为空';
			return false;
    	}
    	if(empty($shop['shop_hours'])){
    		$this->error = '营业时间不可为空';
			return false;
    	}
    	if(empty($shop['address'])){
    		$this->error = '门店地址不可为空';
			return false;
    	}
    	if(empty($shop['poi_id'])){
    		$this->error = '地图poi_id不可为空';
			return false;
    	}
    	if(empty($shop['phone'])){
    		$this->error = '门店联系电话不可为空';
			return false;
    	}
		if (!isset($shop['front']['file_name']) || empty($shop['front']['file_name'])) {
			$this->error = '门头照片不可为空';
			return false;
		}
		if (!isset($data['qualification_list']) || empty($data['qualification_list'])) {
			$this->error = '请上传营业执照';
			return false;
		}
		//上传永久素材并获取访问链接
		$url = upWechatUrl($shop['front']['file_name']);
		if(!$url){
			$this->error = '门头照片上传到微信端错误';
			return false;
		}
		$qualification = up_temp_material($data['qualification_list']);
		if(!$qualification){
			$this->error = '营业执照上传到微信端错误';
			return false;
		}
		$data['shop_id'] = $shop['shop_id'];
		$data['qualification_list'] = $qualification;
		$data['url'] = $url;
		$data['wxapp_id'] = self::$wxapp_id;
		$wxapp = Wxapp::detail();
		//提交到微信端
		$access_token = getAccessToken();
		$url = 'https://api.weixin.qq.com/wxa/addnearbypoi?access_token='.$access_token;
		$post = [
			'is_comm_nearby' => '1',
			'kf_info' => [
				'open_kf' => false,
				'kf_headimg' => '',
				'kf_name' => ''
			],
			'pic_list' => [
				'list' => [$data['url']]
			],
			'service_infos' => [
				'service_infos' => [
					[
						'id' => 6,
						'type' => 1,
						'name' => '点餐',
						'appid' => $wxapp['app_id'],
						'path' => 'pages/index/index'
					],
					[
						'id' => 1,
						'type' => 1,
						'name' => '外送',
						'appid' => $wxapp['app_id'],
						'path' => 'pages/index/index'
					],
					[
						'id' => 10,
						'type' => 1,
						'name' => '买单',
						'appid' => $wxapp['app_id'],
						'path' => 'pages/index/index'
					]
				]
			],
			'store_name' => $shop['shop_name'],
			'contract_phone' => $shop['phone'],
			'hour' => $shop['shop_hours'],
			'company_name' => $data['company_name'],
			'credential' => $data['credential'],
			'address' => $shop['address'],
			'qualification_list' => $qualification,
			'poi_id' => $shop['poi_id']
		];
		$post['kf_info'] = json_encode($post['kf_info']);
		$post['pic_list'] = json_encode($post['pic_list']);
		$post['service_infos'] = json_encode($post['service_infos']);
		$result = json_decode(http_post($url,$post),true);
		if($result['errcode']!=0){
			//$this->error = wx_err_msg($result['errcode']);
			$this->error = $result['errcode'] . $result['errmsg'];
			return false;
		}
		$data['audit_id'] = $result['data']['audit_id'];
        return $this->allowField(true)->save($data);
    }

    /**
     * 删除
     */
    public function remove()
    {
		$access_token = getAccessToken();
		$url = 'https://api.weixin.qq.com/wxa/delnearbypoi?access_token='.$access_token;
		$data = ['poi_id' => $this['poi_id']];
		$result = json_decode(http_post($url,$data),true);
		if($result['errcode']!=0){
			$this->error = wx_err_msg($result['errcode']);
			return false;
		}
        return $this->delete();
    }
}
