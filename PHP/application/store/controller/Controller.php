<?php
namespace app\store\controller;
use think\Config;
use think\Session;
use think\Cookie;
use app\store\model\WebSet;
use app\store\model\Shop;

/**
 * 后台控制器基类
 */
class Controller extends \think\Controller
{
    protected $store;//商家登录信息
    protected $is_admin;    //是否为管理员 true是 false 不是
    protected $controller = '';//当前控制器名称
    protected $action = '';//当前方法名称
    protected $routeUri = '';//当前路由uri
    protected $group = '';//当前路由：分组名称
	protected $store_id = '';//商户id
	protected $wxapp_id = '';//app_id
	protected $agent_id = '';//代理id
    protected $shop_id = ''; //门店id
	protected $shop_mode;//门店模式 10=单门店 20=多门店
    protected $app_type;    //应用类型 food=餐饮 shop=商城

    /* 登录验证白名单 */
    protected $allowAllAction = [
        // 登录页面
        'shop.clerk/login',
    ];

    /* 无需全局layout */
    protected $notLayoutAction = [
        // 登录页面
        'shop.clerk/login',
    ];

    /**
     * 后台初始化
     */
    public function _initialize()
    {
        $this->getWxapp();//初始化应用
        $this->getRouteinfo();// 当前路由信息
        $this->checkLogin();// 验证登录
        $this->layout();// 全局layout
    }

    /**
     * 全局layout模板输出
     */
    private function layout()
    {
        // 验证当前请求是否在白名单
        if (!in_array($this->routeUri, $this->notLayoutAction)) {
            // 输出到view
            $this->assign([
                'base_url' => web_url(),            // 当前站点域名
				'web' => WebSet::getItem('web'),    //站点设置
                'store_url' => url('/store'),       // 后台模块url
                'group' => $this->group,
                'menus' => $this->menus(),          // 后台菜单
                'store' => $this->store,            // 商家登录信息
                'app_type' => $this->app_type,      //小程序类型
            ]);
        }
    }

    /**
     * 解析当前路由参数 （分组名称、控制器名称、方法名）
     */
    protected function getRouteinfo()
    {
        // 控制器名称
        $this->controller = toUnderScore($this->request->controller());
        // 方法名称
        $this->action = $this->request->action();
        // 控制器分组 (用于定义所属模块)
        $groupstr = strstr($this->controller, '.', true);
        $this->group = $groupstr !== false ? $groupstr : $this->controller;
        // 当前uri
        $this->routeUri = $this->controller . '/' . $this->action;
    }

    /**
     * 后台菜单配置
     */
    private function menus()
    {
        $this->is_admin ? $role = '' : $role = '_clerk';
		foreach ($data = Config::get($this->app_type . $role) as $group => $first) {
            $data[$group]['active'] = $group === $this->group;
			//$data[0]['active']
            // 遍历：二级菜单
            if (isset($first['submenu'])) {
                foreach ($first['submenu'] as $secondKey => $second) {
                    // 二级菜单所有uri
                    $secondUris = [];
                    if (isset($second['submenu'])) {
                        // 遍历：三级菜单
                        foreach ($second['submenu'] as $thirdKey => $third) {
                            $thirdUris = [];
                            if (isset($third['uris'])) {
                                $secondUris = array_merge($secondUris, $third['uris']);
                                $thirdUris = array_merge($thirdUris, $third['uris']);
                            } else {
                                $secondUris[] = $third['index'];
                                $thirdUris[] = $third['index'];
                            }
                            $data[$group]['submenu'][$secondKey]['submenu'][$thirdKey]['active'] = in_array($this->routeUri, $thirdUris);
							$data[$group]['submenu'][$secondKey]['active'] = in_array($this->routeUri, $secondUris);
                        }
                    } else {
                        if (isset($second['uris']))
                            $secondUris = array_merge($secondUris, $second['uris']);
                        else
                            $secondUris[] = $second['index'];
                    }
                    // 二级菜单：active
                    !isset($data[$group]['submenu'][$secondKey]['active'])
                    && $data[$group]['submenu'][$secondKey]['active'] = in_array($this->routeUri, $secondUris);	
                }
            }
        }
        return $data;
    }

    /**
     * 验证登录状态
     */
    private function checkLogin()
    {
        // 验证当前请求是否在白名单
        if (in_array($this->routeUri, $this->allowAllAction)) {
            return true;
        }
        // 验证登录状态
        if (empty($this->store)
            || (int)$this->store['is_login'] !== 1
            || !isset($this->store['wxapp'])
            || empty($this->store['wxapp'])
        ) {
			$this->redirect('/login.php');
            return false;
        }
        return true;
    }

    /**
     * 初始化应用数据
     */
    protected function getWxapp()
    {
        $this->store = Session::get('hema_store');// 商家登录信息
        $this->is_admin = $this->store['is_admin'];//是否为管理员
        $this->agent_id = $this->store['user']['agent_id'];//代理id
        $this->store_id = $this->store['user']['store_user_id'];//商户id
        if(!isset($this->store['wxapp']) AND (int)$this->store['is_admin'] === 1){
            Session::clear('hema_store');
            return false;
        }
        $this->app_type = $this->store['wxapp']['app_type']['value'];//应用类型
        $this->shop_mode = $this->store['wxapp']['shop_mode']['value'];//门店模式
        $this->wxapp_id = $this->store['wxapp']['wxapp_id'];//应用ID
        if(!$this->is_admin){
           $this->shop_id = $this->store['user']['shop_id']; 
           return true;
        }
        if($this->shop_mode == 10){
            $shop = Shop::detail();
            $this->shop_id = $shop['shop_id'];
        }
    }

    /**
     * 返回封装后的 API 数据到客户端
     */
    protected function renderJson($code = 1, $msg = '', $url = '', $data = [])
    {
        return compact('code', 'msg', 'url', 'data');
    }

    /**
     * 返回操作成功json
     */
    protected function renderSuccess($msg = 'success', $url = '', $data = [])
    {
        return $this->renderJson(1, $msg, $url, $data);
    }

    /**
     * 返回操作失败json
     */
    protected function renderError($msg = 'error', $url = '', $data = [])
    {
        return $this->renderJson(0, $msg, $url, $data);
    }

    /**
     * 获取post数据 (数组)
     */
    protected function postData($key)
    {
        return $this->request->post($key . '/a');
    }

}
