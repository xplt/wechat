<?php
namespace app\store\controller\user;
use app\store\controller\Controller;
use app\store\model\UserAddress as UserAddressModel;

/**
 * 用户收货地址控制器
 */
class Addres extends Controller
{
    /**
     * 用户充值列表
     */
    public function index($search='')
    {
        $model = new UserAddressModel;
        $list = $model->getList($search);
        return $this->fetch('index', compact('list','search'));
    }
}
