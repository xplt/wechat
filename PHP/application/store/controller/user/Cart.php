<?php
namespace app\store\controller\user;
use app\store\controller\Controller;
use think\Cache;
use app\store\model\User as UserModel;
use app\store\model\Shop as ShopModel;
use app\store\model\Goods as GoodsModel;

/**
 * 购物车控制器
 */
class Cart extends Controller
{
    /**
     * 购物车列表
     */
    public function index($search='')
    {
    	if($this->shop_mode == 10){
            $shop[0] = ShopModel::detail($this->shop_id);
        }else{
        	$shop = (new ShopModel)->select();
        }
        $user = (new UserModel)->where('nickName','not null')->select();
        $list = array();
        if(empty($search)){
        	for($n=0;$n<sizeof($user);$n++){
        		for($m=0;$m<sizeof($shop);$m++){
        			if($cart = Cache::get('cart_' . $user[$n]['user_id'] . '_' . $shop[$m]['shop_id'])){
        				//读取数据键名
        				$key = array_keys($cart);
        				//循环获取商品详情
        				$goods = array();
        				for($k=0;$k<sizeof($key);$k++){
        					if($detail = GoodsModel::detail($cart[$key[$k]]['goods_id'])){
        						$detail['goods_num'] = $cart[$key[$k]]['goods_num'];
        						array_push($goods,$detail);
        					}
        				}
        				$new['user'] = $user[$n];
        				$new['shop'] = $shop[$m];
        				$new['goods'] = $goods;
        				array_push($list,$new);
        			}	
        		}
        	}
        }else{
        	for($m=0;$m<sizeof($shop);$m++){
    			if($cart = Cache::get('cart_' . $search . '_' . $shop[$m]['shop_id'])){
    				//读取数据键名
    				$key = array_keys($cart);
    				//循环获取商品详情
    				$goods = array();
    				for($k=0;$k<sizeof($key);$k++){
    					if($detail = GoodsModel::detail($cart[$key[$k]]['goods_id'])){
    						$detail['goods_num'] = $cart[$key[$k]]['goods_num'];
    						array_push($goods,$detail);
    					}
    				}
    				$new['user'] = $user[$n];
    				$new['shop'] = $shop[$m];
    				$new['goods'] = $goods;
    				array_push($list,$new);
    			}	
    		}
        }
        return $this->fetch('index', compact('list','search'));
    }
}
