<?php
namespace app\store\controller\user;
use app\store\controller\Controller;
use app\store\model\Recharge as RechargeModel;

/**
 * 用户充值记录
 */
class Recharge extends Controller
{
    /**
     * 用户充值列表
     */
    public function index()
    {
        $model = new RechargeModel;
        $list = $model->getList();
        return $this->fetch('index', compact('list'));
    }
}
