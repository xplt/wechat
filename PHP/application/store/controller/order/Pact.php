<?php
namespace app\store\controller\order;
use app\store\controller\Controller;
use app\store\model\Pact as PactModel;

/**
 * 预约控制器
 */
class Pact extends Controller
{
	
    /**
     * 排号等座
     */
    public function sorts($shop_id = '')
    {
        if(!$this->is_admin OR $this->shop_mode == 10){
           $shop_id = $this->shop_id;
        }
        $model = new PactModel;
        $list = $model->getList(10,$shop_id);
        return $this->fetch('sorts', compact('list'));
    }
	
	/**
     * 预约订座
     */
    public function table($shop_id = '')
    {
        if(!$this->is_admin OR $this->shop_mode == 10){
           $shop_id = $this->shop_id;
        }
        $model = new PactModel;
        $list = $model->getList(20,$shop_id);
        return $this->fetch('table', compact('list'));
    }
	
	/**
     * 状态编辑
     */
    public function status($pact_id)
    {
        $model = PactModel::detail($pact_id);
		$model->status['value']==10 ? $model->status = 20 : $model->status = 10;
		$model->save();
        return $this->renderSuccess('更新成功');
    }
	
}
