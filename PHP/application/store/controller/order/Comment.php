<?php
namespace app\store\controller\order;
use app\store\controller\Controller;
use app\store\model\Comment as CommentModel;

/**
 * 评论制器
 */
class Comment extends Controller
{
	
    /**
     * 列表
     */
    public function index($shop_id='')
    {
        if(!$this->is_admin OR $this->shop_mode == 10){
           $shop_id = $this->shop_id;
        }
        $model = new CommentModel;
        $list = $model->getList($shop_id);
        return $this->fetch('index', compact('list'));
    }
	
	/**
     * 编辑
     */
    public function edit($comment_id)
    {
        $model = CommentModel::get($comment_id);
		if (!$this->request->isAjax()) {
            return $this->fetch('edit', compact('model'));
        }
        // 更新记录
        if ($model->edit($this->postData('cmt'))) {
            return $this->renderSuccess('更新成功', url('order.comment/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
	
	/**
     * 显示状态编辑
     */
    public function show($comment_id)
    {
        $model = CommentModel::get($comment_id);
		$model->is_show['value']==0 ? $model->is_show = 1 : $model->is_show = 0;
		$model->save();
        return $this->renderSuccess('操作成功');
    }
	
}
