<?php
namespace app\store\controller\market\free;
use app\store\controller\Controller;
use app\store\model\Shop as ShopModel;
use app\store\model\FreeGoods as FreeGoodsModel;

/**
 * 免费吃商品控制器
 */
class Goods extends Controller
{
	/**
	 * 门店选择
	 */
	public function opt()
	{
		if($this->shop_mode == 10){
			$shop = ShopModel::detail();
			$this->redirect('/'.url('market.free.goods/index',['shop_id' => $shop['shop_id']]));
		}
	    $model = new ShopModel;
	    $shoplist = $model->getList();
	    return $this->fetch('opt', compact('shoplist'));
	}
	
    /**
     * 列表
     */
    public function index($shop_id)
    {
		$shop = ShopModel::detail($shop_id);
		$shop_name = $shop['shop_name'];
        $model = new FreeGoodsModel;
        $list = $model->getList($shop_id);
        return $this->fetch('index', compact('list','shop_id','shop_name'));
    }

    /**
     * 添加
     */
    public function add($shop_id)
    {
        if (!$this->request->isAjax()) {
            return $this->fetch('add', compact('shop_id'));
        }
        $model = new FreeGoodsModel;
        if ($model->add($this->postData('goods'),$shop_id)) {
            return $this->renderSuccess('添加成功', url('market.free.goods/index',['shop_id'=>$shop_id]));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($free_goods_id)
    {
        $model = FreeGoodsModel::get($free_goods_id);
        if ($model->remove()) {
           return $this->renderSuccess('删除成功'); 
        }
		$error = $model->getError() ?: '删除失败';
        return $this->renderError($error);
        
    }

}
