<?php
namespace app\store\controller\market\group;
use app\store\controller\Controller;
use app\store\model\Shop as ShopModel;
use app\store\model\GoodsGroup as GoodsGroupModel;
use app\store\model\Goods as GoodsModel;

/**
 * 拼团商品控制器
 */
class Goods extends Controller
{
	/**
	 * 门店选择
	 */
	public function opt()
	{
		if($this->shop_mode == 10){
			$shop = ShopModel::detail();
			$this->redirect('/'.url('market.group.goods/index',['shop_id' => $shop['shop_id']]));
		}
	    $model = new ShopModel;
	    $shoplist = $model->getList();
	    return $this->fetch('opt', compact('shoplist'));
	}
	
    /**
     * 列表
     */
    public function index($shop_id)
    {
		$shop = ShopModel::detail($shop_id);
		$shop_name = $shop['shop_name'];
        $model = new GoodsGroupModel;
        $list = $model->getList($shop_id);
        return $this->fetch('index', compact('list','shop_id','shop_name'));
    }

    /**
     * 添加
     */
    public function add($step,$shop_id,$goods=[])
    {
        $specData = 'null';
        //第一步
        if ($step==1) {
            return $this->fetch('add', compact('shop_id','step', 'specData'));
        }
        $model = GoodsModel::detail($goods['goods_id']);
        if(!isset($goods['goods_id'])){
            return $this->renderError('请选择一件商品');
        }
        //第二步
        if ($step==2) {
            if (!$this->request->isPost()) {
                if ($model['spec_type'] == 20){
                    $specData = json_encode(getManySpecData($model['spec_rel'], $model['spec']));
                }
                return $this->fetch('add', compact('model', 'specData','shop_id','step'));
            }
            $model = new GoodsGroupModel;
            if ($model->add($this->postData('goods'),$shop_id)) {
               return $this->renderSuccess('添加成功', url('market.group.goods/index',['shop_id'=>$shop_id])); 
            }
            $error = $model->getError() ?: '添加失败';
            return $this->renderError($error);
        }
    }

   /**
     * 编辑
     */
    public function edit($goods_group_id,$shop_id)
    {
        // 详情
        $model = GoodsGroupModel::detail($goods_group_id);
        $model['goods'] = GoodsModel::detail($model['goods_id']);
        if (!$this->request->isAjax()) {
            // 多规格信息
            $specData = 'null';
            if ($model['goods']['spec_type'] == 20)
                $specData = json_encode(getManySpecData($model['goods']['spec_rel'], $model['goods']['spec']));
            return $this->fetch('edit', compact('model','specData','shop_id'));
        }
        // 更新记录
        if ($model->edit($this->postData('goods'))) {
            return $this->renderSuccess('更新成功', url('market.group.goods/index',['shop_id'=>$shop_id]));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($goods_group_id)
    {
        $model = GoodsGroupModel::get($goods_group_id);
        if ($model->remove()) {
           return $this->renderSuccess('删除成功'); 
        }
        $error = $model->getError() ?: '删除失败';
        return $this->renderError($error);
        
    }
    
    /**
     * 上架/下架
     */
    public function status($goods_group_id)
    {
        $model = GoodsGroupModel::get($goods_group_id);
        if($model->status()){
            return $this->renderSuccess('操作成功');
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

}
