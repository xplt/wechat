<?php
namespace app\store\controller\market;
use app\store\controller\Controller;
use app\store\model\Setting as SettingModel;
use app\store\model\Shop as ShopModel;

/**
 * 分销商配置控制器
 */
class Dealer extends Controller
{
    /**
     * 更新设置
     */
    public function setting()
    {
        if (!$this->request->isAjax()) {
            $values = SettingModel::getItem('dealer');
            return $this->fetch('setting', compact('values'));
        }
        $model = new SettingModel;
        if ($model->edit('dealer',$this->postData('dealer'))) {
            return $this->renderSuccess('更新成功');
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
	
	/**
     * 分销海报设置
     */
    public function poster()
    {
        if (!$this->request->isAjax()) {
			$values = SettingModel::getItem('poster');
			$shop = (new ShopModel)->find()->toArray();
			$values['shop']['file_name'] = $shop['logo']['file_name'];
			$values['shop']['logo'] = $shop['logo']['file_path'];
			if(empty($values['shop']['shop_name'])){
				$values['shop']['shop_name'] = $shop['shop_name'];
				$values['shop']['shop_id'] = $shop['shop_id'];
				$values['shop']['address'] = $shop['address'];
				$values['shop']['phone'] = $shop['phone'];
			}
            $values = json_encode($values);
            return $this->fetch('poster', compact('values'));
        }
        $model = new SettingModel;
        if ($model->edit('poster',$this->postData('poster'))) {
            return $this->renderSuccess('更新成功');
        }
		$error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

}
