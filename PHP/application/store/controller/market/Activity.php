<?php
namespace app\store\controller\market;
use app\store\controller\Controller;
use app\store\model\Activity as ActivityModel;
use app\store\model\Shop as ShopModel;

/**
 * 优惠活动控制器
 */
class Activity extends Controller
{
    /**
     * 列表
     */
    public function index($shop_id='')
    {
        if(!$this->is_admin OR $this->shop_mode == 10){
            $shop_id = $this->shop_id;
        }
        $model = new ShopModel;
        $catgory = $model->getList();
        $model = new ActivityModel;
        $list = $model->getList($shop_id);
        return $this->fetch('index', compact('list','catgory'));
    }

    /**
     * 添加
     */
    public function add()
    {
        $model = new ActivityModel;
        if (!$this->request->isAjax()) {
            $shop = new ShopModel;
            $shoplist = $shop->getList();
            return $this->fetch('add',compact('shoplist'));
        }
        // 新增记录
        $data = $this->postData('activity');
        if(!$this->is_admin OR $this->shop_mode == 10){
            $data['shop_id'] = $this->shop_id;
        }
        if ($model->add($data)) {
            return $this->renderSuccess('添加成功', url('market.activity/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($activity_id)
    {
        $model = ActivityModel::detail($activity_id);
        if (!$model->remove()) {
            return $this->renderError('删除失败');
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 编辑
     */
    public function edit($activity_id)
    {
        // 详情
        $model = ActivityModel::detail($activity_id);
        if (!$this->request->isAjax()) {
            $shop = new ShopModel;
            $shoplist = $shop->getList();
            return $this->fetch('edit', compact('model','shoplist'));
        }
        // 更新记录
        if ($model->edit($this->postData('activity'))) {
            return $this->renderSuccess('更新成功', url('market.activity/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

}
