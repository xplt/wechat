<?php
namespace app\store\controller\wxapp;
use app\store\controller\Controller;
use app\store\model\StoreApply as StoreApplyModel;
use app\store\model\StoreDetail as StoreDetailModel;
use app\store\model\StoreUser as StoreUserModel;
/**
 * 用户注册申请
 */
class Register extends Controller
{
    /**
     * 注册小程序
     */
    public function app()
    {	
		if(!$this->request->isAjax()) {
			$model = StoreDetailModel::getStoreDetail($this->store_id);
            return $this->fetch('app', compact('model'));
        }
        //提交动作
		$data = $this->postData('register');		
		if(!isset($data['details']['license_copy']) OR empty($data['details']['license_copy'])){
			return $this->renderError('请上传营业执照');  
		}else{
			copy('./uploads/'.$data['details']['license_copy'],'./uploads/apply/'.$data['details']['license_copy']);
		}
		if(!isset($data['details']['id_card_copy']) OR empty($data['details']['id_card_copy'])){
			return $this->renderError('请上传身份证（正面）');  
		}else{
			copy('./uploads/'.$data['details']['id_card_copy'],'./uploads/apply/'.$data['details']['id_card_copy']);
		}
		if(!isset($data['details']['id_card_national']) OR empty($data['details']['id_card_national'])){
			return $this->renderError('请上传身份证（反面）');  
		}else{
			copy('./uploads/'.$data['details']['id_card_national'],'./uploads/apply/'.$data['details']['id_card_national']);	
		}
		$data['store_user_id'] = $this->store_id;
		$data['apply_mode'] = 50;
		$data['wxapp_id'] = $this->wxapp_id;
		//增加
		$model = new StoreApplyModel;
        if ($model->add($data)) {
			return $this->renderSuccess('申请成功，等待审核', '/index.php?s=/user/apply/index');
        }
        $error = $model->getError() ?: '申请失败';
        return $this->renderError($error);      
    }

    /**
     * 注册支付商户号
     */
    public function pay()
    {	
		if(!$this->request->isAjax()) {
			$model = StoreDetailModel::getStoreDetail($this->store_id);
            return $this->fetch('pay', compact('model'));
        }
        //提交动作
		$data = $this->postData('register');		
		if(!isset($data['details']['license_copy']) OR empty($data['details']['license_copy'])){
			return $this->renderError('请上传营业执照');  
		}else{
			copy('./uploads/'.$data['details']['license_copy'],'./uploads/apply/'.$data['details']['license_copy']);
		}
		if(!isset($data['details']['id_card_copy']) OR empty($data['details']['id_card_copy'])){
			return $this->renderError('请上传身份证（正面）');  
		}else{
			copy('./uploads/'.$data['details']['id_card_copy'],'./uploads/apply/'.$data['details']['id_card_copy']);
		}
		if(!isset($data['details']['id_card_national']) OR empty($data['details']['id_card_national'])){
			return $this->renderError('请上传身份证（反面）');  
		}else{
			copy('./uploads/'.$data['details']['id_card_national'],'./uploads/apply/'.$data['details']['id_card_national']);	
		}
		if(!empty($data['details']['qualifications'])){
			copy('./uploads/'.$data['details']['qualifications'],'./uploads/apply/'.$data['details']['qualifications']);
		}
		$data['store_user_id'] = $this->store_id;
		$data['apply_mode'] = 20;
		$data['wxapp_id'] = $this->wxapp_id;
		$data['app_id'] = $this->store['wxapp']['app_id'];
		//增加
		$model = new StoreApplyModel;
        if ($model->add($data)) {
			return $this->renderSuccess('申请成功，等待审核', '/index.php?s=/user/apply/index');
        }
        $error = $model->getError() ?: '申请失败';
        return $this->renderError($error);      
    }

}
