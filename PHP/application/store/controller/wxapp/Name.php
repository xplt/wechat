<?php
namespace app\store\controller\wxapp;
use app\store\controller\Controller;
use app\store\model\WxappName as WxappNameModel;

/**
 * 设置小程序呢称
 */
class Name extends Controller
{
    /**
     * 设置
     */
    public function setting()
    {
        $model = WxappNameModel::detail();
        if (!$this->request->isAjax()) {
			if(!$model){
				$model['nick_name'] = '';
			}
            return $this->fetch('setting', compact('model'));
        }
		if($model){
			if ($model->edit($this->postData('name'))) {
				return $this->renderSuccess('更新成功', url('wxapp/setting'));
			}
			$error = $model->getError() ?: '更新失败';
			return $this->renderError($error);
		}
		$model = new WxappNameModel;
		if ($model->add($this->postData('name'))) {
			return $this->renderSuccess('更新成功', url('wxapp/setting'));
		}
		$error = $model->getError() ?: '更新失败';
		return $this->renderError($error);
    }
	
	/**
     * 名称检测
     */
    public function checkName($nick_name)
    {
		$model = new WxappNameModel;
		$model->check($nick_name);
		return $this->renderError($model->getError());
    }
}
