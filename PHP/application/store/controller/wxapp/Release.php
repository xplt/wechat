<?php
namespace app\store\controller\wxapp;
use app\store\controller\Controller;
use app\store\model\WxappTpl as WxappTplModel;
use app\store\model\Template as TemplateModel;
use app\store\model\Wxapp as WxappModel;
use app\store\model\WebWxappPlugins as WebWxappPluginsModel;
/**
 * 小程序模板代码发布管理
 */
class Release extends Controller
{
    /**
     * 类目列表
     */
    public function index()
    {
    	//获取当前小程序信息
		$wxapp = WxappModel::detail();
		//获取该小程序发布记录
		$model = new WxappTplModel;
        $list = $model->getList();
        //获取当前平台发布的最新小程序模板版本
		$new_tpl = TemplateModel::getNew($this->app_type);//获取最新版本
		if(!$new_tpl){
			$new_tpl['user_version'] = 0;
			$new_tpl['status'] = 2;//没有模板记录
		}else{
			$new_tpl['status'] = 1;//有模板记录
		}
		for($n=0;$n<sizeof($list);$n++){
			$list[$n]['desc'] = getAuditStatus($list[$n]['auditid']); //查询指定版本的审核状态
		}
		if(sizeof($list)>0 AND isset($new_tpl['template_id'])){
			
			if($list[0]['template']['template_id']<$new_tpl['template_id']){
				$new_tpl['status'] = 1;//有新版本，请上传
			}
			if($list[0]['template']['template_id']==$new_tpl['template_id']){
				if($list[0]['status']==1){
					//已经上传新版本，并且发布
					$new_tpl['status'] = 0;//没有新版本
				}else{
					//已经上传新版本，但未发布
					$new_tpl['status'] = 2;//没有新版本
					$new_tpl['desc'] = $list[0]['desc'];//审核信息
				}	
			}
		}
        return $this->fetch('index', compact('list','new_tpl'));
    }
	
	/**
     * 编辑
     */
    public function edit($wxapp_tpl_id)
    {
        $model = WxappTplModel::detail($wxapp_tpl_id); //获取模板详情
		if($model['status']==0){
			//未发布 - 查询模板状态
			$model['desc'] = getAuditStatus($model['auditid']);
		}else{
			//已发布
			$model['desc'] = ['status' => 9];//该参数为随意赋值，修复模板获取不到$model['desc']出现的错误
		}
        if (!$this->request->isAjax()) {
            return $this->fetch('edit', compact('model'));
        }
        $data = $this->postData('release');
		if($data['opt']==1){
			//发布小程序模板
			if($result = release()){
				return $this->renderError(wx_err_msg($result));
			}
			$model->edit(['status' => 1]);
			return $this->renderSuccess('发布成功', url('wxapp.release/index'));
		}
		if($data['opt']==2){
			//撤回审核
			if($result = $this->undocodeaudit()){
				return $this->renderError(wx_err_msg($result));
			}
			return $this->renderSuccess('撤回成功', url('wxapp.release/index'));
		}
        return $this->renderSuccess('正在返回', url('wxapp.release/index'));
        
    }

    /**
     * 添加与发布
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
        	//获取最新版本
			$new_tpl = TemplateModel::getNew($this->app_type);
            return $this->fetch('add', compact('new_tpl'));
        }
		$model = WxappModel::detail();//获取商户配置信息
		if($this->store['wxapp']['is_empower']['value']==0){
            return $this->renderError('请先绑定您的微信小程序', url('wxapp/setting'));
		}
		if($model->publish()){
			return $this->renderSuccess('上传成功，等待审核', url('wxapp.release/index'));
		}
		$error = $model->getError() ?: '上传失败';
        return $this->renderError($error);
    }
	
	/**
	* 撤回审核中的小程序
	* 单个帐号每天审核撤回次数最多不超过 1 次，一个月不超过 10 次。
	*/
	private function undocodeaudit()
	{
		$access_token = getAccessToken();
		$url = 'https://api.weixin.qq.com/wxa/undocodeaudit?access_token='.$access_token;
		$result = json_decode(curl($url),true);
		if($result['errcode'] == 0){
			return false;
		}
		return $result['errcode'];
	}
}
