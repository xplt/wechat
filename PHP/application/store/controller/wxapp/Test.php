<?php
namespace app\store\controller\wxapp;
use app\store\controller\Controller;
use app\store\model\TestUser as TestUserModel;

/**
 * 小程序体验者管理
 */
class Test extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
		$model = new TestUserModel;
		$list = $model->getList();
		return $this->fetch('index', compact('list'));	
    }

    /**
     * 删除
     */
    public function delete($test_user_id)
    {
        if($this->store['wxapp']['is_empower']['value']==0){
            return $this->renderError('请先绑定您的微信小程序', url('wxapp/setting'));
        }
        $model = TestUserModel::get($test_user_id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 添加
     */
    public function add()
    {
        $model = new TestUserModel;
        if (!$this->request->isAjax()) {
            return $this->fetch('add');
        }
        if($this->store['wxapp']['is_empower']['value']==0){
            return $this->renderError('请先绑定您的微信小程序', url('wxapp/setting'));
        }
        if ($model->add($this->postData('test'))) {
            return $this->renderSuccess('添加成功', url('wxapp.test/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }
}
