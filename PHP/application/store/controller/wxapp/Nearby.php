<?php
namespace app\store\controller\wxapp;
use app\store\controller\Controller;
use app\store\model\Nearby as NearbyModel;
use app\store\model\Shop as ShopModel;

/**
 * 附近的小程序管理
 */
class Nearby extends Controller
{
    /**
     * 类目列表
     */
    public function index()
    {
		$model = new NearbyModel;
        $list = $model->getList();
		return $this->fetch('index', compact('list'));	
    }

    /**
     * 删除
     */
    public function delete($nearby_id)
    {
        $model = NearbyModel::get($nearby_id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
			return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 添加
     */
    public function add()
    {

        if (!$this->request->isAjax()) {
            $model = new ShopModel;
            $shoplist = $model->getList();
            return $this->fetch('add', compact('shoplist'));
        }
		$model = new NearbyModel;
		if ($model->add($this->postData('nearby'))) {
            return $this->renderSuccess('添加成功', url('wxapp.nearby/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }
}
