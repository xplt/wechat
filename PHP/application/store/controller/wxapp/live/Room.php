<?php
namespace app\store\controller\wxapp\live;
use app\store\controller\Controller;
use app\store\model\LiveRoom as LiveRoomModel;
use app\store\model\Shop as ShopModel;

/**
 * 直播间管理控制器
 */
class Room extends Controller
{

    /**
     * 列表
     */
    public function index()
    {
        $model = new LiveRoomModel;
        $list = $model->getList();
        return $this->fetch('index', compact('list'));
    }

    /**
     * 添加
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
            return $this->fetch('add');
        }
        if($this->store['wxapp']['is_empower']['value']==0){
            return $this->renderError('请先绑定您的微信小程序', url('wxapp/setting'));
        }
        $model = new LiveRoomModel;
        if ($model->add($this->postData('room'))) {
            return $this->renderSuccess('创建成功', url('wxapp.live.room/index'));
        }
        $error = $model->getError() ?: '创建失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($live_room_id)
    {
        if($this->store['wxapp']['is_empower']['value']==0){
            return $this->renderError('请先绑定您的微信小程序', url('wxapp/setting'));
        }
        $model = LiveRoomModel::get($live_room_id);
        if (!$model->remove()) {
            return $this->renderError('删除失败');
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 编辑
     */
    public function edit($live_room_id)
    {
        // 详情
        $model = LiveRoomModel::detail($live_room_id);
        if (!$this->request->isAjax()) {
            return $this->fetch('edit', compact('model'));
        }
        if($this->store['wxapp']['is_empower']['value']==0){
            return $this->renderError('请先绑定您的微信小程序', url('wxapp/setting'));
        }
        // 更新记录
        if ($model->edit($this->postData('room'))) {
            return $this->renderSuccess('更新成功', url('wxapp.live.room/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
	
	/**
     * 一键同步微信端
     */
    public function refresh()
    {
        if($this->store['wxapp']['is_empower']['value']==0){
            return $this->renderError('请先绑定您的微信小程序', url('wxapp/setting'));
        }
        $model = new LiveRoomModel;
        // 更新记录
        if ($model->refresh()) {
            return $this->renderSuccess('更新成功', url('wxapp.live.room/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

}
