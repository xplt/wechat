<?php
namespace app\store\controller\wxapp;
use app\store\controller\Controller;
use app\store\model\WebWxappPlugins as WebWxappPluginsModel;
use app\store\model\Wxapp as WxappModel;

/**
 * 小程序插件管理
 */
class Plugins extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
		$model = new WebWxappPluginsModel;
		$list = $model->getList();
		$res = $this->getPlugins(['action' => 'list']);
		$status = ['未安装','申请中','申请通过','被拒绝','申请已超时'];
		for($n=0;$n<sizeof($list);$n++){
			$list[$n]['status'] = [
				'value' => 0,
				'text' => $status[0]
			];
			if($res['errcode']==0){
				for($m=0;$m<sizeof($res['plugin_list']);$m++){
					if($list[$n]['plugin_appid'] == $res['plugin_list'][$m]['appid']){
						$list[$n]['status'] = [
							'value' => $res['plugin_list'][$m]['status'],
							'text' => $status[$res['plugin_list'][$m]['status']]
						];
						if($list[$n]['key'] == 'live' AND $res['plugin_list'][$m]['status']==2){
							$wxapp = WxappModel::detail();
							$wxapp->edit(['is_live' => 1]);
						}
						break;
					}
				}
				for($m=0;$m<sizeof($res['apply_list']);$m++){
					if($list[$n]['plugin_appid'] == $res['apply_list'][$m]['appid']){
						$list[$n]['status'] = [
							'value' => $res['apply_list'][$m]['status'],
							'text' => $status[$res['apply_list'][$m]['status']]
						];
						break;
					}
				}
			}else{
				$list[$n]['status'] = [
					'value' => 0,
					'text' => '同步错误'
				];
			}
		}
        return $this->fetch('index', compact('list'));	
    }

    /**
     * 删除
     */
    public function delete($plugin_appid)
    {
    	if($this->store['wxapp']['is_empower']['value']==0){
            return $this->renderError('请先绑定您的微信小程序', url('wxapp/setting'));
        }
        $res = $this->getPlugins(['action' => 'unbind','plugin_appid' => $plugin_appid]);
		if($res['errcode']!=0){
			return $this->renderError(wx_err_msg($res['errcode']));
		}
		$plugin = WebWxappPluginsModel::getPlugins(['plugin_appid' => $plugin_appid]);
		if($plugin['key'] == 'live'){
			$wxapp = WxappModel::detail();
			$wxapp->edit(['is_live' => 0]);
		}
        return $this->renderSuccess('卸载成功', url('wxapp.plugins/index'));
    }

    /**
     * 添加
     */
    public function add($plugin_appid)
    {
    	if($this->store['wxapp']['is_empower']['value']==0){
            return $this->renderError('请先绑定您的微信小程序', url('wxapp/setting'));
        }
		$res = $this->getPlugins(['action' => 'apply','plugin_appid' => $plugin_appid]);
		if($res['errcode']!=0){
			return $this->renderError(wx_err_msg($res['errcode']));
		}
		$plugin = WebWxappPluginsModel::getPlugins(['plugin_appid' => $plugin_appid]);
		if($plugin['key'] == 'live'){
			$wxapp = WxappModel::detail();
			$wxapp->edit(['is_live' => 1]);
		}
        return $this->renderSuccess('安装成功', url('wxapp.plugins/index'));
    }
	
	/**
     * 获取微信端插件数据
     */
    private function getPlugins($data)
    {
		$access_token = getAccessToken();
		$url = 'https://api.weixin.qq.com/wxa/plugin?access_token='.$access_token;
		return json_decode(http_post($url,$data),true);
    }
}
