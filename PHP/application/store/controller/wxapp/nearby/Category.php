<?php
namespace app\store\controller\wxapp\nearby;
use app\store\controller\Controller;
use app\store\model\NearbyCategory as NearbyCategoryModel;

/**
 * 附近的小程序 类目管理
 */
class Category extends Controller
{
    /**
     * 类目列表
     */
    public function index()
    {
		$model = new NearbyCategoryModel;
        $list = $model->getList();
		return $this->fetch('index', compact('list'));	
    }

    /**
     * 删除
     */
    public function delete($nearby_category_id)
    {
        $model = NearbyCategoryModel::get($nearby_category_id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
			return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 添加
     */
    public function add()
    {
		$category = NearbyCategoryModel::defaultData();
        if (!$this->request->isAjax()) {
            return $this->fetch('add',compact('category'));
        }
		$model = new NearbyCategoryModel;
		if ($model->add($this->postData('category'))) {
            return $this->renderSuccess('添加成功', url('wxapp.nearby.category/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }
}
