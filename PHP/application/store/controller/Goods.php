<?php
namespace app\store\controller;
use app\store\model\Category;
use app\store\model\Goods as GoodsModel;
use app\store\model\Shop as ShopModel;

/**
 * 商品管理控制器
 */
class Goods extends Controller
{
	/**
	 * 门店选择
	 */
	public function opt()
	{
		if(!$this->is_admin OR $this->shop_mode == 10){
			$this->redirect('/'.url('goods/index',['shop_id' => $this->shop_id]));
		}
	    $model = new ShopModel;
	    $shoplist = $model->getList();
	    return $this->fetch('opt', compact('shoplist'));
	}
	
    /**
     * 商品列表
     */
    public function index($shop_id,$category_id='',$search='')
    {
		$shop = ShopModel::detail($shop_id);
		$shop_name = $shop['shop_name'];
        // 分类
        $category = Category::getCacheTree($shop_id);
        $model = new GoodsModel;
        $list = $model->getList($shop_id,'',$category_id,$search);
        return $this->fetch('index', compact('list','category','shop_id','category_id','shop_name'));
    }

    /**
     * 添加
     */
    public function add($shop_id)
    {
        if (!$this->request->isAjax()) {
            // 分类
            $category = Category::getCacheTree($shop_id);
            return $this->fetch('add', compact('category','shop_id'));
        }
        $model = new GoodsModel;
        if ($model->add($this->postData('goods'),$shop_id)) {
            return $this->renderSuccess('添加成功', url('goods/index',['shop_id'=>$shop_id]));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 删除商品
     */
    public function delete($goods_id)
    {
        $model = GoodsModel::get($goods_id);
        if ($model->remove()) {
           return $this->renderSuccess('删除成功'); 
        }
		$error = $model->getError() ?: '删除失败';
        return $this->renderError($error);
        
    }
	
	
	/**
     * 上架/下架
     */
	public function status($goods_id)
	{
		$model = GoodsModel::get($goods_id);
		if($model->status()){
			return $this->renderSuccess('操作成功');
		}
		$error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
	}

    /**
     * 编辑
     */
    public function edit($goods_id,$shop_id)
    {
        // 菜品详情
        $model = GoodsModel::detail($goods_id);
        if (!$this->request->isAjax()) {
            // 菜品分类
            $category = Category::getCacheTree($shop_id);
            // 多规格信息
            $specData = 'null';
            if ($model['spec_type'] == 20)
                $specData = json_encode(getManySpecData($model['spec_rel'], $model['spec']));
            return $this->fetch('edit', compact('model', 'category', 'specData','shop_id'));
        }
        // 更新记录
        if ($model->edit($this->postData('goods'))) {
            return $this->renderSuccess('更新成功', url('goods/index',['shop_id'=>$shop_id]));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
	
	/**
     * 一键复制
     */
    public function copys($goods_id)
    {
        // 菜品详情
        $model = GoodsModel::detail($goods_id);
		$shop_id = $model['shop_id'];
        if (!$this->request->isAjax()) {
			$shop = $this->category($shop_id);
			$shoplist = $shop['list'];
			$category = $shop['category'];
            // 多规格信息
            $specData = 'null';
            if ($model['spec_type'] == 20)
                $specData = json_encode(getManySpecData($model['spec_rel'], $model['spec']));
            return $this->fetch('copys', compact('model', 'category', 'shoplist', 'specData'));
        }
		$model = new GoodsModel;
		$data = $this->postData('goods');
        // 更新记录
        if ($model->add($data,$data['shop_id'])) {
            return $this->renderSuccess('复制成功', url('goods/index',['shop_id'=>$shop_id]));
        }
        $error = $model->getError() ?: '复制失败';
        return $this->renderError($error);
    }
	
	/**
     * 一键生成产品缩略图 - 所有
     */
    public function thumbnail()
    {
        $model = new GoodsModel;
        if (!$model->thumbnail()) {
            $this->error('失败', 'goods/index');
            return false;
        }
        $this->error('成功', 'goods/index');
        return true;
    }
	
	/**
     * 获取门店列表和分类列表
     */
	private function category($shop_id)
	{
		$shop = new ShopModel;
		$shoplist = $shop->getList();
		$category = [];
		$list = [];
		for($n=0;$n<sizeof($shoplist);$n++){
			if($shoplist[$n]['shop_id'] != $shop_id){
				array_push($list,$shoplist[$n]);
				$category[$shoplist[$n]['shop_id']] = Category::getCacheTree($shoplist[$n]['shop_id']);
			}
		}
		return compact('category','list');
	}
	
}
