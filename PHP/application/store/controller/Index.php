<?php
namespace app\store\controller;
use app\store\model\Order as OrderModel;
use app\store\model\Comment as CommentModel;
use app\store\model\Recharge as RechargeModel;
use app\store\model\Goods as GoodsModel;
use app\store\model\User as UserModel;
use app\store\model\Shop as ShopModel;
use app\store\model\Pact as PactModel;


/**
 * 商户后台首页
 */
class Index extends Controller
{
	
    public function index($shop_id=0)
    {
    	if(!$this->is_admin OR $this->shop_mode == 10){
           $shop_id = $this->shop_id;
        }
		$model = new ShopModel;
	    $shop = $model->getList();
		$count = array();
		$count['order'] = OrderModel::getCount($shop_id);	//订单和收入统计
		$count['comment'] = CommentModel::getCount($shop_id);	//评价统计
		$count['recharge'] = RechargeModel::getCount($shop_id);	//充值统计
		$count['user'] = UserModel::getCount();	//用户统计
		$count['goods'] = GoodsModel::getCount($shop_id);	//产品统计
		$count['pact'] = PactModel::getCount($shop_id);	//预约统计
		return $this->fetch('index', compact('count','shop','shop_id'));
    }
}
