<?php
namespace app\store\controller;
use app\store\model\User as UserModel;
use app\store\model\Shop as ShopModel;
use app\store\model\UserGrade as UserGradeModel;

/**
 * 用户管理
 */
class User extends Controller
{
	
	/**
     * 用户列表
     */
    public function index($user_grade_id = '', $gender = '', $search='')
    {
        $shop = ShopModel::getAll();
        $model = new UserModel;
        $list = $model->getList($user_grade_id, $gender, $search);
		$grade = UserGradeModel::getAll();
        return $this->fetch('index', compact('list','grade','shop','user_grade_id','gender','search'));
    }
	
	/**
     * 用户充值
	 *$recharge 接收表单数据（数组）
	 *$source 0为充值余额，1为充值积分
    */
    public function recharge($user_id,$recharge,$source)
    {
        if(!$this->is_admin OR $this->shop_mode == 10){
            $recharge['points']['shop_id'] = $this->shop_id;
            $recharge['balance']['shop_id'] = $this->shop_id;
        }
        $model = UserModel::get($user_id);
		if($model->recharge($user_id,$recharge,$source)){
			 return $this->renderSuccess('操作成功', url('user/index'));
		}
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

}
