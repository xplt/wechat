<?php
namespace app\store\controller;
use app\store\model\Shop as ShopModel;
use app\store\model\WebSet;
use app\store\model\ShopCategory as ShopCategoryModel;

/**
 * 门店控制器
 */
class Shop extends Controller
{
	/**
     * 门店地图
     */
    public function getpoint()
    {
		$values = WebSet::getItem('web');
		$this->view->engine->layout(false);
		$this->assign('wxmap_key', $values['wxmap']);
        return $this->fetch('getpoint');
    }
	
    /**
     * 列表
     */
    public function index()
    {
        $model = new ShopModel;
        $list = $model->getList();
        return $this->fetch('index', compact('list'));
    }

    /**
     * 添加
     */
    public function add()
    {
		if($this->shop_mode == 10){
			return $this->error('单门店版，无权新增门店');
		}
        if (!$this->request->isAjax()) {
            $catgory = ShopCategoryModel::getCacheAll();
            return $this->fetch('add', compact('catgory'));
        }
		//全局判断
		if($err = checking(1)){
			return $this->renderError($err);
		}
        $model = new ShopModel;
        if ($model->add($this->postData('shop'))) {
            return $this->renderSuccess('添加成功', url('shop/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($shop_id)
    {
		//全局判断
		if($err = checking(1)){
			return $this->renderError($err);
		}
        $model = new ShopModel;
        if ($model->deleteBatch(0,$shop_id)) {
			return $this->renderSuccess('删除成功');
        }
		$error = $model->getError() ?: '删除失败';
		return $this->renderError($error);
        
    }

    /**
     * 编辑
     */
    public function edit($shop_id='')
    {
        $url = 'shop/index';
        if(empty($shop_id) AND !$this->is_admin){
            $shop_id = $this->shop_id;
            $url = 'shop/edit';
        }
        // 门店详情
        $model = ShopModel::detail($shop_id);
        if (!$this->request->isAjax()) {
            $catgory = ShopCategoryModel::getCacheAll();
            return $this->fetch('edit', compact('model','catgory'));
        }
        // 更新记录
        if ($model->edit($this->postData('shop'))) {
            return $this->renderSuccess('更新成功', url($url));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
	
	/**
     * 状态编辑
     */
    public function status($shop_id)
    {
        $model = ShopModel::get($shop_id);
		$model->status['value'] ? $model->status = 0 : $model->status = 1;
		$model->save();
        return $this->renderSuccess('更新成功', url('shop/index'));
    }	
}
