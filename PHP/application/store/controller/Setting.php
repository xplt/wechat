<?php
namespace app\store\controller;
use app\common\library\sms\Driver as SmsDriver;
use app\store\model\Setting as SettingModel;

/**
 * 系统设置
 */
class Setting extends Controller
{
    /**
     * 站点设置
     */
    public function store()
    {
        return $this->updateEvent('store');
    }
	
	/**
     * 功能设置
     */
    public function mode()
    {
        return $this->updateEvent('mode');
    }

    /**
     * 交易设置
     */
    public function trade()
    {
        return $this->updateEvent('trade');
    }
	
	/**
     * 配送设置
     */
    public function delivery()
    {
        return $this->updateEvent('delivery');
    }

    /**
     * 短信通知
     */
    public function sms()
    {
        return $this->updateEvent('sms');
    }

    /**
     * 支付设置
     */
    public function payment()
    {
        return $this->updateEvent('payment');
    }

    /**
     * 微信小程序订阅消息设置
     */
    public function wxapptpl()
    {
        return $this->updateEvent('wxapptpl');
    }

    /**
     * 微信公众号模板消息设置
     */
    public function wechattpl()
    {
        return $this->updateEvent('wechattpl');
    }

    /**
     * 发送短信通知测试
     */
    public function smsTest($AccessKeyId, $AccessKeySecret, $sign, $msg_type, $template_code, $accept_phone)
    {
        $SmsDriver = new SmsDriver([
            'default' => 'aliyun',
            'engine' => [
                'aliyun' => [
                    'AccessKeyId' => $AccessKeyId,
                    'AccessKeySecret' => $AccessKeySecret,
                    'sign' => $sign,
                    $msg_type => compact('template_code', 'accept_phone'),
                ],
            ],
        ]);
        $templateParams = [];
        if ($msg_type === 'order_pay') {
            $templateParams = ['order_no' => '2018071200000000'];
        }
        if ($SmsDriver->sendSms($msg_type, $templateParams, true)) {
            return $this->renderSuccess('发送成功');
        }
        return $this->renderError('发送失败 ' . $SmsDriver->getError());
    }

    /**
     * 上传设置
     */
    public function storage()
    {
        return $this->updateEvent('storage');
    }

    /**
     * 更新设置事件
     */
    private function updateEvent($key)
    {
        if (!$this->request->isAjax()) {
			$values = SettingModel::getItem($key);
            return $this->fetch($key, compact('values'));
        }
        if($this->store['wxapp']['is_empower']['value']==0 AND $key=='wxapptpl'){
            return $this->renderError('请先绑定您的微信小程序');
        }
        $model = new SettingModel;
        if($model->edit($key, $this->postData($key))) {
            return $this->renderSuccess('更新成功');
        }
        return $this->renderError('更新失败');
    }

}
