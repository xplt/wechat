<?php
namespace app\store\controller\data;
use app\store\controller\Controller;
use app\store\model\Shop as ShopModel;

/**
 * 门店管理控制器
 */
class Shop extends Controller
{ 
	public function lists(){
		$model = new ShopModel;
        $list = $model->getList();
		for($n=0;$n<sizeof($list);$n++){
			$params = [
				"shop_id" => $list[$n]['shop_id'],
				"logo_image" => $list[$n]['logo']['file_path'],
				"shop_name" => $list[$n]['shop_name'],
				"address" => $list[$n]['address'],
				"phone" => $list[$n]['phone']
			];
			$list[$n]['params'] = json_encode($params);
		}
		$this->view->engine->layout(false);
		return $this->fetch('lists', compact('list'));
	}
	
}
