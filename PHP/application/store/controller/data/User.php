<?php
namespace app\store\controller\data;
use app\store\controller\Controller;
use app\store\model\User as UserModel;

/**
 * 用户管理控制器
 */
class User extends Controller
{ 
	//列表
	public function lists($user_grade_id='', $gender='', $search=''){
		$model = new UserModel;
        $list = $model->getList($user_grade_id, $gender, $search);
		for($n=0;$n<sizeof($list);$n++){
			$params = [
				"user_id" => $list[$n]['user_id'],
				"nickName" => $list[$n]['nickName'],
				"avatarUrl" => $list[$n]['avatarUrl']
			];
			$list[$n]['params'] = json_encode($params);
		}
		$this->view->engine->layout(false);
		return $this->fetch('lists', compact('list'));
	}
	
}
