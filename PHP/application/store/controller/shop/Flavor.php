<?php
namespace app\store\controller\shop;
use app\store\controller\Controller;
use app\store\model\Flavor as FlavorModel;

/**
 * 口味选项控制器
 */
class Flavor extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
        $model = new FlavorModel;
        $list = $model->getList();
        return $this->fetch('index', compact('list'));
    }

    /**
     * 添加
     */
    public function add()
    {
        $model = new FlavorModel;
        if (!$this->request->isAjax()) {
            return $this->fetch('add');
        }
        // 新增记录
        if ($model->add($this->postData('flavor'))) {
            return $this->renderSuccess('添加成功', url('shop.flavor/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 更新
     */
    public function edit($flavor_id)
    {
        // 详情
        $model = FlavorModel::detail($flavor_id);
        if (!$this->request->isAjax()) {
            return $this->fetch('edit', compact('model'));
        }
        // 更新记录
        if ($model->edit($this->postData('flavor'))) {
            return $this->renderSuccess('更新成功', url('shop.flavor/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($flavor_id)
    {
        // 详情
        $model = FlavorModel::detail($flavor_id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

}
