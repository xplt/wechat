<?php
namespace app\store\controller\shop\article;
use app\store\controller\Controller;
use app\store\model\ArticleCategory as ArticleCategoryModel;
use app\store\model\Shop as ShopModel;

/**
 * 图文资讯分类
 */
class Category extends Controller
{
    /**
     * 分类列表
     */
    public function index()
    {
        $model = new ArticleCategoryModel;
        $list = $model->getList();
        return $this->fetch('index', compact('list'));
    }

    /**
     * 删除分类
     */
    public function delete($article_category_id)
    {
        $model = ArticleCategoryModel::detail($article_category_id);
        if (!$model->remove($article_category_id)) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 添加分类
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
            return $this->fetch('add');
        }
        $model = new ArticleCategoryModel;
        // 新增记录
        if ($model->add($this->postData('category'))) {
            return $this->renderSuccess('添加成功', url('shop.article.category/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 编辑
     */
    public function edit($article_category_id)
    {
        // 模板详情
        $model = ArticleCategoryModel::detail($article_category_id);
        if (!$this->request->isAjax()) {
            return $this->fetch('edit', compact('model'));
        }
        // 更新记录
        if ($model->edit($this->postData('category'))) {
            return $this->renderSuccess('更新成功', url('shop.article.category/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

}
