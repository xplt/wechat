<?php
namespace app\store\controller\shop;
use app\store\controller\Controller;
use app\store\model\Printer as PrinterModel;
use app\store\model\Shop as ShopModel;

/**
 * 云打印机控制器
 */
class Printer extends Controller
{
    /**
     * 获取云打印机列表
     */
    public function index($shop_id='')
    {
        if(!$this->is_admin OR $this->shop_mode == 10){
            $shop_id = $this->shop_id;
        }
        $model = new PrinterModel;
        $list = $model->getList($shop_id);
        return $this->fetch('index', compact('list'));
    }

    /**
     * 添加云打印机
     */
    public function add()
    {
        $model = new PrinterModel;
        if (!$this->request->isAjax()) {
			$shop = new ShopModel;
			$shoplist = $shop->getList();
            return $this->fetch('add', compact('shoplist'));
        }
        // 新增记录
        $data = $this->postData('printer');
        if(!$this->is_admin OR $this->shop_mode == 10){
            $data['shop_id'] = $this->shop_id;
        }
        if ($model->add($data)) {
            return $this->renderSuccess('添加成功', url('shop.printer/index'));
        }
		$error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 更新云打印机
     */
    public function edit($printer_id)
    {
        $model = PrinterModel::detail($printer_id);
        if (!$this->request->isAjax()) {
			$shop = new ShopModel;
			$shoplist = $shop->getList();
            return $this->fetch('edit', compact('model','shoplist'));
        }
		$printer = $this->postData('printer');
        // 更新记录
        if ($model->edit($printer)) {
            return $this->renderSuccess('更新成功', url('shop.printer/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

    /**
     * 删除云打印机
     */
    public function delete($printer_id)
    {
        $model = PrinterModel::detail($printer_id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 开启/关闭
     */
    public function open($printer_id)
    {
        $model = PrinterModel::detail($printer_id);
        $model->is_open = $model['is_open']['value']?0:1;
        if($model->save()){
            return $this->renderSuccess('操作成功');
        }
        return $this->renderError('操作失败');
    }

}
