<?php
namespace app\store\controller\shop;
use app\store\controller\Controller;
use app\store\model\Shop as ShopModel;
use app\store\model\ShopClerk as ShopClerkModel;
use app\store\model\WebSet;

/**
 * 店员管理控制器
 */
class Clerk extends Controller
{
    /**
     * 列表
     */
    public function index($shop_id='',$search='')
    {
        if(!$this->is_admin OR $this->shop_mode == 10){
            $shop_id = $this->shop_id;
        }
		$model = new ShopModel;
        $catgory = $model->getList();
        $model = new ShopClerkModel;
        $list = $model->getList($shop_id,$search);
        return $this->fetch('index', compact('list','catgory'));
    }

    /**
     * 添加
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
            $model = new ShopModel;
			$catgory = $model->getList();
            return $this->fetch('add', compact('catgory'));
        }
        $model = new ShopClerkModel;
        $data = $this->postData('clerk');
        if(!$this->is_admin OR $this->shop_mode == 10){
            $data['shop_id'] = $this->shop_id;
        }
        if ($model->add($data)) {
            return $this->renderSuccess('添加成功', url('shop.clerk/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($shop_clerk_id)
    {
        $model = ShopClerkModel::get($shop_clerk_id);
        if (!$model->remove($shop_clerk_id)) {
            return $this->renderError('删除失败');
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 编辑
     */
    public function edit($shop_clerk_id)
    {
        // 详情
        $model = ShopClerkModel::detail($shop_clerk_id);
        if (!$this->request->isAjax()) {
            $shop = new ShopModel;
			$catgory = $shop->getList();
            return $this->fetch('edit', compact('model', 'catgory'));
        }
        // 更新记录
        if ($model->edit($this->postData('clerk'))) {
            return $this->renderSuccess('更新成功', url('shop.clerk/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

    /**
     * 店长登录
     */
    public function login()
    {
         if (!$this->request->isAjax()) {
            // 验证登录状态
            if (isset($this->store) AND (int)$this->store['is_login'] === 1 AND (int)$this->store['is_admin'] === 0) {
                $this->redirect('index/index');
            }
            $this->view->engine->layout(false);
            $this->assign('web', WebSet::getItem('web'));
            return $this->fetch('login');
        }
        $model = new ShopClerkModel;
        if ($model->login($this->postData('User'))) {
            return $this->renderSuccess('登录成功', url('index/index'));
        }
        $error = $model->getError() ?: '登录失败';
        return $this->renderError($error);
    }

    /**
     * 修改管理员密码
     */
    public function renew()
    {
        if ($this->request->isAjax()) {
            $model = ShopClerkModel::getClerk([
                'mobile' => $this->store['user']['user_name']
            ]);
            if ($model->renew($this->postData('user'))) {
                return $this->renderSuccess('修改成功');
            }
            return $this->renderError($model->getError() ?: '修改失败');
        }
        return $this->fetch('renew');
    }

}
