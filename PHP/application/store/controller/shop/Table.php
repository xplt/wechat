<?php
namespace app\store\controller\shop;
use app\store\controller\Controller;
use app\store\model\Table as TableModel;
use app\store\model\Shop as ShopModel;

/**
 * 餐桌/包间控制器
 */
class Table extends Controller
{
    /**
     * 列表
     */
    public function index($shop_id='',$search='')
    {
        if(!$this->is_admin OR $this->shop_mode == 10){
            $shop_id = $this->shop_id;
        }
        $model = new ShopModel;
        $catgory = $model->getList();
        $model = new TableModel;
        $list = $model->getList('',$shop_id,$search);
        return $this->fetch('index', compact('list','catgory'));
    }

    /**
     * 添加
     */
    public function add()
    {
        $model = new TableModel;
        if (!$this->request->isAjax()) {
			$shop = new ShopModel;
			$shoplist = $shop->getList();
            return $this->fetch('add',compact('shoplist'));
        }
        // 新增记录
        $data = $this->postData('table');
        if(!$this->is_admin OR $this->shop_mode == 10){
            $data['shop_id'] = $this->shop_id;
        }
        if ($model->add($data)) {
            return $this->renderSuccess('添加成功', url('shop.table/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 更新
     */
    public function edit($table_id)
    {
        // 详情
        $model = TableModel::detail($table_id);
        if (!$this->request->isAjax()) {
			$shop = new ShopModel;
			$shoplist = $shop->getList();
            return $this->fetch('edit', compact('model','shoplist'));
        }
        // 更新记录
        if ($model->edit($this->postData('table'))) {
            return $this->renderSuccess('更新成功', url('shop.table/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($table_id)
    {
        // 详情
        $model = TableModel::detail($table_id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

}
