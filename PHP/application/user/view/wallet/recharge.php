<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
				<div class="widget-body">
					<div class="widget-head am-cf">
						<div class="widget-title am-fl">微信扫码支付</div>
					</div>
					<div class="tips am-margin-bottom-sm am-u-sm-12">
                        <div class="pre">
                            <p>
								本次充值 ￥<?= $money?>元，请用微信扫描下方二维码完成支付
							</p>
                        </div>
                    </div>
					<div class="am-form-group">
						<label class="am-u-sm-3"></label>
						<div class="am-u-sm-9">
							<div id="qrcode"></div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
<script src="/assets/plugins/qrcode/qrcode.min.js"></script>
<script type="text/javascript">
	new QRCode(document.getElementById("qrcode"), "<?= $code_url?>"); 
</script>