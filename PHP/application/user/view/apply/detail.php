<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" enctype="multipart/form-data" method="post">
                    <div class="widget-body">
                        <div class="tips am-margin-bottom-sm am-u-sm-12">
                            <div class="pre">
                                <p> 
                                    <?= $model['apply_status']['value']==10?'提示：资料审核中，有问题会给您电话联系':''?>
                                    <?= $model['apply_status']['value']==20?'提示：资料验证中，请在您微信中完成实名验证':''?>
                                    <?= $model['apply_status']['value']==30?'提示：申请已通过,您已注册成功':''?>
                                    <?= $model['apply_status']['value']==40?'提示：申请被驳回，请按要求修改后提交':''?>
                                </p>
                                <?PHP if($model['apply_status']['value']==40):?>
                                <p><?= $model['reject']?></p>
                                <?PHP endif; ?>
                            </div>
                        </div>
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl"><?= $model['apply_mode']['text'] ?></div>
                            </div>
                            <?php if($model['apply_mode']['value'] == 10 OR $model['apply_mode']['value'] == 20 OR $model['apply_mode']['value'] == 50):?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label"> <?= $model['app_mode']['text'] ?>小程序ID </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $model['wxapp_id'] ?>" disabled>
                                </div>
                            </div>
                            <?php endif;?>
                            <?php if($model['apply_mode']['value'] == 20):?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label"> <?= $model['pay_mode']['text'] ?>支付商户号 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $model['mchid'] ?>" disabled>
                                </div>
                            </div>
                            <?php endif;?>
                            <?php if($model['apply_mode']['value'] <> 30):?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">营业执照 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <button type="button" class="license-copy am-btn am-btn-secondary am-radius am-btn-sm" 
                                        <?= $model['apply_status']['value']==40?'':'disabled' ?>>
                                            <i class="am-icon-cloud-upload"></i> 选择图片
                                        </button>
                                        <div class="uploader-list am-cf">
                                            <?php if ($model['details']['license_copy']['url']): ?>
                                                <div class="file-item">
                                                    <a href="<?= $model['details']['license_copy']['url'] ?>" title="点击查看大图" target="_blank">
                                                        <img src="<?= $model['details']['license_copy']['url'] ?>">
                                                    </a>
                                                    <input type="hidden" name="register[details][license_copy]"
                                                           value="<?= $model['details']['license_copy']['value'] ?>">
                                                    <i class="iconfont iconshanchu file-item-delete"></i>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="help-block am-margin-top-sm">
                                        <small>大小2M以下</small>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">名称 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" 
                                    name="register[details][merchant_name]" value="<?= $model['details']['merchant_name']?>" 
                                    <?= $model['apply_status']['value']==40?'':'disabled' ?> required>
                                    <small>营业执照上面的名称</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">统一社会信用代码 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" 
                                    name="register[details][license_number]" value="<?= $model['details']['license_number']?>" 
                                    <?= $model['apply_status']['value']==40?'':'disabled' ?> required>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">身份证（正面） </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <button type="button" class="id-card-copy am-btn am-btn-secondary am-radius am-btn-sm" 
                                        <?= $model['apply_status']['value']==40?'':'disabled' ?>>
                                            <i class="am-icon-cloud-upload"></i> 选择图片
                                        </button>
                                        <div class="uploader-list am-cf">
                                            <?php if ($model['details']['id_card_copy']['url']): ?>
                                                <div class="file-item">
                                                    <a href="<?= $model['details']['id_card_copy']['url'] ?>" title="点击查看大图" target="_blank">
                                                        <img src="<?= $model['details']['id_card_copy']['url'] ?>">
                                                    </a>
                                                    <input type="hidden" name="register[details][id_card_copy]"
                                                           value="<?= $model['details']['id_card_copy']['value'] ?>">
                                                    <i class="iconfont iconshanchu file-item-delete"></i>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="help-block am-margin-top-sm">
                                        <small>身份证所有人必须跟营业执照法人（所有人）姓名一致。大小2M以下</small>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">身份证（反面） </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <button type="button" class="id-card-national am-btn am-btn-secondary am-radius am-btn-sm" 
                                        <?= $model['apply_status']['value']==40?'':'disabled' ?> >
                                            <i class="am-icon-cloud-upload"></i> 选择图片
                                        </button>
                                        <div class="uploader-list am-cf">
                                            <?php if ($model['details']['id_card_national']['url']): ?>
                                                <div class="file-item">
                                                    <a href="<?= $model['details']['id_card_national']['url'] ?>" title="点击查看大图" target="_blank">
                                                        <img src="<?= $model['details']['id_card_national']['url'] ?>">
                                                    </a>
                                                    <input type="hidden" name="register[details][id_card_national]"
                                                           value="<?= $model['details']['id_card_national']['value'] ?>">
                                                    <i class="iconfont iconshanchu file-item-delete"></i>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="help-block am-margin-top-sm">
                                        <small>身份证所有人必须跟营业执照法人（所有人）姓名一致。大小2M以下</small>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require"> 证件姓名 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" 
                                    name="register[details][id_card_name]" value="<?= $model['details']['id_card_name']?>" 
                                    <?= $model['apply_status']['value']==40?'':'disabled' ?> required>
                                    <small>身份证上面的姓名</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require"> 证件号码 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" 
                                    name="register[details][id_card_number]" value="<?= $model['details']['id_card_number']?>" 
                                    <?= $model['apply_status']['value']==40?'':'disabled' ?> required>
                                </div>
                            </div>
                            <?php if($model['apply_mode']['value'] == 20):?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">特殊资质 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <div class="am-form-file">
                                        <button type="button" class="qualifications am-btn am-btn-secondary am-radius am-btn-sm" 
                                        <?= $model['apply_status']['value']==40?'':'disabled' ?> >
                                            <i class="am-icon-cloud-upload"></i> 选择图片
                                        </button>
                                        <div class="uploader-list am-cf">
                                            <?php if ($model['details']['qualifications']['url']): ?>
                                                <div class="file-item">
                                                    <a href="<?= $model['details']['qualifications']['url'] ?>" title="点击查看大图" target="_blank">
                                                        <img src="<?= $model['details']['id_card_national']['url'] ?>">
                                                    </a>
                                                    <input type="hidden" name="register[details][qualifications]"
                                                           value="<?= $model['details']['qualifications']['value'] ?>">
                                                    <i class="iconfont iconshanchu file-item-delete"></i>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="help-block am-margin-top-sm">
                                        <small>非必需，如食品行业需提交《食品经营许可证》（提交后可参与相关行业活动）。大小2M以下</small>
                                    </div>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">开户银行</label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $model['details']['bank_name'] ?>" 
                                     name="register[details][bank_name]" <?= $model['apply_status']['value']==40?'':'disabled' ?> required>
                                     <small>例如：xx银行xx市xx区xx县xx支行</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">银行账号</label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $model['details']['account_number'] ?>" 
                                     name="register[details][account_number]" <?= $model['apply_status']['value']==40?'':'disabled' ?> required>
                                     <small>如营业执照类型为公司，必须填写公司公户账号。如为个体工商户，只需填写负责人私户账号</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">商户简称</label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $model['merchant_shortname'] ?>" 
                                     name="register[merchant_shortname]" <?= $model['apply_status']['value']==40?'':'disabled' ?> required>
                                     <small>用户付款时显示的收款名称，如设置为门店名称或者小程序名称，例如：肯德基、河马科技</small>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require">电子邮箱</label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $model['details']['contact_email'] ?>" 
                                     name="register[details][contact_email]" <?= $model['apply_status']['value']==40?'':'disabled' ?> required>
                                </div>
                            </div>
                            <?php endif;?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require"> 手机号 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" 
                                    name="register[details][mobile_phone]" value="<?= $model['details']['mobile_phone']?>" 
                                    <?= $model['apply_status']['value']==40?'':'disabled' ?> required>
                                </div>
                            </div>
                            <?php if($model['apply_mode']['value'] == 10 OR $model['apply_mode']['value'] == 50):?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label form-require"> 微信号 </label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" 
                                    name="register[details][legal_persona_wechat]" value="<?= $model['details']['legal_persona_wechat']?>" 
                                    <?= $model['apply_status']['value']==40?'':'disabled' ?> required>
                                    <small>不是绑定的手机号！请打开微信->我的->点击头像->查看微信号<br>
                                    微信必须实名认证，并与上面身份证姓名一致。</small>
                                </div>
                            </div>
                            <?php endif;?>
                            <?php else:?>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">级别</label>
                                <div class="am-u-sm-9">
                                    <label class="am-radio-inline">
                                        <input type="radio" data-am-ucheck <?= $model['agent_mode']['value'] == 10?'checked':'' ?> disabled> 县区
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" data-am-ucheck <?= $model['agent_mode']['value'] == 20?'checked':'' ?> disabled> 市级
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" data-am-ucheck <?= $model['agent_mode']['value'] == 30?'checked':'' ?> disabled> 省级
                                    </label>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">姓名</label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $model['details']['id_card_name'] ?>" disabled>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">电话</label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $model['details']['mobile_phone'] ?>" disabled>
                                </div>
                            </div>
                            <div class="am-form-group">
                                <label class="am-u-sm-3 am-form-label">区域</label>
                                <div class="am-u-sm-9">
                                    <input type="text" class="tpl-form-input" value="<?= $model['province'] . $model['city'] . $model['district'] ?>" disabled>
                                </div>
                            </div>
                            <?php endif;?>
                            <?PHP if($model['apply_status']['value']==40):?>
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交</button>
                                </div>
                            </div>
                            <?php endif;?>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- 图片文件列表模板 -->
{{include file="layouts/_template/tpl_file_item_filename" /}}

<!-- 文件库弹窗 -->
{{include file="layouts/_template/file_library" /}}

<script>
    $(function () {
        
        // 选择图片 - 身份证正面
        $('.license-copy').selectImages({
            name: 'register[details][license_copy]'
        });
        // 选择图片 - 身份证正面
        $('.id-card-copy').selectImages({
            name: 'register[details][id_card_copy]'
        });
        // 选择图片 - 身份证反面
        $('.id-card-national').selectImages({
            name: 'register[details][id_card_national]'
        });
        // 选择图片 - 特殊资质
        $('.qualifications').selectImages({
            name: 'register[details][qualifications]'
        });
        $('#my-form').formPost();

    });
</script>
