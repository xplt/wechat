<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <div class="widget-head am-cf">
                    <div class="widget-title am-cf">小程序列表</div>
                </div>
                <div class="widget-body am-fr">
                    <div class="tips am-margin-bottom-sm am-u-sm-12">
                       <!--  <div class="pre">
                            <p> 开放平台：用于打通公众号与小程序的数据，实现数据共享，互相调用与访问等</p>
                        </div>-->
                    </div>
                    <!-- 工具栏 -->
                    <div class="page_toolbar am-margin-bottom-xs am-cf">
                        <div class="am-u-sm-12 am-u-md-3">
                            <div class="am-form-group">
                                <div class="am-btn-group am-btn-group-xs">
                                    <a class="am-btn am-btn-default am-btn-success" href="<?= url('wxapp/add') ?>">
                                        <span class="am-icon-plus"></span> 创建
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="am-scrollable-horizontal am-u-sm-12">
                        <table width="100%" class="am-table am-table-compact am-table-striped
                         tpl-table-black am-text-nowrap">
                            <thead>
                            <tr>
                                <th>编号ID</th>
                                <th>小程序头像</th>
                                <th>小程序名称</th>
                                <th>版本类型</th>
                                <th>注册来源</th>
                                <th>是否授权</th>
                                <th>到期时间</th>
                                <th>创建时间</th>
                                <th>管理操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$list->isEmpty()): foreach ($list as $item): ?>
                                <tr>
                                    <td class="am-text-middle"><?= $item['wxapp_id'] ?></td>
                                    <td class="am-text-middle">
                                        <a href="<?= $item['head_img']?$item['head_img']:'/assets/store/img/wechatapp.png' ?>" title="点击查看大图" target="_blank">
                                            <img src="<?= $item['head_img']?$item['head_img']:'/assets/store/img/wechatapp.png' ?>" width="50" height="50" alt="小程序图片">
                                        </a>
                                    </td>
                                    <td class="am-text-middle">
                                        <p class="item-title"><?= $item['app_name'] ?></p>
                                    </td>
                                    <td class="am-text-middle">
                                        <?= $item['app_type']['text'] ?>
                                        <span style="position:absolute;margin-top:-10px;" class="am-badge am-round <?= $item['shop_mode']['value'] == 10? 'am-badge-secondary':'am-badge-success'?>">
                                        <?= $item['shop_mode']['text'] ?>
                                        </span>
                                    </td>
                                    <td class="am-text-middle"><?= $item['source']['text'] ?></td>
                                    <td class="am-text-middle"><?= $item['is_empower']['text'] ?></td>
                                    <td class="am-text-middle">剩余<?= $item['expire_time']['day'] ?>天</td>
                                    <td class="am-text-middle"><?= $item['create_time'] ?></td>
                                    <td class="am-text-middle">
                                        <div class="tpl-table-black-operation">
                                            <a href="<?= url('wxapp/login',
                                                ['wxapp_id' => $item['wxapp_id']]) ?>" target="_blank">
                                                <i class="am-icon-pencil"></i> 管理
                                            </a>
                                            <a href="<?= url('wxapp/upgrade',
                                                ['wxapp_id' => $item['wxapp_id']]) ?>" 
                                                class="item-delete tpl-table-black-operation-green" >
                                                <i class="am-icon-credit-card"></i> 续费
                                            </a>
                                            <a href="javascript:;" class="item-del tpl-table-black-operation-del"
                                               data-ids="<?= $item['wxapp_id'] ?>">
                                                <i class="am-icon-trash"></i> 删除
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                                <tr>
                                    <td colspan="9" class="am-text-center">暂无记录</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="am-u-lg-12 am-cf">
                        <div class="am-fr"><?= $list->render() ?> </div>
                        <div class="am-fr pagination-total am-margin-right">
                            <div class="am-vertical-align-middle">总记录：<?= $list->total() ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        // 删除元素
        var url = "<?= url('wxapp/delete') ?>";
        $('.item-del').del('wxapp_id', url,'确定要删除吗？删除后相关数据将无法恢复！');

    });
</script>