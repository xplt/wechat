<div class="row-content am-cf">
    <div class="row">
        <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
            <div class="widget am-cf">
                <form id="my-form" class="am-form tpl-form-line-form" method="post">
                    <div id="edition" class="widget-body">
                        <fieldset>
                            <div class="widget-head am-cf">
                                <div class="widget-title am-fl">微信小程序续费</div>
                            </div>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">版本类型 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input"
                                           value="<?= $edition['name'].' - '.$wxapp['shop_mode']['text'] ?>" disabled>
                                </div>
                            </div>
							<div class="am-form-group">
								<label class="am-u-sm-3 am-u-lg-2 am-form-label form-require">续费时间</label>
								<div class="am-u-sm-9 am-u-end">
									<label class="am-radio-inline">
										<input type="radio" name="wxapp[year]" :value="1" v-model="year" data-am-ucheck>
										一年
									</label>
									<label class="am-radio-inline">
										<input type="radio" name="wxapp[year]" :value="2" v-model="year" data-am-ucheck>
										二年
									</label>
									<label class="am-radio-inline">
										<input type="radio" name="wxapp[year]" :value="3" v-model="year" data-am-ucheck>
										三年
									</label>
									<label class="am-radio-inline">
										<input type="radio" name="wxapp[year]" :value="4" v-model="year" data-am-ucheck>
										四年
									</label>
									<label class="am-radio-inline">
										<input type="radio" name="wxapp[year]" :value="5" v-model="year" data-am-ucheck>
										五年
									</label>
								</div>
							</div>
							<?php if($wxapp['is_test']['value']==1):?>
							<div class="am-form-group">
                                <label class="am-u-sm-3 am-u-lg-2 am-form-label">备注说明 </label>
                                <div class="am-u-sm-9 am-u-end">
                                    <input type="text" class="tpl-form-input"
                                           value="<?= $edition['detail']?>" disabled>
                                </div>
                            </div>
							<?php endif;?>
							<div class="am-form-group">
								<label class="am-u-sm-3 am-u-lg-2 am-form-label">支付费用 </label>
								<div class="am-u-sm-9 am-u-end">
									<span style="font-weight:900;color:#ff0000;" v-if="shop_mode==10">￥{{single[year-1+is_test]}}元</span>
									<span style="font-weight:900;color:#ff0000;" v-if="shop_mode==20">￥{{many[year-1+is_test]}}元</span>
								</div>
							</div>
							<!-- 表单提交按钮 -->
                            <div class="am-form-group">
                                <div class="am-u-sm-9 am-u-sm-push-3 am-margin-top-lg">
                                    <button type="submit" class="j-submit am-btn am-btn-secondary">提交</button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
		
		// 渲染页面
		new Vue({
			el: '#edition',
			data: {
				single: <?= json_encode($wxapp['is_test']['value']?$edition['buy_single']:$edition['renew_single']) ?>,
				many: <?= json_encode($wxapp['is_test']['value']?$edition['buy_many']:$edition['renew_many']) ?>,
				shop_mode:<?= $wxapp['shop_mode']['value'] ?>,
				year:1,
				is_test:<?= $wxapp['is_test']['value'] ?>
			}
		});
		
        $('#my-form').formPost();

    });
</script>
