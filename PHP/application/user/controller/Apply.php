<?php
namespace app\user\controller;
use app\user\model\StoreApply as StoreApplyModel;

/**
 * 用户认证申请控制器
 */
class Apply extends Controller
{
    /**
     * 获取申请列表
     */
    public function index()
    {
        $model = new StoreApplyModel;
        $list = $model->getList('','',$this->store_user_id);
        return $this->fetch('index',compact('list'));
    }

    /**
     * 编辑
     */
    public function detail($store_apply_id)
    {   

        $model = StoreApplyModel::detail($store_apply_id);
        if(!$this->request->isAjax()) {
            return $this->fetch('detail', compact('model'));
        }
        //提交动作
        $data = $this->postData('register');        
        if(!isset($data['details']['license_copy']) OR empty($data['details']['license_copy'])){
            return $this->renderError('请上传营业执照');  
        }else{
            copy('./uploads/'.$data['details']['license_copy'],'./uploads/apply/'.$data['details']['license_copy']);
        }
        if(!isset($data['details']['id_card_copy']) OR empty($data['details']['id_card_copy'])){
            return $this->renderError('请上传身份证（正面）');  
        }else{
           copy('./uploads/'.$data['details']['id_card_copy'],'./uploads/apply/'.$data['details']['id_card_copy']); 
        }
        if(!isset($data['details']['id_card_national']) OR empty($data['details']['id_card_national'])){
            return $this->renderError('请上传身份证（反面）');  
        }else{
           copy('./uploads/'.$data['details']['id_card_national'],'./uploads/apply/'.$data['details']['id_card_national']); 
        }
        if(isset($data['details']['qualifications'])){
            copy('./uploads/'.$data['details']['qualifications'],'./uploads/apply/'.$data['details']['qualifications']); 
        }
        $data['apply_status'] = 10;//驳回后提交
        if ($model->edit($data)) {
            return $this->renderSuccess('修改成功', url('apply/index'));
        }
        $error = $model->getError() ?: '修改失败';
        return $this->renderError($error);      
    }
   
}
