<?php
namespace app\user\controller;
use app\user\model\StoreUser as StoreUserModel;

/**
 * 用户管理
 */
class Setting extends Controller
{
    
	/**
     * 修改管理员密码
     */
    public function renew()
    {
        if ($this->request->isAjax()) {
			if($err = checking(1)){
				return $this->renderError($err);
			}
            $model = StoreUserModel::detail($this->store_user_id);
            if ($model->renew($this->postData('user'))) {
                return $this->renderSuccess('修改成功');
            }
            return $this->renderError($model->getError() ?: '修改失败');
        }
        return $this->fetch('renew');
    }
}
