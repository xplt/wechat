<?php
namespace app\user\controller;
use think\Config;
use think\Session;
use think\Cookie;
use app\user\model\WebSet;

/**
 * 后台控制器基类
 */
class Controller extends \think\Controller
{
    protected $store;//商家登录信息
    protected $controller = '';//当前控制器名称
    protected $action = '';//当前方法名称
    protected $routeUri = '';//当前路由uri
    protected $group = '';//当前路由：分组名称
	protected $store_user_id = '';//商户id
	protected $agent_id = '';//代理id

    /**
     * 后台初始化
     */
    public function _initialize()
    {
        $this->getWxapp();//初始化数据
        $this->getRouteinfo();// 当前路由信息
        $this->checkLogin();// 验证登录
        $this->layout();// 全局layout
    }

    /**
     * 初始化数据
     */
    protected function getWxapp()
    {
        $this->store = Session::get('hema_store');// 商家登录信息
        $this->store_user_id = $this->store['user']['store_user_id']; // 用户ID
        $this->agent_id = $this->store['user']['agent_id'];//代理id
    }

    /**
     * 全局layout模板输出
     */
    private function layout()
    {
        // 输出到view
        $this->assign([
            'base_url' => web_url(),            // 当前站点域名
            'web' => WebSet::getItem('web'),//站点设置
            'store_url' => url('/user'),              // 后台模块url
            'group' => $this->group,
            'menus' => $this->menus(),                     // 后台菜单
            'store' => $this->store,                       // 商家登录信息
        ]);
    }

    /**
     * 解析当前路由参数 （分组名称、控制器名称、方法名）
     */
    protected function getRouteinfo()
    {
        // 控制器名称
        $this->controller = toUnderScore($this->request->controller());
        // 方法名称
        $this->action = $this->request->action();
        // 控制器分组 (用于定义所属模块)
        $groupstr = strstr($this->controller, '.', true);
        $this->group = $groupstr !== false ? $groupstr : $this->controller;
        // 当前uri
        $this->routeUri = $this->controller . '/' . $this->action;
    }

    /**
     * 后台菜单配置
     * @return array
     */
    private function menus()
    {
		foreach ($data = Config::get('menus') as $group => $first) {
            $data[$group]['active'] = $group === $this->group;
			//$data[0]['active']
            // 遍历：二级菜单
            if (isset($first['submenu'])) {
                foreach ($first['submenu'] as $secondKey => $second) {
                    // 二级菜单所有uri
                    $secondUris = [];
                    if (isset($second['submenu'])) {
                        // 遍历：三级菜单
                        foreach ($second['submenu'] as $thirdKey => $third) {
                            $thirdUris = [];
                            if (isset($third['uris'])) {
                                $secondUris = array_merge($secondUris, $third['uris']);
                                $thirdUris = array_merge($thirdUris, $third['uris']);
                            } else {
                                $secondUris[] = $third['index'];
                                $thirdUris[] = $third['index'];
                            }
                            $data[$group]['submenu'][$secondKey]['submenu'][$thirdKey]['active'] = in_array($this->routeUri, $thirdUris);
							$data[$group]['submenu'][$secondKey]['active'] = in_array($this->routeUri, $secondUris);
                        }
                    } else {
                        if (isset($second['uris']))
                            $secondUris = array_merge($secondUris, $second['uris']);
                        else
                            $secondUris[] = $second['index'];
                    }
                    // 二级菜单：active
                    !isset($data[$group]['submenu'][$secondKey]['active'])
                    && $data[$group]['submenu'][$secondKey]['active'] = in_array($this->routeUri, $secondUris);	
                }
            }
        }
        return $data;
    }

    /**
     * 验证登录状态
     */
    private function checkLogin()
    {
        // 验证登录状态
        if (empty($this->store)
            || (int)$this->store['is_login'] !== 1
            || (int)$this->store['is_admin'] !== 1
        ) {
			$this->redirect('/login.php');
            return false;
        }
        return true;
    }

    /**
     * 返回封装后的 API 数据到客户端
     */
    protected function renderJson($code = 1, $msg = '', $url = '', $data = [])
    {
        return compact('code', 'msg', 'url', 'data');
    }

    /**
     * 返回操作成功json
     */
    protected function renderSuccess($msg = 'success', $url = '', $data = [])
    {
        return $this->renderJson(1, $msg, $url, $data);
    }

    /**
     * 返回操作失败json
     */
    protected function renderError($msg = 'error', $url = '', $data = [])
    {
        return $this->renderJson(0, $msg, $url, $data);
    }

    /**
     * 获取post数据 (数组)
     */
    protected function postData($key)
    {
        return $this->request->post($key . '/a');
    }

}
