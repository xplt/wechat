<?php

return [
    'index' => [
        'name' => '首页',
        'icon' => 'iconshouye',
        'index' => 'index/index',
    ],
    'wxapp' => [
        'name' => '小程序',
        'icon' => 'iconweixinxiaochengxu',
        'color' => '#36b313',
        'index' => 'wxapp/index',
        'submenu' => [
            [
                'name' => '小程序管理',
                'index' => 'wxapp/index',
				'urls' => [
					'wxapp/index',
					'wxapp/add',
				    'wxapp/upgrade',
				    'wxapp/delete'
				]
            ],
        ],
    ],
    'wallet' => [
        'name' => '我的钱包',
        'icon' => 'iconqiandai',
        'index' => 'wallet/index',
		'submenu' => [
		    [
		        'name' => '钱包总览',
		        'index' => 'wallet/index',
				'urls' => [
				    'wallet/index',
				    'wallet/recharge'
				]
		    ],
		    [
		        'name' => '支付流水',
		        'index' => 'wallet/log',
				'urls' => [
				    'wallet/log'
				]
		    ],
		]
    ],
    'apply' => [
        'name' => '我的申请',
        'icon' => 'iconrenzheng',
        'index' => 'apply/index',
        'submenu' => [
            [
                'name' => '申请列表',
                'index' => 'apply/index',
                'urls' => [
                    'apply/index',
                    'apply/detail'
                ]
            ]
        ]
    ],
    'setting' => [
        'name' => '我的账号',
        'icon' => 'iconshezhi',
        'index' => 'setting/renew',
        'submenu' => [
            /*[
                'name' => '我的资料',
                'index' => 'setting/details',
            ],*/
            [
                'name' => '修改密码',
                'index' => 'setting/renew',
            ]
        ],
    ],
];
