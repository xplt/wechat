<?php
namespace app\user\model;
use app\common\model\Wxapp as WxappModel;
use think\Session;

/**
 * 小程序模型
 */
class Wxapp extends WxappModel
{

    /**
     * 一键登录应用
     */
    public function login($wxapp)
    {
		// 更新session
        Session::set('hema_store.wxapp',$wxapp->toArray());
		return true;
    }	

}