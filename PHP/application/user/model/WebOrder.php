<?php
namespace app\user\model;
use app\common\model\WebOrder as WebOrderModel;
use think\Db;

/**
 * 站点订单模型
 */
class WebOrder extends WebOrderModel
{
	/**
     * 续费
     */
    public function upgrade($data)
    {
		$app = Wxapp::getWxapp(['wxapp_id' => $data['wxapp_id']]);
		$edition = WebEdition::detail($data['web_edition_id']);//获取版本详情
		$store = StoreUser::detail($data['store_user_id']);//获取商户详情
		$pay = 0;//支付费用
		$agent_price = 0;//代理费用
		$title = '续费';
		//计算费用
		if($data['shop_mode']==10){
			//单商户
			if($app['is_test']['value']==1){
				//试用-新购买
				if($store['wallet'] < $edition['buy_single'][$data['year']]){
					$this->error = '账户余额不足，请充值';
					return false;
				}
				$pay = $edition['buy_single'][$data['year']];
				$title = '购买';
			}else{
				//续费
				if($store['wallet'] < $edition['renew_single'][$data['year']-1]){
					$this->error = '账户余额不足，请充值';
					return false;
				}
				$pay = $edition['renew_single'][$data['year']-1];
			}
			$agent_price = $edition['single_price']*$data['year'];
		}
		if($data['shop_mode']==20){
			//多商户
			if($app['is_test']['value']==1){
				//试用-新购买
				if($store['wallet'] < $edition['buy_many'][$data['year']]){
					$this->error = '账户余额不足，请充值';
					return false;
				}
				$pay = $edition['buy_many'][$data['year']];
				$title = '购买';
			}else{
				//续费
				if($store['wallet'] < $edition['renew_many'][$data['year']-1]){
					$this->error = '账户余额不足，请充值';
					return false;
				}
				$pay = $edition['renew_many'][$data['year']-1];
			}
			$agent_price = $edition['many_price']*$data['year'];
		}
		$day = $data['year']*3600*24*365;//计算到期时间
		if($pay==0){
			$this->error = '计算错误';
			return false;
		}
		$wallet = $store['wallet']-$pay;//计算扣除后的余额
		 // 开启事务
        Db::startTrans();
        try {
			$order_list = [];
			array_push($order_list,[
				'order_no' => orderNo(),
				'order_type' => 20,	//扣费
				'pay_price' => $pay,
				'pay_status' => 20,	
				'pay_time' => time(),	
				'purpose' => $title.'小程序',
				'affair_id' => $data['wxapp_id'],
				'agent_id' => $data['agent_id'],
				'store_user_id' => $data['store_user_id']
			]);
			//扣除余额
			$store->wallet = ['dec',$pay];
			$store->save();
			//代理分账
			if($data['agent_id']>0){
				//管理总账户扣费
				$admin = StoreUser::getStore(['is_agent' => 2]);
				$admin->wallet = ['dec',$pay-$agent_price];
				$admin->save();
				//代理分红
				$agent = StoreUser::getStore(['agent_id' => $data['agent_id']]);
				$agent->wallet = ['inc',$pay-$agent_price];
				$agent->save();
				array_push($order_list,[
					'order_no' => orderNo(),
					'order_type' => 30,
					'pay_price' => $pay-$agent_price,
					'pay_status' => 20,
					'pay_time' => time(),
					'purpose' => $title.'分红',
					'affair_id' => $data['agent_id'],
					'agent_id' => $data['agent_id'],
					'store_user_id' => $data['agent_id']
				]);						
			}
			//增加到期时间
			if($app['is_test']['value']==1){
				//试用用户续费
				$expire_time = time()+$day;
			}else{
				if($app['expire_time']['value']<time()){
					$expire_time = time()+$day;//如果超过到期时间，则重新计算
				}else{
					$expire_time = $app['expire_time']['value']+$day;
				}
			}
			$app->expire_time = $expire_time;
			$app->is_test = 0;
			$app->save();
            $this->saveAll($order_list);
			//账户资金变动提醒
			sand_account_change_msg($title.'小程序管理系统',$pay,$wallet,$data['store_user_id']);
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }
}
