<?php
namespace app\user\model;
use app\common\model\StoreUser as StoreUserModel;
use think\Session;

/**
 * 商家用户模型
 */
class StoreUser extends StoreUserModel
{

    /**
     * 修改管理员密码
     */
    public function renew($data)
    {
        //验证旧密码是否正确
        if($this->password !== hema_hash($data['password'])){
            $this->error = '旧密码输入错误';
            return false;
        }
        //验证密码长度是否合法
		if(strlen($data['password_new'])<6){
			$this->error = '新密码长度不足6位';
            return false;
		}
		if ($data['password_new'] !== $data['password_confirm']) {
            $this->error = '两次输入的新密码不一致';
            return false;
        }
        // 更新管理员信息
        return $this->save([
            'password' => hema_hash($data['password_new'])
        ]) !== false;
    }

}
