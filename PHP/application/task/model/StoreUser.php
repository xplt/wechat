<?php
namespace app\task\model;
use app\common\model\StoreUser as StoreUserModel;
use think\Db;

/**
 * 商户模型
 */
class StoreUser extends StoreUserModel
{
    /**
     * 新增
     */
    public function add($data)
    {
        //获取第三方配置信息
        $values = WebSet::getItem('register');
        // 开启事务
        Db::startTrans();
        try {
            $this->allowField(true)->save($data);//添加纪录
            $user_name = $values['prefix'] . $this->store_user_id;
            $password = rand(1000,9999);
            $this->save([
                'store_user_id' => $this->store_user_id,
                'user_name' => $user_name,
                'password' => hema_hash($password)
            ]);
            Db::commit();
            return [
                'user_name' => $user_name,
                'password' => $password
            ];
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }
	
	/**
     * 更新记录
     */
    public function edit($data = [])
    {
         $data['user_name'] = $this->user_name;
        //随机生成四位数的密码
        $password = rand(1000,9999);
        $data['password'] = hema_hash($password);
        if(empty($this->user_name)){
            //获取第三方配置信息
            $values = WebSet::getItem('register');
            $data['user_name'] = $values['prefix'] . $this->store_user_id;
        }
        if($this->allowField(true)->save($data) !== false){
            return [
                'user_name' => $data['user_name'],
                'password' => $password
            ];
        }
        return false;
    }
	
	/**
     * 更新记录
     */
    public function unSubscribe()
    {
        return $this->save(['wechat_open_id' => '']) !== false;
    }
}
