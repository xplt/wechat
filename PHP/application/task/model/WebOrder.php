<?php
namespace app\task\model;
use app\common\model\WebOrder as WebOrderModel;
use app\common\model\StoreUser as StoreUserModel;
use think\Db;

/**
 * 站点订单模型
 */
class WebOrder extends WebOrderModel
{
    /**
     * 待支付订单详情
     */
    public function payDetail($order_no)
    {
        return self::get(['order_no' => $order_no]);
    }

    /**
     * 更新付款状态
     */
    public function updatePayStatus($transaction_id)
    {
		if($this['pay_status']['value']==20){
			//避免重复回调
			return true;
		}
		// 开启事务
        Db::startTrans();
        try {
			//用户增加充值额度
			$store = StoreUserModel::detail($this['store_user_id']);
			$wallet = $store->wallet+$this['pay_price'];//计算充值后余额
			$store->wallet = ['inc',$this['pay_price']];
			$store->save(); 
			//管理员增加充值额度
			$admin = StoreUserModel::getStore(['is_agent' => 2]);
			$admin->wallet = ['inc',$this['pay_price']];
			$admin->save(); 
			// 更新订单状态
			$this->save([
				'pay_status' => 20,
				'pay_time' => time(),
				'transaction_id' => $transaction_id,
			]);
			sand_account_change_msg('账户充值',$this['pay_price'],$wallet,$this['store_user_id']);
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }

}
