<?php
namespace app\task\model;
use app\common\model\NearbyCategory as NearbyCategoryModel;

/**
 * 附近门店类目模型
 */
class NearbyCategory extends NearbyCategoryModel
{
	/**
     * 修改状态
     */
    public function edit($data)
    {
		$reason = '';
		$status = $data['status'];
		if($status==2){
			$reason = $data['reason'];
		}
        return $this->allowField(true)->save([
			'status' => $status,
			'reason' => $reason
		]);
    }
}
