<?php
namespace app\task\model;
use app\common\model\WxappTpl as WxappTplModel;

/**
 * 商户小程序模板模型
 */
class WxappTpl extends WxappTplModel
{
    /**
     * 更新
     */
    public function edit($data)
    {
        return $this->allowField(true)->save($data) !== false;
    }
}
