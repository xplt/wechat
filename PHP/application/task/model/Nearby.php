<?php
namespace app\task\model;
use app\common\model\Nearby as NearbyModel;

/**
 * 附近门店展示地点模型
 */
class Nearby extends NearbyModel
{
	/**
     * 修改状态
     */
    public function edit($data)
    {
		$reason = '';
		$status = $data['status'];
		if($status==2){
			$reason = $data['reason'];
		}
        return $this->save([
			'status' => $status,
			'reason' => $reason
		]) !== false;
    }
}
