<?php
namespace app\task\controller;
use app\task\model\Order as OrderModel;
use app\common\library\wechat\WxPay;
use app\task\model\Recharge as RechargeModel;
use app\task\model\WebOrder as WebOrderModel;
use app\task\model\WebSet as WebSetModel;

/**
 * 支付成功异步通知接口
 */
class Notify
{
    /**
     * 支付成功异步通知
     */
    public function order()
    {
		$WxPay = new WxPay([]);
        $WxPay->notify(new OrderModel);
    }
	
	/**
     * 支付成功异步通知 - 充值
     */
    public function recharge()
    {
        $WxPay = new WxPay([]);
        $WxPay->notify(new RechargeModel);
    }

	/**
     * 站点扫码支付成功异步通知
     */
    public function native()
    {
		$values = WebSetModel::getItem('payment');
        $WxPay = new WxPay([]);
        $WxPay->notify(new WebOrderModel,$values['wx']['apikey']);
    }
	
	/**
     * 助手小程序充值成功异步通知
     */
    public function store()
    {
		$values = WebSetModel::getItem('payment');
        $WxPay = new WxPay([]);
        $WxPay->notify(new WebOrderModel,$values['wx']['apikey']);
    }
	
	/**
     * 商家用户退款成功异步通知
     */
    public function refund()
    {
        $WxPay = new WxPay([]);
        $WxPay->notifyRefund(new OrderModel);
    }
}
