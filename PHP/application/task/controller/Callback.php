<?php
namespace app\task\controller;
use app\common\library\wechat\WxBizMsgCrypt;
use app\task\model\Config as ConfigModel;
use app\task\model\Wxapp as WxappModel;
use app\task\model\Wechat as WechatModel;
use app\task\model\StoreApply as StoreApplyModel;
use app\task\model\StoreDetail as StoreDetailModel;
use app\task\model\WxappName as WxappNameModel;
use app\task\model\WxappTpl as WxappTplModel;
use think\Cache;
/**
 * 第三方平台与微信通讯接口
 */

class Callback
{
    /**
     * 异步通知处理
     */
    public function ticket()
    {
		//获取第三方配置信息
		$config = ConfigModel::detail();
		// 接收公众号平台发送的消息
		$timeStamp = empty ( $_GET ['timestamp']) ? '' : trim ( $_GET ['timestamp'] );//时间戳
        $nonce = empty ( $_GET ['nonce'] ) ? '' : trim ( $_GET ['nonce'] );//随机数
        $msg_signature = empty ( $_GET['msg_signature'] ) ? '' : trim ( $_GET ['msg_signature'] );//签名
		$encrypt_type = empty ( $_GET['encrypt_type'] ) ? '' : trim ( $_GET ['encrypt_type'] );//加密类型，为 aes
        $encryptMsg = file_get_contents ('php://input' );
        //创建解密类
		$pc = new WxBizMsgCrypt($config['token'], $config['encoding_aes_key'], $config['app_id']);
		$msg = '';
		$errCode = $pc->decryptMsg($msg_signature, $timeStamp, $nonce, $encryptMsg, $msg);
		if($errCode == 0){
			$data = _xmlToArr($msg);	//XML转换为数组
			//write_log($data, __DIR__);//用于测试callback.php接口
			//推送的ticket
			if($data['InfoType'] == 'component_verify_ticket'){
				$config->component_verify_ticket = $data['ComponentVerifyTicket']; 
				//更新过期的令牌component_access_token
				if($config['expires_in'] < time()){
					//过期更新access_token
					$token = $this->getToken($config['app_id'], $config['app_secret'], $config->component_verify_ticket);
					$config->component_access_token = $token['component_access_token'];
					$config->expires_in = time()+3600;
				}
				$config->save();	//保存ticket
				echo 'success';
			}
			//取消授权
			if($data['InfoType'] == 'unauthorized'){
				if($wechat = WechatModel::getWechat(['app_id' => $data['AuthorizerAppid']])){
					$wechat->delAll($wechat['wxapp_id']);
				}
				if($wxapp = WxappModel::getWxapp(['app_id' => $data['AuthorizerAppid']])){
					$wxapp->edit([
						'is_empower' => 0,	//0=取消授权
						'app_id' => '',	//app_id值空，否则影响下次授权
					]);
				}
			}
			//更新授权
			if($data['InfoType'] == 'updateauthorized'){
				/*
				Array
				(
					[AppId] => wxd081a139beb02c77
					[CreateTime] => 1572001402
					[InfoType] => updateauthorized
					[AuthorizerAppid] => wxfd9875920f95854c
					[AuthorizationCode] => queryauthcode@@@x51tQWCqAh2vrSm6LOkLUDEvazWHdq8bZ05mwEwMDVGCBVZiydGjEMhbO0qDTriq-7F1sFNuicI_lqSp4SWUqg
					[AuthorizationCodeExpiredTime] => 1572005002
					[PreAuthCode] => preauthcode@@@eJqcbkFUN6N1hrO1S4H_zymHzftXj902fhTR5EM1zUCFHCUmSYISd-dYq_Gt_x0w
				)
				*/
			}
			
			/*
			//成功授权
			if($data['InfoType'] == 'authorized'){
				
			}
			*/
			//小程序注册成功
			if($data['InfoType'] == 'notify_third_fasteregister'){
				//验证状态
				$this->fasteregister($data,$config);				
			}
			//名称审核结果事件推送
			if($data['MsgType'] == 'event'){
				write_log('名称审核结果事件推送', __DIR__);
				write_log($data, __DIR__);
				if($data['Event'] == 'wxa_nickname_audit'){
					$wxapp = WxappModel::getWxapp(['user_name' => $data['ToUserName']]); //获取商户数据
					if($setName = WxappNameModel::detail(['nick_name' => $data['nickname']],$wxapp['wxapp_id'])){
						if($data['ret']==2){
							$setName['status'] = 2;
							$setName['reason'] = $data['reason'];
							$setName->save();
						}
						if($data['ret']==3){
							$setName['status'] = 1;
							$setName->save();
							$wxapp['app_name'] = $data['nickname'];
							$wxapp->save();
							Cache::rm('wxapp_' . $wxapp['wxapp_id']);
						}
					}
				}				
			}
			return 'success';
		}else{
			//write_log('Callback 解密失败，code：'.$errCode, __DIR__);
		}
    }
	
	/**
     * 获取 component_access_token
    */
	private function getToken($appid, $appsecret, $ticket)
	{
		$url = 'https://api.weixin.qq.com/cgi-bin/component/api_component_token';
		$data = [
			'component_appid' => $appid,
			'component_appsecret' => $appsecret,
			'component_verify_ticket' => $ticket,
		];
		return json_decode(http_post($url,$data),true);
	}
	
	/**
     * 快速注册小程序状态验证
    */
	private function fasteregister($data,$config)
	{
		//第一步、获取申请者资料
		$detail = StoreDetailModel::getDetail([
			'merchant_name' => $data['info']['name'],
			'license_number' => $data['info']['code'],
			'legal_persona_wechat' => $data['info']['legal_persona_wechat'],
			'id_card_name' => $data['info']['legal_persona_name']
		]);
		//第二步、获取到了资料
		if($detail){
			//第三步、查询申请记录
			$apply = StoreApplyModel::getDetail([
				'store_user_id' => $detail['store_user_id'],
				'apply_mode' => ['in','10,50'],
				'apply_status' => 20
			]);
			//如果查询到了申请记录
			if($apply){
				//判断微信端审核状态
				if($data['status'] == 0){
					//审核成功 - 获取授权信息
					$result = getAuth($config['app_id'], $config['component_access_token'], $data['auth_code']);
					$auth = $result['authorization_info'];//得到授权信息
					//获取授权应用的帐号基本信息
					$result = getAppInfo($auth['authorizer_appid'],$config['app_id'], $config['component_access_token']);
					$app = $result['authorizer_info'];//得到授权应用的帐号基本信息
					$appData = [
						'user_name' => $app['user_name'],					//原始ID
						'principal_name' => $app['principal_name'],			//主体名称
						'app_id' => $auth['authorizer_appid'],				//授权方APPid
						'access_token' => $auth['authorizer_access_token'],	//令牌
						'expires_in' => time()+7000,						//令牌过期时间
						'authorizer_refresh_token' => $auth['authorizer_refresh_token'],	//刷新令牌
						'is_empower' => 1,	//是否授权
						'source' => 20 //注册来源
					];
					//判断是平台入驻还是小程序注册
					if(empty($apply['wxapp_id'])){
						//入驻
						$title = '商户入驻';
						$msg = '入驻成功';
						//新增小程序
						$appData['app_type'] = 'food';
						$appData['shop_mode'] = $apply['shop_mode'];
						$appData['phone'] = $detail['mobile_phone'];
						$appData['is_test'] = 0;
						$appData['store_user_id'] = $apply['store_user_id'];
						$appData['expire_time'] = strtotime('+1year');
						$wxapp = new WxappModel;
						$apply->wxapp_id = $wxapp->add($appData,'apply');
					}else{
						//注册
						$title = '注册微信小程序';
						$msg = '注册成功';
						//更新小程序信息
						$wxapp = WxappModel::getWxapp(['wxapp_id' => $apply['wxapp_id']]); //获取商户数据
						$wxapp->edit($appData);
					}
					$apply->apply_status = 30;
				}else{
					//审核失败 (驳回操作)
					$status = [
						100001 => '已下发的模板消息法人并未确认且已超时（24h），未进行身份证校验',
						100002 => '已下发的模板消息法人并未确认且已超时（24h），未进行人脸识别校验',
						100003 => '已下发的模板消息法人并未确认且已超时（24h）',
						101 => '工商数据返回：“企业已注销”',
						102 => '工商数据返回：“企业不存在或企业信息未更新”',
						103 => '工商数据返回：“企业法定代表人姓名不一致”',
						104 => '工商数据返回：“企业法定代表人身份证号码不一致”',
						105 => '法定代表人身份证号码，工商数据未更新，请 5-15 个工作日之后尝试',
						1000 => '工商数据返回：“企业信息或法定代表人信息不一致”',
						1001 => '主体创建小程序数量达到上限',
						1002 => '主体违规命中黑名单',
						1003 => '管理员绑定账号数量达到上限',
						1004 => '管理员违规命中黑名单',
						1005 => '管理员手机绑定账号数量达到上限',
						1006 => '管理员手机号违规命中黑名单',
						1007 => '管理员身份证创建账号数量达到上限',
						1008 => '管理员身份证违规命中黑名单'
					];
					if($data['status'] == -1){
						$apply->reject = '企业与法人姓名不一致';
					}else{
						if(isset($status[$data['status']])){
							$apply->reject = $status[$data['status']];
						}else{
							$apply->reject = '未知原因';
						}
					}	
					$apply->apply_status = 40;
					//判断是平台入驻还是小程序注册
					if(empty($apply['wxapp_id'])){
						//入驻
						$title = '商户入驻 - 被驳回';
						$msg = '入驻失败';
					}else{
						//注册
						$title = '注册小程序 - 被驳回';
						$msg = '注册失败';
					}
				}
			}
			//发送微信通知
			sand_examine_msg($apply['store_apply_id'],$title,$msg,$apply['store_user_id']);
			$apply->save();
		}
		return true;
	}
}
