<?php
namespace app\task\controller;
use think\Controller;
use app\task\model\Config as ConfigModel;
use app\task\model\Wxapp as WxappModel;
use app\task\model\WxappTpl as WxappTplModel;
use app\task\model\User as UserModel;
use app\task\model\Wechat as WechatModel;

/**
 * 授权回调接口
 */

class Auth extends Controller
{
	/**
     * 公众号授权
     */
    public function wechat($wxapp_id)
    {
		$auth_code = empty ( $_GET ['auth_code'] ) ?"" : trim ( $_GET ['auth_code'] );	//获取授权码
		if(!empty($auth_code)){
			$config = ConfigModel::detail();
			//获取授权信息
			$result = getAuth($config['app_id'], $config['component_access_token'], $auth_code);
			$auth = $result['authorization_info'];//得到授权信息
			//获取授权应用的帐号基本信息
			$result = getAppInfo($auth['authorizer_appid'], $config['app_id'], $config['component_access_token']);
			$app = $result['authorizer_info'];//得到授权应用的帐号基本信息
			if($wechat = WechatModel::getWechat(['wxapp_id' => $wxapp_id])){
				$wechat->delAll($wxapp_id);
			}
			$model = new WechatModel;
			$model->save([
				'app_id' => $auth['authorizer_appid'],//授权方APPid
				'user_name' => $app['user_name'],//原始ID
				'app_name' => isset($app['nick_name'])?$app['nick_name']:'',	//昵称
				'head_img' => isset($app['head_img'])?$app['head_img']:'',//头像
				'qrcode_url' =>$app['qrcode_url'],	//二维码地址
				'principal_name' => $app['principal_name'],	//主体名称
				'access_token' => $auth['authorizer_access_token'],	//令牌
				'expires_in' => time()+7000,//令牌过期时间
				'authorizer_refresh_token' => $auth['authorizer_refresh_token'],//刷新令牌	
				'wxapp_id' => $wxapp_id			
			]);
			if($wxapp_id>0){
				$this->redirect('/index.php?s=/store/wechat/index');
			}
			$this->redirect('/index.php?s=/admin/wechat/index');
		}
		echo "error";
	}

	/**
     * 小程序授权
     */
    public function wxapp($wxapp_id='')
    {
		$auth_code = empty ( $_GET ['auth_code'] ) ?"" : trim ( $_GET ['auth_code'] );	//获取授权码
		if(!empty($auth_code)){
			$config = ConfigModel::detail();
			//获取授权信息
			$result = getAuth($config['app_id'], $config['component_access_token'], $auth_code);
			$auth = $result['authorization_info'];//得到授权信息
			//验证是否已授权过
			if($wxapp = WxappModel::getWxapp([
				'app_id' => $auth['authorizer_appid'],
				'is_empower' => 1
			])){
				$this->error('该小程序已经在账号：'.$wxapp['store_user_id'].'内授权绑定过了，请先解除绑定！', '/index.php?s=/store/wxapp/setting');
            	return false;
			}
			//获取授权应用的帐号基本信息
			$result = getAppInfo($auth['authorizer_appid'], $config['app_id'], $config['component_access_token']);
			$app = $result['authorizer_info'];//得到授权应用的帐号基本信息
			$wxapp = WxappModel::getWxapp(['wxapp_id' => $wxapp_id]); //获取商户数据
			$data = [
				'app_name' => isset($app['nick_name'])?$app['nick_name']:'',	//昵称
				'head_img' => isset($app['head_img'])?$app['head_img']:'',		//头像
				'qrcode_url' => isset($app['qrcode_url'])?$app['qrcode_url']:'',	//二维码
				'user_name' => $app['user_name'],					//原始ID
				'principal_name' => $app['principal_name'],			//主体名称
				'signature' => isset($app['signature'])?$app['signature']:'一个值得信赖的小程序',	//账号介绍
				'api_domain' => $config['api_domain'],//服务器域名
				'app_id' => $auth['authorizer_appid'],				//授权方APPid
				'access_token' => $auth['authorizer_access_token'],	//令牌
				'expires_in' => time()+7000,						//令牌过期时间
				'authorizer_refresh_token' => $auth['authorizer_refresh_token'],	//刷新令牌
				'is_empower' => 1	//是否授权				
			];
			if($wxapp->edit($data)){
				WxappTplModel::delALL($wxapp_id);
				UserModel::delALL($wxapp_id);
				$this->redirect('/index.php?s=/store/wxapp/setting');
			}
		}
		echo "error";
	}
}
