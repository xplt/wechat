<?php

// [ 公众号授权登陆入口文件 ]

// 手动定义路由
$_GET['s'] = '/apiout/user/auth';

// 定义运行目录
define('WEB_PATH', __DIR__ . '/');

// 定义应用目录
define('APP_PATH', WEB_PATH . 'application/');

// 加载框架引导文件
require APP_PATH . '../thinkphp/start.php';
