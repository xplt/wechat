SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

#公众号表#
DROP TABLE IF EXISTS `hema_wechat`;
CREATE TABLE `hema_wechat` (
  `wechat_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号id',
  `app_name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `head_img` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `qrcode_url` varchar(255) NOT NULL DEFAULT '' COMMENT '二维码',
  `user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '原始ID',
  `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'AppID',
  `principal_name` varchar(255) NOT NULL DEFAULT '' COMMENT '主体名称',
  `access_token` varchar(255) NOT NULL DEFAULT '' COMMENT '令牌凭证',
  `expires_in` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '令牌到期时间',
  `authorizer_refresh_token` varchar(255) NOT NULL DEFAULT '' COMMENT '刷新令牌',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`wechat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#外卖平台页面设计表#
DROP TABLE IF EXISTS `hema_out_page`;
CREATE TABLE `hema_out_page` (
  `page_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '页面id',
  `province` varchar(20) NOT NULL DEFAULT '' COMMENT '所在省份',
  `city` varchar(20) NOT NULL DEFAULT '' COMMENT '所在城市',
  `district` varchar(20) NOT NULL DEFAULT '' COMMENT '所在区/县',
  `page_data` longtext NOT NULL COMMENT '页面数据',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#认证申请表#
DROP TABLE IF EXISTS `hema_store_apply`;
CREATE TABLE `hema_store_apply` (
  `store_apply_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '认证申请ID',
  `apply_mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '认证类型(10入驻 20支付 30代理 40实名 50小程序注册)',
  `store_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '申请人ID',
  `wxapp_id` varchar(50) NOT NULL DEFAULT '' COMMENT '小程序ID',
  `app_mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '小程序类型(10微信 20支付宝)',
  `shop_mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '门店类型(10单门店 20多门店)',
  `mchid` varchar(50) NOT NULL DEFAULT '' COMMENT '支付商户号',
  `pay_mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '支付类型(10微信 20支付宝)',
  `merchant_shortname` varchar(50) NOT NULL DEFAULT '' COMMENT '商户简称',
  `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT '小程序AppID',
  `auth_mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '认证类型(10工商 20个人)',
  `agent_mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '代理级别(10 区县 20 市级 30 省级)',
  `location` varchar(50) NOT NULL DEFAULT '' COMMENT '位置坐标',
  `province` varchar(20) NOT NULL DEFAULT '' COMMENT '所在省份',
  `city` varchar(20) NOT NULL DEFAULT '' COMMENT '所在城市',
  `district` varchar(20) NOT NULL DEFAULT '' COMMENT '所在区/县',
  `pay_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '支付状态(10待支付，20已支付)',
  `pay_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '支付时间',
  `reject` varchar(255) NOT NULL DEFAULT '' COMMENT '驳回原因',
  `apply_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '审核状态(10待审核，20验证中，30已审核，40已驳回)',
  `apply_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '审查时间',
  `agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属代理',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`store_apply_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#商户详情表#
DROP TABLE IF EXISTS `hema_store_detail`;
CREATE TABLE `hema_store_detail` (
  `store_detail_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '商家详情ID',
  `mchid` varchar(50) NOT NULL DEFAULT '' COMMENT '微信支付商户号',
  `id_card_copy` varchar(50) NOT NULL DEFAULT '' COMMENT '身份证正面',
  `id_card_national` varchar(50) NOT NULL DEFAULT '' COMMENT '身份证反面',
  `id_card_name` varchar(50) NOT NULL DEFAULT '' COMMENT '证件姓名',
  `id_card_number` varchar(50) NOT NULL DEFAULT '' COMMENT '证件号码',
  `legal_persona_wechat` varchar(50) NOT NULL DEFAULT '' COMMENT '微信号',
  `mobile_phone` varchar(50) NOT NULL DEFAULT '' COMMENT '手机号',
  `contact_email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `subject_type` varchar(50) NOT NULL DEFAULT 'SUBJECT_TYPE_INDIVIDUAL' COMMENT '主体类型',
  `license_copy` varchar(50) NOT NULL DEFAULT '' COMMENT '营业执照',
  `merchant_name` varchar(50) NOT NULL DEFAULT '' COMMENT '营业执照名称',
  `license_number` varchar(50) NOT NULL DEFAULT '' COMMENT '统一社会信用代码',
  `qualifications` varchar(50) NOT NULL DEFAULT '' COMMENT '特殊资质',
  `bank_name` varchar(100) NOT NULL DEFAULT '' COMMENT '开户银行全称',
  `account_number` varchar(50) NOT NULL DEFAULT '' COMMENT '银行账号',
  `store_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商家ID',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`store_detail_id`),
  UNIQUE KEY `store_user_id` (`store_user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#门店分类表#
DROP TABLE IF EXISTS `hema_shop_category`;
CREATE TABLE `hema_shop_category` (
  `shop_category_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品分类id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '分类名称',
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类id',
  `image_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '分类图片id',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序方式(数字越小越靠前)',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`shop_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;


#拼团商品表#
DROP TABLE IF EXISTS `hema_goods_group`;
CREATE TABLE `hema_goods_group` (
  `goods_group_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '拼团商品id',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品id',
  `deduct_stock_type` tinyint(3) unsigned NOT NULL DEFAULT '20' COMMENT '库存计算方式(10下单减库存 20付款减库存)',
  `sales_initial` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '初始销量',
  `sales_actual` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '实际销量',
  `goods_sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '商品排序(数字越小越靠前)',
  `goods_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '商品状态(10上架 20下架)',
  `is_alone` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否允许单买',
  `people` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '成团人数',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`goods_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#砍价商品表#
DROP TABLE IF EXISTS `hema_goods_bargain`;
CREATE TABLE `hema_goods_bargain` (
  `goods_bargain_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '砍价商品id',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品id',
  `deduct_stock_type` tinyint(3) unsigned NOT NULL DEFAULT '20' COMMENT '库存计算方式(10下单减库存 20付款减库存)',
  `sales_initial` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '初始销量',
  `sales_actual` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '实际销量',
  `goods_sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '商品排序(数字越小越靠前)',
  `goods_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '商品状态(10上架 20下架)',
  `people` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '帮砍人数',
  `is_self_cut` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '自砍一刀',
  `is_floor_buy` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '底价购买',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`goods_bargain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#秒杀商品表#
DROP TABLE IF EXISTS `hema_goods_seckill`;
CREATE TABLE `hema_goods_seckill` (
  `goods_seckill_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '秒杀商品id',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品id',
  `deduct_stock_type` tinyint(3) unsigned NOT NULL DEFAULT '20' COMMENT '库存计算方式(10下单减库存 20付款减库存)',
  `sales_initial` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '初始销量',
  `sales_actual` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '实际销量',
  `goods_sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '商品排序(数字越小越靠前)',
  `goods_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '商品状态(10上架 20下架)',
  `limit_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '限购数量',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`goods_seckill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#云叫号器表#
DROP TABLE IF EXISTS `hema_calling`;
CREATE TABLE `hema_calling` (
  `calling_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '打印机ID',
  `calling_name` varchar(50) NOT NULL DEFAULT '' COMMENT '打印机名称',
  `device_name` varchar(50) NOT NULL DEFAULT '' COMMENT '设备编号',
  `device_key` varchar(50) NOT NULL DEFAULT '' COMMENT '设备密钥',
  `volume` tinyint(3) unsigned NOT NULL DEFAULT '15' COMMENT '音量(0-15)',
  `is_open` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '开关(0关闭 1开启)',
  `is_new` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '新订单提醒(0关闭 1开启)',
  `is_pay` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '微信收款提醒(0关闭 1开启)',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`calling_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#外卖平台站点设置表#
DROP TABLE IF EXISTS `hema_out_set`;
CREATE TABLE `hema_out_set` (
  `key` varchar(30) NOT NULL COMMENT '设置项标示',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '设置项描述',
  `values` mediumtext NOT NULL COMMENT '设置内容（json格式）',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  UNIQUE KEY `unique_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#免费吃商品表#
DROP TABLE IF EXISTS `hema_free_goods`;
CREATE TABLE `hema_free_goods` (
  `free_goods_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品编号',
  `free_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '免单比例',
  `partake_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '参与数量',
  `reward_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '奖励数量',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`free_goods_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

# 图文资讯表
DROP TABLE IF EXISTS `hema_article`;
CREATE TABLE `hema_article` (
  `article_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `article_title` varchar(300) NOT NULL DEFAULT '' COMMENT '文章标题',
  `show_type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '列表显示方式(10小图展示 20大图展示)',
  `article_category_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文章分类id',
  `image_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '封面图id',
  `article_content` longtext NOT NULL COMMENT '文章内容',
  `article_sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文章排序(数字越小越靠前)',
  `article_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '文章状态(0隐藏 1显示)',
  `virtual_views` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '虚拟阅读量(仅用作展示)',
  `actual_views` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '实际阅读量',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='图文信息表';


# 图文资讯分类表
DROP TABLE IF EXISTS `hema_article_category`;
CREATE TABLE `hema_article_category` (
  `article_category_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '分类名称',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序方式(数字越小越靠前)',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`article_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='图文分类表';

#微信批量发送信息表#
DROP TABLE IF EXISTS `hema_wechat_batch_send`;
CREATE TABLE `hema_wechat_batch_send` (
  `wechat_batch_send_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号ID',
  `msg_id` varchar(50) NOT NULL DEFAULT '' COMMENT '任务ID',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '消息标题',
  `msg_type` varchar(20) NOT NULL DEFAULT '' COMMENT '消息类型',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '消息内容',
  `recommend` varchar(255) NOT NULL DEFAULT '' COMMENT '推荐语',
  `need_open_comment` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启评论，0关闭，1开启',
  `only_fans_can_comment` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '谁可评论，0所有人，1仅粉丝',
  `send_ignore_reprint` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '为转载时，0停止群发，1继续群发',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '内容描述',
  `status` int(11) unsigned NOT NULL DEFAULT '10' COMMENT '群发状态',
  `fans_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '群发人数',
  `send_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '成功人数',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`wechat_batch_send_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;


#站点配置#
DROP TABLE IF EXISTS `hema_config`;
CREATE TABLE `hema_config` (
  `config_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '站点配置ID',
  `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'AppID',
  `app_secret` varchar(50) NOT NULL DEFAULT '' COMMENT 'AppSecret',
  `encoding_aes_key` varchar(50) NOT NULL DEFAULT '' COMMENT 'encoding_aes_key',
  `token` varchar(50) NOT NULL DEFAULT '' COMMENT 'token',
  `api_domain` varchar(255) NOT NULL DEFAULT '' COMMENT '服务器域名',
  `authorize_domain` varchar(255) NOT NULL DEFAULT '' COMMENT '授权发起页域名',
  `component_verify_ticket` varchar(255) NOT NULL DEFAULT '' COMMENT 'ticket',
  `component_access_token` varchar(255) NOT NULL DEFAULT '' COMMENT '令牌',
  `expires_in` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '令牌过期时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10002 DEFAULT CHARSET=utf8;
INSERT INTO `hema_config` VALUES ('10001','','','','','','','','','0','1529926348','1529926348');

#公众号素材表#
DROP TABLE IF EXISTS `hema_material`;
CREATE TABLE `hema_material` (
  `material_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '素材名称',
  `file_type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '文件类型 10=图片，20=音频，30=视频，40=图文',
  `file_name` varchar(50) NOT NULL DEFAULT '' COMMENT '本地文件名称',
  `media_id` varchar(50) NOT NULL DEFAULT '' COMMENT '素材media_id',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '素材网络地址',
  `text_no` varchar(20) NOT NULL DEFAULT '' COMMENT '图文素材编号',
  `introduction` varchar(255) NOT NULL DEFAULT '' COMMENT '视频素材描述',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`material_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#公众号图文素材详情表#
DROP TABLE IF EXISTS `hema_material_text`;
CREATE TABLE `hema_material_text` (
  `material_text_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号',
  `text_no` varchar(20) NOT NULL DEFAULT '' COMMENT '素材编号',
  `id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '素材信息序号',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `author` varchar(50) NOT NULL DEFAULT '' COMMENT '作者',
  `digest` varchar(100) NOT NULL DEFAULT '' COMMENT '摘要',
  `content` longtext NOT NULL COMMENT '正文',
  `media_id` varchar(50) NOT NULL DEFAULT '' COMMENT '素材media_id',
  `file_name` varchar(50) NOT NULL DEFAULT '' COMMENT '本地文件名称',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '封面网络地址',
  `content_source_url` varchar(255) NOT NULL DEFAULT '#' COMMENT '图文链接地址',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`material_text_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#小程序插件表#
DROP TABLE IF EXISTS `hema_web_wxapp_plugins`;
CREATE TABLE `hema_web_wxapp_plugins` (
  `web_wxapp_plugins_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `plugin_appid` varchar(50) NOT NULL DEFAULT '' COMMENT '插件APPID',
  `nickname` varchar(50) NOT NULL DEFAULT '' COMMENT '插件名称',
  `headimgurl` varchar(255) NOT NULL DEFAULT '' COMMENT '插件头像',
  `user_version` varchar(50) NOT NULL DEFAULT '' COMMENT '插件版本',
  `key` varchar(50) NOT NULL DEFAULT '' COMMENT '识别名称',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`web_wxapp_plugins_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10002 DEFAULT CHARSET=utf8;

INSERT INTO `hema_web_wxapp_plugins` VALUES (10001,'wx2b03c6e691cd7370','小程序直播组件','http://wx.qlogo.cn/mmhead/Q3auHgzwzM4aKMKicpWPwZDJzeq7zktj6kNarkO3hwu5rCqzEMW82Qw/0','1.2.10','live',1580703326,1580744990);


#直播间表#
DROP TABLE IF EXISTS `hema_live_room`;
CREATE TABLE `hema_live_room` (
  `live_room_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '直播类型 1推流，0手机直播',
  `roomid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '房间编号',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '直播房间名',
  `cover_img` varchar(255) NOT NULL DEFAULT '' COMMENT '直播间背景图',
  `share_img` varchar(255) NOT NULL DEFAULT '' COMMENT '直播间分享图',
  `anchor_name` varchar(50) NOT NULL DEFAULT '' COMMENT '主播昵称',
  `anchor_wechat` varchar(50) NOT NULL DEFAULT '' COMMENT '主播微信号',
  `start_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '开播时间',
  `end_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '停播时间',
  `screen_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '画面尺寸 1横屏 0竖屏',
  `close_like` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '关闭点赞 1关闭 0开启',
  `close_goods` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '关闭货架 1关闭 0开启',
  `close_comment` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '关闭评论 1关闭 0开启',
  `goods` longtext COMMENT '货架产品列表',
  `live_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '直播间状态',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`live_room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;


#预约表#
DROP TABLE IF EXISTS `hema_pact`;
CREATE TABLE `hema_pact` (
  `pact_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '模式，10排号 20订桌',
  `linkman` varchar(20) NOT NULL DEFAULT '' COMMENT '联系人',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '联系电话',
  `pact_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '约定时间',
  `people` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '约定人数',
  `row_no` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单排号',
  `table_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '桌位id',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `message` varchar(255) NOT NULL DEFAULT '' COMMENT '客户留言',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '状态，10预约中 20已过期',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`pact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#公众号关键字回复#
DROP TABLE IF EXISTS `hema_keyword`;
CREATE TABLE `hema_keyword` (
  `keyword_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号ID',
  `keyword` varchar(50) NOT NULL DEFAULT '' COMMENT '关键字',
  `type` varchar(10) NOT NULL DEFAULT 'text' COMMENT '消息类型 text=文字，image=图片，voice=音频，video=视频，music=音乐，news=图文',
  `is_open` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启 0=关闭，1=开启',
  `content` longtext COMMENT '消息内容',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`keyword_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#小程序版本#
DROP TABLE IF EXISTS `hema_web_edition`;
CREATE TABLE `hema_web_edition` (
  `web_edition_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '版本名称',
  `app_type` varchar(50) NOT NULL DEFAULT '' COMMENT '模板类型',
  `buy_single` varchar(255) NOT NULL DEFAULT '' COMMENT '单门店购买价格',
  `renew_single` varchar(255) NOT NULL DEFAULT '' COMMENT '单门店续费价格',
  `buy_many` varchar(255) NOT NULL DEFAULT '' COMMENT '多门店购买价格',
  `renew_many` varchar(255) NOT NULL DEFAULT '' COMMENT '多门店续费价格',
  `trial_day` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '免费试用天数',
  `detail` varchar(255) NOT NULL DEFAULT '' COMMENT '备注说明',
  `single_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单门店代理价',
  `many_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '多门店代理价',
  `agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属代理',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`web_edition_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10003 DEFAULT CHARSET=utf8;
INSERT INTO `hema_web_edition` VALUES (10001,'餐饮','food','["0","1","1","1","1","1"]','["1","1","1","1","1"]','["0","1","1","1","1","1"]','["1","1","1","1","1"]',30,'',0,0,0,100,1580703326,1580744990);
INSERT INTO `hema_web_edition` VALUES (10002,'商城','shop','["0","1","1","1","1","1"]','["1","1","1","1","1"]','["0","1","1","1","1","1"]','["1","1","1","1","1"]',30,'',0,0,0,100,1580703326,1580744990);

#前端友情链接#
DROP TABLE IF EXISTS `hema_web_link`;
CREATE TABLE `hema_web_link` (
  `web_link_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '站点名称',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '站点地址',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`web_link_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10005 DEFAULT CHARSET=utf8;

INSERT INTO `hema_web_link` VALUES (10001,'河马云店','https://www.hemaphp.com/',100,1580703326,1580744990);
INSERT INTO `hema_web_link` VALUES (10002,'ThinkPHP','http://www.thinkphp.cn/',100,1580703326,1580744990);
INSERT INTO `hema_web_link` VALUES (10003,'码云Gitee','https://gitee.com/',100,1580745030,1580745030);
INSERT INTO `hema_web_link` VALUES (10004,'腾讯云','https://cloud.tencent.com/',100,1580745067,1580745067);


#前端菜单#
DROP TABLE IF EXISTS `hema_web_menu`;
CREATE TABLE `hema_web_menu` (
  `web_menu_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级菜单id',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '栏目标题',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '页面描述',
  `keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '页面关键字',
  `key` varchar(30) NOT NULL COMMENT '别名标示',
  `url` varchar(100) NOT NULL DEFAULT '' COMMENT '外部链接地址',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态(1显示 0隐藏)',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`web_menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10012 DEFAULT CHARSET=utf8;

INSERT INTO `hema_web_menu` VALUES (10001,'首页',0,'首页','','','index','',1,100,1529926348,1578285898);
INSERT INTO `hema_web_menu` VALUES (10002,'产品中心',0,'产品中心','','','product','',1,100,1529926348,1578275514);
INSERT INTO `hema_web_menu` VALUES (10003,'餐饮小吃',10002,'餐饮小吃','','','product_food','',1,100,1529926348,1580705223);
INSERT INTO `hema_web_menu` VALUES (10004,'超市商城',10002,'超市商城','','','product_shop','',1,100,1529926348,1580705223);
INSERT INTO `hema_web_menu` VALUES (10005,'水果生鲜',10002,'水果生鲜','','','product_fruits','',1,100,1529926348,1580705223);
INSERT INTO `hema_web_menu` VALUES (10006,'服装鞋帽',10002,'服装鞋帽','','','product_clothing','',1,100,1529926348,1580705223);
INSERT INTO `hema_web_menu` VALUES (10007,'护肤美容',10002,'护肤美容','','','product_makeup','',1,100,1529926348,1580705223);
INSERT INTO `hema_web_menu` VALUES (10008,'客户案例',0,'客户案例','','','case','',1,100,1529926348,1580733056);
INSERT INTO `hema_web_menu` VALUES (10009,'产品价格',0,'产品价格','','','price','',1,100,1529926348,1580733056);
INSERT INTO `hema_web_menu` VALUES (10010,'区域合作',0,'区域合作','','','proxy','',1,100,1529926348,1580733056);
INSERT INTO `hema_web_menu` VALUES (10011,'联系我们',0,'联系我们','','','contact','',1,100,1578275764,1580737018);


#分销商进出帐表#
DROP TABLE IF EXISTS `hema_dealer`;
CREATE TABLE `hema_dealer` (
  `dealer_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '模式，10进账 20提现',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '金额',
  `take_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '状态，10审核中 20已打款',
  `order_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`dealer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#口味选项表#
DROP TABLE IF EXISTS `hema_flavor`;
CREATE TABLE `hema_flavor` (
  `flavor_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '口味名称',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`flavor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#小程序附近地点-类目表#
DROP TABLE IF EXISTS `hema_nearby_category`;
CREATE TABLE `hema_nearby_category` (
  `nearby_category_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `audit_id` varchar(50) NOT NULL DEFAULT '' COMMENT '审核编号',
  `first_name` varchar(50) NOT NULL DEFAULT '' COMMENT '一级类目名称',
  `first_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '一级类目ID',
  `second_name` varchar(50) NOT NULL DEFAULT '' COMMENT '二级类目名称',
  `second_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '二级类目ID',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态，1审核中 2审核失败 3审核通过',
  `reason` varchar(255) NOT NULL DEFAULT '' COMMENT '驳回原因',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`nearby_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#小程序附近地点表#
DROP TABLE IF EXISTS `hema_nearby`;
CREATE TABLE `hema_nearby` (
  `nearby_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店编号',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '微信端门头链接',
  `company_name` varchar(255) NOT NULL DEFAULT '' COMMENT '主体名称',
  `credential` varchar(20) NOT NULL DEFAULT '' COMMENT '主体编号',
  `qualification_list` varchar(255) NOT NULL DEFAULT '' COMMENT '其它证明',
  `audit_id` varchar(255) NOT NULL DEFAULT '' COMMENT '审核单编号',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态，1审核中 2审核失败 3审核通过',
  `reason` varchar(255) NOT NULL DEFAULT '' COMMENT '驳回原因',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`nearby_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#小程序体验用户表#
DROP TABLE IF EXISTS `hema_test_user`;
CREATE TABLE `hema_test_user` (
  `test_user_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `wechatid` varchar(50) NOT NULL DEFAULT '' COMMENT '体验者微信号',
  `userstr` varchar(50) NOT NULL DEFAULT '' COMMENT '体验者授权码',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`test_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#小程序服务类目#
DROP TABLE IF EXISTS `hema_category_serve`;
CREATE TABLE `hema_category_serve` (
  `category_serve_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品分类id',
  `first` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '一级类目ID',
  `first_name` varchar(100) NOT NULL DEFAULT '' COMMENT '一级类目名称',
  `second` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '二级类目id',
  `second_name` varchar(100) NOT NULL DEFAULT '' COMMENT '二级类目名称',
  `ca_name` varchar(100) NOT NULL DEFAULT '' COMMENT '证书名称',
  `certicates` varchar(100) NOT NULL DEFAULT '' COMMENT '资质素材',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序ID',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`category_serve_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#商户发布的模板#
DROP TABLE IF EXISTS `hema_wxapp_tpl`;
CREATE TABLE `hema_wxapp_tpl` (
  `wxapp_tpl_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `template` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '模板ID',
  `auditid` varchar(255) NOT NULL DEFAULT '' COMMENT '审核编号',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否发布，0=未发布，1=已发布',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`wxapp_tpl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#站点订单表#
DROP TABLE IF EXISTS `hema_web_order`;
CREATE TABLE `hema_web_order` (
  `web_order_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `order_no` varchar(20) NOT NULL DEFAULT '' COMMENT '订单号',
  `order_type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '订单类型(10充值,20扣费,30收益,40提现)',
  `pay_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '扣支付金额',
  `pay_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '扣付款状态(10失败 20成功)',
  `pay_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '扣付款时间',
  `transaction_id` varchar(30) NOT NULL DEFAULT '' COMMENT '微信支付交易号',
  `purpose` varchar(20) NOT NULL DEFAULT '' COMMENT '款项用途',
  `affair_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '事务ID',
  `agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属代理',
  `store_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属商户',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`web_order_id`),
  UNIQUE KEY `order_no` (`order_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#站点设置表#
DROP TABLE IF EXISTS `hema_web_set`;
CREATE TABLE `hema_web_set` (
  `key` varchar(30) NOT NULL COMMENT '设置项标示',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '设置项描述',
  `values` mediumtext NOT NULL COMMENT '设置内容（json格式）',
  `store_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  UNIQUE KEY `unique_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#小程序代码模板 - 管理员#
DROP TABLE IF EXISTS `hema_template`;
CREATE TABLE `hema_template` (
  `template_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '模板ID',
  `app_type` varchar(50) NOT NULL DEFAULT '' COMMENT '模板类型',
  `id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '服务端ID',
  `user_version` varchar(255) NOT NULL DEFAULT '' COMMENT '版本号',
  `user_desc` varchar(255) NOT NULL DEFAULT '' COMMENT '版本描述',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#小程序名称表#
DROP TABLE IF EXISTS `hema_wxapp_name`;
CREATE TABLE `hema_wxapp_name` (
  `wxapp_name_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `nick_name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `audit_id` varchar(50) NOT NULL DEFAULT '' COMMENT '审核编号',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态(0审核中，1设置成功 ，2被驳回)',
  `reason` varchar(255) NOT NULL DEFAULT '' COMMENT '驳回原因',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`wxapp_name_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;


#订单评价表#
DROP TABLE IF EXISTS `hema_comment`;
CREATE TABLE `hema_comment` (
  `comment_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '模板ID',
  `serve` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '服务评分',
  `speed` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '速度评分',
  `flavor` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '服务评分',
  `ambient` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '服务评分',
  `content` varchar(255) NOT NULL DEFAULT '这家伙很懒，什么都没写' COMMENT '详情',
  `reply` varchar(255) NOT NULL DEFAULT '' COMMENT '回复详情',
  `is_show` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否显示(0隐藏，1显示)',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `store_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '平台用户id',
  `order_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单id',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#充值套餐#
DROP TABLE IF EXISTS `hema_recharge_plan`;
CREATE TABLE `hema_recharge_plan` (
  `recharge_plan_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '充值套餐id',
  `plan_name` varchar(50) NOT NULL DEFAULT '' COMMENT '套餐名称',
  `money` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值金额',
  `gift_money` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '赠送金额',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`recharge_plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#充值记录#
DROP TABLE IF EXISTS `hema_recharge`;
CREATE TABLE `hema_recharge` (
  `recharge_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '充值id',
  `mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '充值方式(10自助充值 20后台充值)',
  `order_no` varchar(20) NOT NULL DEFAULT '' COMMENT '订单号',
  `money` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值金额',
  `gift_money` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '赠送金额',
  `recharge_plan_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '套餐ID',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `pay_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '支付状态(10待付款 20已付款)',
  `pay_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '支付时间',
  `transaction_id` varchar(30) NOT NULL DEFAULT '' COMMENT '微信支付交易号',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`recharge_id`),
  UNIQUE KEY `order_no` (`order_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#优惠活动#
DROP TABLE IF EXISTS `hema_activity`;
CREATE TABLE `hema_activity` (
  `activity_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '优惠活动id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `color` varchar(50) NOT NULL DEFAULT 'warning' COMMENT '显示颜色',
  `activity_type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '类型',
  `reduce_price` int(11) NOT NULL DEFAULT '0' COMMENT '减免金额',
  `min_price` int(11) NOT NULL DEFAULT '0' COMMENT '最低消费',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;


#云打印机表#
DROP TABLE IF EXISTS `hema_printer`;
CREATE TABLE `hema_printer` (
  `printer_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '打印机ID',
  `brand` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '品牌(10对对机 20飞鹅 30易联云)',
  `model` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '型号',
  `printer_name` varchar(50) NOT NULL DEFAULT '' COMMENT '打印机名称',
  `open_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '云平台唯一编号',
  `uuid` varchar(50) NOT NULL DEFAULT '' COMMENT '设备编号',
  `key` varchar(50) NOT NULL DEFAULT '' COMMENT '设备密钥',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '流量卡号码',
  `is_open` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '开关(0关闭 1开启)',
  `printer_num` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '打印份数',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`printer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#打印失败任务表#
DROP TABLE IF EXISTS `hema_printer_log`;
CREATE TABLE `hema_printer_log` (
  `printer_log_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '打印机ID',
  `open_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '云平台唯一编号',
  `uuid` varchar(50) NOT NULL DEFAULT '' COMMENT '设备编号',
  `tpl` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '模板类型(0订单 1退款)',
  `content` longtext COMMENT '订单详情',
  `reason` varchar(100) NOT NULL DEFAULT '' COMMENT '打印失败的原因',
  `order_no` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单号',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`printer_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#餐桌/包间表#
DROP TABLE IF EXISTS `hema_table`;
CREATE TABLE `hema_table` (
  `table_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '餐桌id',
  `table_name` varchar(50) NOT NULL DEFAULT '' COMMENT '餐桌名称',
  `people` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '容纳人数',
  `consume` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '最低消费',
  `ware_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '餐具调料费',
  `row_no` varchar(10) NOT NULL DEFAULT '' COMMENT '餐桌编号',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '状态(10空闲 20忙碌 30预定)',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店ID',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;


#店员表#
DROP TABLE IF EXISTS `hema_shop_clerk`;
CREATE TABLE `hema_shop_clerk` (
  `shop_clerk_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店ID',
  `real_name` varchar(20) NOT NULL DEFAULT '' COMMENT '店员姓名',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  `pwd` varchar(50) NOT NULL DEFAULT '' COMMENT '密码',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '身份(10店员 20店长 30 配送)',
  `store_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`shop_clerk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;


#商品分类表#
DROP TABLE IF EXISTS `hema_category`;
CREATE TABLE `hema_category` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品分类id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '分类名称',
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类id',
  `image_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '分类图片id',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序方式(数字越小越靠前)',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#产品表#
DROP TABLE IF EXISTS `hema_goods`;
CREATE TABLE `hema_goods` (
  `goods_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `goods_name` varchar(255) NOT NULL DEFAULT '' COMMENT '商品名称',
  `selling_point` varchar(255) NOT NULL DEFAULT '' COMMENT '商品卖点',
  `category_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品分类id',
  `spec_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '商品规格(10单规格 20多规格)',
  `deduct_stock_type` tinyint(3) unsigned NOT NULL DEFAULT '20' COMMENT '库存计算方式(10下单减库存 20付款减库存)',
  `content` longtext NOT NULL COMMENT '商品详情',
  `sales_initial` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '初始销量',
  `sales_actual` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '实际销量',
  `goods_sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '商品排序(数字越小越靠前)',
  `goods_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '商品状态(10上架 20下架)',
  `pack_price` decimal(10,2) unsigned NOT NULL DEFAULT '1.00' COMMENT '餐盒费用',
  `is_delete` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除',
  `is_recommend` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否推荐',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`goods_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#产品图片表#
DROP TABLE IF EXISTS `hema_goods_image`;
CREATE TABLE `hema_goods_image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品id',
  `image_id` int(11) NOT NULL COMMENT '图片id(关联文件记录表)',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#产品规格表#
DROP TABLE IF EXISTS `hema_goods_spec`;
CREATE TABLE `hema_goods_spec` (
  `goods_spec_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品规格id',
  `spec_sku_id` varchar(255) NOT NULL DEFAULT '' COMMENT '商品spu标识',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品id',
  `image_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '多规格商品id',
  `goods_no` varchar(100) NOT NULL DEFAULT '' COMMENT '商品编码',
  `goods_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '店内售价',
  `line_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '商品划线价',
  `stock_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '当前库存数量',
  `goods_weight` double unsigned NOT NULL DEFAULT '0' COMMENT '商品重量(Kg)',
  `goods_sales` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品销量',
  `seckill_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '秒杀价格',
  `seckill_stock_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '秒杀库存',
  `group_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '拼团价格',
  `group_stock_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '拼团库存',
  `bargain_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '砍价价格',
  `bargain_stock_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '砍价库存',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`goods_spec_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#产品规格属性表#
DROP TABLE IF EXISTS `hema_goods_spec_rel`;
CREATE TABLE `hema_goods_spec_rel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品id',
  `spec_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '规格组id',
  `spec_value_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '规格值id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#规格表#
DROP TABLE IF EXISTS `hema_spec`;
CREATE TABLE `hema_spec` (
  `spec_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '规格组id',
  `spec_name` varchar(255) NOT NULL DEFAULT '' COMMENT '规格组名称',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`spec_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#规格属性表#
DROP TABLE IF EXISTS `hema_spec_value`;
CREATE TABLE `hema_spec_value` (
  `spec_value_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '规格值id',
  `spec_value` varchar(255) NOT NULL COMMENT '规格值',
  `spec_id` int(11) NOT NULL COMMENT '规格组id',
  `wxapp_id` int(11) NOT NULL COMMENT '小程序id',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`spec_value_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#订单表#
DROP TABLE IF EXISTS `hema_order`;
CREATE TABLE `hema_order` (
  `order_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `order_no` varchar(20) NOT NULL DEFAULT '' COMMENT '订单号',
  `order_mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '订单类型(10堂食 20自取 30外卖)',
  `row_no` varchar(10) NOT NULL DEFAULT '' COMMENT '订单排号',
  `table_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '餐桌/包间id',
  `people` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '就餐人数',
  `arrive_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '自取到店时间',
  `flavor` varchar(255) NOT NULL DEFAULT '' COMMENT '口味选项',
  `message` varchar(255) NOT NULL DEFAULT '' COMMENT '买家留言',
  `total_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '商品总金额',
  `activity_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '优惠价格',
  `express_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '配送费用',
  `pack_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '餐盒费用',
  `ware_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '餐具调料费',
  `pay_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '订单实付款金额',
  `pay_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '付款状态(10未付款 20已付款)',
  `transaction_id` varchar(100) NOT NULL DEFAULT '' COMMENT '微信支付交易号',
  `pay_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '付款时间',
  `shop_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '商家状态(10未接单 20已接单)',
  `shop_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '接单时间',
  `delivery_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '配送状态(10未配送 20配送中 30已配送)',
  `delivery_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '配送时间',
  `receipt_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '收货状态(10未收货 20已收货)',
  `receipt_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '收货时间',
  `order_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '订单状态(10进行中 20取消 30已完成)',
  `refund_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '退款金额',
  `refund_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '退款状态(10退款中 20已退款)',
  `refund_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '退款时间',
  `refund_desc` varchar(255) NOT NULL DEFAULT '' COMMENT '退款理由',
  `refund_id` varchar(100) NOT NULL DEFAULT '' COMMENT '微信退款单号',
  `commission` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '我的佣金',
  `is_cmt` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否评价(0待评价 1已评价)',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `store_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '平台用户id',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `order_no` (`order_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#订单表配送表#
DROP TABLE IF EXISTS `hema_order_delivery`;
CREATE TABLE `hema_order_delivery` (
  `order_delivery_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号id',
  `company` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '配送公司(10自配 20顺丰 30达达 40UU)',
  `order_no` varchar(50) NOT NULL DEFAULT '' COMMENT '配送单号',
  `distance` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '配送距离',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '配送费用',
  `linkman` varchar(50) NOT NULL DEFAULT '' COMMENT '骑手姓名',
  `phone` varchar(50) NOT NULL DEFAULT '' COMMENT '骑手电话',
  `delivery_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '配送状态(10待骑手接单 20骑手正赶往商家 30骑手已到店 40骑手开始配送 50骑手已送达)',
  `delivery_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '配送时间',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '订单状态(10进行中 20被取消 30已完成)',
  `order_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单号',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`order_delivery_id`),
  UNIQUE KEY `order_no` (`order_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;


#订单收货地址表#
DROP TABLE IF EXISTS `hema_order_address`;
CREATE TABLE `hema_order_address` (
  `order_address_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '地址id',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '联系电话',
  `location` varchar(50) NOT NULL DEFAULT '' COMMENT '位置坐标',
  `province` varchar(20) NOT NULL DEFAULT '' COMMENT '所在省份',
  `city` varchar(20) NOT NULL DEFAULT '' COMMENT '所在城市',
  `district` varchar(20) NOT NULL DEFAULT '' COMMENT '所在区/县',
  `detail` varchar(255) NOT NULL DEFAULT '' COMMENT '详细地址',
  `order_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单id',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `store_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '平台用户id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`order_address_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#订单产品表#
DROP TABLE IF EXISTS `hema_order_goods`;
CREATE TABLE `hema_order_goods` (
  `order_goods_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品id',
  `goods_name` varchar(255) NOT NULL DEFAULT '' COMMENT '商品名称',
  `image_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品封面图id',
  `deduct_stock_type` tinyint(3) unsigned NOT NULL DEFAULT '20' COMMENT '库存计算方式(10下单减库存 20付款减库存)',
  `spec_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '规格类型(10单规格 20多规格)',
  `spec_sku_id` varchar(255) NOT NULL DEFAULT '' COMMENT '商品sku标识',
  `goods_spec_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品规格id',
  `goods_attr` varchar(500) NOT NULL DEFAULT '' COMMENT '商品规格信息',
  `content` longtext NOT NULL COMMENT '商品详情',
  `goods_no` varchar(100) NOT NULL DEFAULT '' COMMENT '商品编码',
  `goods_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '商品售价',
  `line_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '商品划线价',
  `goods_weight` double unsigned NOT NULL DEFAULT '0' COMMENT '商品重量(Kg)',
  `total_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '购买数量',
  `total_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '商品总价',
  `refund_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '退单数量',
  `refund_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '退单总价',
  `order_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单id',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `store_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '平台用户id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`order_goods_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#站点配置表#
DROP TABLE IF EXISTS `hema_setting`;
CREATE TABLE `hema_setting` (
  `key` varchar(30) NOT NULL COMMENT '设置项标示',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '设置项描述',
  `values` mediumtext NOT NULL COMMENT '设置内容（json格式）',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  UNIQUE KEY `unique_key` (`key`,`wxapp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#上传图片表#
DROP TABLE IF EXISTS `hema_upload_file`;
CREATE TABLE `hema_upload_file` (
  `file_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件id',
  `storage` varchar(20) NOT NULL DEFAULT '' COMMENT '存储方式',
  `group_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文件分组id',
  `file_url` varchar(255) NOT NULL DEFAULT '' COMMENT '存储域名',
  `file_name` varchar(255) NOT NULL DEFAULT '' COMMENT '文件路径',
  `file_size` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小(字节)',
  `file_type` varchar(20) NOT NULL DEFAULT '' COMMENT '文件类型',
  `extension` varchar(20) NOT NULL DEFAULT '' COMMENT '文件扩展名',
  `is_delete` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '软删除',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `path_idx` (`file_name`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#站图片分组表#
DROP TABLE IF EXISTS `hema_upload_group`;
CREATE TABLE `hema_upload_group` (
  `group_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `group_type` varchar(10) NOT NULL DEFAULT '' COMMENT '文件类型',
  `group_name` varchar(30) NOT NULL DEFAULT '' COMMENT '分类名称',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '分类排序(数字越小越靠前)',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`group_id`),
  KEY `type_index` (`group_type`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#用户表#
DROP TABLE IF EXISTS `hema_user`;
CREATE TABLE `hema_user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `union_id` varchar(255) NOT NULL DEFAULT '' COMMENT '开放平台唯一标示',
  `open_id` varchar(255) NOT NULL DEFAULT '' COMMENT '微信openid(唯一标示)',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '用户类型 10小程序，20公众号',
  `nickName` varchar(255) NOT NULL DEFAULT '' COMMENT '微信昵称',
  `avatarUrl` varchar(255) NOT NULL DEFAULT '' COMMENT '微信头像',
  `gender` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '性别',
  `country` varchar(50) NOT NULL DEFAULT '' COMMENT '国家',
  `province` varchar(50) NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(50) NOT NULL DEFAULT '' COMMENT '城市',
  `subscribe` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否关注公众号1=关注，0=取消关注',
  `address_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '默认收货地址',
  `user_grade_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户等级id',
  `wallet` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '钱包余额',
  `pay` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '消费金额',
  `score` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户积分',
  `mobile` varchar(15) NOT NULL DEFAULT '' COMMENT '手机号码',
  `commission` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '我的佣金',
  `recommender` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '推荐人',
  `is_dealer` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否是分销商1=是，0=不是',
  `login_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`user_id`),
  KEY `openid` (`open_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#用户地址表#
DROP TABLE IF EXISTS `hema_user_address`;
CREATE TABLE `hema_user_address` (
  `address_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '联系电话',
  `location` varchar(50) NOT NULL DEFAULT '' COMMENT '位置坐标',
  `province` varchar(20) NOT NULL DEFAULT '' COMMENT '所在省份',
  `city` varchar(20) NOT NULL DEFAULT '' COMMENT '所在城市',
  `district` varchar(20) NOT NULL DEFAULT '' COMMENT '所在区/县',
  `detail` varchar(255) NOT NULL DEFAULT '' COMMENT '详细地址',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `store_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '平台用户id',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#商家帮助文档#
DROP TABLE IF EXISTS `hema_wxapp_help`;
CREATE TABLE `hema_wxapp_help` (
  `help_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '帮助标题',
  `content` text NOT NULL COMMENT '帮助内容',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序(数字越小越靠前)',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`help_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#门店表#
DROP TABLE IF EXISTS `hema_shop`;
CREATE TABLE `hema_shop` (
  `shop_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `shop_name` varchar(50) NOT NULL DEFAULT '' COMMENT '门店名称',
  `shop_category_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店分类',
  `logo_image_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店logo',
  `front_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店照片',
  `linkman` varchar(20) NOT NULL DEFAULT '' COMMENT '联系人',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '联系电话',
  `shop_hours` varchar(20) NOT NULL DEFAULT '' COMMENT '营业时间',
  `province` varchar(20) NOT NULL DEFAULT '' COMMENT '所在省份',
  `city` varchar(20) NOT NULL DEFAULT '' COMMENT '所在城市',
  `district` varchar(20) NOT NULL DEFAULT '' COMMENT '所在区/县',
  `address` varchar(50) NOT NULL DEFAULT '' COMMENT '详细地址',
  `coordinate` varchar(50) NOT NULL DEFAULT '' COMMENT '门店坐标',
  `poi_id` varchar(50) NOT NULL DEFAULT '' COMMENT '位置识别码',
  `summary` varchar(255) NOT NULL DEFAULT '' COMMENT '门店简介',
  `tang_mode` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '堂食模式(1选桌 2排号)',
  `is_order` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '自动接单(0关闭 1开启)',
  `is_delivery` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '自动配送(0关闭 1开启)',
  `delivery_id` int(11) unsigned NOT NULL DEFAULT '10' COMMENT '配送方ID',
  `is_grab` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '骑手抢单(0关闭 1开启)',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '门店状态(1开启 0关闭)',
  `out_show` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '平台状态(1开启 0关闭)',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序(数字越小越靠前)',
  `sf_shop_id` varchar(20) NOT NULL DEFAULT '' COMMENT '顺丰门店ID',
  `dada_shop_id` varchar(20) NOT NULL DEFAULT '' COMMENT '达达门店ID',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#商户管理员表#
DROP TABLE IF EXISTS `hema_store_user`;
CREATE TABLE `hema_store_user` (
  `store_user_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `union_id` varchar(50) NOT NULL DEFAULT '' COMMENT '开放平台识别号',
  `wechat_open_id` varchar(50) NOT NULL DEFAULT '' COMMENT '公众号openid(唯一标识)',
  `wxapp_open_id` varchar(50) NOT NULL DEFAULT '' COMMENT '助手小程序openid(唯一标识)',
  `out_open_id` varchar(50) NOT NULL DEFAULT '' COMMENT '外卖小程序openid(唯一标识)',
  `web_open_id` varchar(50) NOT NULL DEFAULT '' COMMENT '网站openid(唯一标识)',
  `user_name` varchar(255) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '登录密码',
  `linkman` varchar(20) NOT NULL DEFAULT '' COMMENT '联系人',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '联系电话',
  `wallet` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '钱包余额',
  `nickName` varchar(100) NOT NULL DEFAULT '' COMMENT '微信昵称',
  `avatarUrl` varchar(255) NOT NULL DEFAULT '' COMMENT '微信头像',
  `gender` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '性别',
  `country` varchar(50) NOT NULL DEFAULT '' COMMENT '国家',
  `province` varchar(50) NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(50) NOT NULL DEFAULT '' COMMENT '城市',
  `address_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '默认收货地址',
  `recommender` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '推荐人',
  `is_agent` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '身份 0用户 1代理 2管理',
  `agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属代理',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`store_user_id`),
  UNIQUE KEY `union_id` (`union_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10002 DEFAULT CHARSET=utf8;
INSERT INTO `hema_store_user` VALUES (10001,'','','','','','admin','7d95a09138d667b108b1252fea5ae15d','','',0,'','',0,'','','',0,0,2,0,'1529926348','1529926348');

#微信小程序表#
DROP TABLE IF EXISTS `hema_wxapp`;
CREATE TABLE `hema_wxapp` (
  `wxapp_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '小程序id',
  `app_type` varchar(50) NOT NULL DEFAULT '' COMMENT '应用类型',
  `shop_mode` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '门店模式（10单门店 20多门店）',
  `source` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '账号来源，10=自助注册，20=平台注册',
  `is_empower` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否授权(0否 1是)',
  `app_name` varchar(50) NOT NULL DEFAULT '' COMMENT '小程序名称',
  `head_img` varchar(255) NOT NULL DEFAULT '' COMMENT '小程序头像',
  `qrcode_url` varchar(255) NOT NULL DEFAULT '' COMMENT '小程序二维码',
  `user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '原始ID',
  `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT '小程序AppID',
  `principal_name` varchar(255) NOT NULL DEFAULT '' COMMENT '主体名称',
  `api_domain` varchar(255) NOT NULL DEFAULT '' COMMENT '服务器域名',
  `webview_domain` varchar(255) NOT NULL DEFAULT '' COMMENT '业务域名',
  `signature` varchar(255) NOT NULL DEFAULT '' COMMENT '功能介绍',
  `is_collection` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '收藏提醒(0关闭 1开启)',
  `is_live` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否开启直播插件 1开启，0关闭',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '商家客服电话',
  `access_token` varchar(255) NOT NULL DEFAULT '' COMMENT '令牌凭证',
  `expires_in` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '令牌到期时间',
  `authorizer_refresh_token` varchar(255) NOT NULL DEFAULT '' COMMENT '刷新令牌',
  `template` int(11) NOT NULL DEFAULT '0' COMMENT '模板ID',
  `category_style` tinyint(3) unsigned NOT NULL DEFAULT '30' COMMENT '分类样式',
  `is_copyright` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '自定义版权(0关闭 1开启)',
  `copyright` varchar(50) NOT NULL DEFAULT '' COMMENT '小程序底部版权',
  `is_test` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '是否试用',
  `store_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商户ID',
  `agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属代理',
  `expire_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序到期时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`wxapp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#小程序页面设计表#
DROP TABLE IF EXISTS `hema_wxapp_page`;
CREATE TABLE `hema_wxapp_page` (
  `page_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '页面id',
  `page_type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '页面类型(10首页 20自定义页)',
  `page_data` longtext NOT NULL COMMENT '页面数据',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '微信小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#用户等级表#
DROP TABLE IF EXISTS `hema_user_grade`;
CREATE TABLE `hema_user_grade` (
  `user_grade_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户等级id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '等级名称',
  `score` int(11) NOT NULL DEFAULT '0' COMMENT '所需积分',
  `discount` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '享受折扣',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序(数字越小越靠前)',
  `wxapp_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`user_grade_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

#微信错误反码表#
DROP TABLE IF EXISTS `hema_wechat_err`;
CREATE TABLE `hema_wechat_err` (
  `errcode` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '错误编码',
  `errmsg` varchar(255) NOT NULL DEFAULT '' COMMENT '错误说明',
  PRIMARY KEY (`errcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信错误反码表';

INSERT INTO `hema_wechat_err` VALUES (1,'未创建直播间');

INSERT INTO `hema_wechat_err` VALUES (1003,'POST参数非法');

INSERT INTO `hema_wechat_err` VALUES (10013,'占用名字过多');
INSERT INTO `hema_wechat_err` VALUES (10014,'+号规则 同一类型关联名主体不一致');
INSERT INTO `hema_wechat_err` VALUES (10015,'原始名不同类型主体不一致');
INSERT INTO `hema_wechat_err` VALUES (10016,'名称占用者 ≥2');
INSERT INTO `hema_wechat_err` VALUES (10017,'+号规则 不同类型关联名主体不一致');

INSERT INTO `hema_wechat_err` VALUES (20002,'商品id不存在');

INSERT INTO `hema_wechat_err` VALUES (40001,'不合法的调用凭证');
INSERT INTO `hema_wechat_err` VALUES (40002,'不合法的 grant_type');
INSERT INTO `hema_wechat_err` VALUES (40003,'不合法的 OpenID');
INSERT INTO `hema_wechat_err` VALUES (40004,'不合法的媒体文件类型');
INSERT INTO `hema_wechat_err` VALUES (40005,'不合法的文件类型');
INSERT INTO `hema_wechat_err` VALUES (40006,'不合法的文件大小');
INSERT INTO `hema_wechat_err` VALUES (40007,'不合法的 media_id');
INSERT INTO `hema_wechat_err` VALUES (40008,'不合法的 message_type');
INSERT INTO `hema_wechat_err` VALUES (40009,'不合法的图片大小');
INSERT INTO `hema_wechat_err` VALUES (40010,'不合法的语音大小');
INSERT INTO `hema_wechat_err` VALUES (40011,'不合法的视频大小');
INSERT INTO `hema_wechat_err` VALUES (40012,'不合法的缩略图大小');
INSERT INTO `hema_wechat_err` VALUES (40013,'不合法的 AppID');
INSERT INTO `hema_wechat_err` VALUES (40014,'不合法的 access_token');
INSERT INTO `hema_wechat_err` VALUES (40015,'不合法的菜单类型');
INSERT INTO `hema_wechat_err` VALUES (40016,'不合法的菜单按钮个数');
INSERT INTO `hema_wechat_err` VALUES (40017,'不合法的按钮类型');
INSERT INTO `hema_wechat_err` VALUES (40018,'不合法的按钮名称长度');
INSERT INTO `hema_wechat_err` VALUES (40019,'不合法的按钮KEY长度');
INSERT INTO `hema_wechat_err` VALUES (40020,'不合法的 url 长度');
INSERT INTO `hema_wechat_err` VALUES (40021,'不合法的菜单版本号');
INSERT INTO `hema_wechat_err` VALUES (40022,'不合法的子菜单级数');
INSERT INTO `hema_wechat_err` VALUES (40023,'不合法的子菜单按钮个数');
INSERT INTO `hema_wechat_err` VALUES (40024,'不合法的子菜单类型');
INSERT INTO `hema_wechat_err` VALUES (40025,'不合法的子菜单按钮名称长度');
INSERT INTO `hema_wechat_err` VALUES (40026,'不合法的子菜单按钮 KEY 长度');
INSERT INTO `hema_wechat_err` VALUES (40027,'不合法的子菜单按钮 url 长度');
INSERT INTO `hema_wechat_err` VALUES (40028,'不合法的自定义菜单使用用户');
INSERT INTO `hema_wechat_err` VALUES (40029,'不合法或已过期的 code');
INSERT INTO `hema_wechat_err` VALUES (40030,'不合法的 refresh_token');
INSERT INTO `hema_wechat_err` VALUES (40031,'不合法的 openid 列表');
INSERT INTO `hema_wechat_err` VALUES (40032,'不合法的 openid 列表长度');
INSERT INTO `hema_wechat_err` VALUES (40033,'不合法的请求字符，不能包含\uxxxx格式的字符');
INSERT INTO `hema_wechat_err` VALUES (40034,'模板大小无效');
INSERT INTO `hema_wechat_err` VALUES (40035,'不合法的参数');
INSERT INTO `hema_wechat_err` VALUES (40036,'不合法的 template_id 长度');
INSERT INTO `hema_wechat_err` VALUES (40037,'不合法的 template_id');
INSERT INTO `hema_wechat_err` VALUES (40038,'不合法的请求格式');
INSERT INTO `hema_wechat_err` VALUES (40039,'不合法的 URL 长度');
INSERT INTO `hema_wechat_err` VALUES (40040,'插件token无效');
INSERT INTO `hema_wechat_err` VALUES (40041,'插件id无效');
INSERT INTO `hema_wechat_err` VALUES (40042,'插件session无效');
INSERT INTO `hema_wechat_err` VALUES (40043,'fav类型无效');
INSERT INTO `hema_wechat_err` VALUES (40044,'无效链接标题');
INSERT INTO `hema_wechat_err` VALUES (40045,'无效链接说明');
INSERT INTO `hema_wechat_err` VALUES (40046,'无效链接图标');
INSERT INTO `hema_wechat_err` VALUES (40047,'无效链接长度');
INSERT INTO `hema_wechat_err` VALUES (40048,'不合法的 url 域名');
INSERT INTO `hema_wechat_err` VALUES (40049,'分数报告类型无效');
INSERT INTO `hema_wechat_err` VALUES (40050,'不合法的分组 id');
INSERT INTO `hema_wechat_err` VALUES (40051,'分组名字不合法');
INSERT INTO `hema_wechat_err` VALUES (40052,'操作名称无效');
INSERT INTO `hema_wechat_err` VALUES (40053,'操作信息无效，请检查文档');
INSERT INTO `hema_wechat_err` VALUES (40054,'不合法的子菜单按钮 url 域名');
INSERT INTO `hema_wechat_err` VALUES (40055,'不合法的菜单按钮 url 域名');
INSERT INTO `hema_wechat_err` VALUES (40056,'无效序列码');
INSERT INTO `hema_wechat_err` VALUES (40057,'无效的tabbar大小');
INSERT INTO `hema_wechat_err` VALUES (40058,'无效的tabbar名字大小');
INSERT INTO `hema_wechat_err` VALUES (40059,'消息id无效');
INSERT INTO `hema_wechat_err` VALUES (40060,'删除单篇图文时，指定的 article_idx 不合法');

INSERT INTO `hema_wechat_err` VALUES (40062,'无效的标题大小');
INSERT INTO `hema_wechat_err` VALUES (40063,'无效的消息扩展名大小');
INSERT INTO `hema_wechat_err` VALUES (40064,'应用程序类型无效');
INSERT INTO `hema_wechat_err` VALUES (40065,'无效的消息状态');
INSERT INTO `hema_wechat_err` VALUES (40066,'不合法的 url');
INSERT INTO `hema_wechat_err` VALUES (40067,'无效的tvid');
INSERT INTO `hema_wechat_err` VALUES (40068,'包含邮件url');
INSERT INTO `hema_wechat_err` VALUES (40069,'无效的硬件类型');
INSERT INTO `hema_wechat_err` VALUES (40070,'sku信息无效');
INSERT INTO `hema_wechat_err` VALUES (40071,'无效的卡类型');
INSERT INTO `hema_wechat_err` VALUES (40072,'位置id无效');
INSERT INTO `hema_wechat_err` VALUES (40073,'无效的卡id');
INSERT INTO `hema_wechat_err` VALUES (40074,'支付模板id无效');
INSERT INTO `hema_wechat_err` VALUES (40075,'无效的加密代码');
INSERT INTO `hema_wechat_err` VALUES (40076,'无效的颜色id');
INSERT INTO `hema_wechat_err` VALUES (40077,'分数类型无效');
INSERT INTO `hema_wechat_err` VALUES (40078,'无效的卡状态');
INSERT INTO `hema_wechat_err` VALUES (40079,'无效时间');
INSERT INTO `hema_wechat_err` VALUES (40080,'无效的卡扩展名');
INSERT INTO `hema_wechat_err` VALUES (40081,'模板id无效');
INSERT INTO `hema_wechat_err` VALUES (40082,'无效的横幅图片大小');
INSERT INTO `hema_wechat_err` VALUES (40083,'无效的横幅url大小');
INSERT INTO `hema_wechat_err` VALUES (40084,'无效的按钮描述大小');
INSERT INTO `hema_wechat_err` VALUES (40085,'无效的按钮url大小');
INSERT INTO `hema_wechat_err` VALUES (40086,'sharelink徽标大小无效');
INSERT INTO `hema_wechat_err` VALUES (40087,'sharelink描述大小无效');
INSERT INTO `hema_wechat_err` VALUES (40088,'sharelink标题大小无效');
INSERT INTO `hema_wechat_err` VALUES (40089,'无效的平台id');
INSERT INTO `hema_wechat_err` VALUES (40090,'无效的请求源（错误的客户端ip）');
INSERT INTO `hema_wechat_err` VALUES (40091,'无效的组件票证');
INSERT INTO `hema_wechat_err` VALUES (40092,'无效的备注名称');
INSERT INTO `hema_wechat_err` VALUES (40093,'请检查json中所需的字段');
INSERT INTO `hema_wechat_err` VALUES (40094,'无效的组件凭据');
INSERT INTO `hema_wechat_err` VALUES (40095,'调用者来源错误');
INSERT INTO `hema_wechat_err` VALUES (40096,'无效的biztype');
INSERT INTO `hema_wechat_err` VALUES (40097,'参数错误');
INSERT INTO `hema_wechat_err` VALUES (40098,'poiid无效');
INSERT INTO `hema_wechat_err` VALUES (40099,'无效代码，此代码已被消耗。');
INSERT INTO `hema_wechat_err` VALUES (40100,'日期信息无效');



INSERT INTO `hema_wechat_err` VALUES (40117,'分组名字不合法');
INSERT INTO `hema_wechat_err` VALUES (40118,'media_id 大小不合法');
INSERT INTO `hema_wechat_err` VALUES (40119,'button 类型错误');
INSERT INTO `hema_wechat_err` VALUES (40120,'子 button 类型错误');
INSERT INTO `hema_wechat_err` VALUES (40121,'不合法的 media_id 类型');

INSERT INTO `hema_wechat_err` VALUES (40125,'无效的appsecret');

INSERT INTO `hema_wechat_err` VALUES (40132,'微信号不合法');

INSERT INTO `hema_wechat_err` VALUES (40137,'不支持的图片格式');

INSERT INTO `hema_wechat_err` VALUES (40155,'请勿添加其他公众号的主页链接');

INSERT INTO `hema_wechat_err` VALUES (40163,'oauth_code已使用');

INSERT INTO `hema_wechat_err` VALUES (41001,'缺失 access_token 参数');
INSERT INTO `hema_wechat_err` VALUES (41002,'缺失 appid 参数');
INSERT INTO `hema_wechat_err` VALUES (41003,'缺失 refresh_token 参数');
INSERT INTO `hema_wechat_err` VALUES (41004,'缺失 secret 参数');
INSERT INTO `hema_wechat_err` VALUES (41005,'缺失二进制媒体文件');
INSERT INTO `hema_wechat_err` VALUES (41006,'缺失 media_id 参数');
INSERT INTO `hema_wechat_err` VALUES (41007,'缺失子菜单数据');
INSERT INTO `hema_wechat_err` VALUES (41008,'缺失 code 参数');
INSERT INTO `hema_wechat_err` VALUES (41009,'缺失 openid 参数');
INSERT INTO `hema_wechat_err` VALUES (41010,'缺失 url 参数');

INSERT INTO `hema_wechat_err` VALUES (42001,'access_token 超时');
INSERT INTO `hema_wechat_err` VALUES (42002,'refresh_token 超时');
INSERT INTO `hema_wechat_err` VALUES (42003,'code 超时');

INSERT INTO `hema_wechat_err` VALUES (42007,'用户修改微信密码， accesstoken 和 refreshtoken 失效，需要重新授权');

INSERT INTO `hema_wechat_err` VALUES (43001,'需要使用 GET 方法请求');
INSERT INTO `hema_wechat_err` VALUES (43002,'需要使用 POST 方法请求');
INSERT INTO `hema_wechat_err` VALUES (43003,'需要使用 HTTPS');
INSERT INTO `hema_wechat_err` VALUES (43004,'需要订阅关系');
INSERT INTO `hema_wechat_err` VALUES (43005,'需要好友关系');

INSERT INTO `hema_wechat_err` VALUES (43019,'需要将接收者从黑名单中移除');

INSERT INTO `hema_wechat_err` VALUES (44001,'空白的二进制数据');
INSERT INTO `hema_wechat_err` VALUES (44002,'空白的 POST 数据');
INSERT INTO `hema_wechat_err` VALUES (44003,'空白的 news 数据');
INSERT INTO `hema_wechat_err` VALUES (44004,'空白的内容');
INSERT INTO `hema_wechat_err` VALUES (44005,'空白的列表');

INSERT INTO `hema_wechat_err` VALUES (45001,'二进制文件超过限制');
INSERT INTO `hema_wechat_err` VALUES (45002,'content 参数超过限制');
INSERT INTO `hema_wechat_err` VALUES (45003,'title 参数超过限制');
INSERT INTO `hema_wechat_err` VALUES (45004,'description 参数超过限制');
INSERT INTO `hema_wechat_err` VALUES (45005,'url 参数长度超过限制');
INSERT INTO `hema_wechat_err` VALUES (45006,'picurl 参数超过限制');
INSERT INTO `hema_wechat_err` VALUES (45007,'播放时间超过限制（语音为 60s 最大）');
INSERT INTO `hema_wechat_err` VALUES (45008,'article 参数超过限制');
INSERT INTO `hema_wechat_err` VALUES (45009,'接口调动频率超过限制');
INSERT INTO `hema_wechat_err` VALUES (45010,'建立菜单被限制');
INSERT INTO `hema_wechat_err` VALUES (45011,'频率限制');
INSERT INTO `hema_wechat_err` VALUES (45012,'模板大小超过限制');

INSERT INTO `hema_wechat_err` VALUES (45015,'回复时间超过限制');
INSERT INTO `hema_wechat_err` VALUES (45016,'不能修改默认组');
INSERT INTO `hema_wechat_err` VALUES (45017,'修改组名过长');
INSERT INTO `hema_wechat_err` VALUES (45018,'组数量过多');

INSERT INTO `hema_wechat_err` VALUES (45047,'客服接口下行条数超过上限');

INSERT INTO `hema_wechat_err` VALUES (45064,'创建菜单包含未关联的小程序');
INSERT INTO `hema_wechat_err` VALUES (45065,'相同 clientmsgid 已存在群发记录，返回数据中带有已存在的群发任务的 msgid');
INSERT INTO `hema_wechat_err` VALUES (45066,'相同 clientmsgid 重试速度过快，请间隔1分钟重试');
INSERT INTO `hema_wechat_err` VALUES (45067,'clientmsgid 长度超过限制');

INSERT INTO `hema_wechat_err` VALUES (46001,'不存在媒体数据');
INSERT INTO `hema_wechat_err` VALUES (46002,'不存在的菜单版本');
INSERT INTO `hema_wechat_err` VALUES (46003,'不存在的菜单数据');
INSERT INTO `hema_wechat_err` VALUES (46004,'不存在的用户');

INSERT INTO `hema_wechat_err` VALUES (47001,'解析 JSON/XML 内容错误');

INSERT INTO `hema_wechat_err` VALUES (48001,'api 功能未授权，请确认公众号已获得该接口，可以在公众平台官网 - 开发者中心页中查看接口权限');
INSERT INTO `hema_wechat_err` VALUES (48002,'粉丝拒收消息（粉丝在公众号选项中，关闭了 “ 接收消息 ” ）');

INSERT INTO `hema_wechat_err` VALUES (48004,'api 接口被封禁，请登录 mp.weixin.qq.com 查看详情');
INSERT INTO `hema_wechat_err` VALUES (48005,'api 禁止删除被自动回复和自定义菜单引用的素材');
INSERT INTO `hema_wechat_err` VALUES (48006,'api 禁止清零调用次数，因为清零次数达到上限');

INSERT INTO `hema_wechat_err` VALUES (48008,'没有该类型消息的发送权限');

INSERT INTO `hema_wechat_err` VALUES (50001,'接口未授权');
INSERT INTO `hema_wechat_err` VALUES (50002,'用户受限，可能是违规后接口被封禁');

INSERT INTO `hema_wechat_err` VALUES (50005,'用户未关注公众号');

INSERT INTO `hema_wechat_err` VALUES (53010,'名称格式不合法');
INSERT INTO `hema_wechat_err` VALUES (53011,'名称检测命中频率限制');
INSERT INTO `hema_wechat_err` VALUES (53012,'禁止使用该名称');
INSERT INTO `hema_wechat_err` VALUES (53013,'公众号：名称与已有公众号名称重复;小程序：该名称与已有小程序名称重复');
INSERT INTO `hema_wechat_err` VALUES (53014,'公众号：公众号已有{名称 A+}时，需与该帐号相同主体才可申请{名称 A};小程序：小程序已有{名称 A+}时，需与该帐号相同主体才可申请{名称 A}');
INSERT INTO `hema_wechat_err` VALUES (53015,'公众号：该名称与已有小程序名称重复，需与该小程序帐号相同主体才可申请;小程序：该名称与已有公众号名称重复，需与该公众号帐号相同主体才可申请');
INSERT INTO `hema_wechat_err` VALUES (53016,'公众号：该名称与已有多个小程序名称重复，暂不支持申请;小程序：该名称与已有多个公众号名称重复，暂不支持申请');
INSERT INTO `hema_wechat_err` VALUES (53017,'公众号：公众号已有{名称 A+}时，需与该帐号相同主体才可申请{名称 A};小程序：小程序已有{名称 A+}时，需与该帐号相同主体才可申请{名称 A}');
INSERT INTO `hema_wechat_err` VALUES (53018,'名称命中微信号');
INSERT INTO `hema_wechat_err` VALUES (53019,'名称在保护期内');

INSERT INTO `hema_wechat_err` VALUES (53200,'本月功能介绍修改次数已用完');
INSERT INTO `hema_wechat_err` VALUES (53201,'功能介绍内容命中黑名单关键字');
INSERT INTO `hema_wechat_err` VALUES (53202,'本月头像修改次数已用完');	

INSERT INTO `hema_wechat_err` VALUES (53300,'超出每月次数限制');
INSERT INTO `hema_wechat_err` VALUES (53301,'超出可配置类目总数限制');
INSERT INTO `hema_wechat_err` VALUES (53302,'当前账号主体类型不允许设置此种类目');
INSERT INTO `hema_wechat_err` VALUES (53303,'提交的参数不合法');
INSERT INTO `hema_wechat_err` VALUES (53304,'与已有类目重复');
INSERT INTO `hema_wechat_err` VALUES (53305,'包含未通过 ICP 校验的类目');
INSERT INTO `hema_wechat_err` VALUES (53306,'修改类目只允许修改类目资质，不允许修改类目 ID');
INSERT INTO `hema_wechat_err` VALUES (53307,'只有审核失败的类目允许修改');
INSERT INTO `hema_wechat_err` VALUES (53308,'审核中的类目不允许删除');

INSERT INTO `hema_wechat_err` VALUES (61007,'api未经授权');

INSERT INTO `hema_wechat_err` VALUES (61070,'法人姓名与微信号不一致');

INSERT INTO `hema_wechat_err` VALUES (61450,'系统错误 (system error)');
INSERT INTO `hema_wechat_err` VALUES (61451,'参数错误 (invalid parameter)');
INSERT INTO `hema_wechat_err` VALUES (61452,'无效客服账号 (invalid kf_account)');
INSERT INTO `hema_wechat_err` VALUES (61453,'客服帐号已存在 (kf_account exsited)');
INSERT INTO `hema_wechat_err` VALUES (61454,'客服帐号名长度超过限制 ( 仅允许 10 个英文字符，不包括 @ 及 @ 后的公众号的微信号 )(invalid kf_acount length)');
INSERT INTO `hema_wechat_err` VALUES (61455,'客服帐号名包含非法字符 ( 仅允许英文 + 数字 )(illegal character in kf_account)');
INSERT INTO `hema_wechat_err` VALUES (61456,'客服帐号个数超过限制 (10 个客服账号 )(kf_account count exceeded)');
INSERT INTO `hema_wechat_err` VALUES (61457,'无效头像文件类型 (invalid file type)');

INSERT INTO `hema_wechat_err` VALUES (61500,'日期格式错误');

INSERT INTO `hema_wechat_err` VALUES (63001,'部分参数为空');
INSERT INTO `hema_wechat_err` VALUES (63002,'无效的签名');

INSERT INTO `hema_wechat_err` VALUES (65301,'不存在此 menuid 对应的个性化菜单');
INSERT INTO `hema_wechat_err` VALUES (65302,'没有相应的用户');
INSERT INTO `hema_wechat_err` VALUES (65303,'没有默认菜单，不能创建个性化菜单');
INSERT INTO `hema_wechat_err` VALUES (65304,'MatchRule 信息为空');
INSERT INTO `hema_wechat_err` VALUES (65305,'个性化菜单数量受限');
INSERT INTO `hema_wechat_err` VALUES (65306,'不支持个性化菜单的帐号');
INSERT INTO `hema_wechat_err` VALUES (65307,'个性化菜单信息为空');
INSERT INTO `hema_wechat_err` VALUES (65308,'包含没有响应类型的 button');
INSERT INTO `hema_wechat_err` VALUES (65309,'个性化菜单开关处于关闭状态');
INSERT INTO `hema_wechat_err` VALUES (65310,'填写了省份或城市信息，国家信息不能为空');
INSERT INTO `hema_wechat_err` VALUES (65311,'填写了城市信息，省份信息不能为空');
INSERT INTO `hema_wechat_err` VALUES (65312,'不合法的国家信息');
INSERT INTO `hema_wechat_err` VALUES (65313,'不合法的省份信息');
INSERT INTO `hema_wechat_err` VALUES (65314,'不合法的城市信息');

INSERT INTO `hema_wechat_err` VALUES (65316,'该公众号的菜单设置了过多的域名外跳（最多跳转到 3 个域名的链接）');
INSERT INTO `hema_wechat_err` VALUES (65317,'不合法的 URL');

INSERT INTO `hema_wechat_err` VALUES (80066,'非法的插件版本');
INSERT INTO `hema_wechat_err` VALUES (80067,'找不到使用的插件');
INSERT INTO `hema_wechat_err` VALUES (80082,'没有权限使用该插件');

INSERT INTO `hema_wechat_err` VALUES (85001,'微信号不存在或微信号设置为不可搜索');
INSERT INTO `hema_wechat_err` VALUES (85002,'小程序绑定的体验者数量达到上限');
INSERT INTO `hema_wechat_err` VALUES (85003,'微信号绑定的小程序体验者达到上限');
INSERT INTO `hema_wechat_err` VALUES (85004,'微信号已经绑定');

INSERT INTO `hema_wechat_err` VALUES (85006,'标签格式错误');
INSERT INTO `hema_wechat_err` VALUES (85007,'页面路径错误');
INSERT INTO `hema_wechat_err` VALUES (85008,'类目填写错误');
INSERT INTO `hema_wechat_err` VALUES (85009,'已经有正在审核的版本');
INSERT INTO `hema_wechat_err` VALUES (85010,'item_list 有项目为空');
INSERT INTO `hema_wechat_err` VALUES (85011,'标题填写错误');
INSERT INTO `hema_wechat_err` VALUES (85012,'无效的审核 id');
INSERT INTO `hema_wechat_err` VALUES (85013,'无效的自定义配置');
INSERT INTO `hema_wechat_err` VALUES (85014,'无效的模版编号');
INSERT INTO `hema_wechat_err` VALUES (85015,'版本输入错误');
INSERT INTO `hema_wechat_err` VALUES (85016,'域名数量超过限制');
INSERT INTO `hema_wechat_err` VALUES (85017,'没有新增域名，请确认小程序已经添加了域名或该域名是否没有在第三方平台添加');
INSERT INTO `hema_wechat_err` VALUES (85018,'域名没有在第三方平台设置');
INSERT INTO `hema_wechat_err` VALUES (85019,'没有审核版本');
INSERT INTO `hema_wechat_err` VALUES (85020,'审核状态未满足发布');
INSERT INTO `hema_wechat_err` VALUES (85021,'状态不可变');
INSERT INTO `hema_wechat_err` VALUES (85022,'action 非法');
INSERT INTO `hema_wechat_err` VALUES (85023,'审核列表填写的项目数不在 1-5 以内');

INSERT INTO `hema_wechat_err` VALUES (85043,'模版错误');
INSERT INTO `hema_wechat_err` VALUES (85044,'代码包超过大小限制');
INSERT INTO `hema_wechat_err` VALUES (85045,'ext_json 有不存在的路径');
INSERT INTO `hema_wechat_err` VALUES (85046,'tabBar 中缺少 path');
INSERT INTO `hema_wechat_err` VALUES (85047,'pages 字段为空');
INSERT INTO `hema_wechat_err` VALUES (85048,'ext_json 解析失败');

INSERT INTO `hema_wechat_err` VALUES (85051,'version_desc或者preview_info超限');

INSERT INTO `hema_wechat_err` VALUES (85064,'找不到草稿');
INSERT INTO `hema_wechat_err` VALUES (85065,'模版库已满');
INSERT INTO `hema_wechat_err` VALUES (85066,'链接错误');

INSERT INTO `hema_wechat_err` VALUES (85068,'测试链接不是子链接');
INSERT INTO `hema_wechat_err` VALUES (85069,'校验文件失败');
INSERT INTO `hema_wechat_err` VALUES (85070,'个人类型小程序无法设置二维码规则');
INSERT INTO `hema_wechat_err` VALUES (85071,'已添加该链接，请勿重复添加');
INSERT INTO `hema_wechat_err` VALUES (85072,'该链接已被占用');
INSERT INTO `hema_wechat_err` VALUES (85073,'二维码规则已满');
INSERT INTO `hema_wechat_err` VALUES (85074,'小程序未发布, 小程序必须先发布代码才可以发布二维码跳转规则');
INSERT INTO `hema_wechat_err` VALUES (85075,'个人类型小程序无法设置二维码规则');
	
INSERT INTO `hema_wechat_err` VALUES (85077,'小程序类目信息失效（类目中含有官方下架的类目，请重新选择类目）');

INSERT INTO `hema_wechat_err` VALUES (85079,'小程序没有线上版本，不能进行灰度');
INSERT INTO `hema_wechat_err` VALUES (85080,'小程序提交的审核未审核通过');
INSERT INTO `hema_wechat_err` VALUES (85081,'无效的发布比例');
INSERT INTO `hema_wechat_err` VALUES (85082,'当前的发布比例需要比之前设置的高');
INSERT INTO `hema_wechat_err` VALUES (85083,'搜索标记位被封禁，无法修改');
INSERT INTO `hema_wechat_err` VALUES (85084,'非法的 status 值，只能填 0 或者 1');
INSERT INTO `hema_wechat_err` VALUES (85085,'小程序提审数量已达本月上限，请点击查看《临时quota申请流程》');
INSERT INTO `hema_wechat_err` VALUES (85086,'提交代码审核之前需提前上传代码');
INSERT INTO `hema_wechat_err` VALUES (85087,'小程序已使用 api navigateToMiniProgram，请声明跳转 appid 列表后再次提交');

INSERT INTO `hema_wechat_err` VALUES (85092,'preview_info格式错误');
INSERT INTO `hema_wechat_err` VALUES (85093,'preview_info 视频或者图片个数超限');
INSERT INTO `hema_wechat_err` VALUES (85094,'需提供审核机制说明信息');

INSERT INTO `hema_wechat_err` VALUES (86000,'不是由第三方代小程序进行调用');
INSERT INTO `hema_wechat_err` VALUES (86001,'不存在第三方的已经提交的代码');
INSERT INTO `hema_wechat_err` VALUES (86002,'小程序未初始化完成');

INSERT INTO `hema_wechat_err` VALUES (86004,'无效微信号');

INSERT INTO `hema_wechat_err` VALUES (86007,'小程序禁止提交');

INSERT INTO `hema_wechat_err` VALUES (87006,'小游戏不能提交');
INSERT INTO `hema_wechat_err` VALUES (87009,'无效的签名');

INSERT INTO `hema_wechat_err` VALUES (87011,'现网已经在灰度发布，不能进行版本回退');
INSERT INTO `hema_wechat_err` VALUES (87012,'该版本不能回退，可能的原因：1:无上一个线上版用于回退 2:此版本为已回退版本，不能回退 3:此版本为回退功能上线之前的版本，不能回退');
INSERT INTO `hema_wechat_err` VALUES (87013,'撤回次数达到上限（每天一次，每个月 10 次）');

INSERT INTO `hema_wechat_err` VALUES (89000,'该公众号/小程序已经绑定了开放平台帐号');
INSERT INTO `hema_wechat_err` VALUES (89001,'Authorizer与开放平台帐号主体不相同');		
INSERT INTO `hema_wechat_err` VALUES (89002,'没有绑定开放平台账号');
INSERT INTO `hema_wechat_err` VALUES (89003,'该开放平台帐号并非通过 api 创建，不允许操作');	
INSERT INTO `hema_wechat_err` VALUES (89004,'该开放平台帐号所绑定的公众号/小程序已达上限（100 个）');

INSERT INTO `hema_wechat_err` VALUES (89019,'业务域名无更改，无需重复设置');
INSERT INTO `hema_wechat_err` VALUES (89020,'尚未设置小程序业务域名，请先在第三方平台中设置小程序业务域名后在调用本接口');
INSERT INTO `hema_wechat_err` VALUES (89021,'请求保存的域名不是第三方平台中已设置的小程序业务域名或子域名');

INSERT INTO `hema_wechat_err` VALUES (89029,'业务域名数量超过限制');

INSERT INTO `hema_wechat_err` VALUES (89231,'个人小程序不支持调用 setwebviewdomain 接口');
	
INSERT INTO `hema_wechat_err` VALUES (89236,'该插件不能申请');
INSERT INTO `hema_wechat_err` VALUES (89237,'已经添加该插件');
INSERT INTO `hema_wechat_err` VALUES (89238,'申请或使用的插件已经达到上限');
INSERT INTO `hema_wechat_err` VALUES (89239,'该插件不存在');

INSERT INTO `hema_wechat_err` VALUES (89243,'该申请为“待确认”状态，不可删除');
INSERT INTO `hema_wechat_err` VALUES (89244,'不存在该插件 appid');

INSERT INTO `hema_wechat_err` VALUES (89247,'内部错误');
INSERT INTO `hema_wechat_err` VALUES (89248,'企业代码类型无效，请选择正确类型填写');
INSERT INTO `hema_wechat_err` VALUES (89249,'该主体已有任务执行中，距上次任务 24h 后再试');
INSERT INTO `hema_wechat_err` VALUES (89250,'未找到该任务');
INSERT INTO `hema_wechat_err` VALUES (89251,'待法人人脸核身校验');
INSERT INTO `hema_wechat_err` VALUES (89252,'法人与企业信息一致性校验中');
INSERT INTO `hema_wechat_err` VALUES (89253,'缺少参数');
INSERT INTO `hema_wechat_err` VALUES (89254,'第三方权限集不全，补全权限集全网发布后生效');	
INSERT INTO `hema_wechat_err` VALUES (89255,'营业执照类型或编号错误');
INSERT INTO `hema_wechat_err` VALUES (89256,'token 信息有误');
INSERT INTO `hema_wechat_err` VALUES (89257,'该插件版本不支持快速更新');
INSERT INTO `hema_wechat_err` VALUES (89258,'当前小程序帐号存在灰度发布中的版本，不可操作快速更新');

INSERT INTO `hema_wechat_err` VALUES (89300,'订单无效');

INSERT INTO `hema_wechat_err` VALUES (89401,'系统不稳定，请稍后再试，如多次失败请通过社区反馈');
INSERT INTO `hema_wechat_err` VALUES (89402,'该审核单不在待审核队列，请检查是否已提交审核或已审完');
INSERT INTO `hema_wechat_err` VALUES (89403,'本单属于平台不支持加急种类，请等待正常审核流程');
INSERT INTO `hema_wechat_err` VALUES (89404,'本单已加速成功，请勿重复提交');
INSERT INTO `hema_wechat_err` VALUES (89405,'本月加急额度不足，请提升提审质量以获取更多额度');

INSERT INTO `hema_wechat_err` VALUES (91001,'不是公众号快速创建的小程序');
INSERT INTO `hema_wechat_err` VALUES (91002,'小程序发布后不可改名');
INSERT INTO `hema_wechat_err` VALUES (91003,'改名状态不合法');
INSERT INTO `hema_wechat_err` VALUES (91004,'昵称不合法');
INSERT INTO `hema_wechat_err` VALUES (91005,'昵称 15 天主体保护');
INSERT INTO `hema_wechat_err` VALUES (91006,'昵称命中微信号');
INSERT INTO `hema_wechat_err` VALUES (91007,'昵称已被占用');
INSERT INTO `hema_wechat_err` VALUES (91008,'昵称命中 7 天侵权保护期');
INSERT INTO `hema_wechat_err` VALUES (91009,'需要提交材料');
INSERT INTO `hema_wechat_err` VALUES (91010,'其他错误');
INSERT INTO `hema_wechat_err` VALUES (91011,'查不到昵称修改审核单信息');
INSERT INTO `hema_wechat_err` VALUES (91012,'其它错误');

INSERT INTO `hema_wechat_err` VALUES (91013,'占用名字过多');
INSERT INTO `hema_wechat_err` VALUES (91014,'+号规则 同一类型关联名主体不一致');
INSERT INTO `hema_wechat_err` VALUES (91015,'原始名不同类型主体不一致');
INSERT INTO `hema_wechat_err` VALUES (91016,'名称占用者 ≥2');
INSERT INTO `hema_wechat_err` VALUES (91017,'+号规则 不同类型关联名主体不一致');
INSERT INTO `hema_wechat_err` VALUES (91018,'组织类型小程序发布后，侵权被清空昵称，需走认证改名');
INSERT INTO `hema_wechat_err` VALUES (91019,'小程序正在审核中');

INSERT INTO `hema_wechat_err` VALUES (92000,'该经营资质已添加，请勿重复添加');

INSERT INTO `hema_wechat_err` VALUES (92002,'附近地点添加数量达到上线，无法继续添加');
INSERT INTO `hema_wechat_err` VALUES (92003,'地点已被其它小程序占用');
INSERT INTO `hema_wechat_err` VALUES (92004,'附近功能被封禁');
INSERT INTO `hema_wechat_err` VALUES (92005,'地点正在审核中');
INSERT INTO `hema_wechat_err` VALUES (92006,'地点正在展示小程序');
INSERT INTO `hema_wechat_err` VALUES (92007,'地点审核失败');
INSERT INTO `hema_wechat_err` VALUES (92008,'小程序未展示在该地点');

INSERT INTO `hema_wechat_err` VALUES (93009,'小程序未上架或不可见');
INSERT INTO `hema_wechat_err` VALUES (93010,'地点不存在');
INSERT INTO `hema_wechat_err` VALUES (93011,'个人类型小程序不可用');
INSERT INTO `hema_wechat_err` VALUES (93012,'非普通类型小程序（门店小程序、小店小程序等）不可用');
INSERT INTO `hema_wechat_err` VALUES (93013,'从腾讯地图获取地址详细信息失败');
INSERT INTO `hema_wechat_err` VALUES (93014,'同一资质证件号重复添加');
INSERT INTO `hema_wechat_err` VALUES (93015,'附近类目审核中');
INSERT INTO `hema_wechat_err` VALUES (93016,'服务标签个数超限制（官方最多5个，自定义最多4个）');
INSERT INTO `hema_wechat_err` VALUES (93017,'服务标签或者客服的名字不符合要求');
INSERT INTO `hema_wechat_err` VALUES (93018,'服务能力中填写的小程序appid不是同主体小程序');
INSERT INTO `hema_wechat_err` VALUES (93019,'申请类目之后才能添加附近地点');
INSERT INTO `hema_wechat_err` VALUES (93020,'qualification_list无效');
INSERT INTO `hema_wechat_err` VALUES (93021,'company_name字段为空');
INSERT INTO `hema_wechat_err` VALUES (93022,'credential字段为空');
INSERT INTO `hema_wechat_err` VALUES (93023,'address字段为空');
INSERT INTO `hema_wechat_err` VALUES (93024,'qualification_list字段为空');
INSERT INTO `hema_wechat_err` VALUES (93025,'服务appid对应的path不存在');

INSERT INTO `hema_wechat_err` VALUES (94001,'缺少证书');
INSERT INTO `hema_wechat_err` VALUES (94002,'使用不注册微信支付');
INSERT INTO `hema_wechat_err` VALUES (94003,'无效的签名sign');
INSERT INTO `hema_wechat_err` VALUES (94004,'用户没有实名信息');
INSERT INTO `hema_wechat_err` VALUES (94005,'无效的用户令牌token');
INSERT INTO `hema_wechat_err` VALUES (94006,'appid未经授权');
INSERT INTO `hema_wechat_err` VALUES (94007,'Appid解除绑定mchid');
INSERT INTO `hema_wechat_err` VALUES (94008,'时间戳无效');
INSERT INTO `hema_wechat_err` VALUES (94009,'无效的证书cert_serialno，证书的大小应为40');
INSERT INTO `hema_wechat_err` VALUES (94010,'无效的mch_id');
INSERT INTO `hema_wechat_err` VALUES (94011,'时间戳已过期');
INSERT INTO `hema_wechat_err` VALUES (94012,'appid和商户号的绑定关系不存在');

INSERT INTO `hema_wechat_err` VALUES (95001,'wxcode解码失败');
INSERT INTO `hema_wechat_err` VALUES (95002,'wxcode识别未授权');

INSERT INTO `hema_wechat_err` VALUES (95101,'按页获取产品参数无效');
INSERT INTO `hema_wechat_err` VALUES (95102,'按条件参数获取产品材料无效');
INSERT INTO `hema_wechat_err` VALUES (95103,'物料id列表大小超出限制');
INSERT INTO `hema_wechat_err` VALUES (95104,'进口产品频率超限');
INSERT INTO `hema_wechat_err` VALUES (95105,'mp正在进口产品，api拒绝进口');
INSERT INTO `hema_wechat_err` VALUES (95106,'原料药拒绝进口，需要先在mp上设置佣金比例');

INSERT INTO `hema_wechat_err` VALUES (101000,'无效的图像url');
INSERT INTO `hema_wechat_err` VALUES (101001,'找不到证书');
INSERT INTO `hema_wechat_err` VALUES (101002,'市场配额不足');

INSERT INTO `hema_wechat_err` VALUES (200002,'入参错误');
	
INSERT INTO `hema_wechat_err` VALUES (200011,'此账号已被封禁，无法操作');
INSERT INTO `hema_wechat_err` VALUES (200012,'个人模板数已达上限，上限25个');
INSERT INTO `hema_wechat_err` VALUES (200013,'此模板已被封禁，无法选用');
INSERT INTO `hema_wechat_err` VALUES (200014,'模板 tid 参数错误');

INSERT INTO `hema_wechat_err` VALUES (200016,'start 参数错误');
INSERT INTO `hema_wechat_err` VALUES (200017,'limit 参数错误');
INSERT INTO `hema_wechat_err` VALUES (200018,'类目 ids 缺失');
INSERT INTO `hema_wechat_err` VALUES (200019,'类目 ids 不合法');
INSERT INTO `hema_wechat_err` VALUES (200020,'关键词列表 kidList 参数错误');
INSERT INTO `hema_wechat_err` VALUES (200021,'场景描述 sceneDesc 参数错误');

INSERT INTO `hema_wechat_err` VALUES (300001,'禁止创建/更新商品 或 禁止编辑与更新房间（如：被封）');
INSERT INTO `hema_wechat_err` VALUES (300002,'名称长度不符合规则');
INSERT INTO `hema_wechat_err` VALUES (300003,'价格输入不合规（如：现价比原价大、传入价格非数字等）');
INSERT INTO `hema_wechat_err` VALUES (300004,'商品名称存在违规违法内容');
INSERT INTO `hema_wechat_err` VALUES (300005,'商品图片存在违规违法内容');
INSERT INTO `hema_wechat_err` VALUES (300006,'图片上传失败（如：mediaID过期）');
INSERT INTO `hema_wechat_err` VALUES (300007,'线上小程序版本不存在该链接');
INSERT INTO `hema_wechat_err` VALUES (300008,'添加商品失败');
INSERT INTO `hema_wechat_err` VALUES (300009,'商品审核撤回失败');
INSERT INTO `hema_wechat_err` VALUES (300010,'商品审核状态不对（如：商品审核中）');
INSERT INTO `hema_wechat_err` VALUES (300011,'操作非法（API不允许操作非API创建的商品）');
INSERT INTO `hema_wechat_err` VALUES (300012,'没有提审额度（每天500次提审额度）');
INSERT INTO `hema_wechat_err` VALUES (300013,'提审失败');
INSERT INTO `hema_wechat_err` VALUES (300014,'审核中，无法删除（非零代表失败）');

INSERT INTO `hema_wechat_err` VALUES (300017,'商品未提审');

INSERT INTO `hema_wechat_err` VALUES (300021,'商品添加成功，审核失败');
INSERT INTO `hema_wechat_err` VALUES (300022,'此房间号不存在');
INSERT INTO `hema_wechat_err` VALUES (300023,'房间状态 拦截（当前房间状态不允许此操作）');
INSERT INTO `hema_wechat_err` VALUES (300024,'商品不存在');
INSERT INTO `hema_wechat_err` VALUES (300025,'商品审核未通过');
INSERT INTO `hema_wechat_err` VALUES (300026,'房间商品数量已经满额');
INSERT INTO `hema_wechat_err` VALUES (300027,'导入商品失败');
INSERT INTO `hema_wechat_err` VALUES (300028,'房间名称违规');
INSERT INTO `hema_wechat_err` VALUES (300029,'主播昵称违规');
INSERT INTO `hema_wechat_err` VALUES (300030,'主播微信号不合法');
INSERT INTO `hema_wechat_err` VALUES (300031,'直播间封面图不合规');
INSERT INTO `hema_wechat_err` VALUES (300032,'直播间分享图违规');
INSERT INTO `hema_wechat_err` VALUES (300033,'添加商品超过直播间上限');
INSERT INTO `hema_wechat_err` VALUES (300034,'主播微信昵称长度不符合要求');
INSERT INTO `hema_wechat_err` VALUES (300035,'主播微信号不存在');
INSERT INTO `hema_wechat_err` VALUES (300036,'主播微信号未实名认证');

INSERT INTO `hema_wechat_err` VALUES (9001001,'POST 数据参数不合法');
INSERT INTO `hema_wechat_err` VALUES (9001002,'远端服务不可用');
INSERT INTO `hema_wechat_err` VALUES (9001003,'Ticket 不合法');
INSERT INTO `hema_wechat_err` VALUES (9001004,'获取摇周边用户信息失败');
INSERT INTO `hema_wechat_err` VALUES (9001005,'获取商户信息失败');
INSERT INTO `hema_wechat_err` VALUES (9001006,'获取 OpenID 失败');
INSERT INTO `hema_wechat_err` VALUES (9001007,'上传文件缺失');
INSERT INTO `hema_wechat_err` VALUES (9001008,'上传素材的文件类型不合法');
INSERT INTO `hema_wechat_err` VALUES (9001009,'上传素材的文件尺寸不合法');
INSERT INTO `hema_wechat_err` VALUES (9001010,'上传失败');

INSERT INTO `hema_wechat_err` VALUES (9001020,'帐号不合法');
INSERT INTO `hema_wechat_err` VALUES (9001021,'已有设备激活率低于 50% ，不能新增设备');
INSERT INTO `hema_wechat_err` VALUES (9001022,'设备申请数不合法，必须为大于 0 的数字');
INSERT INTO `hema_wechat_err` VALUES (9001023,'已存在审核中的设备 ID 申请');
INSERT INTO `hema_wechat_err` VALUES (9001024,'一次查询设备 ID 数量不能超过 50');
INSERT INTO `hema_wechat_err` VALUES (9001025,'设备 ID 不合法');
INSERT INTO `hema_wechat_err` VALUES (9001026,'页面 ID 不合法');
INSERT INTO `hema_wechat_err` VALUES (9001027,'页面参数不合法');
INSERT INTO `hema_wechat_err` VALUES (9001028,'一次删除页面 ID 数量不能超过 10');
INSERT INTO `hema_wechat_err` VALUES (9001029,'页面已应用在设备中，请先解除应用关系再删除');
INSERT INTO `hema_wechat_err` VALUES (9001030,'一次查询页面 ID 数量不能超过 50');
INSERT INTO `hema_wechat_err` VALUES (9001031,'时间区间不合法');
INSERT INTO `hema_wechat_err` VALUES (9001032,'保存设备与页面的绑定关系参数错误');
INSERT INTO `hema_wechat_err` VALUES (9001033,'门店 ID 不合法');
INSERT INTO `hema_wechat_err` VALUES (9001034,'设备备注信息过长');
INSERT INTO `hema_wechat_err` VALUES (9001035,'设备申请参数不合法');
INSERT INTO `hema_wechat_err` VALUES (9001036,'查询起始值 begin 不合法');

INSERT INTO `hema_wechat_err` VALUES (9400001,'该小程序为开发者小程序');



